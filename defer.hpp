namespace efj
{

template <typename F>
struct defer_finalizer
{
    F    func;
    bool moved;

    template <typename T>
    defer_finalizer(T&& f)
        : func(efj::forward<T>(f)), moved(false)
    {}

    defer_finalizer(const defer_finalizer&) = delete;

    defer_finalizer(defer_finalizer&& other)
        : func(efj::move(other.func)), moved(other.moved)
    {
        other.moved = true;
    }

    ~defer_finalizer()
    {
        if (!moved)
            func();
    }
};

struct defer_helper
{
    template <typename F> EFJ_MACRO
    defer_finalizer<F> operator << (F&& f)
    { return defer_finalizer<F>(efj::forward<F>(f)); }
};

#define efj_defer_impl2(counter) auto efj_deferred_lambda_##counter __attribute__((unused)) = efj::defer_helper{} << [&]
#define efj_defer_impl1(counter) efj_defer_impl2(counter)
#define efj_defer efj_defer_impl1(__COUNTER__)

} // namespace efj
