namespace efj
{

//{ TLS.
// :ContextConstructor
// Since we use a pointer to the context, we might as well also allocate memory for it right next to it.
// We could also just malloc it during context initialization.
thread_local efj::execution_context* globalContext;
thread_local char globalContextMemory[sizeof(efj::execution_context)] alignas(efj::execution_context);

// IMPORTANT: We use this to guarantee that the app's constructor is called before the initialization of
// any other globals, particularly any tests written with the test API and EFJ_OBJECTs.
// This is is GCC/clang specific, but it greatly simplifies this implementation. Other static objects
// will have a default init-order of 65535, and priorities 0-100 are reserved for compiler use.
efj::application globalApp __attribute__((init_priority(101)));
//}

static void _init_context();

inline bool _initting_objects() { return efj::app()._inittingObjects; }
inline bool _done_initting_objects() { return efj::app()._doneInittingObjects; }

static void
_init_context()
{
    using namespace efj::mem_literals;

    globalContext = efj_placement_new(globalContextMemory) efj::execution_context();

    // NOTE: For reference, on x86, the default call stack size for a thread is 2 MB.
    globalContext->permMemory  = efj::allocate_arena("Permanent", 4_KB);
    globalContext->tempMemory  = efj::allocate_arena("Temp",      8_KB, 32_KB);
    globalContext->eventMemory = efj::allocate_arena("Event",     4_KB);

    globalContext->curAllocator = globalContext->heapAllocator;

    globalContext->thisObject   = nullptr;
    globalContext->thisThread   = nullptr;
    globalContext->thisFd       = -1;
    globalContext->thisFdEvents = 0;

    globalContext->name      = nullptr;
    globalContext->realId    = ::syscall(SYS_gettid);
    globalContext->logicalId = 0;

    globalContext->logUnformatted = false;

    efj::init_user_context(&globalContext->user);
}

// Assumes the name is a literal.
static inline void
_set_thread_name(const char* name)
{
    efj::context().name = name;
    // NOTE: Do the GNU pthread naming just to be a good citizen for
    // external tools or whatever that use the non-posix pthread name for
    // debugging or something. In practice, this shows up in gdb and in the
    // output of commands like ps, which helps.
    //
    ::pthread_setname_np(::pthread_self(), name);
}

application::application()
{
    efj_assert(!_initted);

    _pageSize = ::sysconf(_SC_PAGESIZE);

    // :SpecialMainThreadInitialization.
    efj::_init_context();
    efj::_set_thread_name("Main");
    // :LogicalThreadId
    efj::context().logicalId = 0;

#if 1
    _arena = efj::allocate_arena("Application Storage", 4_KB);

    _threads.reset(_arena);
    _objects.reset(_arena);

    _initted = true;

    efj::_eventsys_create_breakpoint(&_threadStartedBp);

    _maxUserEventId = -1;

    // :InternalSystemSetup :FixedInitOrder
    // IMPORTANT: The allocation of the systems needs to happen before any thread creation, b/c the
    // system thread contexts depend on getting a system pointer, even though the systems aren't initialized, yet.
    _eventSystem     = efj_push_new(_arena, efj::event_system)();
    _watchdogSystem  = efj_push_new(_arena, efj::watchdog_system)();
    _debugSystem     = efj_push_new(_arena, efj::debug_system)();
    _pcSystem        = efj_push_new(_arena, efj::pc_system)();
    _userEventSystem = efj_push_new(_arena, efj::user_event_system)();
    _logSystem       = efj_push_new(_arena, efj::log_system)();
    _configSystem    = efj_push_new(_arena, efj::config_system)();
    _testSystem      = efj_push_new(_arena, efj::test_system)();

    // :SpecialMainThreadInitialization.
    auto* mainThread    = _create_thread("Main");
    mainThread->realId  = efj::context().realId;
    mainThread->pthread = pthread_self();
    _mainThread = mainThread;
    efj::_set_this_thread(mainThread);

    // :InternalSystemSetup :FixedInitOrder
    // To keep things simple, we enforce and initialization order for all systems. We want initialize to
    // always look like this: event system -> watchdog system -> debug system -> every thing else.
    efj::_eventsys_pre_init(_eventSystem, &_arena);
    efj::_watchdog_pre_init(_watchdogSystem, &_arena);
    efj::_debugsys_pre_init(*_debugSystem, _arena);

    efj::_pcsys_pre_init(_pcSystem, &_arena);
    efj::_uesys_pre_init(_userEventSystem, &_arena);
    efj::_configsys_pre_init(_configSystem, &_arena);
    efj::_logsys_pre_init(_logSystem, &_arena);
    efj::_testsys_pre_init(_testSystem, &_arena);
#endif
}

void application::_add_object(efj::object* o)
{
    efj_assert(_initted);
    // We currently don't allow objects to be created past init time.
    efj_panic_if(_started);

    o->id = _objects.size();
    _objects.add(o);
}

// TODO: Work more towards this free function API, without methods.
// There is less noise / duplication in the API this way.

//{ TODO: Move this to logging api.
extern void
set_log_file(const char* path)
{
    efj::app()._logConfig.path = path;
}

extern void
set_log_formatter(efj::log_formatter* formatter)
{
    efj::app()._logConfig.formatter = formatter;
}

extern efj::log_formatter*
get_log_formatter()
{
    return efj::app()._logConfig.formatter;
}

//}

// NOTE: Called from the main thread.
extern efj::thread*
application::_create_thread(const char* name)
{
    efj::thread& thread = *efj_push_new(_arena, efj::thread);
    thread.name         = name;
    thread.realId       = 0; // Set after actual thread creation. :SetRealTid
    thread.logicalId    = _threads.size();
    thread.killed       = false;
    thread.logged       = efj::log_level_all;

    // Format an arena name, and copy it into global memory since the arena struct expects a global string.
    efj::string arenaName = efj::fmt("Thread Arena %u", thread.logicalId).dup(efj::perm_memory());
    thread.arena = efj::allocate_arena(arenaName.c_str(), 4_KB);

    thread.cpus.reset(thread.arena);
    thread.schedPolicy = SCHED_OTHER;
    thread.priority    = 0;
    thread.tasks       = 0;
    thread.objects.reset(thread.arena);

    // :InternalSystemSetup
    // To resolve dependecies that make our fixed system initialization order hard to implement,
    // we make sure to construct all thread contexts before initialization to allow for certain
    // APIs to work before thread contexts are fully initialized.
    thread.eventContext    = efj_push_new(thread.arena, efj::eventsys_thread_ctx)();
    thread.debugContext    = efj_push_new(thread.arena, efj::debugsys_thread_ctx)();
    thread.watchdogContext = efj_push_new(thread.arena, efj::watchdog_thread_ctx)();
    thread.pcContext       = efj_push_new(thread.arena, efj::pcsys_thread_ctx)();
    thread.ueContext       = efj_push_new(thread.arena, efj::uesys_thread_ctx)();
    thread.logContext      = efj_push_new(thread.arena, efj::logsys_thread_ctx)();
    thread.configContext   = efj_push_new(thread.arena, efj::configsys_thread_ctx)();
    thread.testContext     = efj_push_new(thread.arena, efj::testsys_thread_ctx)();

    // :InternalSystemSetup :FixedInitOrder
    efj::_eventsys_init_thread_context(_eventSystem, &thread, &thread.arena);
    efj::_watchdog_init_thread_context(_watchdogSystem, &thread, &thread.arena);
    efj::_debugsys_init_thread_context(*_debugSystem, thread, thread.arena);

    efj::_pcsys_init_thread_context(_pcSystem, &thread, &thread.arena);
    efj::_uesys_init_thread_context(_userEventSystem, &thread, &thread.arena);
    efj::_logsys_init_thread_context(_logSystem, &thread, &thread.arena);
    efj::_configsys_init_thread_context(_configSystem, &thread, &thread.arena);
    efj::_testsys_init_thread_context(_testSystem, &thread, &thread.arena);

    _threads.add(&thread);

    return &thread;
}

static inline void
_check_debug_thread(efj::application* app);

static inline void
_check_disk_io_thread(efj::application* app);

static inline void
_check_watchdog_thread(efj::application* app);

static inline void
_check_scheduling_thread(efj::application* app);

static inline void
_finalize_object_thread_relationships(efj::application* app);

static inline void
_start_background_thread(efj::thread* thread);

extern bool
application::_init(int argc, const char** argv)
{
    //{ Implicit thread creation and responsibility finalization.
    efj::_check_watchdog_thread(this);
    efj::_check_debug_thread(this);
    efj::_check_disk_io_thread(this);
    efj::_check_scheduling_thread(this);
    //}

    efj::_finalize_object_thread_relationships(this);

    // Init all the object contexts now that we know which thread all the objects live on.
    // :InternalSystemSetup
    for (efj::thread* thread : efj::threads()) {
        for (efj::object* object : thread->objects) {
            // :FixedInitOrder
            efj::_debugsys_init_object_context(*efj::app()._debugSystem, *object);
            efj::_logsys_init_object_context(efj::app()._logSystem, object);
            efj::_uesys_init_object_context(efj::app()._userEventSystem, object);
            efj::_configsys_init_object_context(efj::app()._configSystem, object);
            efj::_testsys_init_object_context(efj::app()._testSystem, object);
        }
    }

    // NOTE: For sanity, we want to make sure that every thread's execution context is
    // initialized before calling init() on objects.
    // TODO: If we just store a thread pointer in the context, instead of a bunch of other stuff,
    // we wouldn't need to care about this...
    //{
    for (auto* thread : _threads) {
        if (thread != _threads[0])
            efj::_start_background_thread(thread);
    }

    _threadStartedBp.wait_for_hits(efj::background_thread_count());

    // At this point, all background threads are stopped on the thread-started breakpoint, and
    // the main thread is implicitly stopped, since this function runs on the main thread.

    // Now that all threads are started and everyone has a valid execution context, we can, on
    // the main thread, go through and init() all objects and finish wiring up objects based
    // on what happend during init(), like subscribing to user events -- before anyone is
    // allowed to handle any events.
    //
    // NOTE: Right now, objects are not allowed to trigger user events until after
    // all objects have been initted because the full list of subscribers isn't known until
    // after all object init calls.

    // :InternalSystemSetup
    // Once efj::init() is called, we know how many events are registered, so we can allocate
    // subscription tables needed for the subscribe() API to work.
    efj::_uesys_handle_event_registrations(_userEventSystem);
    efj::test_call_dev_init();

    // If we exit prematurely b/c we are running tests or something, we want to behave like an
    // empty program with no objects. We still report to internal systems as if we initialized objects.

    // :FixedInitOrder
    efj::_eventsys_pre_object_init(_eventSystem);
    efj::_watchdog_pre_object_init(_watchdogSystem, _watchdogThread);
    efj::_debugsys_pre_object_init(*_debugSystem, _debugThread);
    //
    efj::_configsys_pre_object_init(_configSystem, _configThread);
    efj::_logsys_pre_object_init(_logSystem, _loggingThread, &_logConfig);
    efj::_testsys_pre_object_init(_testSystem, argc, argv);

    _inittingObjects = true;
    efj::object* failedObject = nullptr;

    // Queue object communication during object-init so objects can't communicate with other objects
    // that haven't had their init() called.
    bool prevForce = efj::_eventsys_force_queue_side_effects(efj::this_thread(), true);
    for (efj::object* o : _objects) {
        efj::_set_this_object(o EFJ_LOC_ARGS);
        if (!o->init()) {
            efj_log_crit("\n\nObject \"%s\" failed to initialize\n", o->name);
            failedObject = o;
        }
        efj::_set_this_object(nullptr EFJ_LOC_ARGS);
    }
    efj::_eventsys_force_queue_side_effects(efj::this_thread(), prevForce);
    _inittingObjects     = false;
    _doneInittingObjects = true;
    //}

    // :InternalSystemSetup
    // All threads have been created and we know their final responsibilities. Now the
    // systems get a chance to do context specific stuff based on that.

    // :FixedInitOrder
    efj::_watchdog_post_object_init(_watchdogSystem);
    efj::_debugsys_post_object_init(*_debugSystem);
    //
    efj::_pcsys_post_object_init(_pcSystem);
    efj::_uesys_post_object_init(_userEventSystem);
    efj::_logsys_post_object_init(_logSystem);
    efj::_configsys_post_object_init(_configSystem);
    efj::_testsys_post_object_init(_testSystem);

    //{ Keep the event system and testing system _start last and in this order.
    efj::_eventsys_start(_eventSystem);

    bool exitAfterTesting = false;
    int  exitCode         = efj::exit_normal;
    efj::_testsys_start(_testSystem, &exitAfterTesting, &exitCode);
    //}

    __atomic_store_n(&_backgroundThreadsContinued, true, __ATOMIC_RELAXED);
    _threadStartedBp.continue_all();
    if (failedObject) {
        efj_internal_log(efj::log_level_crit, "%s failed to init. Continuing anyway", failedObject->name);
    }

    _started = true;

    if (exitAfterTesting) {
        _earlyExitCode = exitCode;
        _exitEarly = true;
    }

    return !failedObject;
}

static inline void
_check_debug_thread(efj::application* app)
{
    for (efj::thread* thread : app->_threads) {
        if (thread->does_debugging()) {
            efj_assert(!app->_debugThread);
            app->_debugThread = thread;
        }
    }

    // :FullThreadingControl
    if (!app->_debugThread) {
        app->_debugThread = app->_create_thread("Debug");
        app->_debugThread->use_for_debugging(true);
    }
}

static inline void
_check_disk_io_thread(efj::application* app)
{
    efj::thread* loggingThread = nullptr;
    efj::thread* configThread  = nullptr;
    for (efj::thread* thread : app->_threads) {
        if (thread->does_logging()) {
            efj_assert(!loggingThread);
            loggingThread = thread;
        }
        if (thread->does_config()) {
            efj_assert(!configThread);
            configThread = thread;
        }
    }

    efj::thread* diskIoThread = (!loggingThread || !configThread)
                              ? app->_create_thread("Disk IO")
                              : nullptr;

    if (!loggingThread) {
        diskIoThread->use_for_logging(true);
        app->_loggingThread = diskIoThread;
    }

    if (!configThread) {
        diskIoThread->use_for_config(true);
        app->_configThread = diskIoThread;
    }
}

static inline void
_check_scheduling_thread(efj::application* app)
{
    for (efj::thread* thread : app->_threads) {
        if (thread->does_event_scheduling()) {
            efj_assert(!app->_eventSchedulingThread);
            app->_eventSchedulingThread = thread;
        }
    }

    if (!app->_eventSchedulingThread) {
        app->_eventSchedulingThread = app->_create_thread("Event Scheduling");
        app->_eventSchedulingThread->use_for_event_scheduling(true);
    }
}

static inline void
_check_watchdog_thread(efj::application* app)
{
    // TODO: Where this stuff happens should probably be configurable by users, to allow
    // full control over threading, but this implicit version is a convenient default.
    // :FullThreadingControl

    if (!app->_watchdogThread) {
        efj::thread* watchdogThread = app->_create_thread("Watchdog");
        watchdogThread->use_for_watchdog(true);
        app->_watchdogThread = watchdogThread;
    }
}

// NOTE: Must be done after all implicit thread creation.
static inline void
_finalize_object_thread_relationships(efj::application* app)
{
    // Give all objects a pointer to their thread, and give them a pointer to their
    // thread's consume queue for convenience in efj_produce().
    for (auto* t : app->_threads) {
        for (auto* o : t->objects) {
            o->thread = t;
        }
    }

    // NOTE: All objects that don't live on threads by now implicitly
    // live on the main thread, and the main thread needs to know about the objects
    // that live on it.
    for (auto* o : app->_objects) {
        if (!o->thread) {
            o->thread = efj::main_thread();
            o->thread->objects.add(o);
        }
    }
}

extern int
application::_exit(int code)
{
    // Make sure only one thread can successfully call efj::exit(). In particular, killing the main thread
    // will cause it to break from it's main loop and call efj::exit(). We want the thread that originally
    // called exit to be the thread that ultimately exits the application for now.
    if (__atomic_exchange_n(&_exiting, true, __ATOMIC_RELAXED)) {
        // Executed by threads that saw exiting == true.
        for (;;) usleep(500000);
    }

    char errBuf[256];
    int errorSize = 0;
    bool exitingTest = code == efj::exit_test_pass || code == efj::exit_test_fail;

    // This logging gets annoying when we are just running tests.
    if (!exitingTest) {
        errorSize = stbsp_sprintf(errBuf, "Thread \"%s\" called efj::exit() with code %d (%s).\n",
            efj::this_thread()->name, code, efj::c_str((efj::exit_code)code));

        // Write this error before anything else, just in case things are _severely_ messed up.
        efj_ignored_write(STDERR_FILENO, errBuf, (umm)errorSize);
    }

    efj::_debugsys_exit(*efj::this_thread()->debugContext, code);

    if (!exitingTest) {
        // Let the log formatter handle newlines.
        if (errorSize > 1 && errorSize < (int)sizeof(errBuf))
            errBuf[errorSize-1] = '\0';
        efj_internal_log(efj::log_level_info, errBuf);
    }

    // Exits should always happen mid-event, so we need to handle deferred actions like logging, but
    // we don't need to and don't want to call _debugsys_end_event().

    // We can't make assumptions about which thread we exit from or which threads are hung, so we
    // just do timed wait for all threads other than this one before exiting.

    efj::_eventsys_kill_all_threads(_eventSystem);

    // TODO: Really, once exit handlers are implemented, making sure logs go out during exit
    // conditions should probably be implemented in one of those. Right now, we only want to try to
    // make sure logs go out.

    // We could've crashed in the logger thread or not waited long enough for it to handle an event. In any case,
    // we don't want to race with it to write log messages unless the logging thread is waiting on another thread
    // that crashed.
    if (__atomic_load_n(&efj::app()._loggingThread->killed, __ATOMIC_RELAXED)) {
        efj::_logsys_write_log_messages(_logSystem);
    }
    else {
        // If we trying to kill everything and we see the logging thread is waiting for other threads, we assume
        // it's waiting on a thread that crashed or something, so we potentially race with it to dump messages.
        // :LoggingThreadBlocked @TS
        if (__atomic_load_n(&efj::app()._loggingThread->waitingOnOtherThreads, __ATOMIC_RELAXED))
            efj::_logsys_write_log_messages(_logSystem);

        auto s = efj::fmt("[c2] Logger thread not killed/waited for correctly. Potentially dropping log messages.\n");
        efj_ignored_write(STDERR_FILENO, s.c_str(), s.size);
    }

    efj::_debugsys_handle_exit(*efj::app()._debugSystem);

    // TODO: Call exit handlers if we add them.

    // If we are calling exit() from the main thread, just return the exit code.
    if (efj::logical_thread_id() == 0)
        return code;

    // NOTE: We could check to see if the main thread is still alive and tell it to stop
    // and return an exit code. That would be good in cases where there is code after the main thread's event loop
    // that an application would like to run. For now, it's easier to just exit the app directly.

    ::_exit(code);

    efj_invalid_code_path();
    return code;
}

// TODO: Return exit code through parameter.
// false => got kill event.
static inline bool
update_thread(efj::thread* thread, int timeoutMs = -1)
{
    efj::exit_code code;
    return efj::_eventsys_update_thread(thread, timeoutMs, &code) != efj::eventsys_update_killed;
}

static void*
_background_thread_func(efj::thread* thread)
{
    efj::_init_context();

    auto* ctx = &efj::context();
    efj::_set_this_thread(thread);
    efj::_set_thread_name(thread->name);
    ctx->logicalId = thread->logicalId;
    thread->realId = ctx->realId; // :SetRealTid

    //efj_internal_log(efj::log_level_debug, "Starting bg thread \"%s\".", thread->name);

    // NOTE: All objects are initialized on the main thread.

    // Give the main thread a way to wait until all background threads have started and
    // all execution contexts are valid.
    efj::app()._threadStartedBp.hit_and_wait();

    efj::exit_code code = efj::exit_normal;
    for (;;) {
        efj::_debugsys_begin_frame(*thread->debugContext);

        //printf("Update bg thread %s\n", thread->name); XXX
        if (efj::_eventsys_update_thread(thread, -1, &code) == efj::eventsys_update_killed)
            break;

        efj::_debugsys_end_frame(*thread->debugContext);

        efj_reset(efj::context().tempMemory);
        efj_reset(efj::context().eventMemory);
        fflush(stdout);
    }

    return (void*)code;
}

static inline void
_start_background_thread(efj::thread* thread)
{
    using pthread_func = void* (void*);

    // IMPORTANT: The pthread handle is stored in the thread struct for the main thread in the application's constructor
    // since this function only runs for background threads (not the main thread). This is kind of error prone.
    // :SpecialMainThreadInitialization.
    //
    efj_check(pthread_create(&thread->pthread, NULL,
        (pthread_func*)_background_thread_func, thread) != -1);

    pthread_detach(thread->pthread);
}

extern void
application::_begin_frame()
{
    efj::_debugsys_begin_frame(*efj::main_thread()->debugContext);
}

extern void
application::_end_frame()
{
    efj::_debugsys_end_frame(*efj::main_thread()->debugContext);
    fflush(stdout); // @Temporary
}

// false => killed.
extern bool
application::_update(int* exitCode, int timeoutMs)
{
    efj_check(efj::logical_thread_id() == 0);
    if (_exitEarly) {
        *exitCode = _earlyExitCode;
        return false;
    }

    efj::exit_code code = efj::exit_normal;
    if (efj::_eventsys_update_thread(_mainThread, timeoutMs, &code) == efj::eventsys_update_killed) {
        *exitCode = code;
        return false;
    }

    return true;
}

} // namespace efj

