namespace efj
{

enum callback_type
{
    callback_invalid,

    callback_proc,
    callback_memfn,

    callback_count_
};

struct callback;
using callback_caller = void(efj::callback* cb, void* e);

struct callback
{
    efj::object*          object;
    efj::callback_caller* caller;
    efj::callback_type    type;
    union
    {
        void* proc;
        // NOTE(bmartin): the size of member function pointers
        // is implementation defined, and they can change size
        // depending on whether the class uses single or multiple
        // inheritance. However, the max size is typically sizeof(void*)*2
        // We have enough type information available in the API to statically check this anyway.
        // Explicitly saying this is aligned as a void* b/c we may get rid of the option of
        // having a non-member callback at some point, but the alignment is still important.
        char memfn[sizeof(void*)*2] alignas(void*);
    };

    EFJ_MACRO bool is_null() const { return type == efj::callback_invalid; }

    EFJ_MACRO bool operator == (const efj::callback& other) const
    {
        return object == other.object && caller == other.caller && type == other.type
            && ((void**)memfn)[0] == ((void**)other.memfn)[0]
            && ((void**)memfn)[1] == ((void**)other.memfn)[1];
    }

    EFJ_MACRO bool operator != (const efj::callback& other) const
    {
        return !(*this == other);
    }
};

EFJ_FORCE_INLINE efj::callback
null_callback()
{
    efj::callback result = {};
    return result;
}

template <typename T, typename EventT> EFJ_MACRO efj::callback
_make_object_callback(efj::object* o, void (T::*callback)(EventT& e));

template <typename T, typename... ArgsT> EFJ_MACRO efj::callback
_make_custom_callback(efj::object* o, void (T::*callback)(ArgsT...), efj::callback_caller* caller);


} // namespace efj
