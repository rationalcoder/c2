namespace efj
{

static efj::test_dev_init_func* globalDevInit;

extern void
test_use_dev_init(efj::test_dev_init_func* devInit)
{
    globalDevInit = devInit;
}

extern void
test_call_dev_init()
{
    if (globalDevInit)
        globalDevInit();
}

extern void
test_disable_object_logging()
{
    for (efj::object* o : efj::objects()) {
        o->logContext->logged = 0;
    }
}

extern void
test_write_to_hex(efj::producer& p, const efj::string& path)
{
    int fd = ::open(path.c_str(), O_CREAT | O_WRONLY | O_TRUNC, S_IRWXU);
    if (fd == -1) {
        efj_internal_log(efj::log_level_warn, "Failed to open '%s' for writing: %s",
            path.c_str(), strerror(errno));
        return;
    }

    efj::_testsys_capture_to_hex(efj::app()._testSystem, &p, path, fd);
}

extern void
test_write_to_network(efj::producer& p, const efj::string& host, u16 port)
{
    efj::_testsys_capture_to_network(efj::app()._testSystem, &p, host, port);
}

test_note::test_note(const char* fmt, ...)
{
    efj_temp_scope();
    va_list va;
    va_start(va, fmt);
    efj::string msg = efj::vfmt(fmt, va);
    va_end(va);
    efj::_testsys_push_note(msg, this);
}

test_note::~test_note()
{
    efj::_testsys_pop_note(this);
}


extern void
test_register_test(const char* name, efj::test_func* func)
{
    efj_panic_if(!func);
    efj::_testsys_register_test(efj::app()._testSystem, name, func);
}

extern void
test_register_test_init(const char* name, efj::test_func* func)
{
    efj_panic_if(!func);
    efj::_testsys_register_test_init(efj::app()._testSystem, name, func);
}

extern void
test_failure(const efj::string& msg, const efj::source_location& loc)
{
    efj::_testsys_report_failure(efj::app()._testSystem, msg, loc);
}

extern void
test_advance(efj::nsec amount)
{
    efj::_eventsys_test_advance(efj::app()._eventSystem, amount);
}

extern efj::timestamp
test_now()
{
    return efj::_eventys_test_get_now(efj::app()._eventSystem);
}

extern efj::posix_time
test_now_ptime()
{
    return efj::_eventsys_test_get_ptime(efj::app()._eventSystem);
}

extern void
_test_disconnect_output(efj::object* o)
{
    o->testContext->disconnected = true;
}

extern void
_test_capture(efj::object* o, efj::test_output_queue* queue)
{
    o->testContext->outputQueue = queue;
}

extern void
_test_capture(efj::producer* p, efj::test_output_queue* queue)
{
    p->testContext->outputQueue = queue;
}

extern void
_test_trigger(u32 id, const efj::component_view& c)
{
    efj::_uesys_test_trigger_event(efj::app()._userEventSystem, id, c);
}

extern void
_test_send_event(efj::object* to, u32 id, const efj::component_view& c)
{
    efj::_uesys_test_send_event(efj::app()._userEventSystem, to, id, c);
}

} // namespace efj
