#include <stdio.h>
#include <c2.hpp>

void GenerateRandomKeys(size_t min, size_t max, uintptr_t* keys_out, size_t n)
{
    bool keys_used[10000] = {};
    for (size_t i = 0; i < n; i++)
    {
        for (;;)
        {
            uintptr_t key = rand() % max;
            if (keys_used[key])
                continue;

            keys_used[key] = true;
            keys_out[i] = key;
            break;
        }
    }
}

void GenerateAscendingKeys(uintptr_t min, uintptr_t* keys_out, size_t n, uintptr_t* keys_end = nullptr)
{
    if (keys_end) assert(keys_out + n <= keys_end);
    for (size_t i = 0; i < n; i++)
    {
        keys_out[i] = min++;
    }
}

void GenerateDescendingKeys(uintptr_t max, uintptr_t* keys_out, size_t n, uintptr_t* keys_end = nullptr)
{
    assert(max >= n);
    if (keys_end) assert(keys_out + n <= keys_end);
    for (size_t i = 0; i < n; i++)
    {
        keys_out[i] = --max;
    }
}

void GenerateRandomExistingKeys(uintptr_t* keys_in, uintptr_t* keys_out, size_t n)
{
    memcpy(keys_out, keys_in, n * sizeof(uintptr_t));
    for (size_t i = 0; i < n; i++)
    {
        int src_i = rand() % n;
        int dst_i = rand() % n;

        uintptr_t temp = keys_out[dst_i];
        keys_out[dst_i] = keys_out[src_i];
        keys_out[src_i] = temp;
    }
}


bool TestInsertAndRemove(const char* test_name, efj::btree_base* tree, size_t max_key,
    uintptr_t* insert_keys, uintptr_t* remove_keys, size_t n_keys)
{
    printf("[%s]: BEGIN\n", test_name);

    struct timespec before = {};
    clock_gettime(CLOCK_MONOTONIC, &before);

    for (uintptr_t i = 0; i < n_keys; i++)
    {
        uintptr_t key = insert_keys[i];
        //PrintTree(tree);
        efj::btree_insert(tree, key, (void*)key);
        //if (!IsValidBtree(tree, max_key, insert_keys, 0, i+1))
        //{
        //    printf("Failed after inserting %zu\n", key);
        //    PrintTree(tree);
        //    printf("[%s]: FAIL\n", test_name);
        //    return false;
        //}
    }

    struct timespec after = {};
    clock_gettime(CLOCK_MONOTONIC, &after);

    uint32_t total_micro = (after.tv_sec - before.tv_sec) * 1000000 + (after.tv_nsec - before.tv_nsec) / 1000;
    printf("Time: %f us\n", total_micro / (float)n_keys);

    for (uintptr_t i = 0; i < n_keys; i++)
    {
        uintptr_t key = remove_keys[i];
        void** value = btree_find(tree, key);
        efj_assert(*(uintptr_t*)value == key);
        printf("kv: (%zu,%zu)\n", key, *(uintptr_t*)value);
        btree_remove(tree, key);
        //if (!IsValidBtree(tree, max_key, remove_keys, i+1, n_keys))
        //{
        //    printf("Failed after removing %zu\n", key);
        //    PrintTree(tree);
        //    printf("[%s]: FAIL\n", test_name);
        //    return false;
        //}
    }

    printf("[%s]: PASS\n", test_name);
    return true;
}

int main(int argc, char** argv)
{
    efj::btree_base tree;
    efj::btree_init(&tree, efj::current_allocator());

    uint32_t seed = (uint32_t)(uintptr_t)&tree;
    //uint32_t seed = 58733440; // Split with prev node failure due to filling the prev node completely instead of n-1.
    printf("Using seed %u\n", seed);
    srand(seed);

    constexpr int kInsertCount = 500;
    constexpr int kMaxKey = 10000;

    uintptr_t insert_keys[kInsertCount] = {};
    uintptr_t remove_keys[kInsertCount] = {};

    //logging = true;
    GenerateRandomKeys(0, kMaxKey, insert_keys, kInsertCount);
    GenerateRandomExistingKeys(insert_keys, remove_keys, kInsertCount);

    TestInsertAndRemove("random_insert_random_remove", &tree, kMaxKey, insert_keys, remove_keys, kInsertCount);

    GenerateAscendingKeys(0, insert_keys, kInsertCount);
    GenerateAscendingKeys(0, remove_keys, kInsertCount);
    TestInsertAndRemove("asc_insert_asc_remove", &tree, kMaxKey, insert_keys, remove_keys, kInsertCount);

    GenerateAscendingKeys(0, insert_keys, kInsertCount);
    GenerateDescendingKeys(kInsertCount, remove_keys, kInsertCount);
    TestInsertAndRemove("asc_insert_desc_remove", &tree, kMaxKey, insert_keys, remove_keys, kInsertCount);

    GenerateDescendingKeys(kInsertCount, insert_keys, kInsertCount);
    GenerateAscendingKeys(0, remove_keys, kInsertCount);
    TestInsertAndRemove("desc_insert_asc_remove", &tree, kMaxKey, insert_keys, remove_keys, kInsertCount);

    GenerateAscendingKeys(0, insert_keys, kInsertCount);
    GenerateAscendingKeys(kInsertCount/2, remove_keys, kInsertCount/2);
    GenerateAscendingKeys(0, remove_keys + kInsertCount/2, kInsertCount - kInsertCount/2, remove_keys + kInsertCount);
    TestInsertAndRemove("asc_insert_middle_remove", &tree, kMaxKey, insert_keys, remove_keys, kInsertCount);

    return 0;
}

//UTEST_STATE();

#define EFJ_C2_IMPLEMENTATION
#include <c2.hpp>

