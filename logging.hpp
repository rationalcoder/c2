namespace efj
{

enum log_level_
{
    log_level_debug = 0x1,
    log_level_info  = 0x2,
    log_level_warn  = 0x4,
    log_level_crit  = 0x8,

    log_level_all_standard = log_level_debug | log_level_info | log_level_warn | log_level_crit,
    log_level_all          = ~(flag32)0,
};

using log_level = int;

// Internal to the log system. Exposing this struct to keep certain functions inline.
// :ExposingObjectContext
struct logsys_object_ctx
{
    efj::log_level logged   = (efj::log_level_all & ~efj::log_level_debug);
    bool           logDebug = true;
    bool           logInfo  = true;
    bool           logWarn  = true;
    bool           logCrit  = true;
};

struct log_message_ctx
{
    efj::object*    object;
    const char*     file;
    int             line;
    int             level;
    efj::posix_time time;
    bool            formatted;
};

struct log_message
{
    efj::log_message*    next;
    efj::log_message_ctx ctx;
    efj::string          msg;
};

using log_formatter = efj::string(const efj::log_message_ctx& ctx, const efj::string& message);
using logger_fn     = void(efj::object* o, const efj::log_message& buffer);

efj::string default_log_formatter(const efj::log_message_ctx& ctx, const efj::string& msg);

struct log_config
{
    efj::log_formatter* formatter = &default_log_formatter;
    efj::logger_fn*     cb        = nullptr;
    efj::object*        object    = nullptr;
    const char*         path      = nullptr;
};

void log_unformatted(bool yes);

#define efj_log(_level, ...)\
do {\
    if (efj::should_log(_level))\
        efj::_log(_level, __FILE__, __LINE__, __VA_ARGS__);\
} while(false)

#define efj_logx(_level, ...)\
do {\
    if (efj::should_log(_level))\
        efj::_logx(_level, __FILE__, __LINE__, __VA_ARGS__);\
} while(false)


#define efj_log_debug(...) do { if (efj::should_log(efj::log_level_debug)) efj::_log(efj::log_level_debug, __FILE__, __LINE__, __VA_ARGS__); } while(false)
#define efj_log_info(...) do { if (efj::should_log(efj::log_level_info)) efj::_log(efj::log_level_info, __FILE__, __LINE__, __VA_ARGS__); } while(false)
#define efj_log_warn(...) do { if (efj::should_log(efj::log_level_warn)) efj::_log(efj::log_level_warn, __FILE__, __LINE__, __VA_ARGS__); } while(false)
#define efj_log_crit(...) do { if (efj::should_log(efj::log_level_crit)) efj::_log(efj::log_level_crit, __FILE__, __LINE__, __VA_ARGS__); } while(false)

// @Cleanup. It's not clear from the name that this is really more like forced log, one that doesn't care about the logging level.
#define efj_internal_log(_level, ...) efj::_log(_level, __FILE__, __LINE__, __VA_ARGS__)

// Versions that use extended stbsprintf format options that don't use the gcc format string checker.

#define efj_log_debugx(...) do { if (efj::should_log(efj::log_level_debug)) efj::_logx(efj::log_level_debug, __FILE__, __LINE__, __VA_ARGS__); } while(false)
#define efj_log_infox(...) do { if (efj::should_log(efj::log_level_info)) efj::_logx(efj::log_level_info, __FILE__, __LINE__, __VA_ARGS__); } while(false)
#define efj_log_warnx(...) do { if (efj::should_log(efj::log_level_warn)) efj::_logx(efj::log_level_warn, __FILE__, __LINE__, __VA_ARGS__); } while(false)
#define efj_log_critx(...) do { if (efj::should_log(efj::log_level_crit)) efj::_logx(efj::log_level_crit, __FILE__, __LINE__, __VA_ARGS__); } while(false)

#define efj_internal_logx(_level, ...) efj::_logx(_level, __FILE__, __LINE__, __VA_ARGS__)

// TODO: Change int log level type to efj::log_level

inline bool should_log(int level);
EFJ_MACRO void log_enable_level(efj::object* o, efj::log_level level);
EFJ_MACRO void log_disable_level(efj::object* o, efj::log_level level);
EFJ_MACRO efj::log_level log_get_level(efj::object* o, efj::log_level level);
EFJ_MACRO void log_set_level(efj::object* o, efj::log_level level);

void log(int level, const char* file, int line, const efj::string& msg);

void _log_va(int level, const char* file, int line, const char* fmt, va_list va);

inline void _log(int level, const char* file, int line, const char* fmt, ...) __attribute__((format (printf, 4, 5)));
//! For non-standard format specifiers. This just disables compiler warnings.
inline void _logx(int level, const char* file, int line, const char* fmt, ...);

void print(const char* fmt, ...) __attribute__((format (printf, 1, 2)));
void print_error(const char* fmt, ...) __attribute__((format (printf, 1, 2)));

void print(const efj::string& msg);
void print_error(const efj::string& msg);
} // end namespace efj
