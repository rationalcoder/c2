namespace efj
{

// Clang
#if defined(__has_feature)
    #if __has_feature(address_sanitizer)
        #define EFJ_USING_ADDRESS_SANITIZER 1
    #endif
#endif

// GCC
#ifndef EFJ_USING_ADDRESS_SANITIZER
    #ifdef __SANITIZE_ADDRESS__
        #define EFJ_USING_ADDRESS_SANITIZER 1
    #endif
#endif

#ifndef EFJ_USING_ADDRESS_SANITIZER
    #define EFJ_USING_ADDRESS_SANITIZER 0
#endif

#ifndef EFJ_THREAD_UNRESPONSIVE_SIGNAL
#define EFJ_THREAD_UNRESPONSIVE_SIGNAL SIGUSR1
#endif

struct source_location
{
    const char* file = nullptr;
    int         line = 0;

    source_location() = default;
    source_location(const char* file, int line) : file(file), line(line) {}
};

#define EFJ_LOCATION efj::source_location{__FILE__, __LINE__}
#if EFJ_DEBUG_LOCATIONS
    #define EFJ_LOC_SIG , const efj::source_location& loc
    #define EFJ_LOC_UNARY_SIG const efj::source_location& loc
    #define EFJ_LOC_ARGS , EFJ_LOCATION
    #define EFJ_LOC_ARG EFJ_LOCATION
    #define EFJ_LOC_ARGS_FWD , loc
    #define EFJ_LOC_ARG_FWD loc
#else
    #define EFJ_LOC_SIG
    #define EFJ_LOC_UNARY_SIG
    #define EFJ_LOC_ARGS
    #define EFJ_LOC_ARG
    #define EFJ_LOC_ARGS_FWD
    #define EFJ_LOC_ARG_FWD
#endif

#define EFJ_UNIQUE_NAME_IMPL1(n, c) n##c
#define EFJ_UNIQUE_NAME_IMPL2(n, c) EFJ_UNIQUE_NAME_IMPL1(n, c)
#define EFJ_UNIQUE_NAME(n) EFJ_UNIQUE_NAME_IMPL2(n, __COUNTER__)

#define EFJ_STRINGIFY(name) #name

void assertion_failed(const char* format, const char* file, int line, const char* func);

// TODO(bmartin): Change api to static_assert style?
//#define efj_assert(cond) do { if (!(cond)) { *((int*)NULL) = 0; } } while (false)
#define efj_handle_assert(cond, file, line, func)\
    if (!(cond)) {\
        efj::assertion_failed("%s:%d: %s: Assertion `" #cond "' failed.\n", file, line, func);\
    }

#define efj_assert(cond) efj_handle_assert(cond, __FILE__, __LINE__, __func__)
#define efj_rare_assert assert
// TODO: panic is a runtime assert, so log()/abort().
#define efj_panic(msg) efj_assert(false && msg)
#define efj_panic_if(cond) do { bool value = (cond); if (value) { efj_assert(!#cond); } } while (false)
#define efj_check(cond) do { bool value = (cond); if (!value) { efj_assert(!#cond); } } while (false)
#define efj_not_implemented() efj_assert(!"Not Implemented!")
#define efj_invalid_code_path() efj_assert(!"Invalid code path!")
// FIXME: ARMv5 probably has something like a 32 byte cache line size.
// x86 has a 64 byte one. We only use this for alignment and padding, so it's not a big deal.
#define EFJ_CACHELINE_SIZE 64
#define EFJ_FORCE_INLINE __attribute__((always_inline)) inline
#define EFJ_MACRO __attribute__((always_inline)) inline

// TODO: post C++17, use [[fallthrough]]
#if defined(__GNUC__) && __GNUC__ >= 7
    #define EFJ_FALLTHROUGH __attribute__ ((fallthrough))
#else
    #define EFJ_FALLTHROUGH ((void)0)
#endif


#define EFJ_DISABLE_WARN_UNUSED \
_Pragma("GCC diagnostic push");\
_Pragma("GCC diagnostic ignored \"-Wunused-value\"")

#define EFJ_ENABLE_WARN_UNUSED _Pragma("GCC diagnostic pop")

// Needed to ignore warnings about ignoring the return value at higher optimization levels.
#define efj_checked_read(fd, buf, size) efj_check(::read(fd, buf, size) == size)

#define efj_trigger_eventfd(fd)\
do {\
    u64 val = 1;\
    efj_check(::write(fd, &val, sizeof(val)) == sizeof(val));\
} while (false)

#define efj_read_eventfd(fd)\
do {\
    u64 val;\
    efj_check(::read(fd, &val, sizeof(val)) == sizeof(val));\
} while (false)

#define efj_zero_struct(ptr) memset((ptr), 0, sizeof(*ptr))
#define efj_debug_zero_struct(ptr) memset((ptr), 0, sizeof(*ptr))

template <typename T>
EFJ_MACRO void ignore_argument(T&&) {}

// Using _Pragma() do disable the unused result warning in a macro doesn't work with
// some version of gcc that we've tested with (7.5.0, for example), but we can just
// use a force inlined function that doesn't do anything to get the same result.
#define efj_ignored_write(fd, data, size)\
efj::ignore_argument(::write(fd, data, size))

#define efj_ignored_writev(fd, buffers, count)\
efj::ignore_argument(::writev(fd, buffers, count))

// TODO: static_assert is_pointer to on ptr. I don't want to litter static_asserts all throughout the
// code though, so maybe use a build flag?
#define efj_container_of(ptr, type, member) ((type*)((u8*)ptr - offsetof(type, member)))
#define efj_typeof(ref) std::remove_reference<decltype(ref)>::type

enum uninitialized_tag { uninitialized };
enum delay_init_tag { delay_init };

// Make sure not to take the result by reference if you passed an rvalue. That could cause a dangling
// pointer if the lifetime is not extended by the reference.
//
// // Could be an error.
// const Value& value = efj::max(val, myExpression + value);
//
template <typename T> EFJ_MACRO constexpr const T&
max(const T& a, const T& b)
{
    return (a < b) ? b : a;
}

} // namespace efj

