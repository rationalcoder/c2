namespace efj
{

using init_fn   = bool(efj::object*);
using config_fn = bool(efj::object*, const efj::config_file&, bool accepted);

using begin_frame_fn = void(efj::object*);
using end_frame_fn   = void(efj::object*);

using notify_event_fn                = void(efj::object*, efj::event_data*);
using notify_value_edit_fn           = void(efj::object*, efj::debug_value_type, void*, const void*, umm, efj::callback);

// TODO: Remove the need for most wrappers in here. We should be able
// to do whatever we need with the callback mechanism. That way,
// callbacks like the ones for editing watch values can be in the
// debug system where you would expect them to be.
// :ReplaceWithCallbackMechanism
struct object_call_wrapper
{
    //{ bool init();
    template <typename T_>
    static auto init(efj::object*)
        -> typename std::enable_if<!efj::has_init<T_>::value, bool>::type
    { return true; }

    template <typename T_>
    static auto init(efj::object* o)
        -> typename std::enable_if< efj::has_init<T_>::value, bool>::type
    { return ((T_*)o)->init(); }
    //}

    // :ReplaceWithCallbackMechanism
    template <typename Class_, typename Type_> static EFJ_FORCE_INLINE void
    _edit_primitive(efj::object* o, void* dst, const void* buf, efj::callback cb)
    {
        Type_ value;
        memcpy(&value, buf, sizeof(value));

        if (cb.type == efj::callback_proc) {
            using CB = void (efj::object*, Type_*, Type_);
            ((CB*)cb.proc)(o, (Type_*)dst, value);
        }
        else {
            using CB = void (Class_::*)(Type_*, Type_);
            auto memfn = *((CB*)cb.memfn);
            (((Class_*)o)->*memfn)((Type_*)dst, value);
        }
    }

    // :ReplaceWithCallbackMechanism
    template <typename T_>
    static void notify_value_edit(efj::object* o, efj::debug_value_type type,
                                  void* dst, const void* buf, umm size, efj::callback cb)
    {
        efj_assert(cb.type == efj::callback_memfn || cb.type == efj::callback_proc);

        switch (type) {
        case efj::debug_value_string: {
            efj_not_implemented();
            //efj::string value((char*)buf, size);

            //if (cb.type == efj::callback_proc) {
            //    using CB = void (efj::object*, const efj::string&);
            //    ((CB*)cb.proc)(o, value);
            //}
            //else {
            //    using CB = void (T_::*)(const efj::string&);
            //    auto memfn = *((CB*)cb.memfn);
            //    (((T_*)o)->*memfn)(value);
            //}

            break;
        }
        case debug_value_u64:  _edit_primitive<T_, u64> (o, dst, buf, cb); break;
        case debug_value_s64:  _edit_primitive<T_, s64> (o, dst, buf, cb); break;
        case debug_value_f64:  _edit_primitive<T_, f64> (o, dst, buf, cb); break;
        case debug_value_u32:  _edit_primitive<T_, u32> (o, dst, buf, cb); break;
        case debug_value_s32:  _edit_primitive<T_, s32> (o, dst, buf, cb); break;
        case debug_value_f32:  _edit_primitive<T_, f32> (o, dst, buf, cb); break;
        case debug_value_u16:  _edit_primitive<T_, u16> (o, dst, buf, cb); break;
        case debug_value_s16:  _edit_primitive<T_, s16> (o, dst, buf, cb); break;
        case debug_value_u8:   _edit_primitive<T_, u8>  (o, dst, buf, cb); break;
        case debug_value_s8:   _edit_primitive<T_, s8>  (o, dst, buf, cb); break;
        case debug_value_bool: _edit_primitive<T_, bool>(o, dst, buf, cb); break;
        default:
            efj_invalid_code_path();
            break;
        }
    }
};

struct producer;
struct consumer;

struct object
{
    u32                        id                  = 0;
    bool                       enabled             = true;
    const char*                name                = nullptr;
    efj::thread*               thread              = nullptr;
    efj::producer*             producers           = nullptr;
    efj::consumer*             consumers           = nullptr;

    efj::init_fn*              initFunc            = nullptr;
    efj::notify_value_edit_fn* notifyValueEditFunc = nullptr;

    efj::uesys_object_ctx*     ueContext           = nullptr;
    efj::debugsys_object_ctx*  debugContext        = nullptr;
    efj::configsys_object_ctx* configContext       = nullptr;
    efj::testsys_object_ctx*   testContext         = nullptr;
    efj::logsys_object_ctx*    logContext          = nullptr;

    template <typename ObjectT>
    object(const char* name, ObjectT* o)
        : name(name)
    {
        efj_assert((void*)this == (void*)o && "EFJ_OBJECT must be the first member of your struct");

        efj::_add_object(this);

        initFunc = &efj::object_call_wrapper::init<ObjectT>;

        // :ReplaceWithCallbackMechanism
        notifyValueEditFunc = &efj::object_call_wrapper::notify_value_edit<ObjectT>;
    }

    bool init();

    static void _log_level_edited(efj::object* o, bool* value, bool newValue);

    // :ReplaceWithCallbackMechanism
    void notify_value_edit(efj::debug_value_type type, void* dst, const void* buf, umm size, efj::callback cb)
    {
        efj::_debugsys_object_touched(this EFJ_LOC_ARGS);
        notifyValueEditFunc(this, type, dst, buf, size, cb);
    }
};

#define EFJ_OBJECT(name)\
static const char* efj_get_name() { return name; }\
efj::object efj_object = efj::object{name, this}

} // namespace efj

