namespace efj
{

struct udp_socket;

enum udp_socket_event_type
{
    udp_socket_event_invalid,
    udp_socket_event_read,
    udp_socket_event_error,
    udp_socket_event_count_,
};

struct udp_socket_event
{
    efj::udp_socket_event_type type = efj::udp_socket_event_invalid;
    efj::udp_socket*           sock = nullptr;
};

template <typename T> inline bool
udp_add_socket(const char* name, efj::udp_socket* result,
               void (T::*callback)(efj::udp_socket_event& e));

bool udp_create_socket(const char* name, efj::udp_socket* result);
void udp_destroy_socket(efj::udp_socket& sock);

template <typename T> inline void
udp_monitor(efj::udp_socket& sock, void (T::*callback)(efj::udp_socket_event& e));

bool udp_bind(efj::udp_socket& sock, const char* ip, u16 port);
bool udp_set_high_priority(efj::udp_socket& sock);

ssize_t udp_read(efj::udp_socket& sock, void* buf, umm size, int flags = 0);
ssize_t udp_recv_from(efj::udp_socket& sock, void* buf, umm size, efj::net_sock_addr* addr, int flags = 0);
ssize_t udp_send_to(efj::udp_socket& sock, const void* buf, umm size, const char* ip, u16 port, int flags = 0);
//! IMPORTANT: This function assumes `ip` and `port` are already in network byte order.
ssize_t udp_send_to(efj::udp_socket& sock, const void* buf, umm size, u32 ipv4, u16 port, int flags = 0);
ssize_t udp_send_to(efj::udp_socket& sock, const void* buf, umm size, const efj::net_sock_addr& addr, int flags = 0);
ssize_t udp_send_to(int fd, const void* buf, umm size, const efj::net_sock_addr& addr, int flags = 0);

// Event API

inline umm udp_read(efj::udp_socket_event& e, void* buf, umm size, int flags = 0);
inline umm udp_recv_from(efj::udp_socket_event& e, void* buf, umm size, efj::net_sock_addr* addr, int flags = 0);
inline umm udp_send_to(efj::udp_socket_event& e, const void* buf, umm size, const char* ip, u16 port, int flags = 0);
inline umm udp_send_to(efj::udp_socket_event& e, const void* buf, umm size, const efj::net_sock_addr& addr, int flags = 0);

} // namespace efj
