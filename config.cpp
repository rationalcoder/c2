namespace efj
{

static inline const efj::xml_element*
_find_element(const efj::xml_document& doc, const efj::string& path, efj::string* childName)
{
    if (!path)       return nullptr;
    if (!doc.root()) return nullptr;

    const auto* parent    = doc.root();
    const char* nameStart = path.data;

    for (const char* c = path.data; *c; c++) {
        if (*c == '/') {
            // No end-begin + 1 b/c the "/" isn't part of the name.
            auto  name = efj::temp_string(nameStart, c - nameStart);
            auto* next = parent->first_child(name);
            if (!next) return nullptr;

            parent    = next;
            nameStart = c + 1;
            continue;
        }
    }

    *childName = nameStart;
    return parent;
}

// TODO: efj::string::split

extern efj::string
config_file::get(const efj::string& path) const
{
    efj_temp_scope();
    efj_panic_if(type != efj::config_xml);

    efj::string childName;
    const auto* parent = efj::_find_element(*xml, path, &childName);
    if (!parent) return efj::string();

    const auto* child = parent->first_child(childName);
    if (!child) return efj::string();

    return child->content();
}

extern efj::config_value_list
config_file::get_list(const efj::string& path) const
{
    efj_temp_scope();
    efj_panic_if(type != efj::config_xml);

    efj::config_value_list result = {};

    efj::string childName;
    const auto* parent = efj::_find_element(*xml, path, &childName);
    if (!parent) return result;

    umm   count  = parent->child_count(childName);
    auto* values = efj_allocate_array(count, efj::string);

    const auto* child = parent->first_child(childName);
    for (umm i = 0; i < count; i++, child = child->next_sibling())
        values[i] = child->content().dup();

    result.values = values;
    result.count  = count;

    return result;
}

extern bool
config_add_file(u32 id, const char* path, efj::config_type type)
{
    efj::config_system* sys = efj::app()._configSystem;
    efj_panic_if(!sys->preInitted);

    // Opening the file just to make sure it exists and we have permission to read it etc.
    int fd = ::open(path, O_RDONLY | O_CLOEXEC);
    if (fd == -1) {
        efj_internal_log(efj::log_level_warn,
            "Failed to load config file (id: %u, path: '%s', type: %d): %s", id, path, type, strerror(errno));
    }
    else {
        ::close(fd);
        efj_panic_if(fd < 3);
    }

    efj::_configsys_add_config_file(sys, id, path, type);
    return true;
}

extern bool
_config_subscribe(u32 id, efj::callback cb)
{
    efj::object* o = efj::this_object();
    if (!o->enabled)
        return true; // Pretend it succeeded

    return efj::_configsys_subscribe(efj::app()._configSystem, id, cb);
}

extern bool
_config_subscribe(efj::config_event_type type, efj::callback cb)
{
    efj::object* o = efj::this_object();
    if (!o->enabled)
        return true; // Pretend it succeeded

    return efj::_configsys_subscribe(efj::app()._configSystem, type, cb);
}

} // namespace efj
