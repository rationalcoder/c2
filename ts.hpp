
// Thread-safety stuff exposed to clients, which should never have to be used by client code...
// @Incomplete. This is really incomplete simply b/c it's not that important right now,
// and normal clients should not need mutexes.

namespace efj
{

struct mutex
{
    pthread_mutex_t _mutex;

    EFJ_FORCE_INLINE mutex() { pthread_mutex_init(&_mutex, NULL); }
    EFJ_FORCE_INLINE ~mutex() { pthread_mutex_destroy(&_mutex); }

    EFJ_FORCE_INLINE void lock() { pthread_mutex_lock(&_mutex); }
    EFJ_FORCE_INLINE void unlock() { pthread_mutex_unlock(&_mutex); }
};

#define efj_lock(mutex) (mutex).lock()
#define efj_unlock(mutex) (mutex).unlock()

template <typename Mutex_>
struct lock_scope
{
    Mutex_* _mutex;

    EFJ_FORCE_INLINE lock_scope(Mutex_& mutex) : _mutex(&mutex) { efj_lock(mutex); }
    EFJ_FORCE_INLINE ~lock_scope() { if (_mutex) efj_unlock(*_mutex); }

    EFJ_FORCE_INLINE lock_scope(lock_scope&& scope) : _mutex(scope._mutex) { scope._mutex = nullptr; }
};

template <typename Mutex_> EFJ_FORCE_INLINE efj::lock_scope<Mutex_>
make_scope(Mutex_& mutex) { return efj::lock_scope<Mutex_>(mutex); }

#define efj_lock_scope_impl2(mutex, counter) auto _scopedLock##counter = efj::make_scope(mutex)
#define efj_lock_scope_impl1(mutex, counter) efj_lock_scope_impl2(mutex, counter)
#define efj_lock_scope(mutex) efj_lock_scope_impl1(mutex, __COUNTER__)


} // namespace efj
