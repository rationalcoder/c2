#include <stdio.h>
#include "c2.hpp"

struct Foo
{
    EFJ_OBJECT("Foo");

    efj::timer _printTimer;
    efj::timer _editTimer;

    u32 _value = 1;
    bool _crash = false;

    bool init()
    {
        efj::debug_add_watch("Value", &_value);
        efj::debug_add_watch("Crash", &_crash);

        efj::add_periodic_timer("Print Timer", 250_ms, &_printTimer, &Foo::onPrint);
        efj::add_periodic_timer("Edit Timer", 250_ms, &_editTimer,  &Foo::onEdit);

        return true;
    }

    void onPrint(efj::timer_event&)
    {
        if (_crash) {
            int* i = nullptr;
            *i = 5;
        }

        //efj_log_info("Timeout! %d", _value);
        //efj_log_debug("Timeout! %d", _value);
    }

    void onEdit(efj::timer_event&)
    {
        _value++;
    }
};

struct Hanger
{
    EFJ_OBJECT("Hanger");

    efj::timer _hangTimer;
    bool       _hang = false;

    bool init()
    {
        efj::debug_add_watch("Hang", &_hang);
        efj::add_periodic_timer("Hang", 1_s, &_hangTimer, &Hanger::onHang);

        return true;
    }

    void onHang(efj::timer_event&)
    {
        if (_hang) {
            efj_log_debug("Hanging...");

            for (;;) {
                usleep(500000);
            }
        }
    }
};

struct Logger
{
    EFJ_OBJECT("Logger");
    EFJ_CONSUMER(efj::log_buffer);

    void consume(const efj::log_buffer& buf)
    {
    }
};

int main(int argc, const char** argv)
{
    Foo foo;
    //Hanger hanger;
    //Logger logger;

    //efj::set_logger(logger);

    //efj::thread* t = efj::create_thread("Hanger");
    //t->add_objects(hanger);

    return efj::exec(argc, argv);
}

#define EFJ_C2_IMPLEMENTATION
#include "c2.hpp"
