#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <common2.hpp>

struct Writer
{
    EFJ_OBJECT("Writer");

    int        _pipe;
    efj::timer _timer;

    u32        _count = 0;

    Writer(int pipe) : _pipe(pipe) {}

    bool init()
    {
        efj::add_periodic_timer("Write", 500_ms, _timer, &Writer::notify);

        return true;
    }

    void notify(efj::timer&)
    {
        auto str = efj::fmt("%u\n", _count++);
        ::write(_pipe, str.data, str.size);
    }
};

struct Reader
{
    EFJ_OBJECT("Reader");

    efj::fd    _fd;
    efj::timer _timer;
    u32        _count = 0;

    Reader(int pipe) { efj::make_fd("Pipe", pipe, _fd); }

    bool init()
    {
        efj::add_periodic_timer("Write", 2_s, _timer, &Reader::notify);
        efj::monitor(_fd, efj::io_in, &Reader::echo1);

        return true;
    }

    void notify(efj::timer&)
    {
        static u32 count = 0;
        if (count++ % 2 == 0) {
            printf("Unmonitor\n");
            efj::unmonitor(_fd);
        }
        else {
            printf("Monitor\n");
            efj::monitor(_fd);
        }
    }

    void echo1(efj::fd& fd, flag32 events)
    {
        if (events & efj::io_in) {
            if (_count++ % 5 == 0) {
                efj::monitor(fd, &Reader::echo2);
            }

            char buf[512];
            ssize_t n = efj::read(fd, buf, sizeof(buf));
            if (n > 0) {
                printf("1: %.*s", (int)n, buf);
            }
        }
    }

    void echo2(efj::fd& fd, flag32 events)
    {
        if (events & efj::io_in)
        {
            if (_count++ % 5 == 0) {
                efj::monitor(fd, &Reader::echo1);
            }

            char buf[512];
            ssize_t n = efj::read(fd, buf, sizeof(buf));
            if (n > 0) {
                printf("2: %.*s", (int)n, buf);
            }
        }
    }
};

int main(int argc, const char** argv)
{
    int fds[2] = { -1, -1 };
    pipe2(fds, O_CLOEXEC);

    Writer writer(fds[1]);
    Reader reader(fds[0]);

    return efj::exec(argc, argv);
}

#define EFJ_COMMON2_IMPLEMENTATION
#include <common2.hpp>

