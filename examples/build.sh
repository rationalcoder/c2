#/usr/bin/bash

ExamplesDir=`pwd`
for Dir in $(ls $ExamplesDir); do
    if [[ -d "$Dir" ]]; then
        echo "$Dir/" 
        cd $Dir
        make all
        cd $ExamplesDir
    fi
done
