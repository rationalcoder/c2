#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <common2.hpp>
#include "proto.hpp"

struct Server
{
    EFJ_OBJECT("Server");
    EFJ_PRODUCER(mock::Message, mock::Header);
    EFJ_CONSUMER(mock::Message, mock::Header);

    efj::tcp_server _server;

    bool init()
    {
        efj::tcp_server_info info;

        efj::tcp_create_server("Server", info, _server);

        efj::tcp_bind(_server, 6666);
        efj::tcp_listen(_server, 2, &Server::handleConnection);

        return true;
    }

    void handleConnection(efj::tcp_server& server, flag32 events)
    {
        if (efj::connection_ready(server, events)) {

            struct sockaddr_in address;
            efj::tcp_accept(server, &address, &Server::handleClient, NULL);

            printf("Accepted connection from %s\n", inet_ntoa(address.sin_addr));

            return;
        }

        if (events & efj::io_hup) {
            printf("Closing (connection)\n");
            efj::close(_server);
        }
    }

    void handleClient(efj::tcp_connected_client& client, flag32 events, void*)
    {
        if (events & efj::io_rdhup || events & efj::io_hup) {
            printf("Closing (client): %s\n", efj::to_string(efj::now_utc()).c_str());
            efj::close(client);
            return;
        }

        if (events & efj::io_in) {
            char buf[256] = {};
            int  len      = 0;
            while ((len = efj::read(client, buf, sizeof(buf))) > 0) {
                printf("Got '%.*s'\n", len, buf);//efj::send_all(client, buf, len);
                efj::send(client, buf, len, 0);

                mock::Message msg = { buf, (umm)len };
                efj_produce(*mock::add_header("(from server): ", &msg));
            }
        }
    }

    void consume(const mock::Message& msg)
    {
        efj_log_infox("Consumed on thread %I64u %zu (%.*s)\n", efj::real_thread_id(), msg.size, (int)msg.size, (char*)msg.data);
        for (auto& c : _server.clients) {
            efj_log_infox("(Thread: %I64u) Client: %d\n", efj::real_thread_id(), c.handle.fd);
            efj::send(c, msg.data, msg.size, 0);
        }
    }

    void consume(const mock::Header& hdr)
    {
        for (auto& c : _server.clients) {
            efj::send(c, "blah", 5, 0);

            for (auto* cur = &hdr; cur; cur = cur->next) {
                if (cur->type == mock::DATA_TYPE_HEADER)
                    efj::send(c, cur->data, strlen((char*)cur->data), MSG_MORE);
                else
                    efj::send(c, ((mock::Message*)cur->data)->data, ((mock::Message*)cur->data)->size, 0);
            }
        }
    }
};

struct Broadcaster
{
    EFJ_OBJECT("Broadcaster");
    EFJ_PRODUCER(mock::Message);

    efj::timer _timer;

    bool init()
    {
        efj::add_periodic_timer("Timer", 100_ms, _timer, &Broadcaster::broadcastTimerExpired);

        return true;
    }

    void broadcastTimerExpired(efj::timer&)
    {
        efj_log_infox("From thread: %I64u\n", efj::real_thread_id());
        mock::Message msg = { (void*)"Test", 5 };

        efj_produce(msg);
    }
};

int main(int argc, const char** argv)
{
    Server      server;
    Broadcaster broadcaster;
    efj_connect(server,      mock::Message, server);
    efj_connect(server,      mock::Header,  server);
    efj_connect(broadcaster, mock::Message, server);

#if 0
    efj_create_chain("Main", {
        efj::add_objects(server);
        efj_connect(server, mock::Message, server);
        efj_connect(server, mock::Header,  server);
    });

    efj_create_background_chain("Broadcast", {
        efj::add_objects(broadcaster);
        efj_connect(broadcaster, mock::Message, server);
    });
#endif

    return efj::exec(argc, argv);
}

#define EFJ_COMMON2_IMPLEMENTATION
#include <common2.hpp>

