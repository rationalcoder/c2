#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <utility>

#include <c2.hpp>

enum
{
    CONFIG_MAIN,
};

struct Foo
{
    EFJ_OBJECT("Foo");

    efj::timer _timer;

    bool init()
    {
        //efj::add_periodic_timer("Timer", 500_ms, _timer, &Foo::timerExpired);
        efj::config_subscribe(CONFIG_MAIN, &Foo::onConfig);
        return true;
    }

    void timerExpired(efj::timer_event&)
    {
        printf("Foo timer on thread %u\n", efj::logical_thread_id());
    }

    void onConfig(efj::config_event& e)
    {
        if (efj::config_accepted(e)) {
            printf("Foo accepted on thread %u\n", efj::logical_thread_id());
        }
        else {
            printf("Foo config on thread %u\n", efj::logical_thread_id());
        }
    }
};

struct Bar
{
    EFJ_OBJECT("Bar");

    efj::timer _timer;

    int value;

    bool init()
    {
        //efj::add_periodic_timer("Timer", 500_ms, _timer, &Bar::timerExpired);
        efj::config_subscribe(CONFIG_MAIN, &Bar::onConfig);
        return true;
    }

    void timerExpired(efj::timer_event& timer)
    {
        printf("Bar timer on thread %u\n", efj::logical_thread_id());
    }

    void onConfig(efj::config_event& e)
    {
        static u32 count = 0;

        if (efj::config_accepted(e)) {
            printf("Bar accepted on thread %u\n", efj::logical_thread_id());
        }
        else {
            printf("Bar config on thread %u\n", efj::logical_thread_id());
            if (count++ % 2) {
                printf("Bar rejecting...\n");
            }
        }
    }
};

int main(int argc, const char** argv)
{
    Foo foo1;
    Foo foo2;
    Bar bar;

    efj::thread* fooThread = efj::create_thread("Foo");
    fooThread->add_objects(foo1, foo2);

    efj::thread* barThread = efj::create_thread("Bar");
    barThread->add_objects(bar);

    efj::config_add_file(CONFIG_MAIN, "config_type.xml", efj::config_xml);

    return efj::exec(argc, argv);
}

#define EFJ_C2_IMPLEMENTATION
#include <c2.cpp>
