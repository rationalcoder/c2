#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <utility>

#include <c2.hpp>

#define VALUE_MAX 32

enum ConfigFileType
{
    MAIN_CFG = 32,
    TUNING_CFG = 31
};

static void
print_tree(const efj::xml_element* e, int level = 0)
{
    efj_temp_scope();

    if (!e) return;

    char indent[level+1];
    memset(indent, ' ', sizeof(indent));
    printf("%.*s", level, indent);

    printf("%s", e->name().c_str());
    for (auto* attr = e->first_attribute(); attr; attr = attr->next()) {
        printf(" '%s'='%s'", attr->name().c_str(), attr->value().c_str());
    }

    for (auto* child = e->first_child(); child; child = child->next()) {
        printf("\n");
        print_tree(child, level + 1);
    }

    auto trimmed = e->content().trim();
    if (trimmed.size && e->child_count() == 0)
        printf(" = \"%s\"", trimmed.c_str());
}

struct FooConfig
{
    char ip[256]          = {};
    u16  port             = 1025;

    int values[VALUE_MAX] = {};
    umm valueCount        = 0;
};

struct Foo
{
    EFJ_OBJECT("Foo");

    FooConfig _curCfg;
    FooConfig _pendingCfg;

    FooConfig _curTuning;
    FooConfig _pendingTuning;

    bool init()
    {
        efj::config_subscribe(MAIN_CFG, &Foo::onConfig);
        return true;
    }

    void onConfig(efj::config_event& e)
    {
        efj::config_file& cfg = *e.file;
        printf("ID: %u\n", cfg.id);

        FooConfig* pending = &_pendingCfg;
        FooConfig* cur     = &_curCfg;

        if (cfg.id == TUNING_CFG) {
            pending = &_pendingTuning;
            cur     = &_curTuning;
        }

        if (efj::config_accepted(e)) {
            printf("Accepted. Applying config.\n");
            *cur = *pending;
            return;
        }

        efj::xml_element* root = cfg.xml->root();
        print_tree(root);
        printf("\n");

        efj::string ip   = cfg.get("section_1/ip");
        efj::string port = cfg.get("section_1/port");

        printf("IP string: %s\n",   ip.c_str());
        printf("Port string: %s\n", port.c_str());

        if (!ip) {
            efj::config_reject_because(e, "missing ip");
            return;
        }

        memcpy(pending->ip, ip.data, ip.size < 256 ? ip.size : 255);
        pending->port = port.to_u16(_pendingCfg.port);

        printf("IP: %s\n",   _pendingCfg.ip);
        printf("Port: %u\n", _pendingCfg.port);

        efj::config_value_list valueList = cfg.get_list("section_2/value");

        printf("Values (%zu):\n", valueList.count);
        for (u32 i = 0; i < valueList.count && i <= VALUE_MAX; i++) {
            printf("values[%u] = %d (%s)\n", i, valueList.values[i].to_int(12345), valueList.values[i].c_str());
            pending->values[i] = valueList.values[i].to_int(12345);
        }
    }
};

// Some object that rejects every other config file.
struct Rejector
{
    EFJ_OBJECT("Rejector");

    efj::tcp_server _server;

    bool init()
    {
        efj::config_subscribe(MAIN_CFG, &Rejector::onConfig);
        return true;
    }

    void onConfig(efj::config_event& e)
    {
        if (!efj::config_accepted(e)) {
            static u32 count = 1;

            if (count++ % 2 == 0)
                efj::config_reject_because(e, "I felt like it...");
        }
    }
};

int main(int argc, const char** argv)
{
    Foo foo;
    Rejector rejector;

    efj::config_add_file(MAIN_CFG, "config_file.xml", efj::config_xml);
    efj::config_add_file(TUNING_CFG, "config_file_tuning.xml", efj::config_xml);

    return efj::exec(argc, argv);
}

#define EFJ_C2_IMPLEMENTATION
#include <c2.cpp>

