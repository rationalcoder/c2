#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>


int main()
{
    bool started = false;
    uint32_t i = 0;

    struct timespec before = {};
    clock_gettime(CLOCK_REALTIME, &before);

    for (;;)
    {
        struct timespec time = {};
        clock_gettime(CLOCK_REALTIME, &time);

        if (!started)
        {
            if (time.tv_sec % 5 == 0 && time.tv_sec % 15 != 0)
            {
                printf("Starting second: %u\n", (uint32_t)time.tv_sec);
                before.tv_nsec = 0;
                started = true;
            }
            else
            {
                continue;
            }
        }
        else if (time.tv_sec % 15 == 0)
        {
            printf("Ending second: %u\n", (uint32_t)time.tv_sec);
            break;
        }

        if (((uint64_t)time.tv_sec * 1000000000 + time.tv_nsec) - ((uint64_t)before.tv_sec * 1000000000 + before.tv_nsec) >= 10000000)
        {
            printf("%u: s: %u ns: %09u\n", i, (uint32_t)time.tv_sec, (uint32_t)time.tv_nsec);
            before = time;
            ++i;
        }
    }

    return 0;
}
