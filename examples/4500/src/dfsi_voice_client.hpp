#pragma once

// [FSHost] <-> this
// Responsibilities:
// * Sending SOS Acks.
// * Setting sequence numbers and timestamps on inbound content.
//
struct DfsiVoiceClient
{
    EFJ_OBJECT("DFSI Voice Client");

    EFJ_INPUT(InboundVoice,  const RtpP25Message);
    EFJ_INPUT(OutboundVoice, const RtpP25Message);

    EFJ_OUTPUT(InboundVoice,  const RtpP25Message);
    EFJ_OUTPUT(OutboundVoice, const RtpP25Message);

    DfsiConnectEvent _dfsi_connect = {};
    bool             _connected    = false;

    u32 _sequence_number = 0;
    u32 _timestamp_origin = 0; // in units of 8 kHz (125 us)

    bool init();
    void onDfsiConnectionEvent(const efj::user_event& e);

    void consumeOutboundVoice(const RtpP25Message& msg);
    void consumeInboundVoice(const RtpP25Message& msg);
};

