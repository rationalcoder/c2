#pragma once

#include "rtp.hpp"

struct RtpP25Message : RtpMessage { using RtpMessage::RtpMessage; };

namespace efj
{
EFJ_DEFINE_MESSAGE(RtpP25Message, "1.0"); // TODO: more accurate version number
} // namespace efj

////////////////////////////////////////////////////////////////////////
// P25 extensions included at bottom of RTP header
//
////////////////////////////////////////////////////////////////////////
//   0               *   1           *       2       *           3
//   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// 0 |V=2|P|X|  CC   |M| PayloadType |       sequence number         |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// 4 |                           timestamp                           |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// 8 |           synchronization source (SSRC) identifier            |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+<-- end of RTP Header
//12 |0|C| HdrCnt=1  |E|  Block PT   | timestamp offset = 0      | Le|
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//16 |ngth (10 bits) |
//   +-+-+-+-+-+-+-+-+<-- end of P25 Fixed Header
//   when C=0
// ---OR----
//   0               *   1           *       2       *           3
//   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// 0 |V=2|P|X|  CC   |M| PayloadType |       sequence number         |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// 4 |                           timestamp                           |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// 8 |           synchronization source (SSRC) identifier            |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+<-- end of RTP Header
//12 |0|C| HdrCnt=1  |E|  Block PT   |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+<-- end of P25 Fixed Header
//   when C=1
//
// We do not support padding (P), the extension bit (X), CSRC
// (contributing source) counts (CC), or the marker bit (M).
// P=0, X=0, CC=0, M=0,
//
// Payload Type= <fill>, sequence number=<fill>, timestamp=<fill>, advance by 160 each IMBE
// SSRC=<fill>
// Signal = x
// C = 0 (verbose) or 1 (compressed)
// Header Count
// E = 0 (uLaw PCM), 1 (P25-specific)
// Block Type (BT):
// if E=0:
//      0   uLaw PCM
// if E=1:
//      0   IMBE
//      2   Link Control (LC)
//      3   Encryption Sync (ES)
//      4   Low Speed Data (LSD)
//      5   Header Data Unit (HDU)
//      6   Partial HDU Part 1
//      7   Partial HDU Part 2
//      9   Start of Stream (SOS)
//     10   End of Stream   (EOS)
//     XX   Other P25 block types

enum
{
    // The "DFSI header size" the size of the standard rtp header that we use for DFSI. No extentions and no CSSRCs.
    // This is the same for both compressed (C=1) and verbose (C=0) messages.
    P25_DFSI_HEADER_SIZE         = 12,
    P25_DFSI_PAYLOAD_TYPE        = 100,
    P25_DFSI_E_BIT               = 128,
};

// These set the E-bit b/c technically the "BlockType" includes the E-bit and "PayloadType" does not.
enum RtpP25BlockType
{
    P25_BT_ULAW                                     = 0x00,
    P25_BT_AUDIO                                    = 0x00 | P25_DFSI_E_BIT,
    P25_BT_PACKET_TYPE                              = 0x01 | P25_DFSI_E_BIT,
    P25_BT_LINK_CONTROL                             = 0x02 | P25_DFSI_E_BIT,
    P25_BT_ENCRYPTION_SYNC                          = 0x03 | P25_DFSI_E_BIT,
    P25_BT_LOW_SPEED_DATA                           = 0x04 | P25_DFSI_E_BIT,
    P25_BT_HEADER_DATA_UNIT                         = 0x05 | P25_DFSI_E_BIT,
    P25_BT_VOICE_HEADER_1                           = 0x06 | P25_DFSI_E_BIT,
    P25_BT_VOICE_HEADER_2                           = 0x07 | P25_DFSI_E_BIT,
    P25_BT_ANALOG_SOS_TIA                           = 0x08 | P25_DFSI_E_BIT, // Currently used in EFJ RTP
    P25_BT_START_OF_STREAM                          = 0x09 | P25_DFSI_E_BIT,
    P25_BT_END_OF_STREAM                            = 0x0A | P25_DFSI_E_BIT,
    P25_BT_PTT_CONTROL_WORD                         = 0x0B | P25_DFSI_E_BIT,
    P25_BT_VOTER_REPORT                             = 0x0C | P25_DFSI_E_BIT,
    P25_BT_VOTER_CONTROL                            = 0x0D | P25_DFSI_E_BIT,
    P25_BT_TX_KEY_ACK                               = 0x0E | P25_DFSI_E_BIT,
    P25_BT_CONSOLE_PTT_CONTROL_WORD                 = 0x0F | P25_DFSI_E_BIT,
    P25_BT_HALF_RATE_ISSI_HEADER_WORD               = 0x21 | P25_DFSI_E_BIT,
    P25_BT_HALF_RATE_VOICE                          = 0x22 | P25_DFSI_E_BIT,
    P25_BT_TALK_SPURT_VOCODER_MODE                  = 0x23 | P25_DFSI_E_BIT,
    P25_BT_SYS_LINK_CAI_VOICE                       = 0x3F | P25_DFSI_E_BIT,
    P25_BT_SYS_LINK_ENCRYPTION_SYNC                 = 0x40 | P25_DFSI_E_BIT,
    P25_BT_MULTI_NET_INFO                           = 0x41 | P25_DFSI_E_BIT,
    P25_BT_AUDIO_VOTER_METRICS                      = 0x42 | P25_DFSI_E_BIT,
    P25_BT_LAUNCH_TIME                              = 0x43 | P25_DFSI_E_BIT,
    P25_BT_TERMINATOR_DATA_UNIT                     = 0x44 | P25_DFSI_E_BIT,
    P25_BT_CONTROL_VOTER_METRICS                    = 0x45 | P25_DFSI_E_BIT,
    P25_BT_ANALOG_SOS_EFJ                           = 0x46 | P25_DFSI_E_BIT, // Defined in EFJ FSI extensions to isolate from volatility in standards
    P25_BT_ANALOG_CALL_INFO                         = 0x47 | P25_DFSI_E_BIT,

    P25_BT_EFJ_MAC_START_OF_STREAM                  = 0x4B | P25_DFSI_E_BIT,
    P25_BT_EFJ_MAC_P2_MAC_INFO                      = 0x4C | P25_DFSI_E_BIT,
    P25_BT_EFJ_MAC_P2_MAC_INFO_WITH_SCRAMBLING      = 0x4D | P25_DFSI_E_BIT,
    P25_BT_EFJ_MAC_P2_MAC_INFO_SYNC                 = 0x4E | P25_DFSI_E_BIT,
    P25_BT_EFJ_MAC_P2_MAC_INFO_SYNC_WITH_SCRAMBLING = 0x4F | P25_DFSI_E_BIT,
    P25_BT_EFJ_MAC_P2_2V                            = 0x50 | P25_DFSI_E_BIT,
    P25_BT_EFJ_MAC_P2_4V                            = 0x51 | P25_DFSI_E_BIT,
    P25_BT_EFJ_MAC_END_OF_STREAM                    = 0x52 | P25_DFSI_E_BIT,
    P25_BT_COUNT_
};

namespace efj
{

inline const char*
c_str(RtpP25BlockType type)
{
    switch (type)
    {
    case P25_BT_ULAW: return "ULAW";
    case P25_BT_AUDIO: return "AUDIO";
    case P25_BT_PACKET_TYPE: return "PACKET_TYPE";
    case P25_BT_LINK_CONTROL: return "LINK_CONTROL";
    case P25_BT_ENCRYPTION_SYNC: return "ENCRYPTION_SYNC";
    case P25_BT_LOW_SPEED_DATA: return "LOW_SPEED_DATA";
    case P25_BT_HEADER_DATA_UNIT: return "HEADER_DATA_UNIT";
    case P25_BT_VOICE_HEADER_1: return "VOICE_HEADER_1";
    case P25_BT_VOICE_HEADER_2: return "VOICE_HEADER_2";
    case P25_BT_ANALOG_SOS_TIA: return "ANALOG_SOS_TIA"; // Currently used in EFJ RTP
    case P25_BT_START_OF_STREAM: return "START_OF_STREAM";
    case P25_BT_END_OF_STREAM: return "END_OF_STREAM";
    case P25_BT_PTT_CONTROL_WORD: return "PTT_CONTROL_WORD";
    case P25_BT_VOTER_REPORT: return "VOTER_REPORT";
    case P25_BT_VOTER_CONTROL: return "VOTER_CONTROL";
    case P25_BT_TX_KEY_ACK: return "TX_KEY_ACK";
    case P25_BT_CONSOLE_PTT_CONTROL_WORD: return "CONSOLE_PTT_CONTROL_WORD";
    case P25_BT_HALF_RATE_ISSI_HEADER_WORD: return "HALF_RATE_ISSI_HEADER_WORD";
    case P25_BT_HALF_RATE_VOICE: return "HALF_RATE_VOICE";
    case P25_BT_TALK_SPURT_VOCODER_MODE: return "TALK_SPURT_VOCODER_MODE";
    case P25_BT_SYS_LINK_CAI_VOICE: return "SYS_LINK_CAI_VOICE";
    case P25_BT_SYS_LINK_ENCRYPTION_SYNC: return "SYS_LINK_ENCRYPTION_SYNC";
    case P25_BT_MULTI_NET_INFO: return "MULTI_NET_INFO";
    case P25_BT_AUDIO_VOTER_METRICS: return "AUDIO_VOTER_METRICS";
    case P25_BT_LAUNCH_TIME: return "LAUNCH_TIME";
    case P25_BT_TERMINATOR_DATA_UNIT: return "TERMINATOR_DATA_UNIT";
    case P25_BT_CONTROL_VOTER_METRICS: return "CONTROL_VOTER_METRICS";
    case P25_BT_ANALOG_SOS_EFJ: return "ANALOG_SOS_EFJ"; // Defined in EFJ FSI extensions to isolate from volatility in standards
    case P25_BT_ANALOG_CALL_INFO: return "ANALOG_CALL_INFO";

    case P25_BT_EFJ_MAC_START_OF_STREAM: return "EFJ_MAC_START_OF_STREAM";
    case P25_BT_EFJ_MAC_P2_MAC_INFO: return "EFJ_MAC_P2_MAC_INFO";
    case P25_BT_EFJ_MAC_P2_MAC_INFO_WITH_SCRAMBLING: return "EFJ_MAC_P2_MAC_INFO_WITH_SCRAMBLING";
    case P25_BT_EFJ_MAC_P2_MAC_INFO_SYNC: return "EFJ_MAC_P2_MAC_INFO_SYNC";
    case P25_BT_EFJ_MAC_P2_MAC_INFO_SYNC_WITH_SCRAMBLING: return "EFJ_MAC_P2_MAC_INFO_SYNC_WITH_SCRAMBLING";
    case P25_BT_EFJ_MAC_P2_2V: return "EFJ_MAC_P2_2V";
    case P25_BT_EFJ_MAC_P2_4V: return "EFJ_MAC_P2_4V";
    case P25_BT_EFJ_MAC_END_OF_STREAM: return "EFJ_MAC_END_OF_STREAM";
    default: return "Unknown";
    }
}

} // namespace efj

// Make a class with a constructor so C++11 automatically generates thread-safe initialization of this.
// We wouldn't need this if C++ supported proper array initializers.
struct RtpBlockSizeTable
{
    u8 sizes[P25_BT_COUNT_];
    RtpBlockSizeTable()
    {
        for (int i = 0; i < P25_BT_COUNT_; i++)
            sizes[i] = -1;

        sizes[P25_BT_ULAW]  = 160,
        sizes[P25_BT_AUDIO] = 14,
        sizes[P25_BT_PACKET_TYPE] = 4;
        sizes[P25_BT_LINK_CONTROL] = 10;
        sizes[P25_BT_ENCRYPTION_SYNC] = 13;
        sizes[P25_BT_LOW_SPEED_DATA] = 3;
        sizes[P25_BT_HEADER_DATA_UNIT] = 18;
        sizes[P25_BT_VOICE_HEADER_1] = 22;
        sizes[P25_BT_VOICE_HEADER_2] = 22;
        sizes[P25_BT_ANALOG_SOS_TIA ] = 3;// Currently used in EFJ RTP
        sizes[P25_BT_START_OF_STREAM] = 3;
        sizes[P25_BT_END_OF_STREAM] = 0;
        sizes[P25_BT_PTT_CONTROL_WORD] = 8;
        sizes[P25_BT_VOTER_REPORT] = 2;
        sizes[P25_BT_VOTER_CONTROL] = 2;
        sizes[P25_BT_TX_KEY_ACK] = 0;
        sizes[15] = 8;
        sizes[P25_BT_CONSOLE_PTT_CONTROL_WORD] = 24;

        sizes[16] = 8;  // Conv RF PTT Ctrl
        sizes[17] = 16; // Conv Console PTT Ctrl
        sizes[18] = 7;  // Conv Console Ctrl
        sizes[19] = 9;  // Conv LC Word
        sizes[20] = 0;  // Reserved
        sizes[21] = 4;  // Station Settings for Console TX
        sizes[22] = 5;  // Station Control Cmd
        sizes[23] = 2;  // Station Control Report Request
        sizes[24] = 12; // SBC Command
        sizes[25] = 4;  // Voter Command
        sizes[26] = 4;  // Voter Update
        sizes[27] = 10; // Connection Request
        sizes[28] = 1;  // Connection Heartbeat
        sizes[29] = 6;  // Control Ack
        sizes[30] = 2;  // Disconnect Request
        sizes[P25_BT_HALF_RATE_ISSI_HEADER_WORD] = 20;
        sizes[P25_BT_HALF_RATE_VOICE] = 33;
        sizes[64] = 3;  // EFJ: Multi-Net SOS
        //sizes[P25_BT_TALK_SPURT_VOCODER_MODE] = 0; // TODO: Figure out what this is!!
        //sizes[P25_BT_SYS_LINK_CAI_VOICE] = 0; // TODO: Figure out what this is!!
        //sizes[P25_BT_SYS_LINK_ENCRYPTION_SYNC] = 0; // TODO: Figure out what this is!!
        sizes[P25_BT_MULTI_NET_INFO] = 10;
        sizes[P25_BT_AUDIO_VOTER_METRICS] = 5;
        sizes[P25_BT_LAUNCH_TIME] = 10; // EFJ: Launch Time
        sizes[P25_BT_TERMINATOR_DATA_UNIT] = 12;
        sizes[P25_BT_CONTROL_VOTER_METRICS] = 7;
        sizes[P25_BT_ANALOG_SOS_EFJ] = 5; // Defined in EFJ FSI extensions to isolate from volatility in standards
        sizes[P25_BT_ANALOG_CALL_INFO] = 5;
        // NOTE: Missing Marker block type, whatever that is.

        sizes[P25_BT_EFJ_MAC_START_OF_STREAM] = 27;
        sizes[P25_BT_EFJ_MAC_P2_MAC_INFO] = 40;
        sizes[P25_BT_EFJ_MAC_P2_MAC_INFO_WITH_SCRAMBLING] = 40;
        sizes[P25_BT_EFJ_MAC_P2_MAC_INFO_SYNC] = 37;
        sizes[P25_BT_EFJ_MAC_P2_MAC_INFO_SYNC_WITH_SCRAMBLING] = 37;
        sizes[P25_BT_EFJ_MAC_P2_2V] = 58;
        sizes[P25_BT_EFJ_MAC_P2_4V] = 58;
        sizes[P25_BT_EFJ_MAC_END_OF_STREAM] = 15;
    }
};

inline umm
p25_get_block_size(int type, bool* ok)
{
    static RtpBlockSizeTable table;
    if (type >= P25_BT_COUNT_ || table.sizes[type] == -1)
    {
        *ok = false;
        return 0;
    }

    *ok = true;
    return table.sizes[type];
}

EFJ_MACRO bool p25_has_signal(const RtpP25Message& msg) { return efj::get_bit(msg, P25_DFSI_HEADER_SIZE, 7); }
EFJ_MACRO void p25_set_has_signal(RtpP25Message& msg, bool value) { return efj::set_bit(msg, P25_DFSI_HEADER_SIZE, 7, value); }
EFJ_MACRO bool p25_is_compressed(const RtpP25Message& msg) { return efj::get_bit(msg, P25_DFSI_HEADER_SIZE, 6); }
EFJ_MACRO void p25_set_is_compressed(RtpP25Message& msg, bool value) { return efj::set_bit(msg, P25_DFSI_HEADER_SIZE, 6, value); }

EFJ_MACRO u8   p25_get_block_count(const RtpP25Message& msg) { return msg[P25_DFSI_HEADER_SIZE] & 0x3F; }
EFJ_MACRO void p25_set_block_count(RtpP25Message& msg, u8 count) { msg[P25_DFSI_HEADER_SIZE] = (msg[P25_DFSI_HEADER_SIZE] & 0xC0) | (count & 0x3F); }
//! Returns the full block type, including the E-bit of the first block.
EFJ_MACRO u8   p25_get_block_type(const RtpP25Message& msg, umm block_offset = 0) { return msg[P25_DFSI_HEADER_SIZE + block_offset + 1]; }
//! Returns the full block type, including the E-bit of the first block.
EFJ_MACRO void p25_set_block_type(RtpP25Message& msg, RtpP25BlockType type, umm block_offset = 0) { msg[P25_DFSI_HEADER_SIZE + block_offset + 1] = type; }

// TODO: const/non-const iterators?
struct RtpP25BlockIterator
{
    const RtpP25Message* msg;
    umm header_blocks_end; // 12 + HdrCnt + 1 (one past last).
    umm header_offset;
    umm block_offset;
};

inline RtpP25BlockIterator
p25_iterate_blocks(const RtpP25Message& msg)
{
    RtpP25BlockIterator result = {};
    result.msg = &msg;
    result.header_blocks_end = 13 + p25_get_block_count(msg);
    result.header_offset = 13;
    result.block_offset = result.header_blocks_end;

    return result;
}

inline bool
p25_is_valid(const RtpP25BlockIterator& it)
{
    return it.header_offset != it.header_blocks_end;
}

//! Advances to the next block in the rtp message, assuming we are dealing with the compressed
//! version of DFSI, in which case if we see an unknown block, we just have to terminate, since
//! we have to know the size of the message to advance past it.
//!
//! TODO: Make version that iterates the verbose version, even though we don't use it.
//!
inline void
p25_advance(RtpP25BlockIterator& it)
{
    u8 bt = (*it.msg)[it.header_offset];
    bool valid_block_type = false;
    umm block_size = p25_get_block_size((RtpP25BlockType)bt, &valid_block_type);

    if (!valid_block_type)
    {
        it.header_offset = it.header_blocks_end;
        return;
    }

    ++it.header_offset;
    it.block_offset += block_size;
}

EFJ_MACRO RtpP25BlockIterator
p25_next(const RtpP25BlockIterator& it)
{
    RtpP25BlockIterator next = it;
    p25_advance(next);
    return next;
}

EFJ_MACRO u8 p25_get_block_type(const RtpP25BlockIterator& it) { return (*it.msg)[it.header_offset]; }
EFJ_MACRO const u8* p25_get_block_data(const RtpP25BlockIterator& it) { return &(*it.msg)[it.block_offset]; }

//! SizeV is the size of the block excluding the RTP and P25 headers.
//! For example, a TX-key-ack (which has no data) would use zero for this.
//!
template <RtpP25BlockType BlockTypeV, umm SizeV>
struct RtpP25Block
{
    static const RtpP25BlockType Type = BlockTypeV;

    u8 data[SizeV];
};

//! TODO: If we ever add an enum for block sizes, we may want to make the size
//! parameter be the full size including the manufacturer header, b/c creating
//! messages would just be
//! struct MyBlock : RtpP25EfjBlock<P25_BT_MY_BLOCK, P25_BT_MY_BLOCK_SIZE> {};
//!
//! SizeV is the block size without the manufacturer-specific header, which
//! is 2 bytes of MFID and length.
template <RtpP25BlockType BlockTypeV, umm SizeV>
struct RtpP25EfjBlock : RtpP25Block<BlockTypeV, SizeV + 2>
{
    enum umm { DataOffset = 2 };
    // Import data into this scope so derived struct can reference data without saying this->data.
    using RtpP25Block<BlockTypeV, SizeV + 2>::data;

    // TODO: default mfid to P25_EFJ_MFID and length to sizeof(*this).

    EFJ_MACRO u8 getMfid() const { return efj::get_byte(data, 0); }
    EFJ_MACRO void setMfid(u8 mfid) { efj::set_byte(data, 0, mfid); }

    EFJ_MACRO u8 getLength() const { return efj::get_byte(data, 1); }
    EFJ_MACRO void setLength(u8 length) { efj::set_byte(data, 1, length); }
};

//! Gets the first block in a message with only one block.
//! FIXME: This only handles compressed messages.
template <typename BlockT> inline BlockT*
p25_get_only_block(const RtpP25Message& msg)
{
    // TODO: message size checking.
    efj_panic_if(msg.size != P25_DFSI_HEADER_SIZE + 2 + sizeof(BlockT)
        || p25_get_block_type(msg) != BlockT::Type);
    return (BlockT*)&msg.data[P25_DFSI_HEADER_SIZE + 2];
}

template <typename BlockT> inline BlockT*
p25_get_block(const RtpP25Message& msg, umm blockOffset = 0)
{
    auto it = p25_iterate_blocks(msg);
    for (umm i = 0; i < blockOffset; i++)
        p25_advance(it);

    efj_panic_if(p25_get_block_type(it) != BlockT::Type);
    return (BlockT*)p25_get_block_data(it);
}

template <typename BlockT> inline BlockT*
p25_get_block_or_null(const RtpP25Message& msg)
{
    auto it = p25_iterate_blocks(msg);
    if (p25_get_block_type(it) != BlockT::Type)
        return nullptr;

    // TODO: message size checking.
    return (BlockT*)p25_get_block_data(it);
}

template <typename BlockT> inline BlockT*
p25_get_block_or_null(const RtpP25BlockIterator& it)
{
    if (p25_get_block_type(it) != BlockT::Type)
        return nullptr;

    // TODO: message size checking.
    return (BlockT*)p25_get_block_data(it);
}

template <typename BlockT> inline BlockT*
p25_get_block(const RtpP25BlockIterator& it)
{
    // TODO: message size checking.
    efj_panic_if(p25_get_block_type(it) != BlockT::Type);
    return (BlockT*)p25_get_block_data(it);
}

