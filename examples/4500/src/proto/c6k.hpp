#pragma once
#include "QFormat.hpp"

// WARNING: Since we often want to put C6K payloads in unions, we've left the data fields in them
// uninitialized for convenience. Don't forget to clear them out with a memset or a Type t = {}.
// That being said. Factory functions in here that return messages make sure messages are cleared out.


struct C6kMessage : efj::message
{
    using message::message;
    efj::optional<efj::ntp_time> launchTime;
};

namespace efj
{
EFJ_DEFINE_MESSAGE(C6kMessage, "1.0");
} // namespace efj

enum C6kMessageType
{
    C6K_START_PROJECT_25_PHASE_2             = 1,
    C6K_PROJECT_25_PHASE_2_STARTING          = 2,
    C6K_TRANSMIT_SLOT                        = 3,
    C6K_TRANSMIT_SACCH_WITH_SCRAMBLING       = 4,
    C6K_TRANSMIT_SACCH                       = 5,
    C6K_TRANSMIT_FACCH_WITH_SCRAMBLING       = 6,
    C6K_TRANSMIT_FACCH                       = 7,
    C6K_TRANSMIT_4V                          = 8,
    C6K_TRANSMIT_2V                          = 9,
    C6K_NO_BURST                             = 10, // 0x0a
    C6K_RECEIVE_SACCH_WITH_SCRAMBLING        = 11, // 0x0b
    C6K_RECEIVE_SACCH                        = 12, // 0x0c
    C6K_RECEIVE_FACCH_WITH_SCRAMBLING        = 13, // 0x0d
    C6K_RECEIVE_FACCH                        = 14, // 0x0e
    C6K_RECEIVE_4V                           = 15, // 0x0f
    C6K_RECEIVE_2V                           = 16, // 0x10
    C6K_STOP_PROJECT_25_PHASE_2              = 17, // 0x11
    C6K_PROJECT_25_PHASE_2_STOPPED           = 18, // 0x12
    C6K_ERROR                                = 19, // 0x13
    C6K_C6000_STARTED                        = 20, // 0x14
    C6K_C6000_RESET                          = 21, // 0x15
    C6K_INITIALIZE_HR_FEC                    = 22, // 0x16
    C6K_HR_FEC_INITIALIZED                   = 23, // 0x17
    C6K_HR_FEC_ENCODE                        = 24, // 0x18
    C6K_HR_FEC_ENCODED                       = 25, // 0x19
    C6K_HR_FEC_DECODE                        = 26, // 0x1a
    C6K_HR_FEC_DECODED                       = 27, // 0x1b
    C6K_SHUTDOWN                             = 28, // 0x1c
    C6K_C6000_SHUTDOWN                       = 29, // 0x1d
    C6K_SLOT_ALIGNMENT_ERROR                 = 30, // 0x1e
    C6K_START_PROJECT_25_PHASE_1_OUTBOUND    = 31, // 0x1f
    C6K_PROJECT_25_PHASE_1_OUTBOUND_STARTING = 32, // 0x20
    C6K_TRANSMIT_REQUEST                     = 33, // 0x21
    C6K_TRANSMIT_DUID                        = 34, // 0x22
    C6K_TRANSMIT_TSBK_DATA                   = 35, // 0x23
    C6K_TRANSMIT_NULL                        = 36, // 0x24
    C6K_TRANSMIT_UNCONFIRMED_DATA            = 37, // 0x25
    C6K_TRANSMIT_STATUS                      = 38, // 0x26
    C6K_STOP_PROJECT_25_PHASE_1_OUTBOUND     = 39, // 0x27
    C6K_PROJECT_25_PHASE_1_OUTBOUND_STOPPED  = 40, // 0x28
    C6K_START_PROJECT_25_PHASE_1_INBOUND     = 41, // 0x29
    C6K_PROJECT_25_PHASE_1_INBOUND_STARTING  = 42, // 0x2a
    C6K_RECEIVE_PHASE_1_UPDATE               = 43, // 0x2b
    C6K_RECEIVE_DUID                         = 44, // 0x2c
    C6K_RECEIVE_TSBK                         = 45, // 0x2d
    C6K_RECEIVE_UNCONFIRMED                  = 46, // 0x2e
    C6K_STOP_PROJECT_25_PHASE_1_INBOUND      = 47, // 0x2f
    C6K_PROJECT_25_PHASE_1_INBOUND_STOPPED   = 48, // 0x30
    C6K_REPORT_RSSI                          = 49, // 0x31
    C6K_RSSI_MEASUREMENT                     = 50, // 0x32
    C6K_C6000_RESET_COMPLETE                 = 51, // 0x33
    C6K_CONFIGURE_HARDWARE                   = 52, // 0x34
    C6K_HARDWARE_CONFIGURED                  = 53, // 0x35
    C6K_TUNE_RECEIVER                        = 54, // 0x36
    C6K_RECEIVER_TUNED                       = 55, // 0x37
    C6K_TRANSMIT_CONFIRMED_DATA              = 56, // 0x38
    C6K_TRANSMIT_TERM_WITH_LC                = 57, // 0x39
    C6K_TRANSMIT_HEADER_PART_1               = 58, // 0x3a
    C6K_TRANSMIT_HEADER_PART_2               = 59, // 0x3b
    C6K_TRANSMIT_LDU_VOICE                   = 60, // 0x3c
    C6K_TRANSMIT_LDU_VOICE_WITH_LC_ES        = 61, // 0x3d
    C6K_TRANSMIT_LDU_VOICE_WITH_LSD          = 62, // 0x3e
    C6K_RECEIVE_CONFIRMED_DATA               = 63, // 0x3f
    C6K_RECEIVE_TERM_WITH_LC                 = 64, // 0x40
    C6K_RECEIVE_HEADER_PART_1                = 65, // 0x41
    C6K_RECEIVE_HEADER_PART_2                = 66, // 0x42
    C6K_RECEIVE_LDU_VOICE                    = 67, // 0x43
    C6K_RECEIVE_LDU_VOICE_WITH_LC_ES         = 68, // 0x44
    C6K_RECEIVE_LDU_VOICE_WITH_LSD           = 69, // 0x45
    C6K_INVALID_MSG                          = 70, // 0x46
    C6K_IEMI_SYNCLOC                         = 71, // 0x47
    C6K_RECEIVE_TIME_UPDATE                  = 72, // 0x48
    C6K_START_ANALOG_INBOUND                 = 73, // 0x49
    C6K_ANALOG_INBOUND_STARTING              = 74, // 0x4a
    C6K_STOP_ANALOG_INBOUND                  = 75, // 0x4b
    C6K_ANALOG_INBOUND_STOPPED               = 76, // 0x4c
    C6K_RECEIVE_ANALOG_RX_INFO               = 77, // 0x4d
    C6K_START_ANALOG_OUTBOUND                = 78, // 0x4e
    C6K_ANALOG_OUTBOUND_STARTING             = 79, // 0x4f
    C6K_STOP_ANALOG_OUTBOUND                 = 80, // 0x50
    C6K_ANALOG_OUTBOUND_STOPPED              = 81, // 0x51
    C6K_TRANSMIT_AUDIO                       = 82, // 0x52
    C6K_TRANSMIT_PREAMBLE                    = 83, // 0x53
    C6K_OUTBOUND_SERIAL_PORT_STARTED         = 84, // 0x54
    C6K_MODULATION_COMPLETE                  = 85, // 0x55
    C6K_CONFIGURE_SOFTWARE                   = 86, // 0x56
    C6K_SOFTWARE_CONFIGURED                  = 87, // 0x57
    C6K_TRANSMIT_MULTI                       = 88, // 0x58
    C6K_SMART_EXCITER_UNDERFLOW_WARNING      = 89, // 0x59
};

inline C6kMessage
c6k_make_message(C6kMessageType type, umm full_size)
{
    auto msg = efj_allocate_message(C6kMessage, full_size);
    efj::write_u32(msg, 0, type);
    for (umm i = sizeof(u32); i < full_size; i++)
        msg.data[i] = 0;

    return msg;
}

template <typename BlockT> EFJ_MACRO C6kMessage
c6k_make_message()
{
    return c6k_make_message(BlockT::Type, sizeof(BlockT));
}

template <typename BlockT> EFJ_MACRO BlockT*
c6k_make_block(BlockT* block)
{
    memset(block, 0, sizeof(BlockT));
    efj::write_u32(block, 0, BlockT::Type);
    return block;
}

template <typename BlockT> EFJ_MACRO BlockT*
c6k_make_block()
{
    return (BlockT*)c6k_make_message(BlockT::Type, sizeof(BlockT)).data;
}

EFJ_MACRO C6kMessageType
c6k_get_type(const C6kMessage& msg)
{
    return (C6kMessageType)efj::read_u32(msg, 0);
}

template <typename BlockT> EFJ_MACRO const BlockT*
c6k_get_block(const C6kMessage& msg)
{
    efj_panic_if(msg.size != sizeof(BlockT));
    return (BlockT*)msg.data;
}

template <typename BlockT> EFJ_MACRO BlockT*
c6k_get_block(C6kMessage& msg)
{
    efj_panic_if(msg.size != sizeof(BlockT));
    return (BlockT*)msg.data;
}

// IMPORTANT: Do _not_ add virtual functions to this or any other block types.
// When you have a *Block*, you have an array of u8's in memory.
// The size here is the total size of the message, not the data field length.
template <C6kMessageType TypeV, umm SizeV>
struct C6kBlock
{
    static const C6kMessageType Type = TypeV;

    u8 data[SizeV];

    EFJ_MACRO void setType()
    {
        efj::write_u32(data, 0, TypeV);
    }
};

struct C6kBlockInvalidMessage : C6kBlock<C6K_INVALID_MSG, 112>
{
    C6kMessageType getMessageType() const { return (C6kMessageType)efj::read_u32(&data[8]); }
    const char* getComment() const { return (char*)&data[12]; }
};


enum C6kInterFreq
{
    C6K_IF_90_MHZ,
    C6K_IF_55_MHZ,
    C6K_IF_COUNT_,
};

struct C6kBlockConfigureHardware : C6kBlock<C6K_CONFIGURE_HARDWARE, 32>
{
    // The TCXO field isn't used, anymore.
    EFJ_MACRO void setRx1InterFreq(C6kInterFreq value) { data[10] = value; }
    EFJ_MACRO void setRx2InterFreq(C6kInterFreq value) { data[12] = value; }
    EFJ_MACRO void setTxRxDelay(u16 value) { efj::write_u16(data, 14, value); }
};

inline C6kMessage
c6k_make_configure_hardware(C6kInterFreq rx1_value, C6kInterFreq rx2_value, u16 tx_rx_delay)
{
    auto msg = c6k_make_message(C6K_CONFIGURE_HARDWARE, sizeof(C6kBlockConfigureHardware));
    auto* block = c6k_get_block<C6kBlockConfigureHardware>(msg);
    block->setRx1InterFreq(rx1_value);
    block->setRx1InterFreq(rx2_value);
    block->setTxRxDelay(tx_rx_delay);

    return msg;
}

struct C6kBlockConfigureSoftware : C6kBlock<C6K_CONFIGURE_SOFTWARE, 9>
{
    EFJ_MACRO void setExciterBufferCount(u8 count) { data[8] = count; }
};

inline C6kMessage
c6k_make_configure_software(u8 exciter_buffer_count)
{
    auto msg = c6k_make_message(C6K_CONFIGURE_SOFTWARE, sizeof(C6kBlockConfigureSoftware));
    auto* block = c6k_get_block<C6kBlockConfigureSoftware>(msg);
    block->setExciterBufferCount(exciter_buffer_count);

    return msg;
}

enum C6kRxPolarity
{
    C6K_RX_POLARITY_INVERTED,
    C6K_RX_POLARITY_NOT_INVERTED,
    C6K_RX_POLARITY_COUNT_,
};

enum C6kOspMode
{
    C6K_OSP_MODE_SINGLE, // Single TSBK.
    C6K_OSP_MODE_MULTI,  // Multiple TSBKs.
    C6K_OSP_MODE_COUNT_,
};

struct C6kBlockStartPhase1Inbound : C6kBlock<C6K_START_PROJECT_25_PHASE_1_INBOUND, 16>
{
    EFJ_MACRO void setPolarity(C6kRxPolarity value) { data[8] = value; }
    EFJ_MACRO void setOspMode(C6kOspMode value) { data[9] = value; }
};

inline C6kMessage
c6k_make_start_phase1_inbound(C6kRxPolarity polarity, C6kOspMode osp_mode)
{
    auto msg = c6k_make_message(C6K_START_PROJECT_25_PHASE_1_INBOUND, sizeof(C6kBlockStartPhase1Inbound));
    auto* block = c6k_get_block<C6kBlockStartPhase1Inbound>(msg);
    block->setPolarity(polarity);
    block->setOspMode(osp_mode);

    return msg;
}

enum C6kModulation
{
    C6K_MOD_CQPSK,
    C6K_MOD_C4FM,
    C6K_MOD_COUNT_,
};

struct C6kBlockStartPhase1Outbound : C6kBlock<C6K_START_PROJECT_25_PHASE_1_OUTBOUND, 28>
{
    EFJ_MACRO void setLaunchTime(QFormat32_32 time) { efj::write_u64(data, 8, time.qValue); }
    EFJ_MACRO void setLaunchTimeOffset(QFormat31_32 offset) { efj::write_u64(data, 16, offset.qValue); }
    EFJ_MACRO void setUseTimeFields(bool use) { data[24] = use; }
    EFJ_MACRO void setSlotSize(u8 value) { data[25] = value; }
    EFJ_MACRO void setModulation(C6kModulation value) { data[26] = value; }
    //! This chooses when status symbols are sent out. We need to start on the same micro slot between
    //! simulcast transmisions, which is why we need some agreed upon starting point for slots. At EFJ, we
    //! say slots start at midnight, so to set this value, compute the microslot offset from the origin
    //! (midnight) for the launch time, then set this value to <offset> % <slot size>, where the slot size
    //! is the slot size set in the message.
    EFJ_MACRO void setStartingMicroSlot(u8 offset) { data[27] = offset; }
};

inline C6kMessage
c6k_make_start_phase1_outbound(QFormat32_32 launch_time, QFormat31_32 time_offset, C6kModulation mod,
    u8 slot_size, u8 starting_micro_slot_offset)
{
    auto msg = c6k_make_message(C6K_START_PROJECT_25_PHASE_1_OUTBOUND, sizeof(C6kBlockStartPhase1Outbound));
    auto* block = c6k_get_block<C6kBlockStartPhase1Outbound>(msg);
    block->setLaunchTime(launch_time);
    block->setLaunchTimeOffset(time_offset);
    block->setUseTimeFields(true);
    block->setModulation(mod);
    block->setStartingMicroSlot(starting_micro_slot_offset);

    return msg;
}

inline C6kMessage
c6k_make_start_phase1_outbound(C6kModulation mod, u8 slot_size, u8 starting_micro_slot_offset)
{
    auto msg = c6k_make_message(C6K_START_PROJECT_25_PHASE_1_OUTBOUND, sizeof(C6kBlockStartPhase1Outbound));
    auto* block = c6k_get_block<C6kBlockStartPhase1Outbound>(msg);
    block->setUseTimeFields(false);
    block->setModulation(mod);
    block->setSlotSize(slot_size);
    block->setStartingMicroSlot(starting_micro_slot_offset);

    return msg;
}

struct C6kBlockPhase1OutboundStarting : C6kBlock<C6K_PROJECT_25_PHASE_1_OUTBOUND_STARTING, 10>
{
    //! 16-bit fractions of a second.
    EFJ_MACRO u16 getWaitFraction() const { return efj::read_u16(data, 8); }
    EFJ_MACRO void setWaitFraction(u16 value) { return efj::write_u16(data, 8, value); }
};

inline C6kMessage
c6k_make_phase1_outbound_starting(u16 waitFraction = 0)
{
    auto msg = c6k_make_message(C6K_PROJECT_25_PHASE_1_OUTBOUND_STARTING, sizeof(C6kBlockPhase1OutboundStarting));
    auto* block = c6k_get_block<C6kBlockPhase1OutboundStarting>(msg);
    block->setWaitFraction(waitFraction);

    return msg;
}

struct C6kBlockTransmitRequest : C6kBlock<C6K_TRANSMIT_REQUEST, 8> {};
struct C6kBlockStopPhase1Inbound : C6kBlock<C6K_START_PROJECT_25_PHASE_1_INBOUND, 8> {};
struct C6kBlockPhase1InboundStarting : C6kBlock<C6K_PROJECT_25_PHASE_1_INBOUND_STARTING, 8> {};
struct C6kBlockPhase1InboundStopped : C6kBlock<C6K_PROJECT_25_PHASE_1_INBOUND_STOPPED, 8> {};

struct C6kBlockSerialPortStarted : C6kBlock<C6K_OUTBOUND_SERIAL_PORT_STARTED, 8> {};
struct C6kBlockUnderflowWarning : C6kBlock<C6K_SMART_EXCITER_UNDERFLOW_WARNING, 8> {};
struct C6kBlockStopPhase1Outbound : C6kBlock<C6K_STOP_PROJECT_25_PHASE_1_OUTBOUND, 8> {};
struct C6kBlockModulationComplete : C6kBlock<C6K_MODULATION_COMPLETE, 8> {};
struct C6kBlockPhase1OutboundStopped : C6kBlock<C6K_PROJECT_25_PHASE_1_OUTBOUND_STOPPED, 8> {};

inline C6kMessage c6k_make_transmit_request() { return c6k_make_message<C6kBlockTransmitRequest>(); }
inline C6kMessage c6k_make_stop_phase1_inbound() { return c6k_make_message<C6kBlockStopPhase1Inbound>(); }
inline C6kMessage c6k_make_stop_phase1_outbound() { return c6k_make_message<C6kBlockStopPhase1Outbound>(); }

enum C6kTxStatusSymbol
{
    C6K_TX_STATUS_TALK_AROUND,
    C6K_TX_STATUS_BUSY,
    C6K_TX_STATUS_UNKNOWN,
    C6K_TX_STATUS_IDLE,
    C6K_TX_STATUS_AUTO,
    C6K_TX_STATUS_COUNT_,
};

struct C6kBlockTxStatus : C6kBlock<C6K_TRANSMIT_STATUS, 9>
{
    EFJ_MACRO void setStatus(C6kTxStatusSymbol status) { data[8] = status; }
};

inline C6kMessage
c6k_make_tx_status(C6kTxStatusSymbol status)
{
    auto* block = c6k_make_block<C6kBlockTxStatus>();
    block->setStatus(status);

    return { block, sizeof(*block) };
}

struct C6kBlockTimeUpdate : C6kBlock<C6K_RECEIVE_TIME_UPDATE, 24>
{
    u32 getSecond() const { return efj::read_u32(data, 8); };
    //! Get the cycle count on the SmartExciter I2s bus when PPS occurred.
    //! 2,880,000 i2s cycles occur every second.
    u32 getI2sCycleCount() const { return efj::read_u32(data, 12); };
};

struct C6kBlockReceivePhase1Update : C6kBlock<C6K_RECEIVE_PHASE_1_UPDATE, 14>
{
};

struct C6kBlockReceiveDuid : C6kBlock<C6K_RECEIVE_DUID, 22>
{
    EFJ_MACRO u8 getSyncErrors() const { return data[9]; }
    EFJ_MACRO u8 getNidErrors() const { return data[11]; }
    EFJ_MACRO u16 getRssi() const { return efj::read_u16(data, 16); }
    EFJ_MACRO u16 getNid() const { return efj::read_u16(data, 20); }
    EFJ_MACRO u16 getNac() const { return (efj::read_u16(data, 20) & 0xFFF0) >> 4; }
    EFJ_MACRO u8 getDuid() const { return efj::read_u16(data, 20) & 0xF; }
};

struct C6kBlockTransmitDuid : C6kBlock<C6K_TRANSMIT_DUID, 10>
{
    EFJ_MACRO void setNid(u16 nid) { efj::write_u16(data, 8, nid); }
    EFJ_MACRO void setNid(u16 nac, u8 duid)
    {
        efj::write_u16(data, 8, ((nac & 0xFFF) << 4) | (duid & 0xF));
    }

    EFJ_MACRO void setNac(u16 nac)
    {
        u16 nid = efj::read_u16(data, 8);
        efj::write_u16(data, 8, (nid & 0xF) | ((nac & 0xFFF) << 4));
    }

    EFJ_MACRO void setDuid(u8 duid)
    {
        u16 nid = efj::read_u16(data, 8);
        // We assume the nid in the message is valid.
        efj::write_u16(data, 8, nid | (duid & 0xF));
    }

    EFJ_MACRO u16 getNac() const { return (efj::read_u16(data, 8) & 0xFFF0) >> 4; }
    EFJ_MACRO u8 getDuid() const { return efj::read_u16(data, 8) & 0xF; }
};

EFJ_MACRO void
c6k_make_transmit_duid(C6kBlockTransmitDuid* block, u16 nac, u8 duid)
{
    c6k_make_block(block);
    block->setNid(nac, duid);
}

inline C6kMessage
c6k_make_transmit_duid(u16 nac, u8 duid)
{
    auto msg = c6k_make_message<C6kBlockTransmitDuid>();
    auto* block = c6k_get_block<C6kBlockTransmitDuid>(msg);
    block->setNid(nac, duid);

    return msg;
}

struct C6kBlockReceiveHeader1 : C6kBlock<C6K_RECEIVE_HEADER_PART_1, 44>
{
    using Buffer = u8[18];

    EFJ_MACRO const Buffer& getHeader() const { return (Buffer&)data[8]; }
    EFJ_MACRO const Buffer& getGolayErrors() const { return (Buffer&)data[26]; }
};

struct C6kBlockTransmitHeader1 : C6kBlock<C6K_TRANSMIT_HEADER_PART_1, 44>
{
    EFJ_MACRO void setHeader(const void* src) { memcpy(&data[8], src, 18); }
};

EFJ_MACRO void
c6k_make_transmit_hdu1(C6kBlockTransmitHeader1* block, const void* data)
{
    c6k_make_block(block);
    block->setHeader(data);
}

inline C6kMessage
c6k_make_transmit_hdu1(const void* data)
{
    auto* block = c6k_make_block<C6kBlockTransmitHeader1>();
    block->setHeader(data);

    return { block, sizeof(*block) };
}

struct C6kBlockReceiveHeader2 : C6kBlock<C6K_RECEIVE_HEADER_PART_2, 44>
{
    using Buffer = u8[18];

    EFJ_MACRO const Buffer& getHeader() const { return (Buffer&)data[8]; }
    EFJ_MACRO const Buffer& getGolayErrors() const { return (Buffer&)data[26]; }
};

struct C6kBlockTransmitHeader2 : C6kBlock<C6K_TRANSMIT_HEADER_PART_2, 44>
{
    EFJ_MACRO void setHeader(const void* src) { memcpy(&data[8], src, 18); }
};

EFJ_MACRO void
c6k_make_transmit_hdu2(C6kBlockTransmitHeader2* block, const void* data)
{
    c6k_make_block(block);
    block->setHeader(data);
}

inline C6kMessage
c6k_make_transmit_hdu2(const void* data)
{
    auto* block = c6k_make_block<C6kBlockTransmitHeader2>();
    block->setHeader(data);

    return { block, sizeof(*block) };
}

struct C6kBlockReceiveLduVoice : C6kBlock<C6K_RECEIVE_LDU_VOICE, 25>
{
    using VoiceBuffer = u8[11];

    EFJ_MACRO const VoiceBuffer& getVoice() const { return (VoiceBuffer&)data[8]; }
    EFJ_MACRO u8 getE1() const { return data[19]; }
    EFJ_MACRO u8 getE4() const { return data[20]; }
    EFJ_MACRO u8 getEt() const { return data[21]; }
    EFJ_MACRO u8 getEr() const { return data[22]; }
    EFJ_MACRO u8 getM() const { return data[23]; }
    EFJ_MACRO u8 getL() const { return data[24]; }
};

struct C6kBlockTransmitLduVoice : C6kBlock<C6K_TRANSMIT_LDU_VOICE, 25>
{
    EFJ_MACRO void setVoice(const void* src) { memcpy(&data[8], src, 11); }
};

EFJ_MACRO void
c6k_make_transmit_voice(C6kBlockTransmitLduVoice* block, const void* data)
{
    c6k_make_block(block);
    block->setVoice(data);
}

inline C6kMessage
c6k_make_transmit_voice(const void* data)
{
    auto* block = c6k_make_block<C6kBlockTransmitLduVoice>();
    block->setVoice(data);

    return { block, sizeof(*block) };
}

struct C6kBlockReceiveLduVoiceLcEs : C6kBlock<C6K_RECEIVE_LDU_VOICE_WITH_LC_ES, 32>
{
    using VoiceBuffer  = u8[11];
    using LcEsBuffer   = u8[3];
    using HamingBuffer = u8[4];

    EFJ_MACRO const VoiceBuffer& getVoice() const { return (VoiceBuffer&)data[8]; }
    EFJ_MACRO u8 getE1() const { return data[19]; }
    EFJ_MACRO u8 getE4() const { return data[20]; }
    EFJ_MACRO u8 getEt() const { return data[21]; }
    EFJ_MACRO u8 getEr() const { return data[22]; }
    EFJ_MACRO u8 getM() const { return data[23]; }
    EFJ_MACRO u8 getL() const { return data[24]; }
    EFJ_MACRO const LcEsBuffer& getLcEs() const { return (LcEsBuffer&)data[25]; }
    EFJ_MACRO const HamingBuffer& getHamingErrors() const { return (HamingBuffer&)data[28]; }
};

struct C6kBlockTransmitLduVoiceLcEs : C6kBlock<C6K_TRANSMIT_LDU_VOICE_WITH_LC_ES, 32>
{
    EFJ_MACRO void setVoice(const void* src) { memcpy(&data[8], src, 11); }
    EFJ_MACRO void setLcEs(const void* src) { memcpy(&data[25], src, 3); }
};

inline C6kMessage
c6k_make_transmit_voice_lc_es(C6kBlockTransmitLduVoiceLcEs* block, const void* voice, const void* control)
{
    c6k_make_block(block);
    block->setVoice(voice);
    block->setLcEs(control);

    return { block, sizeof(*block) };
}

inline C6kMessage
c6k_make_transmit_voice_lc_es(const void* voice, const void* control)
{
    auto* block = c6k_make_block<C6kBlockTransmitLduVoiceLcEs>();
    block->setVoice(voice);
    block->setLcEs(control);

    return { block, sizeof(*block) };
}

struct C6kBlockReceiveLduVoiceLsd : C6kBlock<C6K_RECEIVE_LDU_VOICE_WITH_LSD, 29>
{
    using VoiceBuffer    = u8[11];
    using LsdBuffer      = u8[2];
    using LsdErrorBuffer = u8[2];

    EFJ_MACRO const VoiceBuffer& getVoice() const { return (VoiceBuffer&)data[8]; }
    EFJ_MACRO u8 getE1() const { return data[19]; }
    EFJ_MACRO u8 getE4() const { return data[20]; }
    EFJ_MACRO u8 getEt() const { return data[21]; }
    EFJ_MACRO u8 getEr() const { return data[22]; }
    EFJ_MACRO u8 getM() const { return data[23]; }
    EFJ_MACRO u8 getL() const { return data[24]; }
    EFJ_MACRO u8 getLsd0() const { return efj::get_byte(data, 25); }
    EFJ_MACRO u8 getLsd1() const { return efj::get_byte(data, 26); }
    EFJ_MACRO u8 getLsdError0() const { return efj::get_byte(data, 27); }
    EFJ_MACRO u8 getLsdError1() const { return efj::get_byte(data, 28); }
    EFJ_MACRO const LsdBuffer& getLsd() const { return (LsdBuffer&)data[25]; }
    EFJ_MACRO const LsdErrorBuffer& getLsdErrors() const { return (LsdErrorBuffer&)data[27]; }
};

struct C6kBlockTransmitLduVoiceLsd : C6kBlock<C6K_TRANSMIT_LDU_VOICE_WITH_LSD, 29>
{
    EFJ_MACRO void setType() { efj::write_u32(data, C6K_TRANSMIT_LDU_VOICE_WITH_LSD); }
    EFJ_MACRO void setVoice(const void* src) { memcpy(&data[8], src, 11); }
    EFJ_MACRO void setLsd(u8 lsd0, u8 lsd1)
    {
        efj::set_byte(data, 25, lsd0);
        efj::set_byte(data, 26, lsd1);
    }
};

inline void
c6k_make_transmit_voice_lsd(C6kBlockTransmitLduVoiceLsd* block, const void* voice, const void* lsd)
{
    c6k_make_block(block);
    block->setVoice(voice);
    block->setLsd(((u8*)lsd)[0], ((u8*)lsd)[1]);
}

inline C6kMessage
c6k_make_transmit_voice_lsd(const void* voice, const void* lsd)
{
    auto* block = c6k_make_block<C6kBlockTransmitLduVoiceLsd>();
    block->setVoice(voice);
    block->setLsd(((u8*)lsd)[0], ((u8*)lsd)[1]);

    return { block, sizeof(*block) };
}

struct C6kBlockTransmitTermWithLc : C6kBlock<C6K_TRANSMIT_TERM_WITH_LC, 17>
{
    EFJ_MACRO void setFormat(u8 format) { efj::set_byte(data, 8, format); }
    EFJ_MACRO void setLc(const void* lc) { memcpy(&data[9], lc, 8); }
};

inline void
c6k_make_transmit_term_with_lc(C6kBlockTransmitTermWithLc* block, u8 format, const void* lc)
{
    c6k_make_block(block);
    block->setFormat(format);
    block->setLc(lc);
}

inline C6kMessage
c6k_make_transmit_term_with_lc(u8 format, const void* lc)
{
    auto* block = c6k_make_block<C6kBlockTransmitTermWithLc>();
    block->setFormat(format);
    block->setLc(lc);

    return { block, sizeof(*block) };
}

struct C6kBlockTransmitNull : C6kBlock<C6K_TRANSMIT_NULL, 8> {};

EFJ_MACRO void
c6k_make_transmit_null(C6kBlockTransmitNull* block)
{
    c6k_make_block(block);
}

EFJ_MACRO C6kMessage
c6k_make_transmit_null()
{
    return c6k_make_message<C6kBlockTransmitNull>();
}

struct C6kBlockTransmitMulti : C6kBlock<C6K_TRANSMIT_MULTI, 968>
{
    template <typename BlockT> EFJ_MACRO BlockT* add()
    {
        static_assert(BlockT::Type == C6K_TRANSMIT_DUID ||
                      BlockT::Type == C6K_TRANSMIT_HEADER_PART_1 ||
                      BlockT::Type == C6K_TRANSMIT_HEADER_PART_2 ||
                      BlockT::Type == C6K_TRANSMIT_NULL ||
                      BlockT::Type == C6K_TRANSMIT_LDU_VOICE ||
                      BlockT::Type == C6K_TRANSMIT_LDU_VOICE_WITH_LC_ES ||
                      BlockT::Type == C6K_TRANSMIT_LDU_VOICE_WITH_LSD ||
                      BlockT::Type == C6K_TRANSMIT_PREAMBLE ||
                      BlockT::Type == C6K_TRANSMIT_TSBK_DATA ||
                      BlockT::Type == C6K_TRANSMIT_UNCONFIRMED_DATA ||
                      BlockT::Type == C6K_TRANSMIT_CONFIRMED_DATA ||
                      BlockT::Type == C6K_TRANSMIT_TERM_WITH_LC,
                      "invalid tx multi message");

        u8 size = data[8];
        efj_assert(size < 17);

        data[8] = size + 1;
        return (BlockT*)(data + 16 + 56 * size);
    }

    EFJ_MACRO C6kMessageType getType(u8 i) const
    {
        efj_assert(i < 17);
        return (C6kMessageType)efj::read_u32(data, 16 + 56 * i);
    }

    EFJ_MACRO void clear()
    {
        data[8] = 0;
    }

    EFJ_MACRO u8 getMessageCount() const
    {
        return data[8];
    }

    //! Returns the size of the message not including space for empty
    //! message slots. Josh Johnson (DSP team) said this was okay.
    EFJ_MACRO C6kMessage toMinimalMessage() const
    {
        umm size = data[8];
        efj_assert(size <= 17);
        return { data, 16 + 56 * size };
    }
};

inline void
c6k_make_tx_multi(C6kBlockTransmitMulti* block)
{
    block->setType();
    block->clear();
}

EFJ_MACRO C6kMessage
c6k_block_to_message(const C6kBlockTransmitMulti& block)
{
    return block.toMinimalMessage();
}

template <C6kMessageType TypeV, umm SizeV> EFJ_MACRO C6kMessage
c6k_block_to_message(const C6kBlock<TypeV, SizeV>& block)
{
    C6kMessage result = { block.data, sizeof(block.data) };
    return result;
}

namespace efj
{

template <> inline const char*
c_str<>(C6kMessageType type)
{
    switch(type)
    {
    case C6K_START_PROJECT_25_PHASE_2: return "C6K_START_PROJECT_25_PHASE_2";
    case C6K_PROJECT_25_PHASE_2_STARTING: return "C6K_PROJECT_25_PHASE_2_STARTING";
    case C6K_TRANSMIT_SLOT: return "C6K_TRANSMIT_SLOT";
    case C6K_TRANSMIT_SACCH_WITH_SCRAMBLING: return "C6K_TRANSMIT_SACCH_WITH_SCRAMBLING";
    case C6K_TRANSMIT_SACCH: return "C6K_TRANSMIT_SACCH";
    case C6K_TRANSMIT_FACCH_WITH_SCRAMBLING: return "C6K_TRANSMIT_FACCH_WITH_SCRAMBLING";
    case C6K_TRANSMIT_FACCH: return "C6K_TRANSMIT_FACCH";
    case C6K_TRANSMIT_4V: return "C6K_TRANSMIT_4V";
    case C6K_TRANSMIT_2V: return "C6K_TRANSMIT_2V";
    case C6K_NO_BURST: return "C6K_NO_BURST";
    case C6K_RECEIVE_SACCH_WITH_SCRAMBLING: return "C6K_RECEIVE_SACCH_WITH_SCRAMBLING";
    case C6K_RECEIVE_SACCH: return "C6K_RECEIVE_SACCH";
    case C6K_RECEIVE_FACCH_WITH_SCRAMBLING: return "C6K_RECEIVE_FACCH_WITH_SCRAMBLING";
    case C6K_RECEIVE_FACCH: return "C6K_RECEIVE_FACCH";
    case C6K_RECEIVE_4V: return "C6K_RECEIVE_4V";
    case C6K_RECEIVE_2V: return "C6K_RECEIVE_2V";
    case C6K_STOP_PROJECT_25_PHASE_2: return "C6K_STOP_PROJECT_25_PHASE_2";
    case C6K_PROJECT_25_PHASE_2_STOPPED: return "C6K_PROJECT_25_PHASE_2_STOPPED";
    case C6K_ERROR: return "C6K_ERROR";
    case C6K_C6000_STARTED: return "C6K_C6000_STARTED";
    case C6K_C6000_RESET: return "C6K_C6000_RESET";
    case C6K_INITIALIZE_HR_FEC: return "C6K_INITIALIZE_HR_FEC";
    case C6K_HR_FEC_INITIALIZED: return "C6K_HR_FEC_INITIALIZED";
    case C6K_HR_FEC_ENCODE: return "C6K_HR_FEC_ENCODE";
    case C6K_HR_FEC_ENCODED: return "C6K_HR_FEC_ENCODED";
    case C6K_HR_FEC_DECODE: return "C6K_HR_FEC_DECODE";
    case C6K_HR_FEC_DECODED: return "C6K_HR_FEC_DECODED";
    case C6K_SHUTDOWN: return "C6K_SHUTDOWN";
    case C6K_C6000_SHUTDOWN: return "C6K_C6000_SHUTDOWN";
    case C6K_SLOT_ALIGNMENT_ERROR: return "C6K_SLOT_ALIGNMENT_ERROR";
    case C6K_START_PROJECT_25_PHASE_1_OUTBOUND: return "C6K_START_PROJECT_25_PHASE_1_OUTBOUND";
    case C6K_PROJECT_25_PHASE_1_OUTBOUND_STARTING: return "C6K_PROJECT_25_PHASE_1_OUTBOUND_STARTING";
    case C6K_TRANSMIT_REQUEST: return "C6K_TRANSMIT_REQUEST";
    case C6K_TRANSMIT_DUID: return "C6K_TRANSMIT_DUID";
    case C6K_TRANSMIT_TSBK_DATA: return "C6K_TRANSMIT_TSBK_DATA";
    case C6K_TRANSMIT_NULL: return "C6K_TRANSMIT_NULL";
    case C6K_TRANSMIT_UNCONFIRMED_DATA: return "C6K_TRANSMIT_UNCONFIRMED_DATA";
    case C6K_TRANSMIT_STATUS: return "C6K_TRANSMIT_STATUS";
    case C6K_STOP_PROJECT_25_PHASE_1_OUTBOUND: return "C6K_STOP_PROJECT_25_PHASE_1_OUTBOUND";
    case C6K_PROJECT_25_PHASE_1_OUTBOUND_STOPPED: return "C6K_PROJECT_25_PHASE_1_OUTBOUND_STOPPED";
    case C6K_START_PROJECT_25_PHASE_1_INBOUND: return "C6K_START_PROJECT_25_PHASE_1_INBOUND";
    case C6K_PROJECT_25_PHASE_1_INBOUND_STARTING: return "C6K_PROJECT_25_PHASE_1_INBOUND_STARTING";
    case C6K_RECEIVE_PHASE_1_UPDATE: return "C6K_RECEIVE_PHASE_1_UPDATE";
    case C6K_RECEIVE_DUID: return "C6K_RECEIVE_DUID";
    case C6K_RECEIVE_TSBK: return "C6K_RECEIVE_TSBK";
    case C6K_RECEIVE_UNCONFIRMED: return "C6K_RECEIVE_UNCONFIRMED";
    case C6K_STOP_PROJECT_25_PHASE_1_INBOUND: return "C6K_STOP_PROJECT_25_PHASE_1_INBOUND";
    case C6K_PROJECT_25_PHASE_1_INBOUND_STOPPED: return "C6K_PROJECT_25_PHASE_1_INBOUND_STOPPED";
    case C6K_REPORT_RSSI: return "C6K_REPORT_RSSI";
    case C6K_RSSI_MEASUREMENT: return "C6K_RSSI_MEASUREMENT";
    case C6K_TRANSMIT_CONFIRMED_DATA: return "C6K_TRANSMIT_CONFIRMED_DATA";
    case C6K_TRANSMIT_TERM_WITH_LC: return "C6K_TRANSMIT_TERM_WITH_LC";
    case C6K_C6000_RESET_COMPLETE: return "C6K_C6000_RESET_COMPLETE";
    case C6K_CONFIGURE_HARDWARE: return "C6K_CONFIGURE_HARDWARE";
    case C6K_HARDWARE_CONFIGURED: return "C6K_HARDWARE_CONFIGURED";
    case C6K_TUNE_RECEIVER: return "C6K_TUNE_RECEIVER";
    case C6K_RECEIVER_TUNED: return "C6K_RECEIVER_TUNED";
    case C6K_TRANSMIT_HEADER_PART_1: return "C6K_TRANSMIT_HEADER_PART_1";
    case C6K_TRANSMIT_HEADER_PART_2: return "C6K_TRANSMIT_HEADER_PART_2";
    case C6K_TRANSMIT_LDU_VOICE: return "C6K_TRANSMIT_LDU_VOICE";
    case C6K_TRANSMIT_LDU_VOICE_WITH_LC_ES: return "C6K_TRANSMIT_LDU_VOICE_WITH_LC_ES";
    case C6K_TRANSMIT_LDU_VOICE_WITH_LSD: return "C6K_TRANSMIT_LDU_VOICE_WITH_LSD";
    case C6K_RECEIVE_CONFIRMED_DATA: return "C6K_RECEIVE_CONFIRMED_DATA";
    case C6K_RECEIVE_TERM_WITH_LC: return "C6K_RECEIVE_TERM_WITH_LC";
    case C6K_RECEIVE_HEADER_PART_1: return "C6K_RECEIVE_HEADER_PART_1";
    case C6K_RECEIVE_HEADER_PART_2: return "C6K_RECEIVE_HEADER_PART_2";
    case C6K_RECEIVE_LDU_VOICE: return "C6K_RECEIVE_LDU_VOICE";
    case C6K_RECEIVE_LDU_VOICE_WITH_LC_ES: return "C6K_RECEIVE_LDU_VOICE_WITH_LC_ES";
    case C6K_RECEIVE_LDU_VOICE_WITH_LSD: return "C6K_RECEIVE_LDU_VOICE_WITH_LSD";
    case C6K_INVALID_MSG: return "C6K_INVALID_MSG";
    case C6K_IEMI_SYNCLOC: return "C6K_IEMI_SYNCLOC";
    case C6K_RECEIVE_TIME_UPDATE: return "C6K_RECEIVE_TIME_UPDATE";
    case C6K_START_ANALOG_INBOUND: return "C6K_START_ANALOG_INBOUND";
    case C6K_ANALOG_INBOUND_STARTING: return "C6K_ANALOG_INBOUND_STARTING";
    case C6K_STOP_ANALOG_INBOUND: return "C6K_STOP_ANALOG_INBOUND";
    case C6K_ANALOG_INBOUND_STOPPED: return "C6K_ANALOG_INBOUND_STOPPED";
    case C6K_RECEIVE_ANALOG_RX_INFO: return "C6K_RECEIVE_ANALOG_RX_INFO";
    case C6K_START_ANALOG_OUTBOUND: return "C6K_START_ANALOG_OUTBOUND";
    case C6K_ANALOG_OUTBOUND_STARTING: return "C6K_ANALOG_OUTBOUND_STARTING";
    case C6K_STOP_ANALOG_OUTBOUND: return "C6K_STOP_ANALOG_OUTBOUND";
    case C6K_ANALOG_OUTBOUND_STOPPED: return "C6K_ANALOG_OUTBOUND_STOPPED";
    case C6K_TRANSMIT_AUDIO: return "C6K_TRANSMIT_AUDIO";
    case C6K_TRANSMIT_PREAMBLE: return "C6K_TRANSMIT_PREAMBLE";
    case C6K_OUTBOUND_SERIAL_PORT_STARTED: return "C6K_OUTBOUND_SERIAL_PORT_STARTED";
    case C6K_MODULATION_COMPLETE: return "C6K_MODULATION_COMPLETE";
    case C6K_CONFIGURE_SOFTWARE: return "C6K_CONFIGURE_SOFTWARE";
    case C6K_SOFTWARE_CONFIGURED: return "C6K_SOFTWARE_CONFIGURED";
    case C6K_TRANSMIT_MULTI: return "C6K_TRANSMIT_MULTI";
    case C6K_SMART_EXCITER_UNDERFLOW_WARNING: return "C6K_SMART_EXCITER_UNDERFLOW_WARNING";
    default:
        return "Unknown";
    }
}

template <> inline const char*
c_str<>(C6kModulation value)
{
    switch (value)
    {
    case C6K_MOD_CQPSK: return "CQPSK";
    case C6K_MOD_C4FM: return "C4FM";
    default: return "Invalid";
    }
}

} // namespace efj
