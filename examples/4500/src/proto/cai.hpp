#pragma once

enum CaiDuid
{
    CAI_DUID_HDU  = 0x00,
    CAI_DUID_TDU  = 0x03,
    CAI_DUID_LDU1 = 0x05,
    CAI_DUID_TSBK = 0x07,
    CAI_DUID_LDU2 = 0x0A,
    CAI_DUID_PDU  = 0x0C,
    CAI_DUID_LTDU = 0x0F,
};

namespace efj
{

template <> inline const char*
c_str(CaiDuid duid)
{
    switch (duid)
    {
    case CAI_DUID_HDU: return "HDU";
    case CAI_DUID_TDU: return "TDU";
    case CAI_DUID_LDU1: return "LDU1";
    case CAI_DUID_TSBK: return "TSBK";
    case CAI_DUID_LDU2: return "LDU2";
    case CAI_DUID_PDU: return "PDU";
    case CAI_DUID_LTDU: return "LTDU";
    default: return "Unknown";
    }
}

} // namespace efj

// TODO: CAI structs

struct CaiImbe
{
};

struct CaiLinkControl
{
};

struct CaiLowSpeedData
{
};

struct CaiEncryptionSync
{
};

