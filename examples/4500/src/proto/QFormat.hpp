/******************************************************************/
/*  EFJohnson Company Restricted Material                         */
/*  Copyright (c) 2014-2016 EFJohnson Company                     */
/*  All rights reserved worldwide.                                */
/*  Unauthorized use, distribution or duplication is              */
/*  strictly prohibited without written authorization.            */
/******************************************************************/

#pragma once
#include <limits>
#include <math.h>

// This class implements Q number format as if it were a POD type,
// where Q is a fixed point number format
// where the number of fractional bits are specified.
//
// Some uses of this notation include an optional number of integer bits.
// There are 2 conflicting notations for fixed point.  Both notations are written as
//
//  Qm.n, where;
//
//  Q designates the number is in the Q format notation.
//  m (optional)
//      is the number of bits set aside to designate the two's complement integer
//      portion of the number, exclusive or inclusive of the sign bit.
//      (therefore if m is not specified it is taken as zero or one)
//      one notation includes the sign bit and one does not.
//      When m + n equals the register size T, then the sign bit is included.
//      If m + n is one less than the register size T, then the sign bit is not included.
//  n is the number of bits used to designate the fractional portion of the number.
//      i.e. the number of bits to the right of the binary point.  (if n = 0, the Q
//      numbers are integers.
//
//  In this implementation, m includes the sign bit and can be determined by
//  subtracting N from the number of bits in T
//
//  e.g.
//          Q7.8       (m does not include sign bit)
//          T = int16_t,  N = 8
//
//          Q31.32      (m does not include sign bit)
//          T = int64_t, N = 32
//
//          Q32.32      (Q is unsigned)
//          T = uint64_t, N = 32


// Forward declaration
template <class T = int16_t, uint16_t N = 8>    //Q7.8 by default
class QFormat;

//some commonly used QFormat Types
typedef QFormat<int64_t, 32> QFormat31_32;
typedef QFormat<uint64_t, 32> QFormat32_32;
typedef QFormat<int16_t, 8> QFormat7_8;
typedef QFormat<int16_t, 15> QFormat0_15;

template <class T, uint16_t N>
class QFormat
{
public:
    static const T Multiplier = (T) 1 << (N);
    typedef std::numeric_limits<T> qLimit;
    static const bool is_signed = qLimit::is_signed;
    static const uint16_t NonSignBits = qLimit::digits;

    T qValue;

    //! Default Constructor
    QFormat ()
    {
        qValue = 0;
    }

    //! Construction from a double
    //! Construction from all other POD types can be handled automatically
    //! by compiler via typecast.
    QFormat(double value)
    {
        setValue(value);
    }

    //! Set the value with that given.
    void setValue(double value)
    {
        double long potentialValue = roundl(value * Multiplier);

        // if this type is signed and has no non-fractional bits, the Multiplier is negative,
        // so the potential value needs to be negated
        if ((N == NonSignBits) && is_signed)
        {
            potentialValue = -potentialValue;
        }
        //range check potential result
        if (potentialValue > qLimit::max())
        {
            potentialValue = qLimit::max();
        }
        if (potentialValue < qLimit::min())
        {
            potentialValue = qLimit::min();
        }
        qValue = (T) potentialValue;
    }

    //! Conversion of the fixed point representation to a double
    double toDouble() const
    {
        double doubleValue = (double) qValue;
        doubleValue = doubleValue / Multiplier;

        // if this type is signed and has no non-fractional bits, the Multiplier is negative,
        // so the double value needs to be negated
        if ((N == NonSignBits) && is_signed)
        {
            doubleValue = -doubleValue;
        }
        return doubleValue;
    }

    //! overload of + operator
    //! for adding a double to the fixed point representation
    QFormat<T, N> operator + (const double& rhs) const
    {
        QFormat<T, N> qRhs(rhs);
        QFormat<T, N> result;
        result.qValue = qValue + qRhs.qValue;
        return result;
    }

    //! overload of += operator
    //! for adding a double value to this fixed point representation
    QFormat<T, N>& operator += (const double& rhs)
    {
        QFormat<T, N> qRhs(rhs);
        qValue += qRhs.qValue;
        return *this;
    }

    //! overload of + operator
    //! for adding two Qformat numbers
    QFormat<T, N> operator + (const QFormat<T, N>& rhs) const
    {
        QFormat<T, N> result;
        result.qValue = qValue + rhs.qValue;
        return result;
    }

    //! overload of += operator
    //! for adding another Qformat number to this one
    //! and updating this one with the result
    QFormat<T, N>& operator += (const QFormat<T, N>& rhs)
    {
        qValue +=  rhs.qValue;
        return *this;
    }

    //! overload of - operator
    //! for subtracting another Qformat number from this one
    QFormat<T, N> operator - (const QFormat<T, N>& rhs) const
    {
        QFormat<T, N> result;
        result.qValue = qValue - rhs.qValue;
        return result;
    }

    //! overload of -= operator
    //! for subtracting another Qformat number from this one
    //! and updating this one with the result
    QFormat<T, N>& operator -= (const QFormat<T, N>& rhs)
    {
        qValue -=  rhs.qValue;
        return *this;
    }

    //! overload of - operator
    //! for subtracting a double value from this Qformat value
    QFormat<T, N> operator - (const double& rhs) const
    {
        QFormat<T, N> qRhs(rhs);
        QFormat<T, N> result;
        result.qValue = qValue - qRhs.qValue;
        return result;
    }

    //! overload of - operator
    //! for subtracting a double value from this Qformat value
    //! and updating this QFormat number with the result
    QFormat<T, N>& operator -= (const double& rhs)
    {
        QFormat<T, N> qRhs(rhs);
        qValue -= qRhs.qValue;
        return *this;
    }

    //! overload of == operator
    //! when comparing to a QFormat value
    bool operator == (const T& rhs) const
    {
        return (rhs == qValue);
    }

    //! overload of != operator
    //! when comparing to a QFormat value
    bool operator != (const T& rhs) const
    {
        return !(rhs == qValue);
    }

    //! overload of == operator
    //! when comparing to a double value
    bool operator == (const double& rhs) const
    {
        QFormat<T, N> qRhs(rhs);
        return (qRhs.qValue == qValue);
    }

    //! overload of != operator
    //! when comparing to a double value
    bool operator != (const double& rhs) const
    {
        QFormat<T, N> qRhs(rhs);
        return (qRhs.qValue != qValue);
    }

    //! overload of == operator
    //! when comparing to another QFormat number that is scaled the same
    bool operator == (const QFormat<T, N>& rhs) const
    {
        return (rhs.qValue == qValue);
    }

    //! overload of != operator
    //! when comparing to another QFormat number that is scaled the same
    bool operator != (const QFormat<T, N>& rhs) const
    {
        return !(rhs == qValue);
    }

    //! assignment operator
    //! when assigning the value of a double
    QFormat<T, N>& operator = (const double& value)
    {
        QFormat<T, N> q(value);
        qValue = q.qValue;
        return *this;
    }

    //! assignment operator
    //! when assigning a QFormat value
    QFormat<T, N>& operator = (const T& value)
    {
        qValue = value;
        return *this;
    }

    //! overload of type cast to double
    //! all other type casts can be handled by compiler from this one.
    operator double() const
    {
        return toDouble();
    }
};

