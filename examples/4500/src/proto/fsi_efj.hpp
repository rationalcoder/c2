#include "fsi.hpp"

// TODO: Iterate devices for messages with multiple devices.

enum FsiEfjMessageType
{
    FSC_LAUNCH_TIME              = 0x01,
    FSC_CONNECT_EXT              = 0x02,
    FS_CONNECT_EXT_COMPLETE      = 0x03,
    FSC_DATA_END                 = 0x04,
    FS_DATA_TDU_LC_COMPLETE      = 0x0F,
    FSC_DATA_TDU_LC              = 0x10,
    FSC2_CONF_FRAG               = 0x11,
    FSC2_UNCONF_MSG              = 0x12,
    FSC2_BLK_ACK                 = 0x13,
    FS2_FRAG_COMPLETE            = 0x14,
    FS2_BLK_ACK_COMPLETE         = 0x15,
    FS2_RESP_ACK_HDU             = 0x16,
    FS2_RESP_ACK_BLK             = 0x17,
    FS2_UNCONF_MSG_HDU           = 0x18,
    FS2_CONF_MSG_HDU             = 0x19,
    FS2_UNCONF_BLK               = 0x1A,
    FS2_CONF_BLK                 = 0x1B,
    FSC2_TSBK                    = 0x1C,
    FSC2_TX_STATUS               = 0x1D,
    FS2_RX_STATUS                = 0x1E,
    FS2_EVENT_RPT                = 0x1F,
    FSC_CONF_FRAG                = 0x20,
    FSC_UNCONF_FRAG              = 0x21,
    FSC_BLK_ACK                  = 0x22,
    FSC_TX_STATUS                = 0x23,
    FSC_TSBK                     = 0x24,
    FSC_MBT                      = 0x25,
    FSC_PARAMETER_QUERY          = 0x26,
    FSC_EVENT_QUERY              = 0x27,
    FS_FRAG_COMPLETE             = 0x28,
    FS_BLK_ACK_COMPLETE          = 0x29,
    FS_RX_STATUS_RESPONSE        = 0x2A,
    FS_TX_STATUS_RESPONSE        = 0x2B,
    FS_RX_CAPABILITIES_RESPONSE  = 0x2C,
    FS_TX_CAPABILITIES_RESPONSE  = 0x2D,
    FS_PARAMETER_RESPONSE        = 0x2E,
    FS_OPER_MODE_COMPLETE        = 0x2F,
    FS_RESP_ACK_HDU              = 0x30,
    FS_RESP_ACK_BLK              = 0x31,
    FS_CONF_BLK                  = 0x32,
    FS_UNCONF_BLK                = 0x33,
    FS_CONF_MSG_HDU              = 0x34,
    FS_UNCONF_MSG_HDU            = 0x35,
    FSC_CWID                     = 0x36,
    FSC_SET_PARAMETER            = 0x37,
    FS_CWID_COMPLETE             = 0x38,
    FSC_RX_OPERATING_MODE        = 0x39,
    FS_RX_STATUS                 = 0x3A,
    FS_EVENT_RPT                 = 0x3B,
    FSC_RX_STATUS_QUERY          = 0x3C,
    FSC_TX_STATUS_QUERY          = 0x3D,
    FSC_RX_CAPABILITIES_QUERY    = 0x3E,
    FSC_TX_CAPABILITIES_QUERY    = 0x3F,
    FSC_TX_OPERATING_MODE        = 0x40,
    FS_TC_SENT                   = 0x41,
    FSC_REMOVE_TCM               = 0x42,
    FSC_REPLACE_LC               = 0x43,
};

enum
{
    FSI_EFJ_MFID = 0xF0,
};

struct FsiEfjMessage : FsiManExt {};

EFJ_MACRO u8   fsi_get_man_type(const FsiEfjMessage& msg) { return fsi_get_mf_data(msg)[0]; }
EFJ_MACRO void fsi_set_man_type(FsiEfjMessage& msg, FsiEfjMessageType type) { fsi_get_mf_data(msg)[0] = (u8)type; }
EFJ_MACRO u8   fsi_get_mfid_version(const FsiEfjMessage& msg) { return fsi_get_mf_data(msg)[1]; }
EFJ_MACRO void fsi_set_mfid_version(FsiEfjMessage& msg, u8 version) { fsi_get_mf_data(msg)[1] = version; }


struct FsiEfjConnectExt : FsiEfjMessage {};

enum FsiEfjConnectExtDataMode
{
    FSI_CONNECT_EXT_STD_DATA,
    FSI_CONNECT_EXT_EXT_DATA,
};
enum FsiEfjConnectExtVoiceMode
{
    FSI_CONNECT_EXT_USE_RTP,
    FSI_CONNECT_EXT_USE_RTR2,
};
enum FsiEfjConnectExtControlMode
{
    FSI_CONNECT_EXT_USE_STD_TSBKS,
    FSI_CONNECT_EXT_USE_EXT_TSBKS,
};
enum FsiEfjConnectExtFragmentsMode
{
    FSI_CONNECT_EXT_SEND_FRAGMENTS,
    FSI_CONNECT_EXT_COMBINE_FRAGMENTS,
};
enum FsiEfjConnectExtRssiMode
{
    FSI_CONNECT_EXT_DONT_SEND_RSSI,
    FSI_CONNECT_EXT_SEND_RSSI,
};
enum FsiEfjConnectExtRtpMode
{
    FSI_CONNECT_EXT_USE_VOICE_RTP,
    FSI_CONNECT_EXT_USE_MAC_RTP,
};
enum FsiEfjConnectExtP2Mode
{
    FSI_CONNECT_EXT_DONT_WANT_P2,
    FSI_CONNECT_EXT_WANT_P2,
};

//! D bit. 0 => Use standard FSI messages, 1 => Use extension messages.
EFJ_MACRO u8   fsi_get_data_mode(const FsiEfjConnectExt& msg) { return efj::get_bit(fsi_get_mf_data(msg), 2, 0); }
//! D bit. 0 => Use standard FSI messages, 1 => Use extension messages.
EFJ_MACRO void fsi_set_data_mode(FsiEfjConnectExt& msg, FsiEfjConnectExtDataMode mode) { efj::set_bit(fsi_get_mf_data(msg), 2, 0, mode); }
//! V bit. 0 => Use RTP, 1 => Use RTR2.
EFJ_MACRO u8   fsi_get_voice_mode(const FsiEfjConnectExt& msg) { return efj::get_bit(fsi_get_mf_data(msg), 2, 1); }
//! V bit. 0 => Use RTP, 1 => Use RTR2.
EFJ_MACRO void fsi_set_voice_mode(FsiEfjConnectExt& msg, FsiEfjConnectExtVoiceMode mode) { efj::set_bit(fsi_get_mf_data(msg), 2, 1, mode); }
//! C bit. 0 => Send TSBKS using standard messages, 1 => Send TSBKS with extension messages.
EFJ_MACRO u8   fsi_get_control_mode(const FsiEfjConnectExt& msg) { return efj::get_bit(fsi_get_mf_data(msg), 2, 2); }
//! C bit. 0 => Send TSBKS using standard messages, 1 => Send TSBKS with extension messages.
EFJ_MACRO void fsi_set_control_mode(FsiEfjConnectExt& msg, FsiEfjConnectExtControlMode mode) { efj::set_bit(fsi_get_mf_data(msg), 2, 2, mode); }
//! F bit. 0 => send data blocks as received. 1 => combine fragments.
EFJ_MACRO u8   fsi_get_combine_fragments(const FsiEfjConnectExt& msg) { return efj::get_bit(fsi_get_mf_data(msg), 2, 3); }
//! F bit. 0 => send data blocks as received. 1 => combine fragments.
EFJ_MACRO void fsi_set_combine_fragments(FsiEfjConnectExt& msg, FsiEfjConnectExtFragmentsMode mode) { efj::set_bit(fsi_get_mf_data(msg), 2, 3, mode); }
//! S bit. Return RSSI values in RX_STATUS_RESPONSE.
EFJ_MACRO u8   fsi_get_report_rssi(const FsiEfjConnectExt& msg) { return efj::get_bit(fsi_get_mf_data(msg), 2, 4); }
//! S bit. Return RSSI values in RX_STATUS_RESPONSE.
EFJ_MACRO void fsi_set_report_rssi(FsiEfjConnectExt& msg, FsiEfjConnectExtRssiMode mode) { efj::set_bit(fsi_get_mf_data(msg), 2, 4, mode); }
//! M bit. 0 => Use RTP specified by V bit. 1 => Use MAC RTP.
EFJ_MACRO u8   fsi_get_use_mac(const FsiEfjConnectExt& msg) { return efj::get_bit(fsi_get_mf_data(msg), 2, 5); }
//! M bit. 0 => Use RTP specified by V bit. 1 => Use MAC RTP.
EFJ_MACRO void fsi_set_use_mac(FsiEfjConnectExt& msg, FsiEfjConnectExtRtpMode mode) { efj::set_bit(fsi_get_mf_data(msg), 2, 5, mode); }
//! P2 bit. 0 => System does not desire P2. 1 => it does.
EFJ_MACRO u8   fsi_get_use_p2(const FsiEfjConnectExt& msg) { return efj::get_bit(fsi_get_mf_data(msg), 2, 6); }
//! P2 bit. 0 => System does not desire P2. 1 => it does.
EFJ_MACRO void fsi_set_use_p2(FsiEfjConnectExt& msg, FsiEfjConnectExtP2Mode mode) { efj::set_bit(fsi_get_mf_data(msg), 2, 6, mode); }
//! The version of EFJ FSI extensions to send to the host.
EFJ_MACRO u8   fsi_get_extensions_version(const FsiEfjConnectExt& msg) { return fsi_get_mf_data(msg)[3]; }
//! The version of EFJ FSI extensions to send to the host.
EFJ_MACRO void fsi_set_extensions_version(FsiEfjConnectExt& msg, u8 version) { fsi_get_mf_data(msg)[3] = version; }


inline FsiEfjConnectExt
fsi_make_connect_ext(
    u8 corTag,
    FsiEfjConnectExtDataMode dataMode, FsiEfjConnectExtVoiceMode voiceMode,
    FsiEfjConnectExtControlMode controlMode, FsiEfjConnectExtFragmentsMode fragments,
    FsiEfjConnectExtRssiMode rssiMode, FsiEfjConnectExtRtpMode rtpMode,
    FsiEfjConnectExtP2Mode p2Mode, u8 extensionsVersion)
{
    auto msg = efj_allocate_message(FsiEfjConnectExt, FSI_MAN_DATA_OFFSET + 4);
    fsi_set_man_ext_fields(msg, corTag, FSI_EFJ_MFID);

    fsi_set_man_type(msg, FSC_CONNECT_EXT);
    fsi_set_mfid_version(msg, 3);
    fsi_set_data_mode(msg, dataMode);
    fsi_set_voice_mode(msg, voiceMode);
    fsi_set_control_mode(msg, controlMode);
    fsi_set_combine_fragments(msg, fragments);
    fsi_set_report_rssi(msg, rssiMode);
    fsi_set_use_mac(msg, rtpMode);
    fsi_set_use_p2(msg, p2Mode);
    fsi_set_extensions_version(msg, extensionsVersion);

    return msg;
}


struct FsiEfjConnectExtComplete : FsiEfjMessage {};

//! D bit. True if requested mode is supported.
EFJ_MACRO u8   fsi_get_data_mode(const FsiEfjConnectExtComplete& msg) { return efj::get_bit(fsi_get_mf_data(msg), 2, 0); }
//! D bit. True if requested mode is supported.
EFJ_MACRO void fsi_set_data_mode(FsiEfjConnectExtComplete& msg, bool supported) { efj::set_bit(fsi_get_mf_data(msg), 2, 0, supported); }
//! V bit. True if requested mode is supported.
EFJ_MACRO u8   fsi_get_voice_mode(const FsiEfjConnectExtComplete& msg) { return efj::get_bit(fsi_get_mf_data(msg), 2, 1); }
//! V bit. True if requested mode is supported.
EFJ_MACRO void fsi_set_voice_mode(FsiEfjConnectExtComplete& msg, bool supported) { efj::set_bit(fsi_get_mf_data(msg), 2, 1, supported); }
//! C bit. True if requested mode is supported.
EFJ_MACRO u8   fsi_get_control_mode(const FsiEfjConnectExtComplete& msg) { return efj::get_bit(fsi_get_mf_data(msg), 2, 2); }
//! C bit. True if requested mode is supported.
EFJ_MACRO void fsi_set_control_mode(FsiEfjConnectExtComplete& msg, bool supported) { efj::set_bit(fsi_get_mf_data(msg), 2, 2, supported); }
//! F bit. True if requested mode is supported.
EFJ_MACRO u8   fsi_get_combine_fragments(const FsiEfjConnectExtComplete& msg) { return efj::get_bit(fsi_get_mf_data(msg), 2, 3); }
//! F bit. True if requested mode is supported.
EFJ_MACRO void fsi_set_combine_fragments(FsiEfjConnectExtComplete& msg, bool supported) { efj::set_bit(fsi_get_mf_data(msg), 2, 3, supported); }
//! S bit. True if requested mode is supported.
EFJ_MACRO u8   fsi_get_report_rssi(const FsiEfjConnectExtComplete& msg) { return efj::get_bit(fsi_get_mf_data(msg), 2, 4); }
//! S bit. True if requested mode is supported.
EFJ_MACRO void fsi_set_report_rssi(FsiEfjConnectExtComplete& msg, bool supported) { efj::set_bit(fsi_get_mf_data(msg), 2, 4, supported); }
//! M bit. True if requested mode is supported.
EFJ_MACRO u8   fsi_get_use_mac(const FsiEfjConnectExtComplete& msg) { return efj::get_bit(fsi_get_mf_data(msg), 2, 5); }
//! M bit. True if requested mode is supported.
EFJ_MACRO void fsi_set_use_mac(FsiEfjConnectExtComplete& msg, bool supported) { efj::set_bit(fsi_get_mf_data(msg), 2, 5, supported); }
//! P2 bit. True if requested mode is supported.
EFJ_MACRO u8   fsi_get_use_p2(const FsiEfjConnectExtComplete& msg) { return efj::get_bit(fsi_get_mf_data(msg), 2, 6); }
//! P2 bit. 0 => System does not desire P2. 1 => it does.
EFJ_MACRO void fsi_set_use_p2(FsiEfjConnectExtComplete& msg, bool supported) { efj::set_bit(fsi_get_mf_data(msg), 2, 6, supported); }
//! The version of EFJ FSI extensions to send to the host.
EFJ_MACRO u8   fsi_get_extensions_version(const FsiEfjConnectExtComplete& msg) { return fsi_get_mf_data(msg)[3]; }
//! The version of EFJ FSI extensions to send to the host.
EFJ_MACRO void fsi_set_extensions_version(FsiEfjConnectExtComplete& msg, u8 version) { fsi_get_mf_data(msg)[3] = version; }


inline FsiEfjConnectExtComplete
fsi_make_connect_ext_complete(u8 corTag, bool dataMode, bool voiceMode,
    bool controlMode, bool fragments, bool rssiMode, bool rtpMode,
    bool p2Mode, u8 extensionsVersion)
{
    auto msg = efj_allocate_message(FsiEfjConnectExtComplete, FSI_MAN_DATA_OFFSET + 4);
    fsi_set_man_ext_fields(msg, corTag, FSI_EFJ_MFID);

    fsi_set_man_type(msg, FS_CONNECT_EXT_COMPLETE);
    fsi_set_mfid_version(msg, 2);
    fsi_set_data_mode(msg, dataMode);
    fsi_set_voice_mode(msg, voiceMode);
    fsi_set_control_mode(msg, controlMode);
    fsi_set_combine_fragments(msg, fragments);
    fsi_set_report_rssi(msg, rssiMode);
    fsi_set_use_mac(msg, rtpMode);
    fsi_set_use_p2(msg, p2Mode);
    fsi_set_extensions_version(msg, extensionsVersion);

    return msg;
}

struct FsiEfjSetParameter : FsiEfjMessage {};

//! \returns `ip` in host byte order.
EFJ_MACRO u32         fsi_get_ip(const FsiEfjSetParameter& msg) { return efj::get_ntoh_u32(msg, FSI_MAN_DATA_OFFSET + 2); }
//! `ip` is in host byte order.
EFJ_MACRO void        fsi_set_ip(FsiEfjSetParameter& msg, u32 ip) { efj::set_hton_u32(msg, FSI_MAN_DATA_OFFSET + 2, ip); }
EFJ_MACRO u8          fsi_get_param_name_length(const FsiEfjSetParameter& msg) { return fsi_get_mf_data(msg)[6]; }
EFJ_MACRO const char* fsi_get_param_name(const FsiEfjSetParameter& msg) { return (char*)&fsi_get_mf_data(msg)[8]; }

EFJ_MACRO u16 fsi_get_param_value_length(const FsiEfjSetParameter& msg)
{
    umm valueLengthOffset = FSI_MAN_DATA_OFFSET + 6 + sizeof(u8) + fsi_get_param_name_length(msg);

    return efj::get_ntoh_u16(msg, valueLengthOffset);
}

EFJ_MACRO const char* fsi_get_param_value(const FsiEfjSetParameter& msg)
{
    umm valueLengthOffset = FSI_MAN_DATA_OFFSET + 6 + sizeof(u8) + fsi_get_param_name_length(msg);
    umm valueOffset  = sizeof(u16) + valueLengthOffset;

    return (char*)&msg.data[valueOffset];
}

struct FsiEfjParameterResponse : FsiEfjMessage {};

inline umm
fsi_get_device_offset(const FsiEfjParameterResponse& msg, umm deviceOffset)
{
    umm offset = FSI_MAN_DATA_OFFSET + 2;
    for (umm i = 1; i < deviceOffset; i++)
    {
        offset += sizeof(u32); // ip address.
        offset += sizeof(u16) + efj::get_ntoh_u16(msg, offset); // name + size field.
        offset += sizeof(u16) + efj::get_ntoh_u16(msg, offset); // value + size field.
    }

    return offset;
}

struct FsiEfjTxOperatingMode : FsiEfjMessage {};

EFJ_MACRO u8  fsi_get_tx_number(const FsiEfjTxOperatingMode& msg, umm offset = 0) { return fsi_get_mf_data(msg)[2 + offset]; }
EFJ_MACRO u8  fsi_get_tx_mode(const FsiEfjTxOperatingMode& msg, umm offset = 0) { return fsi_get_mf_data(msg)[2 + offset + 1]; }
EFJ_MACRO u32 fsi_get_tx_freq(const FsiEfjTxOperatingMode& msg, umm offset = 0) { return efj::get_ntoh_u32(msg, FSI_MAN_DATA_OFFSET + 2 + offset + 2); }
EFJ_MACRO u16 fsi_get_tx_power(const FsiEfjTxOperatingMode& msg, umm offset = 0) { return efj::get_ntoh_u16(msg, FSI_MAN_DATA_OFFSET + 2 + offset + 6); }

// NOTE: Right now, the RX operating mode doesn't support multiple devices in the same message like the TX ones, which probably
// makes sense, honestly. It's unnecessarily complicated to bundle these up for messages that aren't sent frequently.
struct FsiEfjRxOperatingMode : FsiEfjMessage {};

EFJ_MACRO u8  fsi_get_rx_number(const FsiEfjRxOperatingMode& msg, umm offset = 0) { return fsi_get_mf_data(msg)[2 + offset]; }
EFJ_MACRO u32 fsi_get_rx_freq(const FsiEfjRxOperatingMode& msg, umm offset = 0) { return efj::get_ntoh_u32(msg, FSI_MAN_DATA_OFFSET + 2 + offset + 1); }

struct FsiEfjOperModeComplete : FsiEfjMessage {};

EFJ_MACRO u8   fsi_get_type_complete(const FsiEfjOperModeComplete& msg, umm offset = 0) { return fsi_get_mf_data(msg)[2 + offset]; }
EFJ_MACRO void fsi_set_type_complete(FsiEfjOperModeComplete& msg, u8 type, umm offset = 0) { fsi_get_mf_data(msg)[2 + offset] = type; }
EFJ_MACRO u8   fsi_get_unit_number(const FsiEfjOperModeComplete& msg, umm offset = 0) { return fsi_get_mf_data(msg)[2 + offset + 1]; }
EFJ_MACRO void fsi_set_unit_number(FsiEfjOperModeComplete& msg, u8 unit, umm offset = 0) { fsi_get_mf_data(msg)[2 + offset + 1] = unit; }
EFJ_MACRO u8   fsi_get_status(const FsiEfjOperModeComplete& msg, umm offset = 0) { return fsi_get_mf_data(msg)[2 + offset + 2]; }
EFJ_MACRO void fsi_set_status(FsiEfjOperModeComplete& msg, bool good, umm offset = 0) { fsi_get_mf_data(msg)[2 + offset + 2] = !good; }

//! Makes an FS_OPER_MODE_COMPLETE with one device filled out.
inline FsiEfjOperModeComplete
fsi_make_oper_mode_complete(u8 corTag, u8 type, u8 unit, bool status)
{
    auto msg = efj_allocate_message(FsiEfjOperModeComplete, FSI_MAN_DATA_OFFSET + 2 + 3);
    fsi_set_man_ext_fields(msg, corTag, FSI_EFJ_MFID);

    fsi_set_man_type(msg, FS_OPER_MODE_COMPLETE);
    fsi_set_mfid_version(msg, 2);
    fsi_set_type_complete(msg, type);
    fsi_set_unit_number(msg, unit);
    fsi_set_status(msg, status);

    return msg;
}

//! Makes an FS_OPER_MODE_COMPLETE with room for `deviceCount` devices which need to have their fields set set manually.
inline FsiEfjOperModeComplete
fsi_make_oper_mode_complete(u8 corTag, umm deviceCount)
{
    auto msg = efj_allocate_message(FsiEfjOperModeComplete, FSI_MAN_DATA_OFFSET + 2 + deviceCount * 3);
    fsi_set_man_ext_fields(msg, corTag, FSI_EFJ_MFID);

    fsi_set_man_type(msg, FS_OPER_MODE_COMPLETE);
    fsi_set_mfid_version(msg, 2);

    return msg;
}

struct FsiEfjTxStatusQuery : FsiEfjMessage {};

inline FsiEfjTxStatusQuery
fsi_make_tx_status_query(u8 corTag)
{
    auto msg = efj_allocate_message(FsiEfjTxStatusQuery, FSI_MAN_DATA_OFFSET + 2);
    fsi_set_man_ext_fields(msg, corTag, FSI_EFJ_MFID);

    fsi_set_man_type(msg, FSC_TX_STATUS_QUERY);
    fsi_set_mfid_version(msg, 2);

    return msg;
}

struct FsiEfjRxStatusQuery : FsiEfjMessage {};

inline FsiEfjRxStatusQuery
fsi_make_rx_status_query(u8 corTag)
{
    auto msg = efj_allocate_message(FsiEfjRxStatusQuery, FSI_MAN_DATA_OFFSET + 2);
    fsi_set_man_ext_fields(msg, corTag, FSI_EFJ_MFID);

    fsi_set_man_type(msg, FSC_RX_STATUS_QUERY);
    fsi_set_mfid_version(msg, 2);

    return msg;
}

struct FsiEfjTxStatusResponse : FsiEfjMessage {};

EFJ_MACRO u8   fsi_get_number(const FsiEfjTxStatusResponse& msg) { return fsi_get_mf_data(msg)[2]; }
EFJ_MACRO void fsi_set_number(FsiEfjTxStatusResponse& msg, u8 number) { fsi_get_mf_data(msg)[2] = number; }
EFJ_MACRO u8   fsi_get_mode(const FsiEfjTxStatusResponse& msg) { return fsi_get_mf_data(msg)[3]; }
EFJ_MACRO void fsi_set_mode(FsiEfjTxStatusResponse& msg, u8 mode) { fsi_get_mf_data(msg)[3] = mode; }
EFJ_MACRO u32  fsi_get_freq(const FsiEfjTxStatusResponse& msg) { return efj::get_ntoh_u32(msg, FSI_MAN_DATA_OFFSET + 4); }
EFJ_MACRO void fsi_set_freq(FsiEfjTxStatusResponse& msg, u32 freq) { efj::set_hton_u32(msg, FSI_MAN_DATA_OFFSET + 4, freq); }
EFJ_MACRO u16  fsi_get_power(const FsiEfjTxStatusResponse& msg) { return efj::get_ntoh_u16(msg, FSI_MAN_DATA_OFFSET + 8); }
EFJ_MACRO void fsi_set_power(FsiEfjTxStatusResponse& msg, u16 power) { efj::set_hton_u16(msg, FSI_MAN_DATA_OFFSET + 8, power); }

EFJ_MACRO bool fsi_get_alarm(const FsiEfjTxStatusResponse& msg) { return efj::get_bit(msg, FSI_MAN_DATA_OFFSET + 10, 7); }
EFJ_MACRO void fsi_set_alarm(FsiEfjTxStatusResponse& msg, bool present) { efj::set_bit(msg, FSI_MAN_DATA_OFFSET + 10, 7, present); }
EFJ_MACRO bool fsi_get_warn(const FsiEfjTxStatusResponse& msg) { return efj::get_bit(msg, FSI_MAN_DATA_OFFSET + 10, 6); }
EFJ_MACRO void fsi_set_warn(FsiEfjTxStatusResponse& msg, bool present) { efj::set_bit(msg, FSI_MAN_DATA_OFFSET + 10, 6, present); }
EFJ_MACRO bool fsi_get_off(const FsiEfjTxStatusResponse& msg) { return efj::get_bit(msg, FSI_MAN_DATA_OFFSET + 10, 5); }
EFJ_MACRO void fsi_set_off(FsiEfjTxStatusResponse& msg, bool off) { efj::set_bit(msg, FSI_MAN_DATA_OFFSET + 10, 5, off); }
EFJ_MACRO bool fsi_get_ready(const FsiEfjTxStatusResponse& msg) { return efj::get_bit(msg, FSI_MAN_DATA_OFFSET + 10, 0); }
EFJ_MACRO void fsi_set_ready(FsiEfjTxStatusResponse& msg, bool ready) { efj::set_bit(msg, FSI_MAN_DATA_OFFSET + 10, 0, ready); }

inline FsiEfjTxStatusResponse
fsi_make_tx_status_response(
    u8 corTag, u8 number, u8 mode, u32 freq, u16 power,
    bool alarm, bool warn, bool off, bool ready)
{
    auto msg = efj_allocate_message(FsiEfjTxStatusResponse, FSI_MAN_DATA_OFFSET + 11);
    memset(msg.data, 0, msg.size);
    fsi_set_man_ext_fields(msg, corTag, FSI_EFJ_MFID);

    fsi_set_man_type(msg, FS_TX_STATUS_RESPONSE);
    fsi_set_mfid_version(msg, 2);
    fsi_set_number(msg, number);
    fsi_set_mode(msg, mode);
    fsi_set_freq(msg, freq);
    fsi_set_power(msg, power);
    fsi_set_alarm(msg, alarm);
    fsi_set_warn(msg, warn);
    fsi_set_off(msg, off);
    fsi_set_ready(msg, ready);

    return msg;
}

// TODO: RSSI
struct FsiEfjRxStatusResponse : FsiEfjMessage {};

enum FsiEfjRxStatusRssiType
{
    FSI_EFJ_RSSI_LINEAR,
    FSI_EFJ_RSSI_DBM,
};

EFJ_MACRO u8   fsi_get_number(const FsiEfjRxStatusResponse& msg) { return fsi_get_mf_data(msg)[2]; }
EFJ_MACRO void fsi_set_number(FsiEfjRxStatusResponse& msg, u8 number) { fsi_get_mf_data(msg)[2] = number; }
EFJ_MACRO u32  fsi_get_freq(const FsiEfjRxStatusResponse& msg) { return efj::get_ntoh_u32(msg, FSI_MAN_DATA_OFFSET + 3); }
EFJ_MACRO void fsi_set_freq(FsiEfjRxStatusResponse& msg, u32 freq) { efj::set_hton_u32(msg, FSI_MAN_DATA_OFFSET + 3, freq); }

EFJ_MACRO bool fsi_get_alarm(const FsiEfjRxStatusResponse& msg) { return efj::get_bit(msg, FSI_MAN_DATA_OFFSET + 7, 7); }
EFJ_MACRO void fsi_set_alarm(FsiEfjRxStatusResponse& msg, bool present) { efj::set_bit(msg, FSI_MAN_DATA_OFFSET + 7, 7, present); }
EFJ_MACRO bool fsi_get_warn(const FsiEfjRxStatusResponse& msg) { return efj::get_bit(msg, FSI_MAN_DATA_OFFSET + 7, 6); }
EFJ_MACRO void fsi_set_warn(FsiEfjRxStatusResponse& msg, bool present) { efj::set_bit(msg, FSI_MAN_DATA_OFFSET + 7, 6, present); }
EFJ_MACRO bool fsi_get_off(const FsiEfjRxStatusResponse& msg) { return efj::get_bit(msg, FSI_MAN_DATA_OFFSET + 7, 5); }
EFJ_MACRO void fsi_set_off(FsiEfjRxStatusResponse& msg, bool off) { efj::set_bit(msg, FSI_MAN_DATA_OFFSET + 7, 5, off); }
EFJ_MACRO bool fsi_get_have_rssi(const FsiEfjRxStatusResponse& msg) { return efj::get_bit(msg, FSI_MAN_DATA_OFFSET + 7, 3); }
EFJ_MACRO void fsi_set_have_rssi(FsiEfjRxStatusResponse& msg, bool present) { efj::set_bit(msg, FSI_MAN_DATA_OFFSET + 7, 3, present); }
EFJ_MACRO bool fsi_get_rx_p25(const FsiEfjRxStatusResponse& msg) { return efj::get_bit(msg, FSI_MAN_DATA_OFFSET + 7, 2); }
EFJ_MACRO void fsi_set_rx_p25(FsiEfjRxStatusResponse& msg, bool rx) { efj::set_bit(msg, FSI_MAN_DATA_OFFSET + 7, 2, rx); }
EFJ_MACRO bool fsi_get_rx_carrier(const FsiEfjRxStatusResponse& msg) { return efj::get_bit(msg, FSI_MAN_DATA_OFFSET + 7, 1); }
EFJ_MACRO void fsi_set_rx_carrier(FsiEfjRxStatusResponse& msg, bool rx) { efj::set_bit(msg, FSI_MAN_DATA_OFFSET + 7, 1, rx); }
EFJ_MACRO bool fsi_get_ready(const FsiEfjRxStatusResponse& msg) { return efj::get_bit(msg, FSI_MAN_DATA_OFFSET + 7, 0); }
EFJ_MACRO void fsi_set_ready(FsiEfjRxStatusResponse& msg, bool ready) { efj::set_bit(msg, FSI_MAN_DATA_OFFSET + 7, 0, ready); }
EFJ_MACRO u8   fsi_get_rssi_type(const FsiEfjRxStatusResponse& msg) { return efj::get_bit(msg, FSI_MAN_DATA_OFFSET + 8, 7); }
EFJ_MACRO void fsi_set_rssi_type(FsiEfjRxStatusResponse& msg, u8 type) { efj::set_bit(msg, FSI_MAN_DATA_OFFSET + 8, 7, type); }
EFJ_MACRO u8   fsi_get_rssi(const FsiEfjRxStatusResponse& msg) { return fsi_get_mf_data(msg)[8] & 127; }
EFJ_MACRO void fsi_set_rssi(FsiEfjRxStatusResponse& msg, u8 rssi) { u8* byte = &fsi_get_mf_data(msg)[8]; *byte = (rssi & 127) | (*byte & 128); }

inline FsiEfjRxStatusResponse
fsi_make_rx_status_response(
    u8 corTag, u8 number, u32 freq, bool alarm, bool warn, bool off,
    bool rxP25, bool rxCarrier, bool ready, u8 rssi)
{
    auto msg = efj_allocate_message(FsiEfjRxStatusResponse, FSI_MAN_DATA_OFFSET + 9);
    memset(msg.data, 0, msg.size);
    fsi_set_man_ext_fields(msg, corTag, FSI_EFJ_MFID);

    fsi_set_man_type(msg, FS_RX_STATUS_RESPONSE);
    fsi_set_mfid_version(msg, 2);
    fsi_set_number(msg, number);
    fsi_set_freq(msg, freq);
    fsi_set_alarm(msg, alarm);
    fsi_set_warn(msg, warn);
    fsi_set_off(msg, off);
    fsi_set_have_rssi(msg, true);
    fsi_set_rx_p25(msg, rxP25);
    fsi_set_rx_carrier(msg, rxCarrier);
    fsi_set_ready(msg, ready);
    fsi_set_rssi_type(msg, FSI_EFJ_RSSI_DBM);
    fsi_set_rssi(msg, rssi);

    return msg;
}

namespace efj
{

template <> inline const char*
c_str<>(FsiEfjMessageType type)
{
    switch (type)
    {
    case FSC_LAUNCH_TIME: return "FSC_LAUNCH_TIME";
    case FSC_CONNECT_EXT: return "FSC_CONNECT_EXT";
    case FS_CONNECT_EXT_COMPLETE: return "FS_CONNECT_EXT_COMPLETE";
    case FSC_DATA_END: return "FSC_DATA_END";
    case FS_DATA_TDU_LC_COMPLETE: return "FS_DATA_TDU_LC_COMPLETE";
    case FSC_DATA_TDU_LC: return "FSC_DATA_TDU_LC";
    case FSC2_CONF_FRAG: return "FSC2_CONF_FRAG";
    case FSC2_UNCONF_MSG: return "FSC2_UNCONF_MSG";
    case FSC2_BLK_ACK: return "FSC2_BLK_ACK";
    case FS2_FRAG_COMPLETE: return "FS2_FRAG_COMPLETE";
    case FS2_BLK_ACK_COMPLETE: return "FS2_BLK_ACK_COMPLETE";
    case FS2_RESP_ACK_HDU: return "FS2_RESP_ACK_HDU";
    case FS2_RESP_ACK_BLK: return "FS2_RESP_ACK_BLK";
    case FS2_UNCONF_MSG_HDU: return "FS2_UNCONF_MSG_HDU";
    case FS2_CONF_MSG_HDU: return "FS2_CONF_MSG_HDU";
    case FS2_UNCONF_BLK: return "FS2_UNCONF_BLK";
    case FS2_CONF_BLK: return "FS2_CONF_BLK";
    case FSC2_TSBK: return "FSC2_TSBK";
    case FSC2_TX_STATUS: return "FSC2_TX_STATUS";
    case FS2_RX_STATUS: return "FS2_RX_STATUS";
    case FS2_EVENT_RPT: return "FS2_EVENT_RPT";
    case FSC_CONF_FRAG: return "FSC_CONF_FRAG";
    case FSC_UNCONF_FRAG: return "FSC_UNCONF_FRAG";
    case FSC_BLK_ACK: return "FSC_BLK_ACK";
    case FSC_TX_STATUS: return "FSC_TX_STATUS";
    case FSC_TSBK: return "FSC_TSBK";
    case FSC_MBT: return "FSC_MBT";
    case FSC_PARAMETER_QUERY: return "FSC_PARAMETER_QUERY";
    case FSC_EVENT_QUERY: return "FSC_EVENT_QUERY";
    case FS_FRAG_COMPLETE: return "FS_FRAG_COMPLETE";
    case FS_BLK_ACK_COMPLETE: return "FS_BLK_ACK_COMPLETE";
    case FS_RX_STATUS_RESPONSE: return "FS_RX_STATUS_RESPONSE";
    case FS_TX_STATUS_RESPONSE: return "FS_TX_STATUS_RESPONSE";
    case FS_RX_CAPABILITIES_RESPONSE: return "FS_RX_CAPABILITIES_RESPONSE";
    case FS_TX_CAPABILITIES_RESPONSE: return "FS_TX_CAPABILITIES_RESPONSE";
    case FS_PARAMETER_RESPONSE: return "FS_PARAMETER_RESPONSE";
    case FS_OPER_MODE_COMPLETE: return "FS_OPER_MODE_COMPLETE";
    case FS_RESP_ACK_HDU: return "FS_RESP_ACK_HDU";
    case FS_RESP_ACK_BLK: return "FS_RESP_ACK_BLK";
    case FS_CONF_BLK: return "FS_CONF_BLK";
    case FS_UNCONF_BLK: return "FS_UNCONF_BLK";
    case FS_CONF_MSG_HDU: return "FS_CONF_MSG_HDU";
    case FS_UNCONF_MSG_HDU: return "FS_UNCONF_MSG_HDU";
    case FSC_CWID: return "FSC_CWID";
    case FSC_SET_PARAMETER: return "FSC_SET_PARAMETER";
    case FS_CWID_COMPLETE: return "FS_CWID_COMPLETE";
    case FSC_RX_OPERATING_MODE: return "FSC_RX_OPERATING_MODE";
    case FS_RX_STATUS: return "FS_RX_STATUS";
    case FS_EVENT_RPT: return "FS_EVENT_RPT";
    case FSC_RX_STATUS_QUERY: return "FSC_RX_STATUS_QUERY";
    case FSC_TX_STATUS_QUERY: return "FSC_TX_STATUS_QUERY";
    case FSC_RX_CAPABILITIES_QUERY: return "FSC_RX_CAPABILITIES_QUERY";
    case FSC_TX_CAPABILITIES_QUERY: return "FSC_TX_CAPABILITIES_QUERY";
    case FSC_TX_OPERATING_MODE: return "FSC_TX_OPERATING_MODE";
    case FS_TC_SENT: return "FS_TC_SENT";
    case FSC_REMOVE_TCM: return "FSC_REMOVE_TCM";
    case FSC_REPLACE_LC: return "FSC_REPLACE_LC";
    default: return "Unknown";
    }
}

} // namespace efj

