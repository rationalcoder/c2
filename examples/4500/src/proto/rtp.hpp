#pragma once

struct RtpMessage : efj::message { using message::message; };
namespace efj
{
EFJ_DEFINE_MESSAGE(RtpMessage, "1.0");
} // namespace efj

//////////////////////////////////////////////////////////////////////
// Taken directly from RFC 1889 http://www.faqs.org/rfcs/rfc1889.html
/////////////////////////////////////////////////////////////////////
//   0               *   1           *       2       *           3
//   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
//  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//  |V=2|P|X|  CC   |M|     PT      |       sequence number         |
//  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//  |                           timestamp                           |
//  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//  |           synchronization source (SSRC) identifier            |
//  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//  |  defined by profile           |   num of 32-bit words in ext  |
//  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//  |                       header extension                        |
//  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//  |            contributing source (CSRC) identifiers             |
//  |                             ....                              |
//  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

//  The first twelve octets are present in every RTP packet, while the
//  list of CSRC identifiers is present only when inserted by a mixer.
//  The fields have the following meaning:

//  version (V): 2 bits
//       This field identifies the version of RTP. The version defined by
//       this specification is two (2). (The value 1 is used by the first
//       draft version of RTP and the value 0 is used by the protocol
//       initially implemented in the "vat" audio tool.)

//  padding (P): 1 bit
//       If the padding bit is set, the packet contains one or more
//       additional padding octets at the end which are not part of the

//       payload. The last octet of the padding contains a count of how
//       many padding octets should be ignored. Padding may be needed by
//       some encryption algorithms with fixed block sizes or for
//       carrying several RTP packets in a lower-layer protocol data
//       unit.

//  extension (X): 1 bit
//       If the extension bit is set, the fixed header is followed by
//       exactly one header extension, with a format defined in Section
//       5.3.1.

//  CSRC count (CC): 4 bits
//       The CSRC count contains the number of CSRC identifiers that
//       follow the fixed header.

//  marker (M): 1 bit
//       The interpretation of the marker is defined by a profile. It is
//       intended to allow significant events such as frame boundaries to
//       be marked in the packet stream. A profile may define additional
//       marker bits or specify that there is no marker bit by changing
//       the number of bits in the payload type field (see Section 5.3).

//  payload type (PT): 7 bits
//       This field identifies the format of the RTP payload and
//       determines its interpretation by the application. A profile
//       specifies a default static mapping of payload type codes to
//       payload formats. Additional payload type codes may be defined
//       dynamically through non-RTP means (see Section 3). An initial
//       set of default mappings for audio and video is specified in the
//       companion profile Internet-Draft draft-ietf-avt-profile, and
//       may be extended in future editions of the Assigned Numbers RFC
//       [6].  An RTP sender emits a single RTP payload type at any given
//       time; this field is not intended for multiplexing separate media
//       streams (see Section 5.2).

//  sequence number: 16 bits
//       The sequence number increments by one for each RTP data packet
//       sent, and may be used by the receiver to detect packet loss and
//       to restore packet sequence. The initial value of the sequence
//       number is random (unpredictable) to make known-plaintext attacks
//       on encryption more difficult, even if the source itself does not
//       encrypt, because the packets may flow through a translator that
//       does. Techniques for choosing unpredictable numbers are
//       discussed in [7].

//  timestamp: 32 bits
//       The timestamp reflects the sampling instant of the first octet
//       in the RTP data packet. The sampling instant must be derived

//       from a clock that increments monotonically and linearly in time
//       to allow synchronization and jitter calculations (see Section
//       6.3.1).  The resolution of the clock must be sufficient for the
//       desired synchronization accuracy and for measuring packet
//       arrival jitter (one tick per video frame is typically not
//       sufficient).  The clock frequency is dependent on the format of
//       data carried as payload and is specified statically in the
//       profile or payload format specification that defines the format,
//       or may be specified dynamically for payload formats defined
//       through non-RTP means. If RTP packets are generated
//       periodically, the nominal sampling instant as determined from
//       the sampling clock is to be used, not a reading of the system
//       clock. As an example, for fixed-rate audio the timestamp clock
//       would likely increment by one for each sampling period.  If an
//       audio application reads blocks covering 160 sampling periods
//       from the input device, the timestamp would be increased by 160
//       for each such block, regardless of whether the block is
//       transmitted in a packet or dropped as silent.

//  The initial value of the timestamp is random, as for the sequence
//  number. Several consecutive RTP packets may have equal timestamps if
//  they are (logically) generated at once, e.g., belong to the same
//  video frame. Consecutive RTP packets may contain timestamps that are
//  not monotonic if the data is not transmitted in the order it was
//  sampled, as in the case of MPEG interpolated video frames. (The
//  sequence numbers of the packets as transmitted will still be
//  monotonic.)

//  SSRC: 32 bits
//       The SSRC field identifies the synchronization source. This
//       identifier is chosen randomly, with the intent that no two
//       synchronization sources within the same RTP session will have
//       the same SSRC identifier. An example algorithm for generating a
//       random identifier is presented in Appendix A.6. Although the
//       probability of multiple sources choosing the same identifier is
//       low, all RTP implementations must be prepared to detect and
//       resolve collisions.  Section 8 describes the probability of
//       collision along with a mechanism for resolving collisions and
//       detecting RTP-level forwarding loops based on the uniqueness of
//       the SSRC identifier. If a source changes its source transport
//       address, it must also choose a new SSRC identifier to avoid
//       being interpreted as a looped source.

//  CSRC list: 0 to 15 items, 32 bits each
//       The CSRC list identifies the contributing sources for the
//       payload contained in this packet. The number of identifiers is
//       given by the CC field. If there are more than 15 contributing
//       sources, only 15 may be identified. CSRC identifiers are

//       inserted by mixers, using the SSRC identifiers of contributing
//       sources. For example, for audio packets the SSRC identifiers of
//       all sources that were mixed together to create a packet are
//       listed, allowing correct talker indication at the receiver.
//

enum
{
    RTP_HEADER_SIZE = 12,
};

// V bits
EFJ_MACRO u8   rtp_get_version(const RtpMessage& msg) { return msg[0] >> 6; }
EFJ_MACRO void rtp_set_version(RtpMessage& msg, u8 version) { msg[0] = (msg[0] & 0x3F) | (version << 6); }
// P bit
EFJ_MACRO bool rtp_has_padding(const RtpMessage& msg) { return efj::get_bit(msg, 0, 5); }
EFJ_MACRO void rtp_set_has_padding(RtpMessage& msg, bool present) { efj::set_bit(msg, 0, 5, present); }
// X bit
EFJ_MACRO bool rtp_has_header_extension(const RtpMessage& msg) { return efj::get_bit(msg, 0, 4); }
EFJ_MACRO void rtp_set_has_header_extension(RtpMessage& msg, bool present) { efj::set_bit(msg, 0, 4, present); }
// CC bits
EFJ_MACRO u8   rtp_get_csrc_count(const RtpMessage& msg) { return (msg[0] & 0x0F); }
EFJ_MACRO void rtp_set_csrc_count(RtpMessage& msg, u8 count) { msg[0] = (msg[0] & 0xF0) | ((count & 0x0F)); }
// M bit
EFJ_MACRO bool rtp_has_marker(const RtpMessage& msg) { return efj::get_bit(msg, 7, 0); }
EFJ_MACRO void rtp_set_has_marker(RtpMessage& msg, bool present) { efj::set_bit(msg, 7, 0, present); }

EFJ_MACRO u8   rtp_get_payload_type(const RtpMessage& msg) { return (msg[1] & 0x7F); }
EFJ_MACRO void rtp_set_payload_type(RtpMessage& msg, u8 type) { msg[1] = (msg[1] & 0x80) | (type & 0x7F); }

EFJ_MACRO u16  rtp_get_sequence_number(const RtpMessage& msg) { return efj::get_ntoh_u16(msg, 2); }
EFJ_MACRO void rtp_set_sequence_number(RtpMessage& msg, u32 value) { efj::set_hton_u16(msg, 2, value); }

EFJ_MACRO u32  rtp_get_timestamp(const RtpMessage& msg) { return efj::get_ntoh_u32(msg, 4); }
EFJ_MACRO void rtp_set_timestamp(RtpMessage& msg, u32 value) { efj::set_hton_u32(msg, 4, value); }

EFJ_MACRO u32  rtp_get_ssrc(const RtpMessage& msg) { return efj::get_ntoh_u32(msg, 8); }
EFJ_MACRO void rtp_set_ssrc(RtpMessage& msg, u32 value) { efj::set_hton_u32(msg, 8, value); }

EFJ_MACRO const u8* rtp_get_header_extension(const RtpMessage& msg) { return &msg[12]; }
EFJ_MACRO u8* rtp_get_header_extension(RtpMessage& msg) { return &msg[12]; }

inline umm
rtp_get_header_size(const RtpMessage& msg)
{
    umm total = 12;
    if (rtp_has_header_extension(msg))
    {
        total += sizeof(u32) * efj::get_ntoh_u32(msg, 14);
    }

    umm csrc_count = rtp_get_csrc_count(msg);
    if (csrc_count)
    {
        total += sizeof(u32) * csrc_count;
    }

    return total;
}

EFJ_MACRO void
rtp_set_header_fields(RtpMessage& msg, u8 version, bool has_padding, bool has_header_extension,
    u8 csrc_count, bool has_marker, u8 payload_type, u16 sequence_number, u32 timestamp, u32 ssrc)
{
    rtp_set_version(msg, version);
    rtp_set_has_padding(msg, has_padding);
    rtp_set_has_header_extension(msg, has_header_extension);
    rtp_set_csrc_count(msg, csrc_count);
    rtp_set_has_marker(msg, has_marker);
    rtp_set_payload_type(msg, payload_type);
    rtp_set_sequence_number(msg, sequence_number);
    rtp_set_timestamp(msg, timestamp);
    rtp_set_ssrc(msg, ssrc);
}

// Right now, we don't support making messages with padding or header extensions a csrc count b/c those are essentially
// never used and/or we don't use them.
template <typename MessageT> EFJ_MACRO MessageT
rtp_make_message(u8 version, bool has_marker, u8 payload_type, u16 sequence_number,
    u32 timestamp, u32 ssrc, umm payload_size)
{
    auto msg = efj_allocate_message(MessageT, RTP_HEADER_SIZE + payload_size);

    rtp_set_version(msg, version);
    rtp_set_has_padding(msg, false);
    rtp_set_has_header_extension(msg, false);
    rtp_set_csrc_count(msg, 0);
    rtp_set_has_marker(msg, has_marker);
    rtp_set_payload_type(msg, payload_type);
    rtp_set_sequence_number(msg, sequence_number);
    rtp_set_timestamp(msg, timestamp);
    rtp_set_ssrc(msg, ssrc);

    return msg;
}

