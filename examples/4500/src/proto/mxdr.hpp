#pragma once

enum MxdrMessageType
{
    MXDR_REQUEST_GEN_INFO         = 0x00,
    MXDR_SET_TX_FREQUENCY         = 0x01,
    MXDR_SET_TX_MOD_CONFIG        = 0x02,
    MXDR_SET_RX1_FREQUENCY        = 0x03,
    MXDR_SET_RX2_FREQUENCY        = 0x04,
    MXDR_SET_TX_STATE             = 0x05,
    MXDR_REQUEST_DIAG_INFO        = 0x06,
    MXDR_SET_LED_STATUS           = 0x07,
    MXDR_FAN_CONTROL              = 0x08,
    MXDR_REPLY_PRODUCT_INFO       = 0x09,
    MXDR_REPLY_DIAG_INFO          = 0x0A,
    MXDR_TX_STATUS                = 0x0B,
    MXDR_SET_TX_OUTPUT_POWER      = 0x0C,
    MXDR_SET_RX_BANDWIDTH         = 0x0D,
    MXDR_SET_TX_FREQ_OFFSET       = 0x0E,
    MXDR_SET_ALARM_PARAMS         = 0x0F,
    MXDR_ALARM_AND_STATUS         = 0x10,
    MXDR_READ_SERIAL_NUMBER       = 0x11,
    MXDR_REPLY_SERIAL_NUMBER      = 0x12,
    MXDR_SET_EXCITER_LICENSE_KEY  = 0x13,
    MXDR_RESET_CONTROLLER         = 0x14,
    MXDR_SET_BOOT_MODE            = 0x15,
    MXDR_SERIAL_PASS_THROUGH      = 0x16,
    MXDR_READ_EXCITER_LICENSE_KEY = 0x17,
    MXDR_SET_TX_BOOT_MODE         = 0x18,
    MXDR_SET_TEST_PATTERN         = 0x19,
    MXDR_MESSAGE_COUNT_,
};

struct MxdrMessage : efj::message { using message::message; };

namespace efj
{
EFJ_DEFINE_MESSAGE(MxdrMessage, "1.0");
} // namespace efj

enum
{
    MXDR_SOH          = 0xC0,
    MXDR_FN_MSG       = 0x00,
    MXDR_FN_ACK       = 0x01,
    MXDR_HEADER_SIZE  = 0x02, // SOH/FN + length
    MXDR_CRC_SIZE     = 0x02,
    MXDR_FRAMING_SIZE = 0x04, // header size + crc
};

inline u16 calc_crc(const void* buf, umm size);

EFJ_MACRO u8   mxdr_get_soh_fn(const MxdrMessage& msg) { return msg[0]; }
EFJ_MACRO void mxdr_set_soh_fn(MxdrMessage& msg, u8 soh_fn) { msg[0] = soh_fn; }
EFJ_MACRO u8   mxdr_get_data_len(const MxdrMessage& msg) { return msg[1]; }
EFJ_MACRO u8   mxdr_get_full_size(const MxdrMessage& msg) { return mxdr_get_data_len(msg) + MXDR_FRAMING_SIZE; }

//! Gets the type field if present. Data types aren't available on acks of zero size, so
//! we panic in that case.
EFJ_MACRO u8
mxdr_get_type(const MxdrMessage& msg)
{
    efj_panic_if(msg[1] == 0 && "Acks don't have types");
    return msg[MXDR_HEADER_SIZE];
}

inline MxdrMessageType
mxdr_get_valid_type(const MxdrMessage& msg)
{
    u8 raw = mxdr_get_type(msg);
    efj_panic_if(raw >= MXDR_MESSAGE_COUNT_);
    return (MxdrMessageType)raw;
}

EFJ_MACRO bool
mxdr_is_ack(const MxdrMessage& msg)
{
    return mxdr_get_soh_fn(msg) & MXDR_FN_ACK;
}

EFJ_MACRO u16
mxdr_calc_crc(const MxdrMessage& msg)
{
    return calc_crc(msg.data, msg.size - MXDR_CRC_SIZE);
}

EFJ_MACRO void
mxdr_set_crc(MxdrMessage& msg)
{
    u16 crc = calc_crc(msg.data, msg.size - MXDR_CRC_SIZE);
    efj::set_hton_u16(msg, MXDR_HEADER_SIZE + mxdr_get_data_len(msg), crc);
}

EFJ_MACRO u16
mxdr_get_crc(const MxdrMessage& msg)
{
    return efj::get_ntoh_u16(msg, MXDR_HEADER_SIZE + mxdr_get_data_len(msg));
}

EFJ_MACRO bool
mxdr_has_valid_crc(const MxdrMessage& msg)
{
    return mxdr_get_crc(msg) == mxdr_calc_crc(msg);
}

inline MxdrMessage
mxdr_make_ack()
{
    auto msg = efj_allocate_message(MxdrMessage, MXDR_FRAMING_SIZE);
    mxdr_set_soh_fn(msg, MXDR_SOH | MXDR_FN_ACK);
    msg[1] = 0;
    mxdr_set_crc(msg);

    return msg;
}

template <typename BlockT> EFJ_MACRO BlockT*
mxdr_get_block(const MxdrMessage& msg)
{
    efj_panic_if(msg.size != sizeof(BlockT));
    return (BlockT*)msg.data;
}

template <typename BlockT> EFJ_MACRO BlockT*
mxdr_get_block_or_null(const MxdrMessage& msg)
{
    if (msg.size != sizeof(BlockT))
        return nullptr;

    return (BlockT*)msg.data;
}

//! Allocates a block, zeroes it out, sets the known header fields, and sets the data type field.
//! \param type The type to put in the DAT field.
//! IMPORTANT: This does not set the CRC, b/c we can't set the CRC until all the data is in the message.
//!
template <typename BlockT> EFJ_MACRO BlockT*
mxdr_make_block(MxdrMessageType type)
{
    BlockT* block = efj_new(BlockT)();
    block->setHeader();
    block->data[MXDR_HEADER_SIZE] = (u8)type;

    return block;
}

// IMPORTANT: Do _not_ add virtual functions to this or any other block types.
// When you have a *Block*, you have an array of u8's in memory.
// The size here is the total size of the message, not the data field length.
template <umm SizeV, u8 AckV>
struct MxdrBlock
{
    u8 data[SizeV];

    static EFJ_MACRO constexpr bool is_ack() { return AckV == MXDR_FN_ACK; }

    EFJ_MACRO void setHeader()
    {
        data[0] = 0xC0 | AckV;
        data[1] = SizeV - MXDR_FRAMING_SIZE;
    }

    EFJ_MACRO void updateCrc()
    {
        MxdrMessage msg = { data, SizeV };
        mxdr_set_crc(msg);
    }
};


template <umm SizeV, u8 AckV> EFJ_MACRO MxdrMessage
mxdr_block_to_message(const MxdrBlock<SizeV, AckV>& block)
{
    MxdrMessage msg = { block.data, sizeof(block.data) };
    return msg;
}


struct MxdrBlockReqGenInfo : MxdrBlock<5, MXDR_FN_MSG>
{
};

inline MxdrMessage
mxdr_make_req_gen_info()
{
    auto* block = mxdr_make_block<MxdrBlockReqGenInfo>(MXDR_REQUEST_GEN_INFO);
    block->updateCrc();

    return { block->data, sizeof(*block) };
}

// WARNING: We currently expect this struct to match byte-for-byte with the data
// fields of the MXDR message.
struct MxdrProductInfo
{
    u8 softwareMajor = 0;
    u8 softwareMinor = 0;
    u8 exciterMajor  = 0;
    u8 exciterMinor  = 0;
    u8 hardwareMajor = 0;
    u8 hardwareMinor = 0;
    u8 txBand        = 0;
    u8 rx1Band       = 0;
    u8 rx2Band       = 0;
    u8 paBand        = 0;

    char txSerial[10]         = {};
    char rx1Serial[10]        = {};
    char rx2Serial[10]        = {};
    char paSerial[10]         = {};
    char controllerSerial[10] = {};
    char radioSerial[10]      = {};
    char radioModelSerial[10] = {};
};

struct MxdrBlockReplyProductInfo : MxdrBlock<85, MXDR_FN_ACK>
{
    MxdrProductInfo getInfo()
    {
        MxdrProductInfo result;
        memcpy(&result, data + MXDR_FRAMING_SIZE, sizeof(result));

        return result;
    }

    void setInfo(const MxdrProductInfo& info)
    {
        memcpy(data + MXDR_FRAMING_SIZE, &info, sizeof(info));
    }
};

inline MxdrMessage
mxdr_make_reply_product_info(const MxdrProductInfo& info)
{
    auto* block = mxdr_make_block<MxdrBlockReplyProductInfo>(MXDR_REPLY_PRODUCT_INFO);
    block->setInfo(info);
    block->updateCrc();

    return { block->data, sizeof(*block) };
}

struct MxdrBlockSetTxFrequency : MxdrBlock<9, MXDR_FN_MSG>
{
    EFJ_MACRO void setFrequency(u32 value) { efj::set_hton_u32(&data[MXDR_HEADER_SIZE + 1], value); }
};

inline MxdrMessage
mxdr_make_set_tx_freq(u32 value)
{
    auto* block = mxdr_make_block<MxdrBlockSetTxFrequency>(MXDR_SET_TX_FREQUENCY);
    block->setFrequency(value);
    block->updateCrc();

    return { block->data, sizeof(*block) };
}

enum MxdrModulation
{
    MXDR_MOD_FM,
    MXDR_MOD_P2,
    MXDR_MOD_LSM,
    MXDR_MOD_COUNT_,
};

enum MxdrPpsState
{
    MXDR_PPS_ENABLED,
    MXDR_PPS_DISABLED,
    MXDR_PPS_COUNT_,
};

enum MxdrTxInvertState
{
    MXDR_TX_NORMAL,
    MXDR_TX_INVERTED,
    MXDR_TX_COUNT_,
};

enum MxdrSimplexState
{
    MXDR_SIMPLEX_ON,
    MXDR_SIMPLEX_OFF,
    MXDR_SIMPLEX_COUNT_,
};

enum MxdrReferenceState
{
    MXDR_REF_AUTO,
    MXDR_REF_FORCE_INTERNAL,
    MXDR_REF_COUNT_,
};

// This doesn't include the mod type because those bits aren't used like flags.
enum MxdrTxModModeFlags_
{
    MXDR_MODE_FLAGS_PPS_DISABLED       = 0x08,
    MXDR_MODE_FLAGS_TX_INVERTED        = 0x10,
    MXDR_MODE_FLAGS_SIMPLEX_ON         = 0x20,
    MXDR_MODE_FLAGS_FORCE_INTERNAL_REF = 0x40,
};

using MxdrTxModModeFlags = int;

struct MxdrBlockSetTxModModeConfig : MxdrBlock<6, MXDR_FN_MSG>
{
    inline void setConfig(MxdrModulation mod, MxdrPpsState pps, MxdrTxInvertState invert,
        MxdrSimplexState simplex, MxdrReferenceState ref)
    {
        u8 mod_flags = (mod + 1) & 0xF;
        if (pps == MXDR_PPS_DISABLED)
            mod_flags |= 0x08;
        if (invert == MXDR_TX_INVERTED)
            mod_flags |= 0x10;
        if (simplex == MXDR_SIMPLEX_ON)
            mod_flags |= 0x20;
        if (ref == MXDR_REF_FORCE_INTERNAL)
            mod_flags |= 0x40;

        data[MXDR_HEADER_SIZE + 1] = mod_flags;
    }

    inline void setConfig(MxdrModulation mod, MxdrTxModModeFlags flags)
    {
        data[MXDR_HEADER_SIZE + 1] = ((mod + 1) & 0xF) | flags;
    }
};

inline MxdrMessage
mxdr_make_set_tx_mod_mode_config(MxdrModulation mod, MxdrPpsState pps, MxdrTxInvertState invert,
    MxdrSimplexState simplex, MxdrReferenceState ref)
{
    auto* block = mxdr_make_block<MxdrBlockSetTxModModeConfig>(MXDR_SET_TX_MOD_CONFIG);
    block->setConfig(mod, pps, invert, simplex, ref);
    block->updateCrc();

    return { block->data, sizeof(*block) };
}

inline MxdrMessage
mxdr_make_set_tx_mod_mode_config(MxdrModulation mod, MxdrTxModModeFlags flags)
{
    auto* block = mxdr_make_block<MxdrBlockSetTxModModeConfig>(MXDR_SET_TX_MOD_CONFIG);
    block->setConfig(mod, flags);
    block->updateCrc();

    return { block->data, sizeof(*block) };
}


struct MxdrBlockSetRx1Frequency : MxdrBlock<9, MXDR_FN_MSG>
{
    EFJ_MACRO void setValue(u32 value) { efj::set_hton_u32(&data[MXDR_HEADER_SIZE + 1], value); }
};

inline MxdrMessage
mxdr_make_set_rx1_freq(u32 value)
{
    auto* block = mxdr_make_block<MxdrBlockSetRx1Frequency>(MXDR_SET_RX1_FREQUENCY);
    block->setValue(value);
    block->updateCrc();

    return { block->data, sizeof(*block) };
}


struct MxdrBlockSetRx2Frequency : MxdrBlock<9, MXDR_FN_MSG>
{
};

struct MxdrBlockSetTxState : MxdrBlock<6, MXDR_FN_MSG>
{
    EFJ_MACRO void setOn(bool on) { data[3] = on; };
};

inline MxdrMessage
mxdr_make_set_tx_state(bool on)
{
    auto* block = mxdr_make_block<MxdrBlockSetTxState>(MXDR_SET_TX_STATE);
    block->setOn(on);
    block->updateCrc();

    return mxdr_block_to_message(*block);
}

struct MxdrBlockReqDiagInfo : MxdrBlock<5, MXDR_FN_MSG>
{
};

// I doubt we'll ever need more LEDs than we have bits an an integer type, so this
// is how we are doing it for convenience.
enum MxdrLedFlags_
{
    MXDR_LED_ALARM  = 0x001,
    MXDR_LED_TX_A   = 0x002,
    MXDR_LED_TX_B   = 0x004,
    MXDR_LED_ANALOG = 0x008,
    MXDR_LED_LINK   = 0x010,
    MXDR_LED_POWER  = 0x020,
    MXDR_LED_PHASE1 = 0x040,
    MXDR_LED_RX_A   = 0x080,
    // Byte 2
    MXDR_LED_RX_B   = 0x800,
    MXDR_LED_PHASE2 = 0x100,
    MXDR_LED_COUNT_,
};

using MxdrLedFlags = int;

struct MxdrBlockSetLedStatus : MxdrBlock<7, MXDR_FN_MSG>
{
    EFJ_MACRO void setFlags(MxdrLedFlags flags)
    {
        data[MXDR_HEADER_SIZE + 1] = (u8)flags;
        data[MXDR_HEADER_SIZE + 2] = (u8)(flags >> 8);
    }
};

inline MxdrMessage
mxdr_make_set_led_status(MxdrLedFlags flags)
{
    auto* block = mxdr_make_block<MxdrBlockSetLedStatus>(MXDR_SET_LED_STATUS);
    block->setFlags(flags);
    block->updateCrc();

    return { block->data, sizeof(*block) };
}

struct MxdrBlockFanControl : MxdrBlock<6, MXDR_FN_MSG>
{
};

struct MxdrBlockSetTxPower : MxdrBlock<6, MXDR_FN_MSG>
{
    EFJ_MACRO void setPower(u8 val) { data[MXDR_HEADER_SIZE + 1] = val; }
};

inline MxdrMessage
mxdr_make_set_tx_power(u8 value)
{
    auto* block = mxdr_make_block<MxdrBlockSetTxPower>(MXDR_SET_TX_OUTPUT_POWER);
    block->setPower(value);
    block->updateCrc();

    return { block->data, sizeof(*block) };
}

// These are the actual bit values as well as being zero-based. For setting RX2 bandwidth these values would
// be shifted over to the high bits of the bandwidth field (not that we plan to ever have an RX2, but whatever).
enum MxdrBandwidth
{
    MXDR_BANDWIDTH_WIDE         = 0x00,
    MXDR_BANDWIDTH_NARROW       = 0x01,
    MXDR_BANDWIDTH_EXTRA_NARROW = 0x02,
    MXDR_BANDWIDTH_COUNT_,
};

inline MxdrBandwidth
mxdr_freq_to_bandwidth(u32 bandwith_hz)
{
    switch (bandwith_hz)
    {
    case 12500: return MXDR_BANDWIDTH_EXTRA_NARROW;
    case 20000: return MXDR_BANDWIDTH_NARROW;
    case 25000: return MXDR_BANDWIDTH_WIDE;
    default: break;
    }

    return MXDR_BANDWIDTH_WIDE;
}

struct MxdrBlockSetRxBandwidth : MxdrBlock<6, MXDR_FN_MSG>
{
    EFJ_MACRO void setBandwidth(MxdrBandwidth width)
    {
        data[MXDR_HEADER_SIZE + 1] = width;
    }

    EFJ_MACRO void setBandwidth(u32 bandwidth_hz)
    {
        setBandwidth(mxdr_freq_to_bandwidth(bandwidth_hz));
    }
};

inline MxdrMessage
mxdr_make_set_rx_bandwidth(u32 bandwidth_hz)
{
    auto* block = mxdr_make_block<MxdrBlockSetRxBandwidth>(MXDR_SET_RX_BANDWIDTH);
    block->setBandwidth(bandwidth_hz);
    block->updateCrc();

    return { block->data, sizeof(*block) };
}

struct MxdrBlockSetTxFreqOffset : MxdrBlock<7, MXDR_FN_MSG>
{
    inline void setOffset(s16 value)
    {
        bool is_negative = false;
        if (value < 0)
        {
            is_negative = true;
            value       = -value;
        }

        u16 swapped_value = efj::get_hton_u16((u16)value);
        swapped_value |= is_negative << 7;
        efj::set_hton_u16(&data[MXDR_HEADER_SIZE + 1], swapped_value);
    }
};

inline MxdrMessage
mxdr_make_set_tx_freq_offset(s16 value)
{
    auto* block = mxdr_make_block<MxdrBlockSetTxFreqOffset>(MXDR_SET_TX_FREQ_OFFSET);
    block->setOffset(value);
    block->updateCrc();

    return { block->data, sizeof(*block) };
}

enum MxdrAlarmVswrPoint
{
    MXDR_ALARM_VSWR_DISABLED,
    MXDR_ALARM_VSWR_151,
    MXDR_ALARM_VSWR_21,
    MXDR_ALARM_VSWR_251,
    MXDR_ALARM_VSWR_31,
    MXDR_ALARM_VSWR_COUNT_,
};

struct MxdrBlockSetAlarmParams : MxdrBlock<10, MXDR_FN_MSG>
{
    EFJ_MACRO void setVswr(MxdrAlarmVswrPoint vswr) { data[MXDR_HEADER_SIZE + 1] = vswr; }
    EFJ_MACRO void setLowTxPower(u8 watts) { data[MXDR_HEADER_SIZE + 2] = watts; }
    EFJ_MACRO void setHighDcVolts(u8 volts_2x) { data[MXDR_HEADER_SIZE + 3] = volts_2x; }
    EFJ_MACRO void setLowDcVolts(u8 volts_2x) { data[MXDR_HEADER_SIZE + 4] = volts_2x; }
    EFJ_MACRO void setHighTemp(u8 high_temp_c) { data[MXDR_HEADER_SIZE + 5] = high_temp_c; }
};

inline MxdrMessage
mxdr_make_set_alarm_params(MxdrAlarmVswrPoint vswr, u8 low_tx_power, u8 high_dc_volts_2x,
    u8 low_dc_volts_2x, u8 high_temp_c)
{
    auto* block = mxdr_make_block<MxdrBlockSetAlarmParams>(MXDR_SET_ALARM_PARAMS);
    block->setVswr(vswr);
    block->setLowTxPower(low_tx_power);
    block->setHighDcVolts(high_dc_volts_2x);
    block->setLowDcVolts(low_dc_volts_2x);
    block->setHighTemp(high_temp_c);
    block->updateCrc();

    return { block->data, sizeof(*block) };
}

struct MxdrBlockReadSerialNumber : MxdrBlock<5, MXDR_FN_MSG>
{
};

struct MxdrBlockSetExciterLicenseKey : MxdrBlock<13, MXDR_FN_MSG>
{
};

struct MxdrBlockResetController : MxdrBlock<5, MXDR_FN_MSG>
{
};

inline MxdrMessage
mxdr_make_reset_controller()
{
    auto* block = mxdr_make_block<MxdrBlockResetController>(MXDR_RESET_CONTROLLER);
    block->updateCrc();

    return { block->data, sizeof(*block) };
}

struct MxdrBlockSetBootMode : MxdrBlock<5, MXDR_FN_MSG>
{
};

struct MxdrBlockSetPassThroughMode : MxdrBlock<5, MXDR_FN_MSG>
{
};

struct MxdrBlockReadExciterLicenseKey : MxdrBlock<5, MXDR_FN_MSG>
{
};

struct MxdrBlockReplyExciterLicenseKey : MxdrBlock<5, MXDR_FN_ACK>
{
};

struct MxdrBlockSetTxBootMode : MxdrBlock<5, MXDR_FN_MSG>
{
};

struct MxdrBlockSetTestPattern : MxdrBlock<7, MXDR_FN_MSG>
{
};

struct MxdrBlockReplyDiagnostic : MxdrBlock<28, MXDR_FN_ACK>
{
};

struct MxdrBlockTxStatus : MxdrBlock<6, MXDR_FN_MSG>
{
    EFJ_MACRO bool getOn() const { return data[3]; }
};

struct MxdrSerialNumberReply : MxdrBlock<16, MXDR_FN_MSG>
{
};

enum MxdrStatusBits1_
{
    MXDR_HIGH_VSWR         = 0x01,
    MXDR_LOW_TX_POWER      = 0x02,
    MXDR_PA_TEMP_FAULT     = 0x04,
    MXDR_FAN_FAULT         = 0x08,
    MXDR_MAIN_PLL_UNLOCK   = 0x10,
    MXDR_AUX_PLL_UNLOCK    = 0x20,
    MXDR_EXT_REFERENCE     = 0x40,
    MXDR_TX_USING_EXT_REF  = 0x80,
};
using MxdrStatusBits1 = int;

enum MxdrStatusBits2_
{
    MXDR_RX1_PLL_UNLOCK      = 0x01,
    MXDR_RX2_PLL_UNLOCK      = 0x02,
    MXDR_HIGH_INPUT_DC_VOLTS = 0x04,
    MXDR_LOW_INPUT_DC_VOLTS  = 0x08,
    MXDR_LOW_POWER_SHUTDOWN  = 0x10,
    MXDR_TX_LUT_CRC_ERROR    = 0x20,
    MXDR_RX1_LUT_CRC_ERROR   = 0x40,
    MXDR_RX2_LUT_CRC_ERROR   = 0x80,
};
using MxdrStatusBits2 = int;

enum MxdrStatusBits3_
{
    MXDR_TX_DPD_FAULT         = 0x01,
    MXDR_TX_PA_NOT_CALIBRATED = 0x02,
    MXDR_TX_NOT_STARTED       = 0x04,
    MXDR_TX_GENERAL_ALARM     = 0x08,
    MXDR_TX_ADLMX_FAULT       = 0x10,
    MXDR_TX_LMX_FAULT         = 0x20,
    MXDR_DPD_TIMING_ALARM     = 0x40,
    MXDR_PPS_LOCKED           = 0x80,
};
using MxdrStatusBits3 = int;

struct MxdrBlockAlarmAndStatus : MxdrBlock<11, MXDR_FN_MSG>
{
    EFJ_MACRO MxdrStatusBits1 getStatusBits1() { return data[3]; }
    EFJ_MACRO MxdrStatusBits2 getStatusBits2() { return data[4]; }
    EFJ_MACRO MxdrStatusBits3 getStatusBits3() { return data[5]; }
};

enum MxdrRxPolarity
{
    MXDR_POLARITY_INVERTED,
    MXDR_POLARITY_NOT_INVERTED,
    MXDR_POLARITY_COUNT_,
};

// The frequency part of this is the intermediate frequency (IF), and
// the low side / high side refers to inverted / not inverted respectively.
enum MxdrRxModuleId
{
    MXDR_RX_ID_LOW_SIDE_55_MHZ,
    MXDR_RX_ID_HIGH_SIDE_55_MHZ,
    MXDR_RX_ID_LOW_SIDE_90_MHZ,
    MXDR_RX_ID_HIGH_SIDE_90_MHZ,
    MXDR_RX_ID_COUNT_,
};

inline MxdrRxPolarity
mxdr_to_polarity(MxdrRxModuleId id)
{
    switch (id)
    {
    case MXDR_RX_ID_LOW_SIDE_55_MHZ: return MXDR_POLARITY_INVERTED;
    case MXDR_RX_ID_HIGH_SIDE_55_MHZ: return MXDR_POLARITY_INVERTED;
    case MXDR_RX_ID_LOW_SIDE_90_MHZ: return MXDR_POLARITY_NOT_INVERTED;
    case MXDR_RX_ID_HIGH_SIDE_90_MHZ: return MXDR_POLARITY_NOT_INVERTED;
    default:
        efj_invalid_code_path();
        return MXDR_POLARITY_INVERTED;
    }
}

struct MxdrBlockRxFrequencyAck : MxdrBlock<6, MXDR_FN_ACK>
{
    EFJ_MACRO MxdrRxModuleId getModuleId() const { return (MxdrRxModuleId)data[MXDR_HEADER_SIZE + 1]; }
};

struct MxdrOutboundBlock
{
    u8 data[128];
};

EFJ_MACRO MxdrMessage
mxdr_block_to_message(const MxdrOutboundBlock& block)
{
    MxdrMessage msg = { block.data, sizeof(block.data) };
    msg.size = mxdr_get_full_size(msg);

    return msg;
}

inline u16
calc_crc(const void* buffer, umm size)
{
    static const u16 crc_table[] =
    {
        0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241,
        0xC601, 0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440,
        0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40,
        0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841,
        0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40,
        0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80, 0xDC41,
        0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641,
        0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,
        0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
        0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441,
        0x3C00, 0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41,
        0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840,
        0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41,
        0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40,
        0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681, 0x2640,
        0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041,
        0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,
        0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
        0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41,
        0xAA01, 0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840,
        0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41,
        0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40,
        0xB401, 0x74C0, 0x7580, 0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640,
        0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080, 0xB041,
        0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241,
        0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,
        0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
        0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841,
        0x8801, 0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40,
        0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41,
        0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641,
        0x8201, 0x42C0, 0x4380, 0x8341, 0x4100, 0x81C1, 0x8081, 0x4040
    };

    u16 acc = 0;
    for (u16 i = 0; i < size; i++)
    {
        acc = crc_table[(((u8*)buffer)[i] ^ (acc & 0xff))] ^ (u8)(acc >> 8);
    }

    return acc;
}

namespace efj
{

template <> inline const char*
c_str(MxdrMessageType type)
{
    switch (type)
    {
    case MXDR_REQUEST_GEN_INFO: return "REQUEST_GEN_INFO";
    case MXDR_SET_TX_FREQUENCY: return "SET_TX_FREQUENCY";
    case MXDR_SET_TX_MOD_CONFIG: return "SET_TX_MOD_CONFIG";
    case MXDR_SET_RX1_FREQUENCY: return "SET_RX1_FREQUENCY";
    case MXDR_SET_RX2_FREQUENCY: return "SET_RX2_FREQUENCY";
    case MXDR_SET_TX_STATE: return "SET_TX_STATE";
    case MXDR_REQUEST_DIAG_INFO: return "REQUEST_DIAG_INFO";
    case MXDR_SET_LED_STATUS: return "SET_LED_STATUS";
    case MXDR_FAN_CONTROL: return "FAN_CONTROL";
    case MXDR_REPLY_PRODUCT_INFO: return "REPLY_PRODUCT_INFO";
    case MXDR_REPLY_DIAG_INFO: return "REPLY_DIAG_INFO";
    case MXDR_TX_STATUS: return "TX_STATUS";
    case MXDR_SET_TX_OUTPUT_POWER: return "SET_TX_OUTPUT_POWER";
    case MXDR_SET_RX_BANDWIDTH: return "SET_RX_BANDWIDTH";
    case MXDR_SET_TX_FREQ_OFFSET: return "SET_TX_FREQ_OFFSET";
    case MXDR_SET_ALARM_PARAMS: return "SET_ALARM_PARAMS";
    case MXDR_ALARM_AND_STATUS: return "ALARM_AND_STATUS";
    case MXDR_READ_SERIAL_NUMBER: return "READ_SERIAL_NUMBER";
    case MXDR_REPLY_SERIAL_NUMBER: return "REPLY_SERIAL_NUMBER";
    case MXDR_SET_EXCITER_LICENSE_KEY: return "SET_EXCITER_LICENSE_KEY";
    case MXDR_RESET_CONTROLLER: return "RESET_CONTROLLER";
    case MXDR_SET_BOOT_MODE: return "SET_BOOT_MODE";
    case MXDR_SERIAL_PASS_THROUGH: return "SERIAL_PASS_THROUGH";
    case MXDR_READ_EXCITER_LICENSE_KEY: return "READ_EXCITER_LICENSE_KEY";
    case MXDR_SET_TX_BOOT_MODE: return "SET_TX_BOOT_MODE";
    case MXDR_SET_TEST_PATTERN: return "SET_TEST_PATTERN";
    default: return "Unknown";
    }
}

inline const char*
mxdr_get_type_name(const MxdrMessage& msg)
{
    if (msg[1] == 0)
        return "ACK";

    // NOTE: We allow this to return "Unknown" for bad message types instead of panicing
    // on purpose, because users aren't likely expecting a crash since we can always
    // return a valid string.
    return efj::c_str((MxdrMessageType)mxdr_get_type(msg));
}


} // namespace efj
