#pragma once

#include "rtp_p25.hpp"

// We assume that dfsi rtp messages have the smallest header size with no extensions.

////////////////////////////////////////////////////////////////////////
// P25 extensions included at bottom of RTP header
//
////////////////////////////////////////////////////////////////////////
//   0               *   1           *       2       *           3
//   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// 0 |V=2|P|X|  CC   |M| PayloadType |       sequence number         |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// 4 |                           timestamp                           |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// 8 |           synchronization source (SSRC) identifier            |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+<-- end of RTP Header
//12 |0|C| HdrCnt=1  |E|  Block PT   | timestamp offset = 0      | Le|
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
//16 |ngth (10 bits) |
//   +-+-+-+-+-+-+-+-+<-- end of P25 Fixed Header
//   when C=0
// ---OR----
//   0               *   1           *       2       *           3
//   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// 0 |V=2|P|X|  CC   |M| PayloadType |       sequence number         |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// 4 |                           timestamp                           |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
// 8 |           synchronization source (SSRC) identifier            |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+<-- end of RTP Header
//12 |0|C| HdrCnt=1  |E|  Block PT   |
//   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+<-- end of P25 Fixed Header
//   when C=1
//
// We do not support padding (P), the extension bit (X), CSRC
// (contributing source) counts (CC), or the marker bit (M).
// P=0, X=0, CC=0, M=0,
//
// Payload Type= <fill>, sequence number=<fill>, timestamp=<fill>, advance by 160 each IMBE
// SSRC=<fill>
// Signal = x
// C = 0 (verbose) or 1 (compressed)
// Header Count
// E = 0 (uLaw PCM), 1 (P25-specific)
// Block Type (BT):
// if E=0:
//      0   uLaw PCM
// if E=1:
//      0   IMBE
//      2   Link Control (LC)
//      3   Encryption Sync (ES)
//      4   Low Speed Data (LSD)
//      5   Header Data Unit (HDU)
//      6   Partial HDU Part 1
//      7   Partial HDU Part 2
//      9   Start of Stream (SOS)
//     10   End of Stream   (EOS)
//     XX   Other P25 block types

//! Makes a p25 rtp message with a header and room for a given number of blocks.
inline RtpP25Message
dfsi_make_message(u32 sequence, u32 timestamp, u32 ssrc, umm blockCount, umm totalBlockSize)
{
    // The resulting P25 payload size is 1 (header-count byte) + blockCount (block-type bytes)
    // + plus the size of all the blocks.
    auto msg = rtp_make_message<RtpP25Message>(2, false, P25_DFSI_PAYLOAD_TYPE, sequence,
        timestamp, ssrc, 1 + blockCount + totalBlockSize);

    p25_set_has_signal(msg, false);
    p25_set_is_compressed(msg, true);
    p25_set_block_count(msg, blockCount);

    return msg;
}

//! Makes a p25 rtp message with a header and room for a single block.
template <typename BlockT> inline RtpP25Message
dfsi_make_message(u32 sequence, u32 timestamp, u32 ssrc)
{
    auto msg = rtp_make_message<RtpP25Message>(2, false, P25_DFSI_PAYLOAD_TYPE, sequence,
        timestamp, ssrc, 2 + sizeof(BlockT));

    p25_set_has_signal(msg, false);
    p25_set_is_compressed(msg, true);
    p25_set_block_type(msg, BlockT::Type);
    p25_set_block_count(msg, 1);

    return msg;
}

//! Makes a p25 rtp message with a header and room for a single block and returns it.
template <typename BlockT> inline RtpP25Message
dfsi_make_message(u32 sequence, u32 timestamp, u32 ssrc, BlockT** block)
{
    auto msg = rtp_make_message<RtpP25Message>(2, false, P25_DFSI_PAYLOAD_TYPE, sequence,
        timestamp, ssrc, 2 + sizeof(BlockT));

    p25_set_has_signal(msg, false);
    p25_set_is_compressed(msg, true);
    p25_set_block_type(msg, BlockT::Type);
    p25_set_block_count(msg, 1);

    *block = (BlockT*)&msg.data[P25_DFSI_HEADER_SIZE + 2];
    return msg;
}

struct DfsiBlockStartOfStream : RtpP25Block<P25_BT_START_OF_STREAM, 3>
{
    using RtpP25Block::RtpP25Block;

    DfsiBlockStartOfStream(u16 nac, u8 duid, u8 errorCount)
    {
        setNid(((nac & 0xFFF) << 4) | (duid & 0xF));
        setErrorCount(errorCount);
    }

    u16 getNid() const { return efj::get_ntoh_u16(data, 0); }
    void setNid(u16 nid) { efj::set_hton_u16(data, 0, nid); }

    u16 getNac() const { return (efj::get_ntoh_u16(data, 0) & 0xFFF0) >> 4; }
    u8 getDuid() const { return efj::get_ntoh_u16(data, 0) & 0xF; }

    u8 getErrorCount() const { return efj::get_ntoh_u16(data, 0); }
    void setErrorCount(u8 duid) { efj::set_byte(data, 2, duid); }
};

inline RtpP25Message
dfsi_make_sos(u32 sequence, u32 timestamp, u32 ssrc, u16 nac, u8 duid, u8 errorCount)
{
    auto msg = dfsi_make_message<DfsiBlockStartOfStream>(sequence, timestamp, ssrc);
    auto* block = p25_get_only_block<DfsiBlockStartOfStream>(msg);
    efj_placement_new(block) DfsiBlockStartOfStream{nac, duid, errorCount};

    return msg;
}

struct DfsiBlockEndOfStream : RtpP25Block<P25_BT_END_OF_STREAM, 0>
{
    using RtpP25Block::RtpP25Block;
};

inline RtpP25Message
dfsi_make_eos(u32 sequence, u32 timestamp, u32 ssrc)
{
    return dfsi_make_message<DfsiBlockEndOfStream>(sequence, timestamp, ssrc);
}

struct DfsiBlockTxKeyAck : RtpP25Block<P25_BT_TX_KEY_ACK, 0>
{
    using RtpP25Block::RtpP25Block;
};

inline RtpP25Message
dfsi_make_tx_key_ack(u32 sequence, u32 timestamp, u32 ssrc)
{
    auto msg = dfsi_make_message<DfsiBlockTxKeyAck>(sequence, timestamp, ssrc);
    return msg;
}

enum DfsiFrameType
{
    DFSI_FT_HDU1       = 0x60,
    DFSI_FT_HDU2       = 0x61,
    DFSI_FT_IMBE1      = 0x62,
    DFSI_FT_IMBE2      = 0x63,
    DFSI_FT_IMBE3_LC   = 0x64,
    DFSI_FT_IMBE4_LC   = 0x65,
    DFSI_FT_IMBE5_LC   = 0x66,
    DFSI_FT_IMBE6_LC   = 0x67,
    DFSI_FT_IMBE7_LC   = 0x68,
    DFSI_FT_IMBE8_LC   = 0x69,
    DFSI_FT_IMBE9_LSD  = 0x6A,
    DFSI_FT_IMBE10     = 0x6B,
    DFSI_FT_IMBE11     = 0x6C,
    DFSI_FT_IMBE12_ES  = 0x6D,
    DFSI_FT_IMBE13_ES  = 0x6E,
    DFSI_FT_IMBE14_ES  = 0x6F,
    DFSI_FT_IMBE15_ES  = 0x70,
    DFSI_FT_IMBE16_ES  = 0x71,
    DFSI_FT_IMBE17_ES  = 0x72,
    DFSI_FT_IMBE18_LSD = 0x73,
};

using DfsiVoiceHeaderBuffer = u8[18];
using DfsiVoiceStatusBuffer = u8[3];
using DfsiImbeBuffer = u8[11];
using DfsiLcEsBuffer = u8[3];
using DfsiLsdBuffer = u8[2];

struct DfsiBlockHeaderPart1 : RtpP25Block<P25_BT_VOICE_HEADER_1, 22>
{
    using RtpP25Block::RtpP25Block;
    using Header = DfsiVoiceHeaderBuffer;
    using Status = DfsiVoiceStatusBuffer;

    DfsiBlockHeaderPart1(const DfsiVoiceHeaderBuffer& header, const DfsiVoiceStatusBuffer& status)
    {
        setFrameType();
        setHeader(header);
        setStatus(status);
    }

    EFJ_MACRO void setFrameType() { efj::set_byte(data, 0, DFSI_FT_HDU1); }
    EFJ_MACRO u8 getFrameType() const { return efj::get_byte(data, 0); }

    EFJ_MACRO const Header& getHeader() const { return (Header&)data[1]; }
    EFJ_MACRO Header& getHeader() { return (Header&)data[1]; }
    EFJ_MACRO void setHeader(const Header& header) { memcpy(&data[1], &header, sizeof(Header)); }

    EFJ_MACRO const Status& getStatus() const { return (Status&)data[18]; }
    EFJ_MACRO Status& getStatus() { return (Status&)data[18]; }
    EFJ_MACRO void setStatus(const Status& status) { memcpy(&data[18], &status, sizeof(Status)); }
};

inline RtpP25Message
dfsi_make_hdu1(u32 sequence, u32 timestamp, u32 ssrc, const DfsiVoiceHeaderBuffer& header,
    const DfsiVoiceStatusBuffer& status)
{
    DfsiBlockHeaderPart1* block = nullptr;
    auto msg = dfsi_make_message<>(sequence, timestamp, ssrc, &block);
    efj_placement_new(block) DfsiBlockHeaderPart1{header, status};

    return msg;
}

struct DfsiBlockHeaderPart2 : RtpP25Block<P25_BT_VOICE_HEADER_2, 22>
{
    using RtpP25Block::RtpP25Block;
    using Header = DfsiVoiceHeaderBuffer;
    using Status = DfsiVoiceStatusBuffer;

    DfsiBlockHeaderPart2(const DfsiVoiceHeaderBuffer& header, const DfsiVoiceStatusBuffer& status)
    {
        setFrameType();
        setHeader(header);
        setStatus(status);
    }

    EFJ_MACRO void setFrameType() { efj::set_byte(data, 0, DFSI_FT_HDU2); }
    EFJ_MACRO u8 getFrameType() const { return efj::get_byte(data, 0); }

    EFJ_MACRO const Header& getHeader() const { return (Header&)data[1]; }
    EFJ_MACRO Header& getHeader() { return (Header&)data[1]; }
    EFJ_MACRO void setHeader(const Header& header) { memcpy(&data[1], &header, sizeof(Header)); }

    EFJ_MACRO const Status& getStatus() const { return (Status&)data[18]; }
    EFJ_MACRO Status& getStatus() { return (Status&)data[18]; }
    EFJ_MACRO void setStatus(const Status& status) { memcpy(&data[18], &status, sizeof(Status)); }
};

inline RtpP25Message
dfsi_make_hdu2(u32 sequence, u32 timestamp, u32 ssrc, const DfsiVoiceHeaderBuffer& header,
    const DfsiVoiceStatusBuffer& status)
{
    DfsiBlockHeaderPart2* block = nullptr;
    auto msg = dfsi_make_message<>(sequence, timestamp, ssrc, &block);
    efj_placement_new(block) DfsiBlockHeaderPart1{header, status};

    return msg;
}

struct DfsiVoiceStatus
{
    u8   et;     // Total number of errors detected in the frame.
    u8   er;     // Scaled version of er, 400 * er truncated to an integer and limited to 7.
    // NOTE: M=1, L=1 => voice can be muted, M=0, L=1 => voice can be repeated. M=1, L=0 is unspecified.
    bool mute;
    bool lost;
    u8   e4;     // Number of errors detected in vector U4, limited to 1.
    u8   e1;     // Number of errors detected in vector U0, limited to 7.
    u8   sf;     // Super-frame counter.
    u8   status; // Busy status (B) as defined in BAAA-A.
};

template <umm SizeV>
struct DfsiVoiceBlock : RtpP25Block<P25_BT_AUDIO, SizeV>
{
    using RtpP25Block<P25_BT_AUDIO, SizeV>::RtpP25Block;
    using RtpP25Block<P25_BT_AUDIO, SizeV>::data;
    using ImbeBuffer = DfsiImbeBuffer;

    EFJ_MACRO void setFrameType(u8 value) { efj::set_byte(data, 0, value); }
    EFJ_MACRO u8 getFrameType() const { return efj::get_byte(data, 0); }

    EFJ_MACRO void setVoice(const ImbeBuffer& imbe) { memcpy(&data[1], &imbe, sizeof(ImbeBuffer)); }
    EFJ_MACRO const ImbeBuffer& getVoice() const { return (ImbeBuffer&)data[1]; }
    EFJ_MACRO ImbeBuffer& getVoice() { return (ImbeBuffer&)data[1]; }

    EFJ_MACRO u8 getEt() const { return efj::get_byte(data, 12) >> 5; }
    EFJ_MACRO u8 getEr() const { return (efj::get_byte(data, 12) & 0x1C) >> 2; }
    EFJ_MACRO u8 getM() const { return (efj::get_byte(data, 12) & 0x02) >> 1; }
    EFJ_MACRO u8 getL() const { return efj::get_byte(data, 12) & 0x01; }
    EFJ_MACRO u8 getE4() const { return efj::get_byte(data, 13) >> 7; }
    EFJ_MACRO u8 getE1() const { return (efj::get_byte(data, 13) & 0x70) >> 4; }
    EFJ_MACRO u8 getSF() const { return (efj::get_byte(data, 13) & 0x0C) >> 2; }
    EFJ_MACRO u8 getB() const { return (efj::get_byte(data, 13) & 0x03); }

    EFJ_MACRO void setEt(u8 value) { data[12] = (data[12] & 0x1F) | ((value & 0x07) << 5); }
    EFJ_MACRO void setEr(u8 value) { data[12] = (data[12] & 0xE3) | ((value & 0x07) << 2); }
    EFJ_MACRO void setM(u8 value) { data[12] = (data[12] & 0xFD) | ((value & 0x01) << 1); }
    EFJ_MACRO void setL(u8 value) { data[12] = (data[12] & 0xFE) | (value & 0x01); }
    EFJ_MACRO void setE4(u8 value) { data[13] = (data[13] & 0x7F) | ((value & 0x01) << 7); }
    EFJ_MACRO void setE1(u8 value) { data[13] = (data[13] & 0x8F) | ((value & 0x07) << 4); }
    EFJ_MACRO void setSF(u8 value) { data[13] = (data[13] & 0xF3) | ((value & 0x03) << 2); }
    EFJ_MACRO void setB(u8 value) { data[13] = (data[13] & 0xFC) | (value & 0x03); }

    void setVoiceStatus(u8 et, u8 er, u8 mute, u8 lost, u8 e4, u8 e1, u8 sf, u8 status)
    {
        data[12] = ((et & 0x07) << 5) | ((er & 0x07) << 2) | ((mute & 0x01) << 1) | (lost & 0x01);
        data[13] = ((e4 & 0x01) << 7) | ((e1 & 0x07) << 4) | ((sf & 0x3) << 2) | (status & 0x03);
    }

    void setVoiceStatus(const DfsiVoiceStatus& status)
    {
        data[12] = ((status.et & 0x07) << 5) | ((status.er & 0x07) << 2) | ((status.mute & 0x01) << 1) | (status.lost & 0x01);
        data[13] = ((status.e4 & 0x01) << 7) | ((status.e1 & 0x07) << 4) | ((status.sf & 0x3) << 2) | (status.status & 0x03);
    }
};

// CAI frames 1, 2, 10, and 11
struct DfsiBlockVoice : DfsiVoiceBlock<14>
{
    using DfsiVoiceBlock::DfsiVoiceBlock;

    DfsiBlockVoice(DfsiFrameType ft, const DfsiImbeBuffer& voice, const DfsiVoiceStatus& status)
    {
        setFrameType(ft);
        setVoice(voice);
        setVoiceStatus(status);
    }
};

inline RtpP25Message
dfsi_make_voice(u32 sequence, u32 timestamp, u32 ssrc, DfsiFrameType ft,
    const DfsiImbeBuffer& buf, const DfsiVoiceStatus& status)
{
    DfsiBlockVoice* block = nullptr;
    auto msg = dfsi_make_message<>(sequence, timestamp, ssrc, &block);
    efj_placement_new(block) DfsiBlockVoice{ft, buf, status};

    return msg;
}

// CAI frames 3..8 (LC) and 12..17 (ES)
struct DfsiBlockVoiceLcEs : DfsiVoiceBlock<18>
{
    using DfsiVoiceBlock::DfsiVoiceBlock;
    using LcEsBuffer = DfsiLcEsBuffer;

    DfsiBlockVoiceLcEs(DfsiFrameType ft, const DfsiImbeBuffer& voice,
        const DfsiVoiceStatus& status, const DfsiLcEsBuffer& lcEs)
    {
        setFrameType(ft);
        setVoice(voice);
        setVoiceStatus(status);
        setLcEs(lcEs);
    }

    EFJ_MACRO void setLcEs(const LcEsBuffer& lc) { memcpy(&data[14], &lc, sizeof(LcEsBuffer)); }
    EFJ_MACRO const LcEsBuffer& getLcEs() const { return (LcEsBuffer&)data[14]; }
    EFJ_MACRO LcEsBuffer& getLcEs() { return (LcEsBuffer&)data[14]; }

    EFJ_MACRO u8 getErrorStatus() const { return efj::get_byte(data, 17); }
    EFJ_MACRO void setErrorStatus(u8 status) { efj::set_byte(data, 17, status); }
};

inline RtpP25Message
dfsi_make_voice_lc_es(u32 sequence, u32 timestamp, u32 ssrc, DfsiFrameType ft,
    const DfsiImbeBuffer& voice, const DfsiVoiceStatus& status,
    const DfsiLcEsBuffer& lcEs)
{
    DfsiBlockVoiceLcEs* block = nullptr;
    auto msg = dfsi_make_message<>(sequence, timestamp, ssrc, &block);
    efj_placement_new(block) DfsiBlockVoiceLcEs{ft, voice, status, lcEs};

    return msg;
}

// CAI frames 9 and 18
struct DfsiBlockVoiceLsd : DfsiVoiceBlock<17>
{
    using DfsiVoiceBlock::DfsiVoiceBlock;
    using LsdBuffer = DfsiLsdBuffer;

    DfsiBlockVoiceLsd(DfsiFrameType ft, const DfsiImbeBuffer& voice,
        const DfsiVoiceStatus& status, const DfsiLsdBuffer& lsd)
    {
        setFrameType(ft);
        setVoice(voice);
        setVoiceStatus(status);
        setLsd(lsd);
    }

    EFJ_MACRO void setLsd(const LsdBuffer& lsd) { memcpy(&data[14], &lsd, sizeof(LsdBuffer)); }
    EFJ_MACRO const LsdBuffer& getLsd() const { return (LsdBuffer&)data[14]; }
    EFJ_MACRO LsdBuffer& getLsd() { return (LsdBuffer&)data[14]; }

    EFJ_MACRO void setErrorStatus(u8 si, u8 sj)
    {
        efj::set_byte(data, 16, ((si & 0x3) << 2) | (sj & 0x3));
    }
};

inline RtpP25Message
dfsi_make_voice_lsd(u32 sequence, u32 timestamp, u32 ssrc, DfsiFrameType ft,
    const DfsiImbeBuffer& voice, const DfsiVoiceStatus& status,
    const DfsiLsdBuffer& lsd)
{
    DfsiBlockVoiceLsd* block = nullptr;
    auto msg = dfsi_make_message<>(sequence, timestamp, ssrc, &block);
    efj_placement_new(block) DfsiBlockVoiceLsd{ft, voice, status, lsd};

    return msg;
}

//{ EFJ extensions

struct DfsiBlockLaunchTime : RtpP25EfjBlock<P25_BT_LAUNCH_TIME, 8>
{
    using RtpP25EfjBlock::RtpP25EfjBlock;

    DfsiBlockLaunchTime(u32 sec, u32 frac)
    {
        setSeconds(sec);
        setFraction(frac);
    }

    EFJ_MACRO u32 getSeconds() const { return efj::get_ntoh_u32(data, DataOffset); }
    EFJ_MACRO void setSeconds(u32 value) { efj::set_hton_u32(data, DataOffset, value); }

    EFJ_MACRO u32 getFraction() const { return efj::get_ntoh_u32(data, DataOffset + 4); }
    EFJ_MACRO void setFraction(u32 value) { efj::set_hton_u32(data, DataOffset + 4, value); }
};

inline RtpP25Message
dfsi_make_launch_time(u32 sequence, u32 timestamp, u32 ssrc, u32 sec, u32 frac)
{
    DfsiBlockLaunchTime* block = nullptr;
    auto msg = dfsi_make_message<>(sequence, timestamp, ssrc, &block);
    efj_placement_new(block) DfsiBlockLaunchTime{sec, frac};

    return msg;
}

enum DfsiTduType
{
    DFSI_TDU_SHORT,
    DFSI_TDU_LONG,
};

struct DfsiBlockTdu : RtpP25EfjBlock<P25_BT_TERMINATOR_DATA_UNIT, 10>
{
    using RtpP25EfjBlock::RtpP25EfjBlock;
    using LcBuffer = u8[8];

    DfsiBlockTdu(DfsiTduType type, u8 format, const DfsiBlockTdu::LcBuffer& lc)
    {
        setType(type);
        setFormat(format);
        setLc(lc);
    }

    EFJ_MACRO u8 getType() const { return efj::get_byte(data, DataOffset) & 0x1; }
    EFJ_MACRO void setType(DfsiTduType type) { efj::set_byte(data, DataOffset, type); }

    EFJ_MACRO u8 getFormat() const { return efj::get_byte(data, DataOffset + 1); }
    EFJ_MACRO void setFormat(u8 format) { efj::set_byte(data, DataOffset + 1, format); }

    EFJ_MACRO const LcBuffer& getLc() const { return (LcBuffer&)data[DataOffset + 2]; }
    EFJ_MACRO LcBuffer& getLc() { return (LcBuffer&)data[DataOffset + 2]; }
    EFJ_MACRO void setLc(const LcBuffer& lc) { memcpy(&data[DataOffset + 2], &lc, sizeof(LcBuffer)); }
};

inline RtpP25Message
dfsi_make_tdu(u32 sequence, u32 timestamp, u32 ssrc, DfsiTduType type, u8 format,
    const DfsiBlockTdu::LcBuffer& lc)
{
    DfsiBlockTdu* block = nullptr;
    auto msg = dfsi_make_message<>(sequence, timestamp, ssrc, &block);
    efj_placement_new(block) DfsiBlockTdu{type, format, lc};

    return msg;
}

//}

// @Incomplete @NeedsTesting
struct DfsiBuilder
{
    efj::array<u8> blockStorage;
    efj::array<u8> blockTypes;

    DfsiBuilder(efj::allocator alloc = efj::current_allocator())
        : blockStorage(alloc), blockTypes(alloc)
    {}

    template <typename BlockT, typename... ArgsT> BlockT*
    add(ArgsT&&... args)
    {
        BlockT* block = (BlockT*)blockStorage.add_space(sizeof(BlockT));
        efj_placement_new(block) BlockT{efj::forward<ArgsT>(args)...};
        blockTypes.add((u8)BlockT::Type);

        return block;
    }

    template <typename BlockT, typename... ArgsT> RtpP25Message
    buildWith(u32 sequence, u32 timestamp, u32 ssrc, ArgsT&&... args)
    {
        RtpP25Message msg = dfsi_make_message(sequence, timestamp, ssrc, blockTypes.size() + 1,
            blockStorage.size() + sizeof(BlockT));

        // We need to set the block types first so type checking doesn't panic.
        for (umm i = 0; i < blockTypes.size(); i++)
            p25_set_block_type(msg, (RtpP25BlockType)blockTypes[i], i);

        p25_set_block_type(msg, BlockT::Type, blockTypes.size());

        auto* block = p25_get_block<BlockT>(msg, blockTypes.size());
        efj_placement_new(block) BlockT{efj::forward<ArgsT>(args)...};

        // RTP header + <block count byte> + <block type bytes>
        // (plus one to include this last block's type that isn't in the array)
        u8* blocksBegin = msg.data + P25_DFSI_HEADER_SIZE + blockTypes.size() + 2;
        memcpy(blocksBegin, blockStorage.data(), (u8*)block - blocksBegin);

        return msg;
    }
};

