#pragma once

// Combined from main and tuning config files.
struct AppConfig
{
    u32 rxFrequency                  = 0;
    u32 rxBandwidth                  = 0;
    u32 txFrequency                  = 0;
    u32 txBandwidth                  = 0;
    // 0 => MXDR_MOD_LSM => C6K_MOD_CQSPK, 1 => MXDR_MOD_FM => C6K_MOD_C4FM.
    u32 txModType                    = 0;
    u32 txPower                      = 0;
    u32 txFrequencyOffset            = 0;

    u32 txMxdrOptions                = 0;
    u32 txRxDelay                    = 0;
    u32 exciterBufferCount           = 0;

    u16 dfsiControlPort              = 0;
    u16 dfsiVoicePort                = 0;
    u32 dfsiHeartbeatIntervalSec     = 4;
    u32 dfsiControlRetryIntervalMsec = 100;
};

EFJ_DEFINE_USER_EVENT(AppConfig);

