#include <sys/stat.h>
#include <sys/sendfile.h>

bool PruInterface::init()
{
    efj::config_subscribe(CONFIG_TUNING, &PruInterface::onConfig);
    efj::subscribe(EVENT_INIT_PRU, &PruInterface::onInitPru);
    return true;
}

void PruInterface::onConfig(efj::config_event& e)
{
    if (efj::config_accepted(e))
    {
        _have_config = true;
        return;
    }

    bool rejected = false;
    efj::string driver_path = efj::config_get_text(e, "pru/pruDrvrPath", &rejected);
    efj::string binary_path = efj::config_get_text(e, "pru/pruBinaryFile", &rejected);

    if (rejected)
        return;

    efj_log_info("PRU driver path: %s", driver_path.c_str());
    efj_log_info("PRU firmware path: %s", binary_path.c_str());

    if ((_pru_binary_fd = ::open(binary_path.c_str(), O_RDONLY)) == -1)
    {
        efj::config_reject(e, efj::fmt("invalid binary path %s", binary_path.c_str()));
        return;
    }

    efj::string firmware_path = efj::fmt("%sfirmware", driver_path.c_str());
    if ((_pru_firmware_fd = ::open(firmware_path.c_str(), O_WRONLY)) == -1)
    {
        efj::config_reject(e, efj::fmt("invalid firmware path %s", firmware_path.c_str()));
        return;
    }

    efj::string run_path = efj::fmt("%srun", driver_path.c_str());
    if ((_pru_run_fd = ::open(run_path.c_str(), O_RDWR)) == -1)
    {
        efj::config_reject(e, efj::fmt("invalid run path %s", run_path.c_str()));
        return;
    }

    efj::string status_path = efj::fmt("%sstatus", driver_path.c_str());
    if ((_pru_status_fd = ::open(status_path.c_str(), O_RDONLY)) == -1)
    {
        efj::config_reject(e, efj::fmt("invalid status path %s", status_path.c_str()));
        return;
    }

    efj::string polarity_path = efj::fmt("%spolarity", driver_path.c_str());
    if ((_pru_pps_polarity_fd = ::open(polarity_path.c_str(), O_RDWR)) == -1)
    {
        efj::config_reject(e, efj::fmt("invalid polarity path %s", polarity_path.c_str()));
        return;
    }

    efj::string pps_high_path = efj::fmt("%sPPS_HIGH", driver_path.c_str());
    if ((_pru_pps_high_fd = ::open(pps_high_path.c_str(), O_RDWR)) == -1)
    {
        efj::config_reject(e, efj::fmt("invalid PPS_HIGH path %s", pps_high_path.c_str()));
        return;
    }

    efj::string pps_low_path = efj::fmt("%sPPS_LOW", driver_path.c_str());
    if ((_pru_pps_low_fd = ::open(pps_low_path.c_str(), O_RDWR)) == -1)
    {
        efj::config_reject(e, efj::fmt("invalid PPS_LOW path %s", pps_low_path.c_str()));
        return;
    }
}

bool PruInterface::resetPru()
{
    u8 stop = '0';
    if (::write(_pru_run_fd, &stop, 1) != 1)
    {
        efj_log_crit("failed to stop PRU: (fd=%d) %s", _pru_run_fd, strerror(errno));
        return false;
    }

    efj_log_info("Waiting for PRU to stop");

    u8 state = '0';
    for (;;)
    {
        lseek(_pru_run_fd, 0, SEEK_SET);
        ssize_t size = ::read(_pru_run_fd, &state, 1);
        if (size == 1 && state == '0')
            break;

        printf("PRU not ready: %d, %u\n", (int)size, state);
        usleep(500);
    }
    efj_log_info("done");

    struct stat info;
    if (fstat(_pru_binary_fd, &info) == -1)
    {
        efj_log_crit("failed to get PRU binary size");
        return false;
    }

    efj_log_info("PRU binary size: %zu", (umm)info.st_size);

    off_t offset = 0;
    while (offset < info.st_size)
    {
        ssize_t result = ::sendfile(_pru_firmware_fd, _pru_binary_fd, &offset, info.st_size - offset);
        if (result <= 0)
        {
            efj_log_crit("failed to transfer PRU code: %s", strerror(errno));
            return false;
        }
        efj_log_info("loading PRU binary: %zu/%zu", (umm)offset, (umm)info.st_size);
    }

    char pps_high = '1';
    if (::write(_pru_pps_high_fd, &pps_high, sizeof(pps_high)-1) != sizeof(pps_high)-1)
    {
        efj_log_crit("failed to write PPS_HIGH");
        return false;
    }

    char pps_low[] = "2000000";
    if (::write(_pru_pps_low_fd, &pps_low, sizeof(pps_low)-1) != sizeof(pps_low)-1)
    {
        efj_log_crit("failed to write PPS_LOW");
        return false;
    }

    char pps_polarity = '0';
    if (::write(_pru_pps_polarity_fd, &pps_polarity, 1) != 1)
    {
        efj_log_crit("failed to write PPS_POLARITY");
        return false;
    }
    ::close(_pru_pps_polarity_fd);

    u8 start = '1';
    if (::write(_pru_run_fd, &start, 1) != 1)
    {
        efj_log_crit("failed to start PRU");
        return false;
    }

    return true;
}

void PruInterface::onInitPru(const efj::user_event& e)
{
    efj_log_debug("Initting PRU");
    if (!resetPru())
    {
        efj_log_crit("failed to reset PRU");
        efj_trigger(EVENT_PRU_INITTED, PruInitted{false});
    }
    else
    {
        efj_trigger(EVENT_PRU_INITTED, PruInitted{true});
    }
}

