#include "dsp_driver.hpp"

bool DspInterface::init()
{
    _dsp_image_arena = efj::allocate_arena("DSP Image", 4_KB);

    int fd = ::open(_dsp_path, O_RDWR);
    if (fd == -1)
    {
        efj_log_crit("Failed to open DSP (%s)", _dsp_path);
        return false;
    }

    int dsp_image_fd = ::open(_dsp_image_path, O_RDONLY);
    if (dsp_image_fd == -1)
    {
        efj_log_crit("No DSP image found");
        return false;
    }
    ::close(dsp_image_fd);

    {
        // Keep the code around in in case we want to reload it during operation in the future.
        // TODO: Would we ever care? We could also map the file to do this.
        efj_allocator_scope(_dsp_image_arena);
        _dsp_image_buf = efj::read_file(_dsp_image_path);
        if (_dsp_image_buf.data == nullptr)
        {
            efj_log_crit("Failed to load DSP image");
            return false;
        }
    }

    _dsp_raw_fd = fd;
    efj::subscribe(EVENT_INIT_DSP, &DspInterface::onInitDsp);
    return true;
}

void DspInterface::onInitDsp(const efj::user_event& e)
{
    // FIXME: We should probably handle initialization like the MXDR interface.
    // If we were called to re-init after a failure, we would crash.

    efj_log_info("Initting DSP");
    if (!resetDsp(_dsp_raw_fd))
    {
        ::close(_dsp_raw_fd);
        efj_trigger(EVENT_DSP_INITTED, DspInitted{false});
        return;
    }

    efj::fd_add_os_handle("DSP", _dsp_raw_fd, efj::io_in, &_dsp_fd, &DspInterface::onDspEvent);
    efj_trigger(EVENT_DSP_INITTED, DspInitted{true});
}

bool DspInterface::resetDsp(int fd)
{
    if (fd == -1)
        return false;

    dsp_reset(fd);

    if (!dsp_load_software(fd, _dsp_image_buf.data, _dsp_image_buf.size))
        return false;

    dsp_execute(fd);

    return true;
}

void DspInterface::consumeOutboundC6k(const C6kMessage& msg)
{
    efj_log_debug("-> %s: %s", efj::c_str(c6k_get_type(msg)), efj::to_hex(msg).c_str());
    umm written = efj::fd_write(_dsp_fd, msg.data, msg.size);
    if (written != msg.size) {
        efj_log_crit("%zu != %zu\n", written, msg.size);
        efj_panic("Failed to write to DSP");
    }
}

void DspInterface::onDspEvent(efj::fd_event& e)
{
    char buf[4096] alignas(u32);

    if (e.events == efj::io_in)
    {
        umm size = efj::fd_read(_dsp_fd, buf, sizeof(buf));
        if (size > 0)
        {
            u32 id = *(u32*)buf;
            if (id == C6K_ERROR)
            {
                efj_log_crit("DSP error: %s", buf + 8);
            }

            // TODO: validate size?
            C6kMessage msg = { buf, size };
            efj_log_debug("<- %s: %s", efj::c_str(c6k_get_type(msg)), efj::to_hex(msg).c_str());

            if (c6k_get_type(msg) == C6K_RECEIVE_TIME_UPDATE)
            {
                efj_log_debug("Size: %zu, bytes: %s\n", size, efj::to_hex(msg).c_str());
                auto* block = c6k_get_block<C6kBlockTimeUpdate>(msg);
                efj_log_debug("Second: %u, cycles: %u", block->getSecond(), block->getI2sCycleCount());
            }

            //if (id == C6K_RECEIVE_HEADER_PART_1)
            //    efj_log_info("Time in: %llu us", efj::now_usec().value);

            efj_produce(InboundC6k, msg);
        }
    }
}

