/*
 * Device driver for ARM/DSP communication
 *
 * Copyright 2009 EF Johnson
 *
 * Version Information:
 * 2 - Modify DSP code loading to speed up boot time - ADV#677 (BSN)
 * 1 - Baseline
 */

#ifndef SP_COMM_H_
#define SP_COMM_H_

/************************************************************************************************************/
/* Included Header Files                                                                                                                */
/************************************************************************************************************/
#include <linux/ioctl.h>

/************************************************************************************************************/
/* Constants                                                                                                                                */
/************************************************************************************************************/
#define SP_COMM_IOC_MAGIC          0x91
#define SP_COMM_IOC_WRITE_SECTION  _IOW(SP_COMM_IOC_MAGIC, 0x00, struct dsp_mem_section)
#define SP_COMM_IOC_READ_SECTION   _IOR(SP_COMM_IOC_MAGIC, 0x01, struct dsp_mem_section)
#define SP_COMM_IOC_RESET          _IO (SP_COMM_IOC_MAGIC, 0x02)
#define SP_COMM_IOC_RUN            _IOW(SP_COMM_IOC_MAGIC, 0x03, void*)

#ifdef EFJ_PORTABLE
#define SP_COMM_DDR_REGION_ADDR     0xC1C00000
#define SP_COMM_DDR_REGION_SIZE     0x400000
#elif defined(EFJ_MOBILE)
#define SP_COMM_DDR_REGION_ADDR     0xC3800000
#define SP_COMM_DDR_REGION_SIZE     0x800000
#else
#define SP_COMM_DDR_REGION_ADDR     0xCE000000
#define SP_COMM_DDR_REGION_SIZE     0x2000000
#endif
#define SP_COMM_SHARED_REGION_ADDR  0x80000000
#define SP_COMM_SHARED_REGION_SIZE  0x20000
#define SP_COMM_L2_REGION_ADDR      0x11800000
#define SP_COMM_L2_REGION_SIZE      0x40000
#define SP_COMM_L1P_REGION_ADDR     0x11E00000
#define SP_COMM_L1P_REGION_SIZE     0x8000
#define SP_COMM_L1D_REGION_ADDR     0x11F00000
#define SP_COMM_L1D_REGION_SIZE     0x8000

/************************************************************************************************************/
/* Type Definitions                                                                                                                     */
/************************************************************************************************************/
struct dsp_mem_section
{
    void*          address;
    size_t         size;
    unsigned char* mapped_memory;
};

#endif /* SP_COMM_H_*/
