#pragma once

struct MxdrInterface
{
    EFJ_OBJECT("MXDR Interface");
    EFJ_INPUT(OutboundMxdr, const MxdrMessage);
    EFJ_OUTPUT(InboundMxdr, MxdrMessage);

    enum State
    {
        // Waiting for user event to tell us to reset the controller.
        WaitingForResetEvent,
        // Waiting for the ack for the Reset Controller message.
        WaitingForResetAck,
        // Waiting for the initial Reply Product Info from the controller.
        WaitingForProductInfo,
        // Actively sending and receiving messages.
        Active,
    };

    const char* _path = "/dev/ttyS1";

    efj::fd _mxdr_fd;
    int _mxdr_raw_fd = -1;

    //printf("On mxdr event: %d\n", e.events);
    char _parse_buf[512];
    size_t _parse_offset = 0;
    size_t _cur_message_size = 0;

    u32 _ack_count = 0;
    State _state = WaitingForResetEvent;

    bool init();
    void onEvent(const efj::user_event& e);

    void consumeOutboundMxdr(const MxdrMessage& msg);
    void writeMessage(const MxdrMessage& msg);
    void onMxdrEvent(efj::fd_event& e);

    void requireReset();
    bool waitForResetAck();
    bool waitForProductInfo();
    void consumeBytes(size_t n);
};

