#include <sys/ioctl.h>
#include <sys/mman.h>
#include "sp_comm.h"

static void* const    SPDL_ResetVector      = (void *)0x11800000;
static const uint16_t SPDL_Version          = 0xC2;
static const uint16_t SPDL_MagicNumber      = 0x108;
static const uint32_t SPDL_SectionFlagsMask = 0xFF;

extern bool
dsp_reset(int fd)
{
   int result = ::ioctl(fd, SP_COMM_IOC_RESET);

   return result == 0;
}

static void
unmap_sections(const DspMemSection* sections, size_t n)
{
    for (size_t i = 0; i < n; i++)
    {
        const DspMemSection* section = &sections[i];

        if (section->mappedMemory != nullptr && section->mappedMemory != MAP_FAILED)
            efj_panic_if(::munmap(section->mappedMemory, section->size) == -1);
    }
}

struct SRecord
{
    const char* error     = nullptr;
    uint32_t    address   = 0;
    uint8_t     type      = 0;
    uint8_t     dataSize  = 0;
    char        data[64];
    uint8_t     checksum  = 0;
};

static uint8_t
get_hex_digit_value(uint8_t ascii, bool* validHex)
{
    if (ascii >= '0' && ascii <= '9')
        return ascii - '0';
    if (ascii >= 'A' && ascii <= 'F')
        return ascii - 'A' + 10;
    if (ascii >= 'a' && ascii <= 'f')
        return ascii - 'a' + 10;

    *validHex = false;
    return 0;
}

static uint8_t
get_hex_byte(const uint8_t* hex, bool* validHex)
{
    return (get_hex_digit_value(hex[0], validHex) << 4) | get_hex_digit_value(hex[1], validHex);
}

static uint32_t
get_hex_address(const uint8_t* hex, size_t hexCharCount, bool* validHex)
{
    uint32_t result = get_hex_byte(&hex[0], validHex);
    for (size_t i = 2; i < hexCharCount << 1; i += 2)
        result = (result << 8) | get_hex_byte(&hex[i], validHex);

    return result;
}

static efj::buffer
get_line(const void* buf, size_t size, size_t* progress)
{
    const uint8_t* line = (const uint8_t*)buf + *progress;

    size_t availableSize = size - *progress;
    size_t lineSizeWithEnding = availableSize;
    size_t i = 0;
    for ( ; i < availableSize; i++)
    {
        if (line[i] == '\r')
        {
            if (i + 1 < availableSize && line[i + 1] == '\n')
                lineSizeWithEnding = i + 2;
            else
                lineSizeWithEnding = i + 1;

            break;
        }
        else if (line[i] == '\n')
        {
            lineSizeWithEnding = i + 1;
            break;
        }
    }

    *progress += lineSizeWithEnding;
    return { (uint8_t*)line, i };
}

static SRecord
parse_srecord(const uint8_t* line, size_t lineSize, bool validateCs = true)
{
    SRecord result;

    constexpr size_t kMinRecordSize = 12;

    if (lineSize < kMinRecordSize) { result.error = "line not big enough"; return result; }
    if (line[0] != 'S') {result.error = "line doesn't start with 'S'"; return result; }

    bool validHex = true;

    uint8_t asciiType = line[1];
    // The "Byte Count" field is the number of hex pairs following this field, including the CS.
    size_t byteCount = get_hex_byte(&line[2], &validHex);
    size_t payloadCharCount = byteCount << 1;
    //printf("payload size: %zu (0x%c%c)\n", payloadCharCount, line[2], line[3]);

    if (!validHex) { result.error = "invalid hex"; return result; }
    if (4 + payloadCharCount > lineSize) { result.error = "line not big enough"; return result; }

    size_t addressByteCount = 0;

    switch (asciiType)
    {
    case '0': result.type = 0; addressByteCount = 2; break;
    case '1': result.type = 1; addressByteCount = 2; break;
    case '2': result.type = 2; addressByteCount = 3; break;
    case '3': result.type = 3; addressByteCount = 4; break;
    case '4': result.type = 4; return result;
    case '5': result.type = 5; addressByteCount = 2; break;
    case '6': result.type = 6; addressByteCount = 3; break;
    case '7': result.type = 7; addressByteCount = 4; break;
    case '8': result.type = 8; addressByteCount = 3; break;
    case '9': result.type = 9; addressByteCount = 2; break;
    default:
        return result;
    }

    uint32_t address = get_hex_address(&line[4], addressByteCount, &validHex);

    if (!validHex) { result.error = "invalid address hex"; return result; }

    size_t addressCharCount = addressByteCount << 1;
    size_t dataCharOffset   = 4 + (addressCharCount);
    size_t dataCharCount    = payloadCharCount - addressCharCount - 2; // - 2 for CS.

    size_t dataByteCount = 0; // We use this to count the represented bytes while storing them.
    for (size_t hexI = dataCharOffset; hexI < dataCharOffset + dataCharCount; hexI += 2, dataByteCount++)
        result.data[dataByteCount] = get_hex_byte(&line[hexI], &validHex);

    if (!validHex) { result.error = "invalid data hex"; return result; }

    uint8_t checksum = get_hex_byte(&line[lineSize-2], &validHex);
    if (!validHex) { result.error = "invalid checksum hex"; return result; }

    if (validateCs)
    {
        // The byteCount is only one byte.
        size_t sum = byteCount + ((u8*)&address)[0] + ((u8*)&address)[1]
            + ((u8*)&address)[2] + ((u8*)&address)[3];

        for (size_t i = 0; i < dataByteCount; i++)
            sum += result.data[i];

        //printf("%x %x %x %x %x ", byteCount, ((u8*)&address)[0], ((u8*)&address)[1], ((u8*)&address)[2], ((u8*)&address)[3]);

        //for (size_t i = 0; i < dataByteCount; i++)
        //    printf("%x ", result.data[i]);
        //printf(" sum=%x\n", sum);

        //printf("CS: %zu %zu/%zu\n", dataByteCount, (~sum & 0xFF), checksum);
        if (((~sum & 0xFF)) != checksum) { result.error = "invalid checksum"; return result; }
    }

    result.address  = address;
    result.dataSize = dataByteCount;
    result.checksum = checksum;

    return result;
}

extern bool
dsp_load_software(int fd, const void* buf, size_t size)
{
    DspMemSection sections[] =
    {
        // Address                    Size                        Mapped Memory
        { SP_COMM_DDR_REGION_ADDR,    SP_COMM_DDR_REGION_SIZE,    NULL },
        { SP_COMM_SHARED_REGION_ADDR, SP_COMM_SHARED_REGION_SIZE, NULL },
        { SP_COMM_L2_REGION_ADDR,     SP_COMM_L2_REGION_SIZE,     NULL },
        { SP_COMM_L1P_REGION_ADDR,    SP_COMM_L1P_REGION_SIZE,    NULL },
        { SP_COMM_L1D_REGION_ADDR,    SP_COMM_L1D_REGION_SIZE,    NULL }
    };

    for (size_t i = 0; i < efj::array_size(sections); i++)
    {
        DspMemSection* section = &sections[i];
        section->mappedMemory = (uint8_t*)mmap(NULL, section->size, PROT_WRITE, MAP_SHARED, fd, section->address);

        if (section->mappedMemory == MAP_FAILED)
        {
            printf("Mapping section %d, %zu\n", fd, i);
            unmap_sections(sections, efj::array_size(sections));
            return false;
        }
    }

    size_t lineNumber = 1;
    size_t progress = 0;
    for ( ; progress < size; lineNumber++)
    {
        efj::buffer line = get_line(buf, size, &progress);
        //printf("Line %zu size = %zu, offset = %zu: %.*s\n", lineNumber, line.size, progress, (int)line.size, line.data);
        if (line.size == 0)
            continue;


        SRecord rec = parse_srecord(line.data, line.size, true);
        if (rec.error)
        {
            // 4 is a reserved record type.
            if (rec.type == 4)
                continue;

            efj_log_crit("Bad record %u on line %zu: %s\n", rec.type, lineNumber, rec.error);
            return false;
        }

        // Skip non-data records. Records 1-3 are all we care about. In practice, the DSP code's records
        // should be S0, S3..., S7.
        switch (rec.type)
        {
        case 1: break;
        case 2: break;
        case 3: break;
        default:
            continue;
        }

        //printf("Data: ");
        //for (int i = 0; i < rec.dataSize; i++)
        //    printf("%u", rec.data[i]);
        //printf("\n");

        bool foundSection = false;
        for (size_t i = 0; i < efj::array_size(sections); i++)
        {
            DspMemSection* section  = &sections[i];
            uintptr_t sectionBegin = section->address;
            uintptr_t sectionEnd   = section->address + section->size;

            if (rec.address >= sectionBegin && rec.address + rec.dataSize <= sectionEnd)
            {
                uint8_t* storeAddress = &section->mappedMemory[rec.address - sectionBegin];
                //printf("Writing %zu bytes to address %p\n", rec.dataSize, storeAddress);
                for (size_t j = 0; j < rec.dataSize; j++)
                    storeAddress[j] = rec.data[j];

                foundSection = true;
                break;
            }
        }

        if (!foundSection)
        {
            efj_log_crit("Failed to find a section for 0x%x\n", rec.address);
            unmap_sections(sections, efj::array_size(sections));
            return false;
        }
    }

    return true;
}

extern bool
dsp_execute(int fd)
{
   int result = ::ioctl(fd, SP_COMM_IOC_RUN, SPDL_ResetVector);
   return result != -1;
}

