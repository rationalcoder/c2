#pragma once

//! Provides an acking and deduplication service when communicating with the MXDR controller.
//! The MXDR serial communcation has a few annoyances that this object deals with (as of 9/23/21):
//! * The controller does not respond to messages until we recieve the first ack, which is a
//!   respond-general-info.
//! * For some reason, we can't send multiple messages and rely on kernel queueing or whatever
//!   you normally can that allows you to send a burst of messages. I assume this is some sort of serial port
//!   configuration thing, but it may also just be a controller logic thing @NeedsTesting. Instead,
//!   we must wait for an ack and send only one message at a time.
//! * Since we have to keep an internal queue of outbound MXDR messages, this object deduplicates
//!   outbound messages, overwriting any messages still in the queue with the latest version.
//!
struct MxdrTransporter
{
    EFJ_OBJECT("MXDR Transporter");
    EFJ_INPUT(InboundMxdr,  MxdrMessage);
    EFJ_INPUT(OutboundMxdr, const MxdrMessage);

    EFJ_OUTPUT(InboundMxdr,  const MxdrMessage);
    EFJ_OUTPUT(OutboundMxdr, const MxdrMessage);

    // There are only a few outbound MXDR messages, and they aren't that big.
    efj::block_queue<MxdrOutboundBlock, 20> _outbound_queue;
    // We queue messages outside of times when the mxdr controller is ready, as determined
    // by user events.
    bool _mxdr_controller_ready = false;
    bool _waiting_for_ack = true;

    MxdrMessageType _last_sent_type = MXDR_REQUEST_GEN_INFO;

    bool init();

    void onEvent(const efj::user_event& e);

    void consumeOutboundMxdr(const MxdrMessage& msg);
    void consumeInboundMxdr(MxdrMessage& msg);

    void sendQueuedMessage();
    void sendOutboundMessage(const MxdrMessage& msg);
};

