#include "hardware_controller.hpp"

bool HardwareController::init()
{
    efj::subscribe_cached(EVENT_CONFIG, &_config, &HardwareController::onConfig, efj::sub_sync);

    efj::subscribe(EVENT_DFSI_CONNECT,    &HardwareController::onControlEvent);
    efj::subscribe(EVENT_DFSI_DISCONNECT, &HardwareController::onControlEvent);
    efj::subscribe(EVENT_UPDATE_LEDS,     &HardwareController::onControlEvent);
    efj::subscribe(EVENT_SET_TX_MODE,     &HardwareController::onControlEvent);
    efj::subscribe(EVENT_SET_RX_MODE,     &HardwareController::onControlEvent);
    efj::subscribe(EVENT_ALLOCATE_DSP,    &HardwareController::onControlEvent);


    efj::subscribe(EVENT_PRU_INITTED,  &HardwareController::onHardwareEvent);
    efj::subscribe(EVENT_DSP_INITTED,  &HardwareController::onHardwareEvent);
    efj::subscribe(EVENT_MXDR_INITTED, &HardwareController::onHardwareEvent);
    efj::subscribe(EVENT_MXDR_DOWN,    &HardwareController::onHardwareEvent);

    return true;
}

void HardwareController::onConfig(const AppConfig&)
{
    efj_log_info("Initting PRU");
    efj_trigger(EVENT_INIT_PRU, PruInit());
}

void HardwareController::onHardwareEvent(const efj::user_event& e)
{
    switch (e.type)
    {
    case EVENT_PRU_INITTED:
    {
        efj_log_info("PRU initted. Initting MXDR");
        auto* pruInitted = e.as<PruInitted>();
        if (pruInitted->result)
            efj_trigger(EVENT_INIT_MXDR, MxdrInit());
        break;
    }
    case EVENT_MXDR_INITTED:
    {
        auto* mxdrInitted = e.as<MxdrInitted>();
        if (!mxdrInitted->result)
        {
            efj_log_crit("the MXDR interface failed to initialize. Retrying");
            _hardwareReady = false;
            efj_trigger(EVENT_HARDWARE_STATUS, HardwareStatus{HARDWARE_STATUS_NOT_READY});
            efj_trigger(EVENT_INIT_MXDR, MxdrInit());
            break;
        }

        efj_log_info("MXDR initted. Configuring hardware and initting DSP");

        // We can't configure the DSP until we get the RX frequency ack from the MXDR controller.
        configureMxdr();
        break;
    }
    case EVENT_MXDR_DOWN:
    {
        efj_log_info("MXDR down. Re-initting MXDR");
        _hardwareReady = false;
        efj_trigger(EVENT_HARDWARE_STATUS, HardwareStatus{HARDWARE_STATUS_NOT_READY});
        efj_trigger(EVENT_INIT_MXDR, MxdrInit());
        break;
    }
    case EVENT_DSP_INITTED:
    {
        efj_log_info("DSP initted. Configuring DSP");
        auto* dspInitted = e.as<DspInitted>();

        if (!dspInitted->result)
        {
            efj_log_crit("the DSP interface failed to initialize");
            break;
        }

        break;
    }
    default:
        break;
    }
}

void HardwareController::onControlEvent(const efj::user_event& e)
{
    efj_log_info("Event: %s", efj::c_str((EventType)e.type));
    switch (e.type)
    {
    case EVENT_DFSI_CONNECT:
    {
        if (!(_leds & MXDR_LED_LINK))
        {
            _leds |= MXDR_LED_LINK;

            auto set_leds = mxdr_make_set_led_status(_leds);
            sendOutboundMxdr(set_leds);
        }

        break;
    }
    case EVENT_DFSI_DISCONNECT:
    {
        if (_leds & MXDR_LED_LINK)
        {
            _leds &= ~MXDR_LED_LINK;

            auto set_leds = mxdr_make_set_led_status(_leds);
            sendOutboundMxdr(set_leds);
        }

        break;
    }
    case EVENT_UPDATE_LEDS:
    {
        auto* update_leds = e.as<UpdateLeds>();
        _leds &= ~update_leds->leds_off;
        _leds |= update_leds->leds_on;

        auto set_leds = mxdr_make_set_led_status(_leds);
        sendOutboundMxdr(set_leds);
        break;
    }
    // NOTE: This hardware will be "ready" regardless of DFSI configuration status.
    // If a call comes through before the dfsi commands to set the operating mode come in for example,
    // that's someone else's problem for now.
    case EVENT_SET_TX_MODE:
    {
        auto* setTxMode = e.as<SetTxMode>();

        auto setFreq = mxdr_make_set_tx_freq(setTxMode->freq);
        efj_log_debug("Set TX Freq: %u", setTxMode->freq);
        sendOutboundMxdr(setFreq);

        auto setPower = mxdr_make_set_tx_power(setTxMode->power);
        efj_log_debug("Set TX Power: %u", setTxMode->power);
        sendOutboundMxdr(setPower);

        _txMode = *setTxMode;
        _latestTxMode = &_txMode;
        break;
    }
    case EVENT_SET_RX_MODE:
    {
        auto* setRxMode = e.as<SetRxMode>();

        auto setFreq = mxdr_make_set_rx1_freq(setRxMode->freq);
        efj_log_debug("Set RX Freq: %u, %0x%0x%0x%0x", setRxMode->freq, setFreq.data[3],
            setFreq.data[4], setFreq.data[5], setFreq.data[6]);
        sendOutboundMxdr(setFreq);

        _rxMode = *setRxMode;
        _latestRxMode = &_rxMode;
        break;
    }
    case EVENT_ALLOCATE_DSP:
    {
        const AllocateDsp* allocDsp = e.as<AllocateDsp>();
        if (_hardwareReady && _c6kStarted)
        {
            // TODO: Validation.
            DspGrant response = {allocDsp->inbound, allocDsp->outbound};
            efj::print("Sender: %s\n", e.sender->name);
            efj_send_event(e.sender, EVENT_DSP_GRANT, response);
        }
        else
        {
            efj::print("Sender: %s\n", e.sender->name);
            DspDeny response = {allocDsp->inbound, allocDsp->outbound};
            efj_send_event(e.sender, EVENT_DSP_DENY, response);
        }

        break;
    }
    default:
        break;
    }
}

void HardwareController::consumeInboundC6k(const C6kMessage& msg)
{
    C6kMessageType type = c6k_get_type(msg);
    efj_log_debug("Inbound c6k: %s", efj::c_str(type));

    if (c6k_get_type(msg) == C6K_C6000_STARTED)
    {
        configureDsp();

        _c6kStarted = true;
        _hardwareReady = true;

        HardwareStatus status = {};
        status.value = HARDWARE_STATUS_READY;
        status.p1_tx_mod = _latestP1TxMod;

        efj_trigger(EVENT_HARDWARE_STATUS, status);
    }

    // We start passing messages once we have a valid config and the hardware is initialized.
    if (_hardwareReady)
    {
        efj_produce(InboundC6k, msg);
    }
    else
    {
        efj_log_debug("Not sending inbound c6k: started:%d, ready:%d", _c6kStarted, _hardwareReady);
    }
}

void HardwareController::consumeInboundMxdr(const MxdrMessage& msg)
{
    MxdrMessageType type = (MxdrMessageType)mxdr_get_type(msg);

    efj_log_debug("Got MXDR message: %s (%s)", efj::c_str(type),
        mxdr_is_ack(msg) ? "ack" : "data");

    if (mxdr_get_type(msg) == MXDR_SET_RX1_FREQUENCY)
    {
        auto* block = mxdr_get_block<MxdrBlockRxFrequencyAck>(msg);
        _rxModuleId = block->getModuleId();

        if (!_c6kStarted && !_inittedDsp)
        {
            _inittedDsp = true;
            efj_trigger(EVENT_INIT_DSP, DspInit());
        }
    }

    efj_produce(InboundMxdr, msg);
}

void HardwareController::consumeOutboundC6k(const C6kMessage& msg)
{
    efj_produce(OutboundC6k, msg);
}

void HardwareController::consumeOutboundMxdr(const MxdrMessage& msg)
{
    efj_produce(OutboundMxdr, msg);
}

void HardwareController::sendOutboundMxdr(const MxdrMessage& msg)
{
    efj_produce(OutboundMxdr, msg);
}

void HardwareController::configureHardware()
{
}

void HardwareController::configureMxdr()
{
    HardwareStatus status = {};
    status.value = HARDWARE_STATUS_CONFIG;
    efj_trigger(EVENT_HARDWARE_STATUS, status);

    auto set_leds           = mxdr_make_set_led_status(_leds);
    auto set_rx_freq        = mxdr_make_set_rx1_freq(_latestRxMode ? _latestRxMode->freq : _config->rxFrequency);
    auto set_rx_bandwidth   = mxdr_make_set_rx_bandwidth(_config->rxBandwidth);
    auto set_tx_freq        = mxdr_make_set_tx_freq(_latestTxMode ? _latestTxMode->freq : _config->txFrequency);
    auto set_tx_freq_offset = mxdr_make_set_tx_freq_offset(_config->txFrequencyOffset);
    auto set_tx_power       = mxdr_make_set_tx_power(_latestTxMode ? _latestTxMode->power : _config->txPower);
    //auto set_tx_power       = mxdr_make_set_tx_power(25); // XXX

    // We have two modulation options to set, one for the MXDR interface and one for the C6K interface.
    // The tx mod type value configures both of them implicitly. The C6K information is needed by
    // any object that starts a phase1 stream since the modulation parameter is sent with the start-outbound
    // message. We broadcast this out with the HARDWARE_STATUS_READY event.
    //
    // 0 => MXDR_MOD_LSM => C6K_MOD_CQPSK
    // 1 => MXDR_MOD_FM => C6K_MOD_C4FM
    //
    MxdrModulation mxdrModulation = MXDR_MOD_LSM;
    C6kModulation c6kModulation = C6K_MOD_CQPSK;
    if (_config->txModType == 1)
    {
        mxdrModulation = MXDR_MOD_FM;
        c6kModulation = C6K_MOD_C4FM;
    }
    _latestP1TxMod = c6kModulation;

    u32 mode_flags = _config->txMxdrOptions;

    auto set_tx_mod_mode = mxdr_make_set_tx_mod_mode_config(mxdrModulation, mode_flags);
    // TODO: from config
    auto set_alarm_params = mxdr_make_set_alarm_params(MXDR_ALARM_VSWR_31, 0, 110, 70, 85);

    efj_log_debug("Set TX Mode: %u", _latestTxMode ? _latestTxMode->freq : _config->txFrequency);
    efj_log_debug("Set RX Mode: %u", _latestRxMode ? _latestRxMode->freq : _config->rxFrequency);
    efj_log_debug("Setting LEDS: to 0x%x", _leds);
    sendOutboundMxdr(set_leds);
    sendOutboundMxdr(set_rx_freq);
    sendOutboundMxdr(set_rx_bandwidth);
    sendOutboundMxdr(set_tx_freq);
    sendOutboundMxdr(set_tx_freq_offset);
    sendOutboundMxdr(set_tx_power);
    sendOutboundMxdr(set_tx_mod_mode);
    sendOutboundMxdr(set_alarm_params);
}

void HardwareController::configureDsp()
{
    C6kInterFreq inter_freq = mxdr_to_c6k_inter_freq(_rxModuleId);

    efj_log_debug("TX RX delay: %u", _config->txRxDelay);
    auto msg = c6k_make_configure_hardware(inter_freq, inter_freq, _config->txRxDelay);
    efj_produce(OutboundC6k, msg);

    msg = c6k_make_configure_software(_config->exciterBufferCount);
    efj_produce(OutboundC6k, msg);
}

