#include "dfsi_voice_client.hpp"

bool DfsiVoiceClient::init()
{
    efj::subscribe(EVENT_DFSI_CONNECT, &DfsiVoiceClient::onDfsiConnectionEvent);
    efj::subscribe(EVENT_DFSI_DISCONNECT, &DfsiVoiceClient::onDfsiConnectionEvent);

    return true;
}

void DfsiVoiceClient::onDfsiConnectionEvent(const efj::user_event& e)
{
    if (e.type == EVENT_DFSI_CONNECT)
    {
        // TODO: Grab SSRC.
        _dfsi_connect = *e.as<DfsiConnectEvent>();
        _sequence_number = 0;
        _timestamp_origin = efj::now_8khz();
        _connected = true;
    }
    else if (e.type == EVENT_DFSI_DISCONNECT)
    {
        _connected = false;
    }
}

void DfsiVoiceClient::consumeOutboundVoice(const RtpP25Message& msg)
{
    bool got_sos = false;
    bool has_at_least_one_valid_block = false;

    //u8 count = p25_get_block_count(msg);
    efj::log_unformatted(true);
    //efj_log_info("Size: %zu Block count=%u (%x),\n{", msg.size, count, msg[P25_DFSI_HEADER_SIZE]);
    for (auto it = p25_iterate_blocks(msg); p25_is_valid(it); p25_advance(it))
    {
        u8 bt = p25_get_block_type(it);
        if (bt == P25_BT_START_OF_STREAM || bt == P25_BT_ANALOG_SOS_TIA || bt == P25_BT_ANALOG_SOS_EFJ)
            got_sos = true;

        //efj_log_info("BT=%u(%s) ", bt, efj::c_str((RtpBlockType)bt));

        bool handled_block = false;
        p25_get_block_size(bt, &handled_block);
        if (handled_block)
            has_at_least_one_valid_block = true;

        if (bt == P25_BT_LAUNCH_TIME)
        {
            //auto* lt = dfsi_get_block<DfsiBlockLaunchTime>(it);
            //efj::string bytes = efj::to_hex(lt->data, sizeof(DfsiBlockLaunchTime));
            //efj_log_debug("LT bytes: %s\n", bytes.c_str());

            //efj::ntp_time launchTime = { lt->getSeconds(), lt->getFraction() };
            //efj::posix_time ltPosix = efj::to_ptime(launchTime);
            //efj::posix_time now = efj::now_ptime();

            //s64 diffNs = efj::to_nsec(ltPosix).value - efj::to_nsec(now).value;

            //efj_log_debug("LT seconds: %u, fraction: %u, to_ns: %u, cur: %u.%u, diff: %lld\n", ltPosix.sec, launchTime.frac,
            //    ltPosix.nsec, now.sec, now.nsec, diffNs);
        }
    }
    //efj_log_info("}\n");
    efj::log_unformatted(false);

    if (!has_at_least_one_valid_block)
        return;

    if (got_sos)
    {
        u32 timestamp = efj::now_8khz() - _timestamp_origin;
        RtpP25Message ack = dfsi_make_tx_key_ack(++_sequence_number, timestamp, _dfsi_connect.fs_vc_ssrc);
        efj_produce(InboundVoice, ack);
    }

    efj_produce(OutboundVoice, msg);
}

void DfsiVoiceClient::consumeInboundVoice(const RtpP25Message& msg)
{
    u32 timestamp = efj::now_8khz() - _timestamp_origin;
    auto* copy = efj::copy_message(msg);

    rtp_set_timestamp(*copy, timestamp);
    rtp_set_sequence_number(*copy, ++_sequence_number);
    rtp_set_ssrc(*copy, _dfsi_connect.fs_vc_ssrc);

    //if (dfsi_get_block_or_null<DfsiBlockHeaderPart1>(msg))
    //{
    //    efj_log_info("Time: %llu us", efj::now_usec().value);
    //}

    efj_produce(InboundVoice, *copy);
}

