
void disableHardwareObjects()
{
    efj::test_disable<MxdrInterface>();
    efj::test_disable<DspInterface>();
    efj::test_disable<PruInterface>();
    efj::test_disable<DfsiInterface>();
}

void devInit()
{
    //disableHardwareObjects();
    //auto* p1vp = efj::test_get_object<P1VoiceProcessor>();
    //efj::test_enable_log_level(p1vp, efj::log_level_debug);
}
