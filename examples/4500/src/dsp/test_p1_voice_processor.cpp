#include "test/test.hpp"

struct TestStreamOptions
{
    bool addStartOfStream = false; // Prepend SOS to messages to simulate pre-TxKeyAck DFSI behavior.
    bool addLaunchTime = false;
};

void sendHeader(efj::consumer_of<const RtpP25Message>& out, u32* seq = nullptr, const TestStreamOptions& opt = {})
{
    u32 curSeq = seq ? *seq : 0;

    DfsiBuilder builder;
    if (opt.addStartOfStream)
        builder.add<DfsiBlockStartOfStream>();
    if (opt.addLaunchTime)
        builder.add<DfsiBlockLaunchTime>();

    DfsiVoiceHeaderBuffer header = {};
    DfsiVoiceStatusBuffer status = {};

    efj::test_produce(out, builder.buildWith<DfsiBlockHeaderPart1>(curSeq++, 0, 0, header, status));
    efj::test_produce(out, builder.buildWith<DfsiBlockHeaderPart2>(curSeq++, 0, 0, header, status));
    if (seq) *seq = curSeq;
}

void sendLdu1(efj::consumer_of<const RtpP25Message>& out, u32* seq = nullptr, const TestStreamOptions& opt = {})
{
    u32 curSeq = seq ? *seq : 0;

    DfsiBuilder builder;
    if (opt.addStartOfStream)
        builder.add<DfsiBlockStartOfStream>();
    if (opt.addLaunchTime)
        builder.add<DfsiBlockLaunchTime>();

    DfsiImbeBuffer vc;
    DfsiVoiceStatus st;
    DfsiLcEsBuffer lcEs;
    DfsiLsdBuffer lsd;

    efj::test_produce(out, builder.buildWith<DfsiBlockVoice>(curSeq++, 0, 0, DFSI_FT_IMBE1, vc, st));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoice>(curSeq++, 0, 0, DFSI_FT_IMBE2, vc, st));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoiceLcEs>(curSeq++, 0, 0, DFSI_FT_IMBE3_LC, vc, st, lcEs));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoiceLcEs>(curSeq++, 0, 0, DFSI_FT_IMBE4_LC, vc, st, lcEs));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoiceLcEs>(curSeq++, 0, 0, DFSI_FT_IMBE5_LC, vc, st, lcEs));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoiceLcEs>(curSeq++, 0, 0, DFSI_FT_IMBE6_LC, vc, st, lcEs));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoiceLcEs>(curSeq++, 0, 0, DFSI_FT_IMBE7_LC, vc, st, lcEs));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoiceLcEs>(curSeq++, 0, 0, DFSI_FT_IMBE8_LC, vc, st, lcEs));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoiceLsd>(curSeq++, 0, 0, DFSI_FT_IMBE9_LSD, vc, st, lsd));
    if (seq) *seq = curSeq;
}

void sendLdu2(efj::consumer_of<const RtpP25Message>& out, u32* seq = nullptr, const TestStreamOptions& opt = {})
{
    u32 curSeq = seq ? *seq : 0;
    
    DfsiBuilder builder;
    if (opt.addStartOfStream)
        builder.add<DfsiBlockStartOfStream>(); 
    if (opt.addLaunchTime)
        builder.add<DfsiBlockLaunchTime>();

    DfsiImbeBuffer vc;
    DfsiVoiceStatus st;
    DfsiLcEsBuffer lcEs;
    DfsiLsdBuffer lsd;

    efj::test_produce(out, builder.buildWith<DfsiBlockVoice>(curSeq++, 0, 0, DFSI_FT_IMBE10, vc, st));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoice>(curSeq++, 0, 0, DFSI_FT_IMBE11, vc, st));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoiceLcEs>(curSeq++, 0, 0, DFSI_FT_IMBE12_ES, vc, st, lcEs));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoiceLcEs>(curSeq++, 0, 0, DFSI_FT_IMBE13_ES, vc, st, lcEs));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoiceLcEs>(curSeq++, 0, 0, DFSI_FT_IMBE14_ES, vc, st, lcEs));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoiceLcEs>(curSeq++, 0, 0, DFSI_FT_IMBE15_ES, vc, st, lcEs));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoiceLcEs>(curSeq++, 0, 0, DFSI_FT_IMBE16_ES, vc, st, lcEs));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoiceLcEs>(curSeq++, 0, 0, DFSI_FT_IMBE17_ES, vc, st, lcEs));
    efj::test_produce(out, builder.buildWith<DfsiBlockVoiceLsd>(curSeq++, 0, 0, DFSI_FT_IMBE18_LSD, vc, st, lsd));
    if (seq) *seq = curSeq;
}

void initHardwareAndGrantDsp(P1VoiceProcessor* p1vp)
{
    // We just need a dummy value for the polarity.
    efj::test_send_event(p1vp, EVENT_HARDWARE_STATUS,
        HardwareStatus{HARDWARE_STATUS_READY, C6K_RX_POLARITY_INVERTED});
    efj::test_send_event(p1vp, EVENT_DSP_GRANT, DspGrant{true, true});
}

void sendTdus(efj::consumer_of<const RtpP25Message>& out, u32 seq = 0)
{
    efj::test_produce(out, dfsi_make_tdu(seq++, 0, 0, DFSI_TDU_LONG, 0, {}));
    efj::test_produce(out, dfsi_make_tdu(seq++, 0, 0, DFSI_TDU_LONG, 0, {}));
    efj::test_produce(out, dfsi_make_tdu(seq++, 0, 0, DFSI_TDU_LONG, 0, {}));
    efj::test_produce(out, dfsi_make_tdu(seq++, 0, 0, DFSI_TDU_SHORT, 0, {}));
}

void testTransmitSequence(P1VoiceProcessor* p1vp, efj::test_output_queue& output,
    const efj::view<C6kMessageType>& types)
{
    auto it = output.begin();
    size_t typesChecked = 0;
    while (typesChecked < types.size)
    {
        TEST_TRUE(!output.find_pc<C6kMessage>(p1vp->OutboundC6k, 0, &it));
        efj::test_produce(p1vp->inputInboundC6k, c6k_make_transmit_request());
        C6kMessage* msg = output.find_pc_or_fail<C6kMessage>(p1vp->OutboundC6k, 0, &it);
        TEST_EQ(c6k_get_type(*msg), C6K_TRANSMIT_MULTI);

        // We need to be careful here b/c the message is likely not big enough for the
        // whole tx multi block. We excpect a simple c6k_get_block conversion to fail to
        // keep programmers aware of this.
        auto* multi = (C6kBlockTransmitMulti*)msg->data;
        for (int i = 0; i < multi->getMessageCount(); i++)
        {
            //efj_log_debug("(%zu/%zu) Testing %s|%s", typesChecked, types.size,
            //    efj::c_str(multi->getType(i)), efj::c_str(types[typesChecked]));
            TEST_EQ(multi->getType(i), types[typesChecked]);
            ++typesChecked;
        }
    }
}

EFJ_TEST(P1VoiceProcessor, queues_until_granted)
{
    auto* p1vp = efj::test_get_object<P1VoiceProcessor>();
    efj::test_disconnect_output(p1vp);

    efj::test_output_queue output;
    efj::test_capture(p1vp, &output);
    efj_defer { printOutputQueue(output); };

    // We just need a dummy value for the polarity.
    efj::test_send_event(p1vp, EVENT_HARDWARE_STATUS,
        HardwareStatus{HARDWARE_STATUS_READY, C6K_RX_POLARITY_INVERTED});

    sendLdu1(p1vp->inputOutboundVoice);
    TEST_EQ(output.size(), 1u);

    auto it = output.begin();

    AllocateDsp* allocateDsp = output.find_event<AllocateDsp>(EVENT_ALLOCATE_DSP, 0, &it);
    TEST_TRUE(allocateDsp);

    efj::test_send_event(p1vp, EVENT_DSP_GRANT, DspGrant{true, true});
    TEST_EQ(output.size(), 5u);

    UpdateLeds* updateLeds = output.find_event<UpdateLeds>(EVENT_UPDATE_LEDS, 0, &it);
    TEST_TRUE(updateLeds);

    C6kMessage* msg = output.find_pc<C6kMessage>(p1vp->OutboundC6k, 0, &it);
    TEST_EQ(c6k_get_type(*msg), C6K_START_PROJECT_25_PHASE_1_INBOUND);

    msg = output.find_pc<C6kMessage>(p1vp->OutboundC6k, 0, &it);
    TEST_EQ(c6k_get_type(*msg), C6K_START_PROJECT_25_PHASE_1_OUTBOUND);
}

void verifyOutboundP1Voice(P1VoiceProcessor* p1vp, efj::test_output_queue& output, const TestStreamOptions& opt = {})
{
    u32 seq = 0;
    sendHeader(p1vp->inputOutboundVoice, &seq, opt);
    output.clear();
    efj::test_produce(p1vp->inputInboundC6k, c6k_make_phase1_outbound_starting());

    C6kMessageType headerTypes[] = {
        C6K_TRANSMIT_DUID,
        C6K_TRANSMIT_HEADER_PART_1,
        C6K_TRANSMIT_HEADER_PART_2,
        C6K_TRANSMIT_NULL,
    };
    testTransmitSequence(p1vp, output, headerTypes);
    output.clear();

    C6kMessageType lduTypes[] = {
        C6K_TRANSMIT_DUID,
        C6K_TRANSMIT_LDU_VOICE,
        C6K_TRANSMIT_LDU_VOICE,
        C6K_TRANSMIT_LDU_VOICE_WITH_LC_ES,
        C6K_TRANSMIT_LDU_VOICE_WITH_LC_ES,
        C6K_TRANSMIT_LDU_VOICE_WITH_LC_ES,
        C6K_TRANSMIT_LDU_VOICE_WITH_LC_ES,
        C6K_TRANSMIT_LDU_VOICE_WITH_LC_ES,
        C6K_TRANSMIT_LDU_VOICE_WITH_LC_ES,
        C6K_TRANSMIT_LDU_VOICE_WITH_LSD,
    };
    sendLdu1(p1vp->inputOutboundVoice, &seq, opt);
    testTransmitSequence(p1vp, output, lduTypes);
    output.clear();

    sendLdu2(p1vp->inputOutboundVoice, &seq, opt);
    testTransmitSequence(p1vp, output, lduTypes);
}

EFJ_TEST(P1VoiceProcessor, translates_voice)
{
    auto* p1vp = efj::test_get_object<P1VoiceProcessor>();
    efj::test_disconnect_output(p1vp);

    efj::test_output_queue output;
    efj::test_capture(p1vp, &output);
    efj_defer { printOutputQueue(output); };

    initHardwareAndGrantDsp(p1vp);

    TestStreamOptions opt;
    opt.addStartOfStream = false;
    opt.addLaunchTime = false;

    TEST_NOTE("translates_voice");

    verifyOutboundP1Voice(p1vp, output, opt);
}

EFJ_TEST(P1VoiceProcessor, translates_voice_sos)
{
    auto* p1vp = efj::test_get_object<P1VoiceProcessor>();
    efj::test_disconnect_output(p1vp);

    efj::test_output_queue output;
    efj::test_capture(p1vp, &output);
    efj_defer { printOutputQueue(output); };

    initHardwareAndGrantDsp(p1vp);

    TestStreamOptions opt;
    opt.addStartOfStream = true;
    opt.addLaunchTime = false;

    TEST_NOTE("translates_voice_sos");

    verifyOutboundP1Voice(p1vp, output, opt);
}

EFJ_TEST(P1VoiceProcessor, translates_voice_lt)
{
    auto* p1vp = efj::test_get_object<P1VoiceProcessor>();
    efj::test_disconnect_output(p1vp);

    efj::test_output_queue output;
    efj::test_capture(p1vp, &output);
    efj_defer { printOutputQueue(output); };

    initHardwareAndGrantDsp(p1vp);

    TestStreamOptions opt;
    opt.addStartOfStream = false;
    opt.addLaunchTime = true;

    TEST_NOTE("translates_voice_lt");

    verifyOutboundP1Voice(p1vp, output, opt);
}

EFJ_TEST(P1VoiceProcessor, translates_voice_sos_lt)
{
    auto* p1vp = efj::test_get_object<P1VoiceProcessor>();
    efj::test_disconnect_output(p1vp);

    efj::test_output_queue output;
    efj::test_capture(p1vp, &output);
    efj_defer { printOutputQueue(output); };

    initHardwareAndGrantDsp(p1vp);

    TestStreamOptions opt;
    opt.addStartOfStream = true;
    opt.addLaunchTime = true;

    TEST_NOTE("translates_voice_sos_lt");

    verifyOutboundP1Voice(p1vp, output, opt);
}
EFJ_TEST(P1VoiceProcessor, translates_tdus)
{
    auto* p1vp = efj::test_get_object<P1VoiceProcessor>();
    efj::test_disconnect_output(p1vp);

    efj::test_output_queue output;
    efj::test_capture(p1vp, &output);
    efj_defer { printOutputQueue(output); };

    initHardwareAndGrantDsp(p1vp);
    output.clear();
    efj::test_produce(p1vp->inputInboundC6k, c6k_make_phase1_outbound_starting());
    sendTdus(p1vp->inputOutboundVoice);

    C6kMessageType types[] = {
        C6K_TRANSMIT_DUID,
        C6K_TRANSMIT_TERM_WITH_LC,
        C6K_TRANSMIT_NULL,
        C6K_TRANSMIT_DUID,
        C6K_TRANSMIT_TERM_WITH_LC,
        C6K_TRANSMIT_NULL,
        C6K_TRANSMIT_DUID,
        C6K_TRANSMIT_TERM_WITH_LC,
        C6K_TRANSMIT_NULL,
        C6K_TRANSMIT_DUID,
        C6K_TRANSMIT_NULL,
    };

    testTransmitSequence(p1vp, output, types);
}

EFJ_TEST(P1VoiceProcessor, ignores_non_content)
{
    auto* p1vp = efj::test_get_object<P1VoiceProcessor>();
    efj::test_disconnect_output(p1vp);

    efj::test_output_queue output;
    efj::test_capture(p1vp, &output);
    efj_defer { printOutputQueue(output); };

    initHardwareAndGrantDsp(p1vp);
    output.clear();
    efj::test_produce(p1vp->inputInboundC6k, c6k_make_phase1_outbound_starting());
    TEST_EQ(output.size(), 0u);

    u32 seq = 0;
    efj::test_produce(p1vp->inputOutboundVoice, dfsi_make_tx_key_ack(seq++, 0, 0));
    efj::test_produce(p1vp->inputOutboundVoice, dfsi_make_sos(seq++, 0, 0, 0, 0, 0));
    efj::test_produce(p1vp->inputOutboundVoice, dfsi_make_eos(seq++, 0, 0));
    sendHeader(p1vp->inputOutboundVoice);

    C6kMessageType headerTypes[] = {
        C6K_TRANSMIT_DUID,
        C6K_TRANSMIT_HEADER_PART_1,
        C6K_TRANSMIT_HEADER_PART_2,
        C6K_TRANSMIT_NULL,
    };
    testTransmitSequence(p1vp, output, headerTypes);
}

