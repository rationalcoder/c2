
bool ControlProcessor::init()
{
    efj::create_oneshot_timer("RX Activity", 80_ms, &_rxActivityTimer,
        &ControlProcessor::onRxActivityTimeout);
    efj::create_oneshot_timer("Keep TX Active", 800_ms, &_keepTxActiveTimer,
        &ControlProcessor::onKeepTxActiveTimeout);
    efj::create_oneshot_timer("TX Activity", 60_ms, &_txActivityTimer,
        &ControlProcessor::onTxActivityTimeout);

    efj::subscribe_cached(EVENT_CONFIG, &_config, &ControlProcessor::onEvent, efj::sub_sync);

    efj::subscribe(EVENT_HARDWARE_STATUS, &ControlProcessor::onEvent);
    efj::subscribe(EVENT_DSP_GRANT, &ControlProcessor::onEvent);
    efj::subscribe(EVENT_DSP_DENY, &ControlProcessor::onEvent);

    return true;
}

void ControlProcessor::onEvent(const efj::user_event& event)
{
}

void ControlProcessor::consumeOutboundVoice(const RtpP25Message& msg)
{
}

void ControlProcessor::consumeInboundMxdr(const MxdrMessage& msg)
{
}

void ControlProcessor::consumeInboundC6k(const C6kMessage& msg)
{
}

void ControlProcessor::onRxActivityTimeout(efj::timer_event&)
{
}

void ControlProcessor::onKeepTxActiveTimeout(efj::timer_event&)
{
}

void ControlProcessor::onTxActivityTimeout(efj::timer_event&)
{
}

