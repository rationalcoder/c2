
bool P1VoiceProcessor::init()
{
    efj::debug_add_watch("TX State", (int*)&_txState);

    efj::create_oneshot_timer("RX Activity", 80_ms, &_rxActivityTimer,
        &P1VoiceProcessor::onRxActivityTimeout);
    efj::create_oneshot_timer("Keep TX Active", 800_ms, &_keepTxActiveTimer,
        &P1VoiceProcessor::onKeepTxActiveTimeout);
    efj::create_oneshot_timer("TX Activity", 60_ms, &_txActivityTimer,
        &P1VoiceProcessor::onTxActivityTimeout);

    efj::subscribe_cached(EVENT_CONFIG, &_config, &P1VoiceProcessor::onEvent, efj::sub_sync);

    efj::subscribe(EVENT_HARDWARE_STATUS, &P1VoiceProcessor::onEvent);
    efj::subscribe(EVENT_DSP_GRANT, &P1VoiceProcessor::onEvent);
    efj::subscribe(EVENT_DSP_DENY, &P1VoiceProcessor::onEvent);

    return true;
}

void P1VoiceProcessor::onEvent(const efj::user_event& e)
{
    switch (e.type)
    {
    case EVENT_HARDWARE_STATUS:
    {
        auto* hardwareStatus = e.as<HardwareStatus>();
        if (hardwareStatus->value == HARDWARE_STATUS_READY)
        {
            _hardwareReady = true;

            _latestModulation = (C6kModulation)hardwareStatus->p1_tx_mod;
            efj_log_info("Starting p1 inbound: %s", efj::c_str((C6kModulation)hardwareStatus->p1_tx_mod));

            AllocateDsp allocDsp;
            allocDsp.inbound = true;
            efj_trigger(EVENT_ALLOCATE_DSP, allocDsp);
        }
        else
        {
            _hardwareReady = false;
        }

        break;
    }
    case EVENT_DSP_GRANT:
    {
        _dspGranted = true;
        efj_assert(_hardwareReady);

        UpdateLeds updateLeds = {};
        updateLeds.leds_on = MXDR_LED_PHASE1;

        efj_trigger(EVENT_UPDATE_LEDS, updateLeds);

        auto msg = c6k_make_start_phase1_inbound(_rxPolarity, C6K_OSP_MODE_SINGLE);
        efj_produce(OutboundC6k, msg);

        startOutboundIfWeHaveEnoughData();
        break;
    }
    default:
        break;
    }
}

void P1VoiceProcessor::onRxActivityTimeout(efj::timer_event& e)
{
    _receiving = false;
    efj_produce(InboundVoice, dfsi_make_eos(0, 0, 0));

    UpdateLeds updateLeds = {};
    updateLeds.leds_off = MXDR_LED_RX_A;
    efj_trigger(EVENT_UPDATE_LEDS, updateLeds);
}

void P1VoiceProcessor::consumeInboundMxdr(const MxdrMessage& msg)
{
    switch (mxdr_get_type(msg))
    {
    case MXDR_SET_RX1_FREQUENCY:
    {
        auto* block = mxdr_get_block<MxdrBlockRxFrequencyAck>(msg);
        _rxPolarity = mxdr_to_c6k_polarity(block->getModuleId());
        break;
    }
    case MXDR_TX_STATUS:
    {
        auto* block = mxdr_get_block<MxdrBlockTxStatus>(msg);
        if (block->getOn())
        {
            efj_log_debug("TX on");

            UpdateLeds updateLeds = {};
            updateLeds.leds_on = MXDR_LED_TX_A;
            efj_trigger(EVENT_UPDATE_LEDS, updateLeds);
        }
        else
        {
            efj_log_debug("TX off");

            UpdateLeds updateLeds = {};
            updateLeds.leds_off = MXDR_LED_TX_A;
            efj_trigger(EVENT_UPDATE_LEDS, updateLeds);
        }
        break;
    }
    case MXDR_ALARM_AND_STATUS:
    {
        efj_log_debug("Alarm and status size: %zu", msg.size);
        auto* block = mxdr_get_block<MxdrBlockAlarmAndStatus>(msg);
        MxdrStatusBits1 bits1 = block->getStatusBits1();
        MxdrStatusBits1 bits2 = block->getStatusBits2();
        efj_log_debug("Alarm and status: %x %x", bits1, bits2);
        break;
    }
    default:
        break;
    }
}

void P1VoiceProcessor::consumeInboundC6k(const C6kMessage& msg)
{
    C6kMessageType type = c6k_get_type(msg);
    //if (type != C6K_TRANSMIT_REQUEST)
        efj_log_debug("Inbound C6k: %s", efj::c_str(type));

    switch (type)
    {
    case C6K_ERROR:
    {
        efj_debug_dump();
        break;
    }
    case C6K_INVALID_MSG:
    {
        auto* block = c6k_get_block<C6kBlockInvalidMessage>(msg);
        efj_log_crit("Error: %s (%s)", block->getComment(),
            efj::c_str<C6kMessageType>(block->getMessageType()));

        efj_debug_dump();
        break;
    }
    case C6K_RECEIVE_TIME_UPDATE:
        break;
    case C6K_RECEIVE_PHASE_1_UPDATE:
        break;
    case C6K_C6000_STARTED:
        break;
    case C6K_SOFTWARE_CONFIGURED:
        break;
    case C6K_HARDWARE_CONFIGURED:
        break;
    case C6K_PROJECT_25_PHASE_1_INBOUND_STARTING:
        break;
    case C6K_PROJECT_25_PHASE_1_OUTBOUND_STARTING:
    {
        auto* block = c6k_get_block<C6kBlockPhase1OutboundStarting>(msg);
        efj_log_info("DSP wait time: %llu ns", ((efj::llu)block->getWaitFraction() * 1000000000) >> 16);
        handleOutboundStarting();
        break;
    }
    case C6K_TRANSMIT_REQUEST:
    {
        handleTransmitRequest();
        break;
    }
    case C6K_OUTBOUND_SERIAL_PORT_STARTED:
    {
        handleOutboundSerialPortStarted();
        break;
    }
    case C6K_SMART_EXCITER_UNDERFLOW_WARNING:
    {
        handleUnderflowWarning();
        break;
    }
    case C6K_MODULATION_COMPLETE:
    {
        handleModulationComplete();
        break;
    }
    case C6K_PROJECT_25_PHASE_1_OUTBOUND_STOPPED:
    {
        handleOutboundStopped();
        break;
    }
    // TODO: Handle data registration.
    case C6K_RECEIVE_UNCONFIRMED:
        break;
    case C6K_RECEIVE_CONFIRMED_DATA:
        break;
    default:
        handleInboundVoice(msg);
        break;
    }
}

void P1VoiceProcessor::handleInboundVoice(const C6kMessage& msg)
{
    C6kMessageType type = c6k_get_type(msg);

    efj::timer_start(_rxActivityTimer);

    switch (type)
    {
    case C6K_RECEIVE_DUID:
    {
        // FIXME: RF-fade/interference recovery. The DSP will send us full LDUs or none.
        // If we get NID errors on an RX DUID after one or more LDUs have been dropped,
        // we won't know which LDU we are one and we could interpret LCs as ESs and vice versa.
        // The current idea of a solution to this is to:
        // 1. Wait until we get a valid DUID for an LDU.
        // 2. Keep track of timestamps to know which LDU we are decoding.
        //
        // Right now, we just hope we will get get a NID/DUID with no errors after a bad LDU.

        auto* block = c6k_get_block<C6kBlockReceiveDuid>(msg);
        // FIXME: Right now, we just ignore data registrations (PDUs) in conventional.
        bool ignoreDuid = false;

        u8 duid = block->getDuid();
        u8 errorCount = block->getNidErrors();
        if (errorCount <= 11)
        {
            switch (duid)
            {
            case CAI_DUID_LDU1:
                _nextImbeFrameType = DFSI_FT_IMBE1;
                break;
            case CAI_DUID_LDU2:
                _nextImbeFrameType = DFSI_FT_IMBE10;
                break;
            case CAI_DUID_HDU: EFJ_FALLTHROUGH;
            case CAI_DUID_TDU: EFJ_FALLTHROUGH;
            case CAI_DUID_LTDU:
                break;
            // We can get PDUs on a traffic channel in conventional.
            case CAI_DUID_PDU:
                ignoreDuid = true;
                break;
            default:
                efj_log_warn("Unexpected DUID in phase1 voice stream: %x (%s)", duid, efj::c_str((CaiDuid)duid));
                break;
            }
        }

        if (!_receiving && !ignoreDuid)
        {
            _receiving = true;

            UpdateLeds updateLeds = {};
            updateLeds.leds_on = MXDR_LED_RX_A;
            efj_trigger(EVENT_UPDATE_LEDS, updateLeds);

            efj_produce(InboundVoice, dfsi_make_sos(0, 0, 0, _nac, duid, errorCount));
        }

        break;
    }
    // FIXME: We need to support data in conventional.
    case C6K_RECEIVE_UNCONFIRMED:
        break;
    case C6K_RECEIVE_CONFIRMED_DATA:
        break;
    case C6K_RECEIVE_HEADER_PART_1:
        efj_produce(InboundVoice, c6k_to_dfsi_hdu1(msg));
        break;
    case C6K_RECEIVE_HEADER_PART_2:
        efj_produce(InboundVoice, c6k_to_dfsi_hdu2(msg));
        break;
    case C6K_RECEIVE_LDU_VOICE:
        efj_produce(InboundVoice, c6k_to_dfsi_voice(msg, _nextImbeFrameType, _superframeCounter, _inboundBusyStatus));
        _nextImbeFrameType++;
        break;
    case C6K_RECEIVE_LDU_VOICE_WITH_LC_ES:
        efj_produce(InboundVoice, c6k_to_dfsi_voice_lc_es(msg, _nextImbeFrameType, _superframeCounter, _inboundBusyStatus));
        _nextImbeFrameType++;
        break;
    case C6K_RECEIVE_LDU_VOICE_WITH_LSD:
        efj_produce(InboundVoice, c6k_to_dfsi_voice_lsd(msg, _nextImbeFrameType, _superframeCounter, _inboundBusyStatus));

        if (_nextImbeFrameType == DFSI_FT_IMBE18_LSD)
        {
            // SF counter: 1 -> 2 -> 1
            _superframeCounter = (_superframeCounter & 0x1) + 1;
            _nextImbeFrameType = DFSI_FT_IMBE1;
        }
        else
        {
            ++_nextImbeFrameType;
        }

        break;
    default:
        efj_log_crit("invalid c6k voice message: %s", efj::c_str(type));
        efj_invalid_code_path();
        break;
    }
}

void P1VoiceProcessor::consumeOutboundVoice(const RtpP25Message& msg)
{
    // IMPORTANT: We only want to queue messages that result in OTA content. That way, we know
    // we can respond to a transmit request if queue size > 0.

    // IMPORTANT: This code assumes we have a complete and valid phase1 stream!
    // We also don't do late packet checking, yet.

    //efj::log_unformatted(true);
    //efj_log_debug("RTP:");
    //for (auto it = p25_iterate_blocks(msg); p25_is_valid(it); p25_advance(it)) {
    //    efj_log_debug("%s ", efj::c_str((RtpP25BlockType)p25_get_block_type(it)));
    //}
    //efj_log_debug("\n");
    //efj::log_unformatted(false);

    u8 blockCount = p25_get_block_count(msg);
    // SOS|LT|block is the biggest message we expect for now.
    if (blockCount > 3)
    {
        efj_log_warn("Ignoring RTP message with more than two blocks");
        return;
    }

    auto firstBlockIt = p25_iterate_blocks(msg);
    int bt = p25_get_block_type(firstBlockIt);
    if (blockCount == 2 && bt != P25_BT_LAUNCH_TIME && bt != P25_BT_START_OF_STREAM)
    {
        efj_log_warn("Ignoring RTP message that doesn't start with a launch time or sos (it starts with %s)",
            efj::c_str((RtpP25BlockType)p25_get_block_type(firstBlockIt)));
        return;
    }

    // Only queue messages that have content, and grab what we need from other messages.
    bool hasContent = false;
    for (auto it = firstBlockIt; p25_is_valid(it); p25_advance(it))
    {
        int bt = p25_get_block_type(it);
        switch (bt)
        {
        case P25_BT_START_OF_STREAM:
            _nac = p25_get_block<DfsiBlockStartOfStream>(it)->getNac();
            break;
        case P25_BT_VOICE_HEADER_1:
        case P25_BT_VOICE_HEADER_2:
        case P25_BT_AUDIO:
        case P25_BT_TERMINATOR_DATA_UNIT:
            hasContent = true;
            break;
        default:
            break;
        }
    }

    if (hasContent)
        queueOutbound(msg);
}

void P1VoiceProcessor::onTransition(TxState newState)
{
    efj_log_debug("TRANSITION: %d->%d", _txState, newState);
    _txState = newState;
}

void P1VoiceProcessor::startOutboundIfWeHaveEnoughData()
{
    efj_log_debug(__func__);
    efj_panic_if(_txState != TX_STATE_IDLE);

    if (_otaTimeInQueue >= 90_ms)
    {
        efj_log_debug("Starting outbound");
        onTransition(TX_STATE_STARTING);
        _outstandingTransmitRequests = 0;
        _sentTerminator = false;

        // FIXME: stale packet processing.

        const RtpP25Message& firstMessage = _outboundQueue.front();

        auto it = p25_iterate_blocks(firstMessage);
        auto* lt = p25_get_block_or_null<DfsiBlockLaunchTime>(it);

        efj::posix_time nowPosix = efj::now_ptime();
        efj::posix_time launchTimePosix = lt
                                        ? efj::to_ptime({lt->getSeconds(), lt->getFraction()})
                                        : efj::posix_time{0, 0};

        auto diffNs = efj::to_nsec(launchTimePosix) - efj::to_nsec(nowPosix);

        if (efj::to_nsec(launchTimePosix).value > efj::to_nsec(nowPosix).value)
        {
            constexpr u64 usPerDay = 1000000ull*60*60*24;
            u64 usSinceMidnight = efj::to_usec(launchTimePosix).value % usPerDay;
            u32 slotOffset = usSinceMidnight / (7500 * _microslotsPerSlot) % _microslotsPerSlot;
            efj_panic_if(slotOffset >= 255);

            efj_log_info("Starting second: %u %u", launchTimePosix.sec, nowPosix.sec);
            efj_log_info("Starting stream with valid launch time, %u ns from now, lt:%u.%u, now:%u.%u, slot offset:%u",
                (u32)diffNs.value, launchTimePosix.sec, launchTimePosix.nsec, nowPosix.sec, nowPosix.nsec, slotOffset);

            QFormat32_32 launchTime;
            launchTime.qValue = ((u64)lt->getSeconds() << 32 | lt->getFraction());
            //launchTime.qValue = launchTimeNtp->frac;

            QFormat31_32 launchTimeOffset;

            efj_produce(OutboundC6k, c6k_make_start_phase1_outbound(launchTime, launchTimeOffset,
                _latestModulation, _microslotsPerSlot, slotOffset));
        }
        else
        {
            auto type = (RtpP25BlockType)p25_get_block_type(it);
            efj_log_info("Starting stream without launch time %s, %u.%u %u.%u", efj::c_str(type),
                launchTimePosix.sec, launchTimePosix.nsec, nowPosix.sec, nowPosix.nsec);
            efj_produce(OutboundC6k, c6k_make_start_phase1_outbound(_latestModulation, _microslotsPerSlot, 0));
        }

        // FIXME: We need to update the status symbols more often than this.
        efj_produce(OutboundC6k, _latestTxStatus);
    }
}

efj::usec getOverTheAirTime(const RtpP25Message& msg)
{
    // We just return here b/c we either have 1 block or a block + a launch time.
    for (auto it = p25_iterate_blocks(msg); p25_is_valid(it); p25_advance(it))
    {
        int bt = p25_get_block_type(it);
        switch (bt)
        {
        case P25_BT_TERMINATOR_DATA_UNIT:
        {
            auto* block = p25_get_block<DfsiBlockTdu>(it);
            return block->getType() == DFSI_TDU_LONG ? 45_ms : 15_ms;
        }
        case P25_BT_VOICE_HEADER_1:
        case P25_BT_VOICE_HEADER_2:
            return 41250_us;
        case P25_BT_AUDIO:
            return 20_ms;
        default:
            break;
        }
    }

    return 0_us;
}

void P1VoiceProcessor::queueOutbound(const RtpP25Message& msg)
{
    _outboundQueue.push(msg);
    _otaTimeInQueue += getOverTheAirTime(msg);

    //efj_log_debug("Queue size: %zu", _outboundQueue.size());

    if (_txState == TX_STATE_IDLE && _dspGranted && _hardwareReady)
    {
        startOutboundIfWeHaveEnoughData();
    }
    else
    {
        handleOutstandingTransmitRequests();
    }
}

void P1VoiceProcessor::handleOutboundStarting()
{
    onTransition(TX_STATE_QUEUEING);
}

void P1VoiceProcessor::handleOutstandingTransmitRequests()
{
    if (_txState != TX_STATE_QUEUEING && _txState != TX_STATE_ACTIVE)
        return;

    if (!_outstandingTransmitRequests)
        return;
    _outstandingTransmitRequests = 0;

    if (!_outboundQueue.size())
    {
        if (_txState == TX_STATE_ACTIVE)
        {
            // We send a terminator at the end of the stream in an attempt to stop
            // the "squelch" commonly heard at the end of calls. It seems to be fixed,
            // but I'm not sure if this is why.
            if (!_sentTerminator)
            {
                efj_log_debug("Sending terminator");
                _sentTerminator = true;
                c6k_make_transmit_duid(_txMulti->add<C6kBlockTransmitDuid>(), _nac, CAI_DUID_TDU);
                c6k_make_transmit_null(_txMulti->add<C6kBlockTransmitNull>());
                efj_produce(OutboundC6k, _txMulti->toMinimalMessage());
                _txMulti->clear();
                return;
            }
            else
            {
                efj_log_debug("Stopping");
                onTransition(TX_STATE_STOPPING);
                efj::timer_stop(_txActivityTimer);
                efj_produce(OutboundC6k, c6k_make_stop_phase1_outbound());
                return;
            }
        }
    }
    else
    {
        // We are assuming we can respond to tx requests if queue size > 0, so we had better have
        // ota time > 0. This will fail if we accidentally queued message with no actual content.
        efj_panic_if(!_otaTimeInQueue.value);

        // TODO: Pack as many messages into a tx multi as possible. The most c6k
        // messages we can generate from one rtp message is three, so
        // while (queue.size() && multi.getMessageCount() <= 14)...

        const RtpP25Message& msg = _outboundQueue.front();

        auto firstDataBlockIter = p25_iterate_blocks(msg);
        int bt = p25_get_block_type(firstDataBlockIter);
        if (bt == P25_BT_LAUNCH_TIME)
        {
            efj_assert(p25_get_block_count(msg) == 2);
            p25_advance(firstDataBlockIter);
            bt = p25_get_block_type(firstDataBlockIter);
        }
        efj_log_debug("producing %s: %s", efj::c_str((RtpP25BlockType)bt), efj::to_hex(msg).c_str());

        if (_txState == TX_STATE_ACTIVE)
        {
            efj_log_debug("Starting activity timer");
            efj::timer_start(_txActivityTimer);
        }

        translateToTxMulti(msg);
        _outboundQueue.pop();

        efj::usec overTheAirTime = getOverTheAirTime(msg);
        efj_panic_if(_otaTimeInQueue < overTheAirTime);
        _otaTimeInQueue -= overTheAirTime;
    }

    if (_txMulti->getMessageCount())
    {
        efj_produce(OutboundC6k, _txMulti->toMinimalMessage());
        _txMulti->clear();
    }
}

void P1VoiceProcessor::translateToTxMulti(const RtpP25Message& msg)
{
    for (auto it = p25_iterate_blocks(msg); p25_is_valid(it); p25_advance(it))
    {
        int bt = p25_get_block_type(it);
        switch (bt)
        {
        case P25_BT_VOICE_HEADER_1:
        {
            auto* block = p25_get_block<DfsiBlockHeaderPart1>(it);
            c6k_make_transmit_duid(_txMulti->add<C6kBlockTransmitDuid>(), _nac, CAI_DUID_HDU);
            dfsi_to_c6k_hdu1(_txMulti->add<C6kBlockTransmitHeader1>(), *block);
            break;
        }
        case P25_BT_VOICE_HEADER_2:
        {
            auto* block = p25_get_block<DfsiBlockHeaderPart2>(it);
            dfsi_to_c6k_hdu2(_txMulti->add<C6kBlockTransmitHeader2>(), *block);
            c6k_make_transmit_null(_txMulti->add<C6kBlockTransmitNull>());
            break;
        }
        case P25_BT_AUDIO:
        {
            auto* imbe = p25_get_block<DfsiBlockVoice>(it);
            u8 ft = imbe->getFrameType();
            switch (ft)
            {
            case DFSI_FT_IMBE1:
                c6k_make_transmit_duid(_txMulti->add<C6kBlockTransmitDuid>(), _nac, CAI_DUID_LDU1);
                EFJ_FALLTHROUGH;
            case DFSI_FT_IMBE2:
                dfsi_to_c6k_voice(_txMulti->add<C6kBlockTransmitLduVoice>(), *imbe);
                break;
            case DFSI_FT_IMBE3_LC: EFJ_FALLTHROUGH;
            case DFSI_FT_IMBE4_LC: EFJ_FALLTHROUGH;
            case DFSI_FT_IMBE5_LC: EFJ_FALLTHROUGH;
            case DFSI_FT_IMBE6_LC: EFJ_FALLTHROUGH;
            case DFSI_FT_IMBE7_LC: EFJ_FALLTHROUGH;
            case DFSI_FT_IMBE8_LC:
            {
                auto* imbeWithLc = p25_get_block<DfsiBlockVoiceLcEs>(it);
                dfsi_to_c6k_voice_lc_es(_txMulti->add<C6kBlockTransmitLduVoiceLcEs>(), *imbeWithLc);
                break;
            }
            case DFSI_FT_IMBE9_LSD:
            {
                auto* imbeWithLsd = p25_get_block<DfsiBlockVoiceLsd>(it);
                dfsi_to_c6k_voice_lsd(_txMulti->add<C6kBlockTransmitLduVoiceLsd>(), *imbeWithLsd);
                break;
            }
            case DFSI_FT_IMBE10:
                c6k_make_transmit_duid(_txMulti->add<C6kBlockTransmitDuid>(), _nac, CAI_DUID_LDU2);
                EFJ_FALLTHROUGH;
            case DFSI_FT_IMBE11:
                dfsi_to_c6k_voice(_txMulti->add<C6kBlockTransmitLduVoice>(), *imbe);
                break;
            case DFSI_FT_IMBE12_ES: EFJ_FALLTHROUGH;
            case DFSI_FT_IMBE13_ES: EFJ_FALLTHROUGH;
            case DFSI_FT_IMBE14_ES: EFJ_FALLTHROUGH;
            case DFSI_FT_IMBE15_ES: EFJ_FALLTHROUGH;
            case DFSI_FT_IMBE16_ES: EFJ_FALLTHROUGH;
            case DFSI_FT_IMBE17_ES:
            {
                auto* imbeWithEs = p25_get_block<DfsiBlockVoiceLcEs>(it);
                dfsi_to_c6k_voice_lc_es(_txMulti->add<C6kBlockTransmitLduVoiceLcEs>(), *imbeWithEs);
                break;
            }
            case DFSI_FT_IMBE18_LSD:
            {
                auto* imbeWithLsd = p25_get_block<DfsiBlockVoiceLsd>(it);
                dfsi_to_c6k_voice_lsd(_txMulti->add<C6kBlockTransmitLduVoiceLsd>(), *imbeWithLsd);
                break;
            }
            default:
                break;
            }

            break;
        }
        case P25_BT_TERMINATOR_DATA_UNIT:
        {
            auto* tdu = p25_get_block<DfsiBlockTdu>(it);
            //efj_log_debug("TDU Type: %u", tdu->data[2]);
            if (tdu->getType() == DFSI_TDU_LONG)
            {
                c6k_make_transmit_duid(_txMulti->add<C6kBlockTransmitDuid>(), _nac, CAI_DUID_LTDU);
                dfsi_to_c6k_ltdu(_txMulti->add<C6kBlockTransmitTermWithLc>(), *tdu);
                c6k_make_transmit_null(_txMulti->add<C6kBlockTransmitNull>());
            }
            else
            {
                c6k_make_transmit_duid(_txMulti->add<C6kBlockTransmitDuid>(), _nac, CAI_DUID_TDU);
                c6k_make_transmit_null(_txMulti->add<C6kBlockTransmitNull>());
            }

            break;
        }
        default:
            efj_log_warn("Ignoring RTP block %x (%s)", bt, efj::c_str((RtpP25BlockType)bt));
            break;
        }
    }
}

void P1VoiceProcessor::handleTransmitRequest()
{
    // We could still receive transmit requests during stopping due to event handling
    // delays as well as the DSP sending us a transmit request right after an underflow
    // warning, etc.
    efj_panic_if(_txState == TX_STATE_IDLE || _txState == TX_STATE_STARTING);

    ++_outstandingTransmitRequests;
    handleOutstandingTransmitRequests();
}

void P1VoiceProcessor::handleOutboundSerialPortStarted()
{
    efj::posix_time time = efj::now_ptime();
    efj_log_info("Serial port started: %u.%u", time.sec, time.nsec);
    onTransition(TX_STATE_ACTIVE);

    efj_produce(OutboundMxdr, mxdr_make_set_tx_state(true));
    // We need to tell the MXDR controller to keep the transmitter active
    // once a second during transmission or it will de-key for safety.
    efj::timer_start(_keepTxActiveTimer, 800_ms);
}

void P1VoiceProcessor::onKeepTxActiveTimeout(efj::timer_event&)
{
    efj_produce(OutboundMxdr, mxdr_make_set_tx_state(true));
}

void P1VoiceProcessor::onTxActivityTimeout(efj::timer_event&)
{
    efj_log_debug("TX activity timeout");
    //onTransition(TX_STATE_STOPPING);
    //efj_produce(OutboundC6k, c6k_make_stop_phase1_outbound());
}

void P1VoiceProcessor::handleUnderflowWarning()
{
    efj_log_debug("TX underflow warning");
    //onTransition(TX_STATE_STOPPING);
    //efj_produce(OutboundC6k, c6k_make_stop_phase1_outbound());
    //efj::timer_start(_lastChanceForContentTimer);
}

void P1VoiceProcessor::handleModulationComplete()
{
    efj_log_debug("Turning transmitter off");
    efj_produce(OutboundMxdr, mxdr_make_set_tx_state(false));
    efj::timer_stop(_keepTxActiveTimer);
}

void P1VoiceProcessor::handleOutboundStopped()
{
    onTransition(TX_STATE_IDLE);
    startOutboundIfWeHaveEnoughData();
}

