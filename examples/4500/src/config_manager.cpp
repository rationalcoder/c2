#include "config_manager.hpp"

bool ConfigManager::init()
{
    efj::config_subscribe(CONFIG_TUNING, &ConfigManager::onTuningConfigChanged);
    efj::config_subscribe(CONFIG_MAIN, &ConfigManager::onMainConfigChanged);
    efj::config_subscribe(efj::config_event_init_done, &ConfigManager::onInitialConfigDone);

    return true;
}

void ConfigManager::onTuningConfigChanged(efj::config_event& e)
{
    if (efj::config_accepted(e))
    {
        _curTuningConfig = _pendingTuningConfig;
        efj::swap(_pendingTuningConfig, _nextTuningConfig);

        // TODO: This will only happen after the initial config loading at app startup.
        // If we notice a tuning config update, we should probably start a timer and wait for
        // a main config file update as well.
        if (_configReadyAndValid)
        {
            combineConfig();
            efj_trigger(EVENT_CONFIG, _config);
        }

        return;
    }

    // TODO: Better error handling for fields like Bandwidth.

    bool rejected = false;
    efj::config_get_value(e, "rx1/Frequency", &_pendingTuningConfig->rxFrequency, &rejected);
    efj::config_get_value(e, "rx1/Bandwidth", &_pendingTuningConfig->rxBandwidth, &rejected);
    efj::config_get_value(e, "tx/Frequency", &_pendingTuningConfig->txFrequency, &rejected);
    efj::config_get_value(e, "tx/Bandwidth", &_pendingTuningConfig->txBandwidth, &rejected);
    efj::config_get_value(e, "tx/Power", &_pendingTuningConfig->txPower, &rejected);
    efj::config_get_value(e, "tx/ModulationType", &_pendingTuningConfig->txModType, &rejected);
    efj::config_get_value(e, "tx/MxdrTxOptions", &_pendingTuningConfig->txMxdrOptions, &rejected);
    efj::config_get_value(e, "txRxDelay", &_pendingTuningConfig->txRxDelay, &rejected);
    efj::config_get_value(e, "tx/exciter_buffers", &_pendingTuningConfig->exciterBufferCount, &rejected);
}

void ConfigManager::onMainConfigChanged(efj::config_event& e)
{
    if (efj::config_accepted(e))
    {
        if (!_curMainConfig)
        {
            _curMainConfig = _pendingMainConfig;
            efj::swap(_pendingMainConfig, _nextMainConfig);
        }

        if (_configReadyAndValid)
        {
            combineConfig();
            efj_trigger(EVENT_CONFIG, _config);
        }

        return;
    }

    bool rejected = false;

    // In the main config, the tx and rx stuff are in the channel_info/channel_0-n elements.
    // FIXME: In practice, there's just a channel_0, and that is the one that gets
    // selected over FSI, so I'm just hardcoding that.
    efj::config_get_value(e, "channel_info/channel_0/rx/Frequency", &_pendingMainConfig->rxFrequency, &rejected);
    efj::config_get_value(e, "channel_info/channel_0/rx/Bandwidth", &_pendingMainConfig->rxBandwidth, &rejected);
    efj::config_get_value(e, "channel_info/channel_0/tx/Frequency", &_pendingMainConfig->txFrequency, &rejected);
    efj::config_get_value(e, "channel_info/channel_0/tx/Bandwidth", &_pendingMainConfig->txBandwidth, &rejected);
    efj::config_get_value(e, "channel_info/channel_0/tx/Power", &_pendingMainConfig->txPower, &rejected);
    efj::config_get_value(e, "channel_info/channel_0/tx/ModulationType", &_pendingMainConfig->txModType, &rejected);
    efj::config_get_value(e, "simulcast/txfreq_offset", &_pendingMainConfig->txFrequencyOffset, &rejected);
    efj::config_get_value(e, "dfsiclient/ctrlport", &_pendingMainConfig->dfsiControlPort, &rejected);
    efj::config_get_value(e, "dfsiclient/vcport", &_pendingMainConfig->dfsiVoicePort, &rejected);
}

void ConfigManager::onInitialConfigDone(efj::config_event& e)
{
    efj_assert(e.type == efj::config_event_init_done);
    efj_log_info("Initial config done. Combining config files and starting app");

    combineConfig();

    // We can operate in the absense of a main config file, but not without a tuning config file.
    if (_curTuningConfig)
    {
        _configReadyAndValid = true;
        efj_trigger(EVENT_CONFIG, _config);
    }
}

void ConfigManager::combineConfig()
{
    // Grab values from the main config if present, falling back to those in the tuning config.
    _config.rxFrequency       = _curMainConfig->rxFrequency.value_or(_curTuningConfig->rxFrequency);
    _config.rxBandwidth       = _curMainConfig->rxBandwidth.value_or(_curTuningConfig->rxBandwidth);
    _config.txFrequency       = _curMainConfig->txFrequency.value_or(_curTuningConfig->txFrequency);
    _config.txBandwidth       = _curMainConfig->txBandwidth.value_or(_curTuningConfig->txBandwidth);
    _config.txModType         = _curMainConfig->txModType.value_or(_curTuningConfig->txModType);
    _config.txPower           = _curMainConfig->txPower.value_or(_curTuningConfig->txPower);

    // Values found in the tuning config only.
    _config.txMxdrOptions      = _curTuningConfig->txMxdrOptions;
    _config.txRxDelay          = _curTuningConfig->txRxDelay;
    _config.exciterBufferCount = _curTuningConfig->exciterBufferCount;

    // Values found in the main config only.
    _config.txFrequencyOffset = _curMainConfig->txFrequencyOffset.value_or(0);
    _config.dfsiControlPort = _curMainConfig->dfsiControlPort;
    _config.dfsiVoicePort   = _curMainConfig->dfsiVoicePort;
}

