// This is the main test file that includes all the other ones throughout the project.

#include "test/test.hpp"

EFJ_TEST(general, abstract_time)
{
    // This could theoretically pass without the test code being correct, but it's so
    // unlikely that I don't think we care.
    efj::timestamp before = efj::now();
    efj::test_advance(50_ms);
    TEST_EQ(efj::now(), before + 50_ms);
}

EFJ_TEST(general, bits)
{
    u8 bytes[] = { 0xee, 0xff, 0xfe, 0xff, 0x0 };

    efj::set_bit(bytes, 0, 0, true);
    efj::set_bit(bytes, 0, 4, true);

    efj::set_bit(bytes, 1, 0, false);
    efj::set_bit(bytes, 2, 0, true);

    efj::set_bit(bytes, 3, 7, false);
    efj::set_bit(bytes, 3, 0, true);

    efj::set_bit(bytes, 4, 0, true);

    TEST_EQ(bytes[0], 0xff);
    TEST_EQ(bytes[1], 0xfe);
    TEST_EQ(bytes[2], 0xff);
    TEST_EQ(bytes[3], 0x7f);
    TEST_EQ(bytes[4], 0x01);
}

EFJ_TEST_INIT()
{
    efj::test_disable<PruInterface>();
    efj::test_disable<DspInterface>();
    efj::test_disable<MxdrInterface>();
    efj::test_disable<DfsiInterface>();
    efj::test_disable<ConfigManager>();
}

#include "dsp/test.cpp"
#include "hw/test.cpp"

