#pragma once

inline void
printOutputQueue(efj::test_output_queue& output)
{
    for (const efj::test_output& cur : output)
    {
        const char* type = cur.type == efj::test_output_pc ? "pc" : "ue";

        efj::log_unformatted(true);
        efj_log_debug("{");
        for (size_t i = 0; i < cur.values.size; i++)
        {
            const char* messageType = "n/a";
            auto* c6kMsg = cur.values[i].as<C6kMessage>();
            if (c6kMsg) messageType = efj::c_str(c6k_get_type(*c6kMsg));

            const char* sep = i == cur.values.size-1 ? "" : ",";
            efj_log_debug("%s:%s:%s%s", type, cur.values[i].name(), messageType, sep);
            if (c6kMsg && c6k_get_type(*c6kMsg) == C6K_TRANSMIT_MULTI)
            {
                efj_log_debug("{");
                auto* multi = (C6kBlockTransmitMulti*)c6kMsg->data;
                int blockCount = multi->getMessageCount();
                for (int i = 0; i < blockCount; i++)
                {
                    const char* sep = i == blockCount-1 ? "" : ",";
                    efj_log_debug("%s%s", efj::c_str(multi->getType(i)), sep);
                }
                efj_log_debug("}");
            }
        }
        efj_log_debug("}\n");
        efj::log_unformatted(false);
    }
}
