#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <utility>

#include <c2.hpp>

enum UserEventType
{
    BLAH_EVENT,
    BAZ_EVENT,
    USER_EVENT_COUNT_
};

struct BlahEvent
{
    int value;
};

struct BazEvent
{
    int value;
};

namespace efj
{

struct user_event
{
    u32 type;
    union
    {
        BlahEvent blah;
        BazEvent  baz;
    };
};

EFJ_DEFINE_UNION_CONTENT_CAST(efj::user_event, type, BLAH_EVENT, blah);
EFJ_DEFINE_UNION_CONTENT_CAST(efj::user_event, type, BAZ_EVENT,  baz);

}


inline efj::user_event
make_blah_event(int val)
{
    efj::user_event result;
    result.type       = BLAH_EVENT;
    result.blah.value = val;

    return result;
}

inline efj::user_event
make_baz_event(int val)
{
    efj::user_event result;
    result.type       = BAZ_EVENT;
    result.baz.value  = val;

    return result;
}


struct Foo
{
    EFJ_OBJECT("Foo");

    efj::timer _timer;

    bool init()
    {
        efj::add_periodic_timer("Timer", 500_ms, &_timer, &Foo::timerExpired);
        efj::subscribe(BLAH_EVENT, &Foo::notify);
        efj::subscribe(BAZ_EVENT,  &Foo::notify);

        return true;
    }

    void notify(const efj::user_event& e)
    {
        auto* baz = efj::content_cast<BazEvent>(e);
        if (baz)
            printf("[%u]: Foo: Got baz event: %u\n", efj::logical_thread_id(), baz->value);
        else
            printf("[%u]: Foo: Got unknown event: %u\n", efj::logical_thread_id(), e.type);
    }

    void timerExpired(efj::timer_event&)
    {
        printf("[%u]: Foo timer\n", efj::logical_thread_id());
    }
};

struct Bar
{
    EFJ_OBJECT("Bar");

    efj::timer _timer;
    int        _value = 0;

    bool init()
    {
        efj::add_periodic_timer("Timer", 1_s, &_timer, &Bar::timerExpired);

        efj::subscribe(BLAH_EVENT, &Bar::notify, efj::sub_async);

        return true;
    }

    void notify(const BlahEvent& e)
    {
        printf("[%u]: Bar: Got blah event: %u\n", efj::logical_thread_id(), e.value);
        _value++;
    }

    void timerExpired(efj::timer_event&)
    {
        static int count = 0;

        if (count++ % 2 == 0)
            efj_trigger(BLAH_EVENT, make_blah_event(10));
        else
            efj_trigger(BAZ_EVENT, make_baz_event(12));
    }
};

int main(int argc, const char** argv)
{
    efj::register_user_events(USER_EVENT_COUNT_);

    Foo foo;
    Bar bar;

    auto& fooThread = *efj::create_thread("Foo");
    fooThread.add_objects(foo);

    auto& barThread = *efj::create_thread("Bar");
    barThread.add_objects(bar);

    return efj::exec(argc, argv);
}

#define EFJ_C2_IMPLEMENTATION
#include <c2.hpp>

