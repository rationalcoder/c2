#include <stdio.h>
#include <common2.hpp>

// TODO: efj::set_log_interval()
// TODO: use udp sockets when upd socket api is completed.

struct LoggerDude
{
    EFJ_OBJECT("Logger Dude");

    efj::timer _timer;

    int value;

    bool init()
    {
        efj::add_periodic_timer("Timer", 10_ms, _timer, &LoggerDude::notify);

        return true;
    }

    void notify(efj::timer&)
    {
        efj_log_debug("The quick brown fox jumps over the lazy dude.");
        efj_log_info("The quick brown fox jumps over the lazy dude.");
        efj_log_warn("The quick brown fox jumps over the lazy dude.");
        efj_log_crit("The quick brown fox jumps over the lazy dude.");
    }
};

struct NmsLogger
{
    EFJ_OBJECT("NMS Logger");
    EFJ_CONSUMER(efj::log_buffer);

    efj::udp_socket _sock;

    bool init()
    {
        efj::create_udp_socket("NMS", _sock);
        //efj::udp_connect(_sock, "127.0.0.1", 50000);

        return true;
    }

    void consume(const efj::log_buffer& buffer)
    {
        for (auto* buf = &buffer; buf; buf = buf->next) {
            //printf("%.*s", buf->size, buf->data);
            efj::send_to(_sock, buf->data, buf->size, "127.0.0.1", 50000);
        }
    }
};

struct FakeNms
{
    EFJ_OBJECT("Fake NMS");

    efj::udp_socket _sock;

    bool init()
    {
        efj::add_udp_socket("NMS", _sock, &FakeNms::notify);
        efj::udp_bind(_sock, "127.0.0.1", 50000);

        return true;
    }

    void notify(efj::udp_socket& sock, flag32 events)
    {
        if (events & efj::io_in) {
            char buf[1024];
            int n = ::read(sock.fd(), buf, sizeof(buf));
            if (n > 0) {
                printf("NMS got:\n");
                printf("%.*s'", n, buf);
                printf("\n");
            }
        }
    }
};

int main(int argc, const char** argv)
{
    LoggerDude dude;
    NmsLogger  nms_logger;
    FakeNms    fake_nms;

    //efj_connect(dfsi_interface, dfsi::Message, atlas4500);
    //efj_connect(dfsi_interface, c6k::Message,  dsp_interface);

    //efj::set_log_file("test.txt");
    //efj::set_log_formatter(formatter);
    //efj::set_logger(nms_logger);
    //efj::set_log_interval(200_ms);

    return efj::exec(argc, argv);
}

#define EFJ_COMMON2_IMPLEMENTATION
#include <common2.hpp>
