namespace efj
{

template <typename T> inline efj::string
to_string(const T& val)
{
    if constexpr (std::is_pointer_v<T> || std::is_same_v<T, nullptr_t>) {
        return efj::fmt("%p", val);
    }
    else {
        return efj::c_str(val);
    }
}

} // namespace efj
