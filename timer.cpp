namespace efj
{

// Convert the internal timer event into the user-facing one.
static void
_dispatch_timer_event(efj::eventsys_event_data* edata)
{
    efj::timer_event e = {};
    // FIXME: We always want a source pointer _and_ a user data pointer. For now, the user data
    // field is the pointer to the timer.
    e.source            = (efj::timer*)edata->udata.as_ptr;
    e.expiredCount      = edata->timer.expiredCount;
    e.expiredTime.value = edata->timer.expiredTime;

    edata->objectCb.caller(&edata->objectCb, &e);
}

// It's nice to have the control-flow come through one place. The compiler should be able to optimize right through this.
static bool
_create_timer(const char* name, efj::nsec initial, efj::nsec interval, efj::timer_type type, efj::timer* result)
{
    efj::object* thisObject = efj::this_object();
    efj_assert(!result->event.created && "Timer already created");

    result->type = type;

    efj::eventsys_timer_desc desc = {};
    desc.dispatch     = &_dispatch_timer_event;
    desc.udata.as_ptr = result;
    desc.initial      = initial;
    desc.interval     = interval;
    desc.type         = type;

    return efj::_eventsys_create_timer(thisObject->thread, name, &desc, &result->event);
}

extern bool
create_oneshot_timer(const char* name, efj::timer* result)
{
    return _create_timer(name, 0_ns, 0_ns, efj::timer_oneshot, result);
}

extern bool
create_oneshot_timer(const char* name, efj::nsec timeout, efj::timer* result)
{
    // If you want an immediately expiring timer, set the timeout to 1_ns.
    // :TimeoutCantBeZero
    efj_panic_if(timeout.value == 0);

    return _create_timer(name, timeout, 0_ns, efj::timer_oneshot, result);
}

extern bool
create_periodic_timer(const char* name, efj::timer* result)
{
    return _create_timer(name, 0_ns, 0_ns, efj::timer_periodic, result);
}

extern bool
create_periodic_timer(const char* name, efj::nsec interval, efj::timer* result)
{
    // :TimeoutCantBeZero
    efj_panic_if(interval.value == 0);

    return _create_timer(name, interval, interval, efj::timer_periodic, result);
}

extern bool
create_periodic_timer(const char* name, efj::nsec initial, efj::nsec interval, efj::timer* result)
{
    // For immediate expiration, use 1_ns.
    // :TimeoutCantBeZero
    efj_panic_if(initial.value == 0);
    efj_panic_if(interval.value == 0);

    return _create_timer(name, initial, interval, efj::timer_periodic, result);
}

extern void
timer_start(efj::timer& t)
{
    //printf("[%u] Starting timer: %s\n", efj::logical_thread_id(), t.event.name);
    efj::_eventsys_start_timer(&t.event);
}

extern void
timer_start(efj::timer& t, efj::nsec timeout)
{
    efj_panic_if(timeout.value == 0);
    //printf("[%u] Starting timer: %s: %llu\n", efj::logical_thread_id(), t.event.name, (efj::llu)timeout.value);
    efj::_eventsys_start_timer(&t.event, timeout);
}

extern void
timer_start_absolute(efj::timer& t, efj::nsec expireTime)
{
    efj::_eventsys_start_timer_absolute(&t.event, expireTime);
}

extern void
timer_start_absolute(efj::timer& t, efj::nsec expireTime, efj::nsec interval)
{
    efj_panic_if(t.type != efj::timer_periodic);
    efj_panic_if(interval.value == 0);

    efj::_eventsys_start_timer_absolute(&t.event, expireTime, interval);
}

extern void
timer_stop(efj::timer& t)
{
    efj::_eventsys_stop_timer(&t.event);
}

extern void
timer_destroy(efj::timer& t)
{
    efj::_eventsys_destroy_timer_event(&t.event);
    t = {};
}

extern void
timer_set_timeout(efj::timer& t, efj::nsec timeout)
{
    efj_panic_if(timeout.value == 0);
    efj::_eventsys_set_timeout(&t.event, timeout, timeout);
}

extern void
timer_set_absolute_timeout(efj::timer& t, efj::nsec expireTime)
{
    efj::_eventsys_set_absolute_timeout(&t.event, expireTime);
}

extern void
timer_set_timeout(efj::timer& t, efj::nsec initial, efj::nsec interval)
{
    efj_panic_if(initial.value == 0);
    efj_panic_if(interval.value == 0);
    efj::_eventsys_set_timeout(&t.event, initial, interval);
}

extern void
timer_set_absolute_timeout(efj::timer& t, efj::nsec initial, efj::nsec interval)
{
    efj_panic_if(interval.value == 0);
    efj::_eventsys_set_timeout(&t.event, initial, interval);
}

} // end namespace efj

