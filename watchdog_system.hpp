namespace efj
{

// NOTE: The watchdog system is designed to be able to loop through many threads efficiently, but honestly,
// we should never have more than a handful of threads in the system, anyway.

enum
{
    WATCHDOG_BACKTRACE_SYMBOL_COUNT = 100
};

// We have one table of these that all threads update when they handle an event, indexed by ltid.
struct thread_activity_entry
{
    union
    {
        u32 cur;

        // It's nice to have one table indexed by ltid that all threads update, but if we just pack them all
        // together, we would get false sharing. We assume the activity count is updated many thousands of
        // times more than the watchdog thread is checking it, so not padding this would mean every thread
        // that is close to each other in logical-id order would cause an extra cache-miss for the others
        // every event.
        //
        // We could just loop through these entries stored in thread contexts and call it a day, since the
        // watchdog thread will have a cache-miss checking every entry that's been updated anyway.
        // The benefit of doing it this way is that we can take advantage of prefetching when looping through
        // an array of these, since prefetching works by loading cache lines at fixed offsets.
        char pad[EFJ_CACHELINE_SIZE];
    };
};

// The activity state checked and maintained by the watchdog thread. The table of these runs parallel
// to the activity table, also indexed by ltid.
struct thread_activity_state
{
    efj::thread* thread;        // Useful at the moment b/c threads aren't stored contiguously.
    u32          prev;          // The last known activity value.
    bool         toldToCheckIn; // If we have told a thread to check-in and it hasn't, it's unresponsive.
};

struct watchdog_thread_ctx
{
    efj::watchdog_system*        sys;
    efj::memory_arena*           arena;

    efj::eventsys_waitable_event checkInEvent;

    bool                         crashed;
    void*                        lastBtSymbols[WATCHDOG_BACKTRACE_SYMBOL_COUNT];
    int                          lastBtSymbolCount;
};

struct watchdog_system
{
    efj::memory_arena*           arena;
    efj::thread*                 watchdogThread;

    efj::thread_activity_entry*  threadActivityTable;
    efj::thread_activity_state*  threadActivityStateTable;

    //{ @TS. For communicating crash information from crashed threads to the watchdog thread.
    int                          crashSignal;
    bool                         threadUnresponsive;
    int                          threadCrashed;
    efj::thread*                 crashedThread;
    efj::object*                 crashedObject;
    const char*                  crashedEventName;
    //}

    efj::eventsys_waitable_event threadCrashedEvent;
    efj::eventsys_timer_event    checkActivityTimer;
};

//{ These are all called in order.
static void _watchdog_pre_init(efj::watchdog_system* sys, efj::memory_arena* arena);
// Called on the main thread before object init, and before thread responsibilites are known.
static void _watchdog_init_thread_context(efj::watchdog_system* sys, efj::thread* thread, efj::memory_arena* arena);
// Called on the main thread before object init, and after thread responsibilities are known.
static void _watchdog_pre_object_init(efj::watchdog_system* sys, efj::thread* watchdogThread);
// Called on the main thread after object init.
static void _watchdog_post_object_init(efj::watchdog_system* sys);
//}

static void _watchdog_on_thread_activity(efj::thread* thread);
static void _watchdog_handle_check_activity_event(efj::eventsys_event_data* edata);
static void _watchdog_handle_checkin_event(efj::eventsys_event_data* edata);
static void _watchdog_handle_thread_crashed_event(efj::eventsys_event_data* edata);

static bool _watchdog_is_thread_crashed(efj::thread* thread);

} // namespace efj

