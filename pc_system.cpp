namespace efj
{

static void
_pcsys_pre_init(efj::pc_system* sys, efj::memory_arena* arena)
{
    sys->arena = arena;
}

static void
_pcsys_init_thread_context(efj::pc_system* sys, efj::thread* thread, efj::memory_arena* arena)
{
    efj::pcsys_thread_ctx* ctx = thread->pcContext;
    ctx->sys   = sys;
    ctx->arena = arena;
}

static void
_pcsys_post_object_init(efj::pc_system* sys)
{
}

static void
_pcsys_handle_deferred(efj::thread* thread)
{
}

extern void
_pcsys_queue_consume(efj::thread* sourceThread, efj::consumer* c,
    const efj::component_view* components, umm count)
{
    efj::thread* targetThread = c->object->thread;

    efj::eventsys_allocated_event handle
        = efj::_eventsys_allocate_event(sourceThread, targetThread, sizeof(efj::pcsys_thread_event));

    auto* threadEvent           = (efj::pcsys_thread_event*)handle.e;
    threadEvent->dispatch       = _pcsys_handle_consume_event;
    threadEvent->consumer       = c;
    threadEvent->components     = efj_push_array(*handle.arena, count, efj::component_view);
    threadEvent->componentCount = count;

    efj::allocator alloc = *handle.arena;
    for (umm i = 0; i < count; i++)
        threadEvent->components[i] = components[i].copy(alloc);

    efj::_eventsys_send_event(sourceThread, targetThread, &handle);
}

static void
_pcsys_handle_consume_event(efj::eventsys_thread_event* e)
{
    auto* ourEvent = (efj::pcsys_thread_event*)e;
    efj::_testsys_on_consume(ourEvent->consumer, ourEvent->components, ourEvent->componentCount);
    ourEvent->consumer->cb.caller(&ourEvent->consumer->cb, ourEvent->components);
}

} // namespace efj

