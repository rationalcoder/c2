// Internal. You can decide to depend on any definitions if you want to.

namespace efj
{

// Internal type that's just here because the definition was needed for an array of config_files.
struct config_file
{
    efj::config_type                  type = efj::config_xml;
    union
    {
        efj::xml_document*            xml = nullptr;
    };

    u32                               id = 0;

    const char*                       path     = nullptr;
    int                               watchFd  = -1;
    bool                              moved    = false;
    bool                              removed  = false;
    bool                              lost     = false;

    // Maintained by the config system.
    efj::array<thread*>               uniqueSubscribedThreads;
    efj::array<callback>              uniqueChangeCallbacks;
    // Indexed by logical thread id.
    efj::array<efj::array<callback>>  uniqueChangeCallbacksByThread;

    // Number of uniquely-subscribed threads that aren't the config thread.
    // Used to know how many threads we need to synchronize when handling config events.
    u32                               otherSubscribedThreadCount;

    u32  get_id() const { return id; }
    auto get_type() const -> efj::config_type { return type; }

    auto get(const efj::string& path) const -> efj::string;
    auto get_list(const efj::string& path) const -> efj::config_value_list;
};

bool _config_subscribe(u32 id, efj::callback cb);
bool _config_subscribe(efj::config_event_type type, efj::callback cb);

inline bool
config_accepted(efj::config_event& e)
{
    return e.type == efj::config_event_apply;
}

inline void
config_reject(efj::config_event& e, const efj::string& reason)
{
    e.rejectReason = reason.dup(efj::event_memory());
    e.rejected = true;
}

template <typename T> inline bool
config_subscribe(u32 id, void (T::*callback)(efj::config_event&))
{
    efj::object* thisObject = efj::this_object();
    efj_assert(thisObject);

    efj::callback cb = efj::_make_object_callback(thisObject, callback);
    return efj::_config_subscribe(id, cb);
}

template <typename T> inline bool
config_subscribe(efj::config_event_type type, void (T::*callback)(efj::config_event&))
{
    efj::object* thisObject = efj::this_object();
    efj_assert(thisObject);

    if (type == efj::config_event_invalid || type == efj::config_event_count_)
        return false;

    efj::callback cb = efj::_make_object_callback(thisObject, callback);
    return efj::_config_subscribe(type, cb);
}

inline efj::string
config_get_text(efj::config_event& e, const efj::string& path, bool* rejected)
{
    efj::string result = e.file->get(path);
    if (!rejected) return result;

    if (!result.data) {
        efj::config_reject(e, efj::fmt("missing %s", path.c_str()));
        if (rejected)
            *rejected = true;
    }

    return result;
}

inline void
_convert_or_reject(efj::config_event& e, const efj::string& path, const efj::string& text, bool* value, bool* rejected)
{
    bool convertible = false;
    bool result = text.to_bool(false, &convertible);
    if (!convertible) {
        efj::config_reject(e, efj::fmt("%s invalid bool", path.c_str()));
        if (rejected)
            *rejected = true;
    }

    *value = result;
}

inline void
_convert_or_reject(efj::config_event& e, const efj::string& path, const efj::string& text, u8* value, bool* rejected)
{
    bool convertible = false;
    u8 result = text.to_u8(0, &convertible);

    if (!convertible) {
        efj::config_reject(e, efj::fmt("%s invalid u8", path.c_str()));
        if (rejected)
            *rejected = true;
        return;
    }

    *value = result;
}

inline void
_convert_or_reject(efj::config_event& e, const efj::string& path, const efj::string& text, u16* value, bool* rejected)
{
    bool convertible = false;
    u16 result = text.to_u16(0, &convertible);

    if (!convertible) {
        efj::config_reject(e, efj::fmt("%s invalid u16", path.c_str()));
        if (rejected)
            *rejected = true;
        return;
    }

    *value = result;
}

inline void
_convert_or_reject(efj::config_event& e, const efj::string& path, const efj::string& text, u32* value, bool* rejected)
{
    bool convertible = false;
    u32 result = text.to_u32(0, &convertible);

    if (!convertible) {
        efj::config_reject(e, efj::fmt("%s invalid u32", path.c_str()));
        if (rejected)
            *rejected = true;
        return;
    }

    *value = result;
}

inline bool
config_get_bool(efj::config_event& e, const efj::string& path, bool* rejected)
{
    efj::string text = efj::config_get_text(e, path, rejected);
    if (rejected && *rejected)
        return false;

    bool result = false;
    _convert_or_reject(e, path, text, &result, rejected);
    return result;
}

inline void
config_get_value(efj::config_event& e, const efj::string& path, bool* value, bool* rejected)
{
    *value = efj::config_get_bool(e, path, rejected);
}

inline void
config_get_value(efj::config_event& e, const efj::string& path, u8* value, bool* rejected)
{
    efj::string text = efj::config_get_text(e, path, rejected);
    if (rejected && *rejected)
        return;

    _convert_or_reject(e, path, text, value, rejected);
}

inline void
config_get_value(efj::config_event& e, const efj::string& path, u16* value, bool* rejected)
{
    efj::string text = efj::config_get_text(e, path, rejected);
    if (rejected && *rejected)
        return;

    _convert_or_reject(e, path, text, value, rejected);
}

inline void
config_get_value(efj::config_event& e, const efj::string& path, u32* value, bool* rejected)
{
    efj::string text = efj::config_get_text(e, path, rejected);
    if (rejected && *rejected)
        return;

    _convert_or_reject(e, path, text, value, rejected);
}

inline void
config_get_value(efj::config_event& e, const efj::string& path, efj::optional<bool>* value, bool* rejected)
{
    // NOTE: Right now the efj::optional overloads do two lookups, but it was easy this way @Slow.
    efj::string text = e.file->get(path);
    if (text.empty()) {
        value->clear();
        return;
    }

    _convert_or_reject(e, path, text, &value->value_or_emplace(), rejected);
}

inline void
config_get_value(efj::config_event& e, const efj::string& path, efj::optional<u8>* value, bool* rejected)
{
    efj::string text = e.file->get(path);
    if (text.empty()) {
        value->clear();
        return;
    }

    _convert_or_reject(e, path, text, &value->value_or_emplace(), rejected);
}

inline void
config_get_value(efj::config_event& e, const efj::string& path, efj::optional<u16>* value, bool* rejected)
{
    efj::string text = e.file->get(path);
    if (text.empty()) {
        value->clear();
        return;
    }

    _convert_or_reject(e, path, text, &value->value_or_emplace(), rejected);
}

inline void
config_get_value(efj::config_event& e, const efj::string& path, efj::optional<u32>* value, bool* rejected)
{
    efj::string text = e.file->get(path);
    if (text.empty()) {
        value->clear();
        return;
    }

    _convert_or_reject(e, path, text, &value->value_or_emplace(), rejected);
}

} // namespace efj
