namespace efj
{

struct arena_queue_entry
{
    efj::arena_queue_entry*  next     = nullptr;
    efj::memory_arena_chunk* endChunk = nullptr;

    template <typename T> EFJ_MACRO const T* as_aligned() const
    {
        return (T*)efj::align_up(this + 1, alignof(T));
    }

    template <typename T> EFJ_MACRO T* as_aligned()
    {
        return (T*)efj::align_up(this + 1, alignof(T));
    }

    template <typename T> EFJ_MACRO const T* as() const
    {
        static_assert(alignof(T) <= alignof(*this), "Payload type has a higher alignment than the header. Use as_aligned instead.");
        return (T*)(this + 1);
    }

    template <typename T> EFJ_MACRO T* as()
    {
        static_assert(alignof(T) <= alignof(*this), "Payload type has a higher alignment than the header. Use as_aligned instead.");
        return (T*)(this + 1);
    }
};

struct arena_queue
{
    efj::memory_arena        _arena    = {};
    efj::arena_queue_entry*  _front    = nullptr;
    efj::arena_queue_entry** _backNext = &_front;
    umm                      _size     = 0;

    arena_queue(efj::delay_init_tag) {}

    arena_queue(umm chunkSize = 4_KB)
    {
        init(chunkSize);
    }

    ~arena_queue()
    {
        efj::free_arena(_arena);
        _front    = nullptr;
        _backNext = nullptr;
    }

    void init(umm chunkSize = 4_KB)
    {
        efj_panic_if(_arena.curChunk);
        _arena = efj::allocate_arena("arena_queue", chunkSize);
    }

    umm size() const { return _size; }

    efj::memory_arena* push_begin()
    {
        auto* entry = efj_push_new(_arena, efj::arena_queue_entry);

        *_backNext = entry;
        _backNext  = &entry->next;
        ++_size;

        return &_arena;
    }

    void push_end()
    {
        efj_container_of(_backNext, efj::arena_queue_entry, next)->endChunk
            = efj::get_current_chunk(_arena);
    }

    void pop()
    {
        efj_assert(_size);

        auto* next = _front->next;
        efj::recycle_chunks(_arena, _front->endChunk);
        _front = next;
        --_size;

        if (!next)
            _backNext = &_front;
    }

    void clear()
    {
        efj_assert(_arena.curChunk);
        efj::reset(_arena);
        _front    = nullptr;
        _backNext = &_front;
        _size     = 0;
    }

    EFJ_MACRO const efj::arena_queue_entry& front() const { efj_assert(_size); return *_front; }
    EFJ_MACRO efj::arena_queue_entry&       front()       { efj_assert(_size); return *_front; }

    // I didn't feel like implementing C++-style iteration for this. Linked list iteration
    // should be maintainable enough since this is intended to be used as an implementation
    // detail of other data structures.
    EFJ_MACRO const efj::arena_queue_entry* front_node() const { efj_assert(_size); return { _front }; };
    EFJ_MACRO efj::arena_queue_entry*       front_node()       { efj_assert(_size); return { _front }; };
};

} // namespace efj
