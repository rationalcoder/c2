Script="$(readlink --canonicalize-existing "$0")"
ScriptPath="$(dirname "$Script")"

if [[ -z $ClocPath ]]; then
    ClocPath=cloc
fi

cd $ScriptPath/../
$ClocPath --by-file --exclude-dir=examples,test,cloc --match-f=.+.[ch]pp --not-match-f='c2_yxml.*' --not-match-f='c2_stb_sprintf.hpp' .
