Script="$(readlink --canonicalize-existing "$0")"
ScriptPath="$(dirname "$Script")"

cd $ScriptPath/../
~/cloc --by-file --exclude-d=cloc --match-f=.+.[ch]pp --not-match-f='c2_yxml.*' --not-match-f='c2_stb_sprintf.hpp' .
