namespace efj
{

struct log_queue
{
    efj::memory_arena  _arena1 = {};
    efj::memory_arena  _arena2 = {};

    efj::log_message*   _head     = nullptr;
    efj::log_message**  _tailNext = &_head;

    umm                _queueSize   = 0;
    efj::memory_arena* _writeBuffer = &_arena1;
    efj::memory_arena* _readBuffer  = &_arena2;

    pthread_mutex_t    _swapMutex;

    log_queue(umm bufferSize);
    void push(const efj::log_message_ctx* ctx, const efj::string& msg);
    auto swap_buffers() -> efj::log_message*;
};

// :ExposingObjectContext
struct logsys_object_ctx;

struct log_system;
struct logsys_thread_ctx
{
    efj::log_system*   sys;
    efj::memory_arena* arena;
};

struct log_system
{
    efj::memory_arena*           arena;
    efj::thread*                 loggingThread;

    efj::log_queue*              logQueue;
    efj::log_formatter*          formatter;
    efj::logger_fn*              loggerCb;
    efj::object*                 loggerObject;
    const char*                  logPath;
    int                          logFd;

    efj::eventsys_waitable_event logsAvailableEvent;

    bool                         initted;
};

static void _logsys_pre_init(efj::log_system* sys, efj::memory_arena* arena);
static void _logsys_init_thread_context(efj::log_system* sys, efj::thread* thread, efj::memory_arena* arena);
static void _logsys_init_object_context(efj::log_system* sys, efj::object* object);
static void _logsys_pre_object_init(efj::log_system* sys, efj::thread* loggingThread, const efj::log_config* config);
static void _logsys_post_object_init(efj::log_system* sys);
// Called from the public logging API.
static void _logsys_queue_log_message(efj::log_system* sys, const char* data, umm size);

static void _logsys_handle_logs_available_event(efj::eventsys_event_data* edata);
// Split out so we can flush log messages on exit().
static void _logsys_write_log_messages(efj::log_system* sys);

static void _logsys_handle_deferred(efj::thread* thread);


} // namespace efj
