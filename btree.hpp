// WIP
//
// NOTE: This will use slightly different coding conventions from the rest of the code
// b/c I'm pulling the guts of this from a different repo. There's no point in changing it
// since that will only increase the likelyhood of introducing errors.
//
namespace efj
{

// TODO: This is essentially internal right now.

enum
{
    kBtreeKeyCount = 8, // This has to be at least 3. Otherwise, just use a binary tree. :MaxKeyCountAtLeast3
    // Very generous. Allows for more than 2^64 (more than practically available address space) key/value pairs.
    kBtreeParentStackSize = 32,
};

// Internal.
template <typename KeyT, typename ValueT>
struct btree_node
{
    btree_node<KeyT, ValueT>* next;
    btree_node<KeyT, ValueT>* prev;
    size_t                    key_count;
    KeyT                      keys[kBtreeKeyCount];
    ValueT                    values[kBtreeKeyCount];
};

// Internal.
struct btree_parent_stack_entry
{
    void*  node;
    size_t key_index;
};

struct btree_finger
{
    efj::btree_parent_stack_entry stack[kBtreeParentStackSize];
};

template <typename KeyT, typename ValueT>
struct btree
{
    // Having different types for internal versus leaf nodes is annoying and adds complexity.
    // This isn't intended to be a truly general-purpose data structure anyways. We just want
    // to have custom key types and get type casting for keys and values. In practice, having
    // a value type <= size of a void* gives us what we need while not allowing the common
    // anti-pattern of storing large, non-trivial objects in general data structures.
    static_assert(sizeof(ValueT) <= sizeof(void*), "Size of value type must be <= sizeof(void*)");
    using node_type = efj::btree_node<KeyT, void*>;

    efj::allocator node_pool = {};
    node_type*     root_node = nullptr;
    size_t         height    = 0;

    void init(efj::allocator alloc)
    {
        node_pool = alloc.get_pool(sizeof(node_type), alignof(node_type));

        auto* root_node      = efj_allocator_alloc_type(node_pool, node_type);
        root_node->next      = root_node;
        root_node->prev      = root_node;
        root_node->key_count = 0;

        this->root_node = root_node;
        this->height    = 1;
    }

    bool insert(const KeyT& key, const ValueT& value)
    {
        efj::btree_parent_stack_entry parent_stack[kBtreeParentStackSize];
        node_type* node = _find_leaf_with_stack(key, parent_stack);

        size_t level        = height;
        size_t insert_index = node->key_count;

        for (size_t i = 0; i < node->key_count; i++)
        {
            const KeyT& cur_key = node->keys[i];
            if (cur_key >= key)
            {
                if (cur_key == key)
                {
                    node->values[i] = *(void**)&value;
                    return false;
                }

                insert_index = i;
                break;
            }
        }

        // We update this if we need to "recurse."
        KeyT  cur_key   = key;
        void* cur_value = *(void**)&value;

        for (;;)
        {
            _insert_key_value(node, insert_index, cur_key, cur_value);

            efj::btree_parent_stack_entry* parent = &parent_stack[level];

            // Here we do the simplest splitting strategy, which always splits full nodes in to two. This works great for random data, but wastes n/2
            // space for sequential data.

            constexpr size_t kHalfKeyCount       = kBtreeKeyCount >> 1;
            constexpr size_t kBiggerHalfKeyCount = kBtreeKeyCount - kHalfKeyCount;

            if (level == 1)
            {
                if (node->key_count < kBtreeKeyCount)
                    return true;

                KeyT split_key = node->keys[kBiggerHalfKeyCount]; // :MaxKeyCountAtLeast3

                node_type* split_node  = efj_allocator_alloc_type(node_pool, node_type);

                split_node->next      = node;
                split_node->prev      = node;
                split_node->key_count = kHalfKeyCount; // e.g. KeyCount==7 => split-node gets 3
                node->prev            = split_node;
                node->next            = split_node;
                node->key_count       = kBiggerHalfKeyCount; // e.g. KeyCount==7 => this node gets 4

                _copy_pairs(node, split_node, kBiggerHalfKeyCount, kBtreeKeyCount, 0);

                node_type* new_root_node = efj_allocator_alloc_type(node_pool, node_type);
                new_root_node->next      = new_root_node;
                new_root_node->prev      = new_root_node;
                new_root_node->key_count = 2;

                new_root_node->keys[0]   = node->keys[0];
                new_root_node->keys[1]   = split_key;
                new_root_node->values[0] = node;
                new_root_node->values[1] = split_node;
                root_node                = new_root_node;
                height                   = height + 1;
                return true;
            }

            if (insert_index == 0)
                _update_key(node, parent, parent_stack);

            if (node->key_count < kBtreeKeyCount)
                return true;

            node_type* parent_node  = (node_type*)parent->node;
            size_t     key_index    = parent->key_index;

            //{ Optional
            //
            // Try to avoid splitting by seeing if there is enough room in our left sibling for half our keys (minus 1).
            // This produces more tightly packed leaf nodes when inserting ascending keys. The waste is roughly n/k pairs instead of n/2.
            if (key_index > 0 && node->prev->key_count <= kBiggerHalfKeyCount-1)
            {
                _copy_pairs(node, node->prev, 0, kBiggerHalfKeyCount, node->prev->key_count);
                _copy_pairs(node, node, kHalfKeyCount, kBtreeKeyCount, 0);

                //printf("Our key count: %zu %zu\n", node->key_count, kBiggerHalfKeyCount);
                node->prev->key_count = node->prev->key_count + kHalfKeyCount;
                node->key_count       = kBiggerHalfKeyCount;
                parent_node->keys[key_index] = node->keys[0];
                return true;
            }
            //}

            node_type* split_node = efj_allocator_alloc_type(node_pool, node_type);
            KeyT       split_key  = node->keys[kBiggerHalfKeyCount];

            split_node->next      = node->next;
            split_node->prev      = node;
            split_node->key_count = kHalfKeyCount;
            node->next->prev      = split_node;
            node->next            = split_node;
            node->key_count       = kBiggerHalfKeyCount;

            _copy_pairs(node, split_node, kBiggerHalfKeyCount, kBtreeKeyCount, 0);

            // This is what we would like to do, but we can't know if this is valid, so we set this up as
            // the next key/value to insert and try the insert in the parent.
            //parent_node->keys[key_index+1] = split_key;

            cur_key      = split_key;
            cur_value    = split_node;
            node         = parent_node;
            insert_index = key_index + 1;
            level        = level - 1;
        }

        efj_assert(!"invalid code path");
        return false;
    }

    void remove(const KeyT& key, const ValueT& value)
    {
        efj::btree_parent_stack_entry parent_stack[kBtreeParentStackSize];
        node_type* node = _find_leaf_with_stack(key, parent_stack);

        size_t level = height;
        size_t remove_index = 0;

        // We handle the case of removing a key that doesn't exist specially so that
        // 1) doing so doesn't modify the tree, and
        // 2) we can assume that a key is actually being removed while in the main loop.

        size_t i = 0;
        for ( ; i < node->key_count; i++)
        {
            if (node->keys[i] == key)
            {
                remove_index = i;
                break;
            }
        }

        if (i == node->key_count)
            return;

        for (;;)
        {
            _remove_key_value(node, remove_index);

            if (level == 1)
            {
                if (node->key_count == 1)
                {
                    if (height > 1)
                    {
                        efj_allocator_free(node_pool, root_node);
                        root_node = (node_type*)node->values[0];
                        --height;
                        return;
                    }
                }

                return;
            }

            efj::btree_parent_stack_entry* parent = &parent_stack[level];

            // We change the invariant here from "all non-root nodes are at least half full" to
            // "no two adjacent nodes can be merged" which means that all non-root nodes are at least half full *on average*.

            // Normally, we want to merge instead removing. However, removing the key that identifies this node means we
            // have to do an update, so we just free the node and do a normal remove instead.
            if (node->key_count == 0)
            {
                node->next->prev = node->prev;
                node->prev->next = node->next;
                // TODO: We shouldn't be able to get into this state without either the left or right sibling being full, since insertion strategies
                // that always result in non-full nodes should hit a merge case when the key count gets to 1. Therefore, we can merge half the
                // right or left node's keys into this one and do a key update.
                efj_allocator_free(node_pool, node);

                remove_index = parent->key_index;
                node         = (node_type*)parent->node;
                level        = level - 1;
                continue;
            }

            // NOTE: We always merge right->left to make sure that we don't update the lowest key in the resulting node. Otherwise, we would
            // have to do a removal _and_ a key update for different nodes since merging left->right implies changing the lowest key in the right.

            // If we have a left node, try to merge it and remove the merged node's key from the parent.
            if (parent->key_index != 0)
            {
                size_t merged_size = node->key_count + node->prev->key_count;
                if (merged_size <= kBtreeKeyCount)
                {
                    _merge_nodes((node_type*)parent->node, merged_size, node->prev, node);
                    efj_allocator_free(node_pool, node);
                    remove_index = parent->key_index;
                    node         = (node_type*)parent->node;
                    level        = level - 1;
                    continue;
                }
            }

            // We can only get away with not doing a key update when this node is merged with the left node. Once we are talking
            // about merging our right node with this node, we have to update this nodes key if the lowest one was removed.
            if (remove_index == 0)
                _update_key(node, parent, parent_stack);

            // If we have a right node, try to merge it and remove the merged node's key from the parent.
            if (parent->key_index < ((node_type*)parent->node)->key_count-1)
            {
                size_t merged_size = node->key_count + node->next->key_count;
                if (merged_size <= kBtreeKeyCount)
                {
                    node_type* next = node->next;
                    _merge_nodes((node_type*)parent->node, merged_size, node, next);
                    efj_allocator_free(node_pool, next);
                    remove_index = parent->key_index + 1;
                    node         = (node_type*)parent->node;
                    level        = level - 1;
                    continue;
                }
            }

            break;
        }
    }

    ValueT* find(const KeyT& key)
    {
        node_type* node = root_node;
        // We don't need the parent stack, here.
        for (size_t level = 1; level < height; level++)
        {
            size_t key_index = 0;
            for (size_t i = 1; i < node->key_count; i++)
            {
                if (key < node->keys[i])
                    break;

                ++key_index;
            }

            node = (node_type*)node->values[key_index];
        }

        for (size_t i = 0; i < node->key_count; i++)
        {
            if (node->keys[i] == key)
                return (ValueT*)&node->values[i];
        }

        return nullptr;
    }

    node_type* _find_leaf_with_stack(const KeyT& key, efj::btree_parent_stack_entry* parent_stack)
    {
        node_type* node = root_node;
        parent_stack[0] = {};
        parent_stack[1] = {};

        size_t level = 1;
        while (level < height)
        {
            ++level;

            // Find the node that contains this key. This is the upper-bound of the key. Keys are stored [first, last).
            size_t key_index = 0;
            for (size_t i = 1; i < node->key_count; i++)
            {
                if (key < node->keys[i])
                    break;

                ++key_index;
            }

            //printf("key index at level %zu: %zu (%zu)\n", level, key_index, key);

            //printf("Parent[%zu] = %p\n", level, node);
            parent_stack[level].node      = node;
            parent_stack[level].key_index = key_index;

            node = (node_type*)node->values[key_index];
        }

        return node;
    }

    EFJ_MACRO void _insert_key_value(node_type* node, size_t insert_index, const KeyT& key, void* value)
    {
        size_t node_key_count = node->key_count;
        //printf("insert_index: %zu, key_count: %zu\n", insert_index, node_key_count);

        if (insert_index >= node_key_count)
        {
            //printf("Appending\n");
            node->keys[node_key_count]   = key;
            node->values[node_key_count] = value;
            node->key_count              = node_key_count + 1;
            return;
        }

        KeyT  cur_key   = node->keys[insert_index];
        void* cur_value = *(void**)&node->values[insert_index];

        // Do the first swap first so we don't have to come back to it later, memory-wise.
        node->keys[insert_index]   = key;
        node->values[insert_index] = value;

        size_t overwrite_index = insert_index + 1;
        for (;;)
        {
            if (overwrite_index == node_key_count)
            {
                node->keys[overwrite_index]   = cur_key;
                node->values[overwrite_index] = cur_value;
                break;
            }

            KeyT  next_key   = node->keys[overwrite_index];
            void* next_value = *(void**)&node->values[overwrite_index];

            node->keys[overwrite_index]   = cur_key;
            node->values[overwrite_index] = cur_value;

            cur_key   = next_key;
            cur_value = next_value;
            ++overwrite_index;
        }

        node->key_count = node_key_count + 1;
    }

    EFJ_MACRO void _remove_key_value(node_type* node, size_t index)
    {
        efj_assert(index < node->key_count);
        for (size_t i = index+1; i < node->key_count; i++)
        {
            node->keys[i-1]   = node->keys[i];
            node->values[i-1] = node->values[i];
        }

        --node->key_count;
    }

    EFJ_MACRO void _merge_nodes(node_type* parent, size_t merged_size, node_type* left, node_type* right)
    {
        left->next        = right->next;
        right->next->prev = left;

        size_t right_i = 0;
        for (size_t left_i = left->key_count; left_i < merged_size; left_i++, right_i++)
        {
            left->keys[left_i]   = right->keys[right_i];
            left->values[left_i] = right->values[right_i];
        }

        left->key_count = merged_size;
    }


    // `src_size` is how many keys the source node keeps.
    EFJ_MACRO void _copy_pairs(node_type* src_node, node_type* dst_node,
        size_t src_offset, size_t src_size, size_t dst_offset)
    {
        size_t dst_i = dst_offset;
        for (size_t src_i = src_offset; src_i < src_size; src_i++, dst_i++)
        {
            dst_node->keys[dst_i]   = src_node->keys[src_i];
            dst_node->values[dst_i] = src_node->values[src_i];
        }
    }

    EFJ_MACRO void _update_key(node_type* node, efj::btree_parent_stack_entry* parent,
        efj::btree_parent_stack_entry* stack)
    {
        efj::btree_parent_stack_entry* update_parent = parent;
        KeyT                           new_min_key   = node->keys[0];

        // If we updated the lowest key, we need to update the key in the parent.
        for (;;)
        {
            size_t update_index = update_parent->key_index;

            ((node_type*)update_parent->node)->keys[update_index] = new_min_key;
            if (update_index != 0 || update_parent == &stack[2])
                break;

            --update_parent;
        }
    }
};

} // namespace efj
