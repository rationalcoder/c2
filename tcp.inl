namespace efj
{

struct tcp_server
{
    efj::eventsys_epoll_event      event;
    efj::callback                  cb   = efj::null_callback();
    efj::tcp_server_info           info = {};

    efj::tcp_connected_client_list clients;
    efj::free_list                 freeClients;
};


struct tcp_client
{
    efj::eventsys_epoll_event event;
    efj::callback             cb              = efj::null_callback();
    bool                      connectHandled  = false;
    bool                      handlingConnect = false;
};

// @Temporary(bmartin). The client list will have to be a more
// complicated data structure in the future. Right now, I'll settle
// for somethinig that gets me foreach and easy deletions.
struct tcp_connected_client
{
    efj::tcp_connected_client* next;
    efj::tcp_connected_client* prev;

    efj::eventsys_epoll_event  event;

    efj::callback              cb;
    efj::tcp_server*           server;
    void*                      udata;
};


inline ssize_t tcp_write(efj::tcp_connected_client& client, const void* buf, umm size) { return ::write(client.event.fd, buf, size); }
inline ssize_t tcp_read(efj::tcp_connected_client& client, void* buf, umm size) { return ::read(client.event.fd, buf, size); }
inline ssize_t tcp_send(efj::tcp_connected_client& client, const void* buf, umm size, int flags) { return ::send(client.event.fd, buf, size, flags); }
inline ssize_t tcp_recv(efj::tcp_connected_client& client, void* buf, umm size, int flags) { return ::recv(client.event.fd, buf, size, flags); }

inline ssize_t tcp_write(efj::tcp_client& client, const void* buf, umm size) { return ::write(client.event.fd, buf, size); }
inline ssize_t tcp_read(efj::tcp_client& client, void* buf, umm size) { return ::read(client.event.fd, buf, size); }
inline ssize_t tcp_send(efj::tcp_client& client, const void* buf, umm size, int flags) { return ::send(client.event.fd, buf, size, flags); }
inline ssize_t tcp_recv(efj::tcp_client& client, void* buf, umm size, int flags) { return ::recv(client.event.fd, buf, size, flags); }

inline ssize_t
tcp_send_all(efj::tcp_client& client, const void* buf, umm size, int flags)
{
    umm sent = 0;
    while (sent < size) {
        ssize_t n = efj::tcp_send(client, (const u8*)buf + sent, size - sent, flags);
        if (n <= 0)
            return n;

        sent += (umm)n;
    }

    return sent;
}

template <typename T_> inline void
tcp_create_client(const char* name, efj::tcp_client& result,
                  void (T_::*callback)(efj::tcp_client& client, flag32 events))
{
    efj::callback cb;
    cb.type = efj::callback_memfn;
    ::memcpy(&cb.memfn, &callback, sizeof(callback));

    static_assert(sizeof(callback) <= sizeof(cb.memfn),
        "efj::callback::memfn not big enough.");

    result = efj::_tcp_create_client(name, cb);
}

inline bool
connection_failed(const efj::tcp_client& c, flag32 events)
{
    return c.handlingConnect == false && ((events & efj::io_hup) || (events & efj::io_err));
}

inline bool
connection_succeeded(const efj::tcp_client& c, flag32 events)
{
    return c.connectHandled == false && (events & efj::io_out) && !(events & efj::io_err) && !(events & efj::io_hup);
}

inline bool
connection_ready(const efj::tcp_server&, flag32 events) { return events & efj::io_in; }

bool _tcp_listen(efj::tcp_server& server, int backlog, const efj::callback& cb);

template <typename T_> inline bool
tcp_listen(efj::tcp_server& server, int backlog,
           void (T_::*callback)(efj::tcp_server& server, flag32 events))
{
    efj::callback cb;
    cb.type = efj::callback_memfn;
    ::memcpy(&cb.memfn, &callback, sizeof(callback));

    static_assert(sizeof(callback) <= sizeof(cb.memfn),
        "efj::callback::memfn not big enough.");

    return efj::_tcp_listen(server, backlog, cb);
}


bool _tcp_accept(efj::tcp_server& server, struct sockaddr_in* clientAddress,
                 const efj::callback& cb, void* udata);

template <typename T_> inline bool
tcp_accept(efj::tcp_server& server, struct sockaddr_in* clientAddress,
           void (T_::*callback)(efj::tcp_connected_client& client,
                                flag32 events, void* udata),
           void* udata)
{
    efj::callback cb;
    cb.type = efj::callback_memfn;
    ::memcpy(&cb.memfn, &callback, sizeof(callback));

    static_assert(sizeof(callback) <= sizeof(cb.memfn),
        "efj::callback::memfn not big enough.");

    return efj::_tcp_accept(server, clientAddress, cb, udata);
}


} // namespace efj
