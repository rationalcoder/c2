namespace efj
{

struct udp_socket
{
    efj::eventsys_epoll_event event;
    bool                      connected = false;
};

template <typename T_> inline bool
udp_add_socket(const char* name, efj::udp_socket* result,
               void (T_::*callback)(efj::udp_socket_event& e))
{
    efj_assert(!result->event.created && "Socket already created.");

    if (!efj::udp_create_socket(name, result))
        return false;

    efj::udp_monitor(*result, callback);
    return true;
}

template <typename T> inline void
udp_monitor(efj::udp_socket& sock, void (T::*callback)(efj::udp_socket_event& e))
{
    efj_assert(sock.event.created && "Socket monitored before creation.");
    efj::object* object = efj::this_object();
    efj::thread* thread = object->thread;

    efj::callback cb = efj::_make_object_callback(object, callback);

    efj::epoll_events events = efj::io_in;
    efj::_eventsys_monitor_epoll_event(thread, &sock.event, &events, &cb);
}

inline umm
udp_read(efj::udp_socket_event& e, void* buf, size_t size)
{
    efj_assert(e.type == efj::udp_socket_event_read);
    ssize_t result = efj::udp_read(*e.sock, buf, size);
    // TODO: Do we generate an error event for this?
    if (result < 0)
        return 0;

    return (umm)result;
}

inline umm
udp_recv_from(efj::udp_socket_event& e, void* buf, umm size, efj::net_sock_addr* addr, int flags)
{
    ssize_t result = efj::udp_recv_from(*e.sock, buf, size, addr, flags);
    // TODO: Do we generate an error event for this?
    if (result < 0)
        return 0;

    return (umm)result;
}

inline umm
udp_send_to(efj::udp_socket_event& e, const void* buf, umm size,
            const char* ip, u16 port, int flags)
{
    ssize_t result = efj::udp_send_to(*e.sock, buf, size, efj::net_make_sock_addr(ip, port), flags);
    // TODO: Do we generate an error event for this?
    if (result < 0)
        return 0;

    return (umm)result;
}

inline umm
udp_send_to(efj::udp_socket_event& e, const void* buf, umm size,
            const efj::net_sock_addr& addr, int flags)
{
    ssize_t result = efj::udp_send_to(*e.sock, buf, size, addr, flags);
    // TODO: Do we generate an error event for this?
    if (result < 0)
        return 0;

    return (umm)result;
}

} // namespace efj
