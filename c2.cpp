#include <sys/unistd.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/mman.h>
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/inotify.h>
#include <sys/timerfd.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <sys/un.h>
#include <sys/uio.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <linux/sockios.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <limits.h>
#include <semaphore.h>
#include <time.h>
#include <execinfo.h>
#include <getopt.h>
#include <spawn.h>
// This is for ARM specific stack trace walking which turned out to not really be useful.
//#include <ucontext.h>

// @Temporary
#include <stdio.h>
#include <errno.h>
#include <signal.h>

#include "dbp.hpp"
#include "user_event_system.hpp"
#include "pc_system.hpp"
#include "log_system.hpp"
#include "config_system.hpp"
#include "watchdog_system.hpp"
#include "debug_system.hpp"
#include "test_system.hpp"

// IMPORTANT:
// if you want to build with address sanitizer with GCC version less than 8, you have to manually
// replace a define in the stb_sprintf file. Otherwise, it will report false positives.
//
// Replace this
// #define STBI__ASAN __attribute__((no_sanitize("address")))
// with this
// #define STBI__ASAN __attribute__((no_sanitize_address))
//
#include "c2_stb_sprintf.hpp"

#include "error_code.cpp"
#include "mem.cpp"
#include "arena.cpp"
#include "btree.cpp"
#include "app.cpp"
#include "message.cpp"
#include "component.cpp"
#include "event_system.cpp"
#include "pc_system.cpp"
#include "user_event_system.cpp"
#include "log_system.cpp"
#include "config_system.cpp"
#include "debug_system.cpp"
#include "watchdog_system.cpp"
#include "test_system.cpp"

#include "fd.cpp"
#include "timer.cpp"
#include "net.cpp"
#include "udp.cpp"
#include "tcp.cpp"
#include "logging.cpp"
#include "time.cpp"
#include "file.cpp"
#include "config.cpp"
#include "object.cpp"
#include "debug.cpp"
#include "user_events.cpp"
#include "test_api.cpp"

#include "c2_yxml.h"
#include "c2_yxml.c"
#include "xml.cpp"

#define STB_SPRINTF_IMPLEMENTATION
#define STB_SPRINTF_NOUNALIGNED // For ARMv5
// We can't workaround asan warnings in stb sprintf for some reason. The global-buffer
// read errors are false positives.
//#define STBI__ASAN __attribute__((no_sanitize("address")))
#include "c2_stb_sprintf.hpp"
#include "string.cpp"

