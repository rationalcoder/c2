namespace efj
{

enum
{
    // IMPORTANT: This can't be zero, even if at some point this implementation is resilient against that.
    array_min_capacity = 8,
    array_max_alignment = sizeof(void*) * 2, // The same as libc malloc.
};

template <typename T>
struct view
{
    using iterator = T*;
    using const_iterator = const T*;

    T*     data;
    size_t size;

    view() : data(), size() {}
    view(T* data, size_t size) : data(data), size(size) {}

    template <size_t ExtentV>
    view(T(&arr)[ExtentV]) : data((T*)arr), size(ExtentV) {}

    const T& operator [] (size_t index) const
    { efj_panic_if(index >= size); return data[index]; }

    T& operator [] (size_t index)
    { efj_panic_if(index >= size); return data[index]; }

    const T& unsafe_at(size_t index) const { return data[index]; }
    T&       unsafe_at(size_t index)       { return data[index]; }

    const_iterator cbegin() const { return data; }
    const_iterator cend() const { return data + size; }

    const_iterator begin() const { return data; }
    const_iterator end() const { return data + size; }

    iterator begin() { return data; }
    iterator end() { return data + size; }
};

using buffer = efj::view<u8>;

template <typename T, size_t ExtentV> constexpr EFJ_MACRO efj::view<T>
view_of(T(&arr)[ExtentV]) { return efj::view<T>(arr, ExtentV); }

template <typename T> constexpr EFJ_MACRO efj::view<T>
view_of(T* arr, size_t n) { return efj::view<T>(arr, n); }

// I have left this inline instead of forcing it to be extern so we can experiment with different compiler
// options like -Os vs -O2, and the compiler can choose to inline right through this code and generate code that is
// as fast as if it were type-specific.
inline void* array_expand(void* begin, size_t elemSize, size_t size, size_t* curCapacity, size_t newCapacity,
    efj::allocator* alloc, bool memcopyable);

// We provide these to at least inline the most common case, which is doing a simple capacity check
// and finding that we don't need to expand.

template <typename T> EFJ_MACRO void
array_reserve(T** begin, size_t size, size_t* capacity, size_t newCapacity, efj::allocator* alloc)
{
    if (*capacity >= newCapacity)
        return;

    // We always try to resize in place if possible. If we can't, we either memcpy if the type is
    // trivially copyable, or we allocate a new array and move-construct the old values over.
    // Any allocation failures are runtime panics/crashes, and we don't try to be exception safe.

    if constexpr (std::is_trivially_copyable_v<T>) {
        T* newBegin = static_cast<T*>(efj::array_expand(*begin, sizeof(T), size, capacity, newCapacity, alloc, true));
        // If we resized in place, we're done. The capacity was updated by the expand call.
        if (*begin == newBegin)
            return;

        efj_allocator_free_array(*alloc, *begin);
        *begin = newBegin;
    }
    else {
        // Here, a failure would be an allocation failure b/c this call will allocate a new array if resizing
        // in place fails. It doesn't free the old array of course, since we need that to copy out of.
        T* newBegin = static_cast<T*>(efj::array_expand(*begin, sizeof(T), size, capacity, newCapacity, alloc, false));
        // If we resized in place, we're done. The capacity was updated by the expand call.
        if (*begin == newBegin)
            return;

        // Otherwise, we have a newly allocated array that we need to copy into using the type's
        // non-trivial copy-constructor.
        for (size_t i = 0; i < size; i++) {
            T* toMove = &(*begin)[i];
            efj_placement_new(&newBegin[i]) T{efj::move(*toMove)};
            // We still need to call destructors.
            if constexpr (!std::is_trivially_destructible_v<T>)
                toMove->~T();
        }

        efj_allocator_free_array(*alloc, *begin);
        *begin = newBegin;
    }
}

template <typename T> EFJ_MACRO T*
array_add_space(T** begin, size_t* curSize, size_t* curCapacity, efj::allocator* alloc)
{
    // TODO: I originally had another type-erased function for adding space for one element b/c it can be
    // implemented slightly faster than a generic reserve. I also had the generic reserve code inlined
    // here. However, expansion is supposed to be exponentially rare, so I've opted to make the code simpler.
    //
    size_t oldSize = *curSize;
    size_t newSize = *curSize + 1;
    efj::array_reserve(begin, oldSize, curCapacity, newSize, alloc);

    *curSize = newSize;
    return &(*begin)[oldSize];
}

// @Incomplete
//
// TODO: A worry here is that we will be generating functions for every different array type in an application
// instead of only the ones we use. If functions are marked EFJ_MACRO, the compiler won't generate a function that
// isn't called, and we can generate calls to type-erased calls to reduce binary size and improve I-cache friendliness.
//
// Right now, the idea is to generate just a few core functions per type, like reserve and add_space, and have the
// others be EFJ_MACROs that just use those. This code is still in a bit of a transitional phase, and not
// particularly optimal in this respect.
//
// We should measure the impact with tools like nm or objdump later, and see if we need to focus more on type-erasure.
// I have tried to leave the code in a form that is relatively easy to finish type-erasing if we want to do that.
// I know that Ubisoft saw that ~80% of their binary was code from their Array class and that they improved that
// significantly through type-erasure. That said, EFJ products are a lot smaller than Assassin's Creed, so I'm
// almost certain this will remain a todo forever. @NeedsTesting
//
template <typename T>
struct array
{
    static_assert(alignof(T) <= efj::array_max_alignment, "can't support this alignment");

    // There are arguments for not using these as iterator/const_iterator, but we probably don't care. Most of them
    // have to do with interop with the standard library.
    using iterator       = T*;
    using const_iterator = const T*;

    efj::allocator _alloc;
    T*             _data;
    size_t         _size;
    size_t         _cap;

    EFJ_MACRO explicit array(efj::delay_init_tag)
        : _alloc(), _data(), _size(), _cap()
    {}

    EFJ_MACRO explicit array(efj::allocator alloc = efj::current_allocator())
        : _alloc(alloc), _data(), _size(), _cap()
    {}

    // We use noexcept here to play (somewhat) nice with data structures defined outside of the project,
    // but we always expect to not use exceptions, so we're not going to go crazy with it.
    // You shouldn't store any types that you expect throw exceptions with an efj::array, so we are just
    // marking all of these unconditionally noexcept for simplicity.
    array(const array<T>&) noexcept;
    array(array<T>&&) noexcept;
    ~array();
    array& operator = (const array<T>&) noexcept;
    array& operator = (array<T>&&) noexcept;

    EFJ_MACRO void reset(efj::allocator alloc = efj::current_allocator());

    EFJ_MACRO void clear()
    {
        // This method won't crash if this isn't initialized, but it's usually an application error.
        efj_panic_if(_alloc.is_null());

        if constexpr (!std::is_trivially_destructible_v<T>) {
            for (size_t i = 0; i < _size; i++)
                _data[i].~T();
        }

        _size = 0;
    }

    EFJ_MACRO const T& operator [] (size_t index) const
    {
        efj_panic_if(index >= _size);
        return _data[index];
    }

    EFJ_MACRO T& operator [] (size_t index)
    {
        efj_panic_if(index >= _size);
        return _data[index];
    }

    EFJ_MACRO const T& front() const { efj_panic_if(!_size); return _data[0]; };
    EFJ_MACRO T&       front()       { efj_panic_if(!_size); return _data[0]; };
    EFJ_MACRO const T& back() const { efj_panic_if(!_size); return _data[_size - 1]; };
    EFJ_MACRO T&       back()       { efj_panic_if(!_size); return _data[_size - 1]; };

    EFJ_MACRO const T* cdata() const { return _data; }
    EFJ_MACRO const T* data() const { return _data; }
    EFJ_MACRO T* data() { return _data; }
    EFJ_MACRO size_t size() const { return _size; }
    EFJ_MACRO ssize_t ssize() const { return (ssize_t)_size; }

    EFJ_MACRO const T& at_unsafe(size_t index) const { return _data[index]; }
    EFJ_MACRO T& at_unsafe(size_t index) { return _data[index]; }

    void reserve(size_t minCapacity)
    {
        efj::array_reserve(&_data, _size, &_cap, minCapacity, &_alloc);
    }

    //! Add space for one element.
    //! This is not force inlined b/c the internal function is, and it's a substantial amount of code.
    T* add_space()
    {
        return efj::array_add_space(&_data, &_size, &_cap, &_alloc);
    }

    //! Adds `n` uninitialized elements.
    //! This is not force inlined b/c the internal function is, and it's a substantial amount of code.
    T* add_space(size_t n)
    {
        size_t newSize = _size + n;
        efj::array_reserve(&_data, _size, &_cap, newSize, &_alloc);
        T* space = &_data[_size];
        _size = newSize;
        return space;
    }

    EFJ_MACRO T* add(const T& val)
    {
        T* space = efj::array_add_space(&_data, &_size, &_cap, &_alloc);
        efj_placement_new(space) T(val);
        return space;
    }

    EFJ_MACRO T* add(T&& val)
    {
        T* space = efj::array_add_space(&_data, &_size, &_cap, &_alloc);
        efj_placement_new(space) T(efj::move(val));
        return space;
    }

    //! Adds a default-constructed element.
    EFJ_MACRO T* add_default()
    {
        T* space = efj::array_add_space(&_data, &_size, &_cap, &_alloc);
        efj_placement_new(space) T();
        return space;
    }

    EFJ_MACRO const T* find(const T& val) const
    {
        return const_cast<T*>(this)->find(val);
    }

    template <typename ComparableT> EFJ_MACRO T*
    find(const ComparableT& val)
    {
        // Cache values in case the defintion for == is not visible, which could cause
        // us to reload members unnecessarily.
        size_t size = _size;
        for (size_t i = 0; i < size; i++) {
            T* cur = &_data[i];
            if (*cur == val)
                return cur;
        }

        return nullptr;
    }

    EFJ_MACRO void remove_nth(size_t i)
    {
        // Cache values to prevent reloading if something in panic_if or move assignment
        // is not visible to the compiler.
        size_t size = _size;
        efj_panic_if(i >= size);

        for (size_t j = i + 1; j < size; j++)
            _data[j - 1] = efj::move(_data[j]);

        if constexpr (!std::is_trivially_destructible_v<T>)
            _data[size - 1].~T();

        --_size;
    }

    EFJ_MACRO void remove_nth_unordered(size_t i)
    {
        // Cache values to prevent reloading if something in panic_if or move assignment
        // is not visible to the compiler.
        size_t backIndex = _size - 1;
        efj_panic_if(i >= _size);
        _data[i] = efj::move(_data[backIndex]);

        if constexpr (!std::is_trivially_destructible_v<T>)
            _data[backIndex].~T();

        --_size;
    }

    template <typename ComparableT> EFJ_MACRO bool
    remove(const ComparableT& val)
    {
        const T* found = find(val);
        if (!found) return false;
        remove_nth(found - _data);
        return true;
    }

    EFJ_MACRO void pop()
    {
        size_t size = _size;
        efj_panic_if(!size);

        if constexpr (!std::is_trivially_destructible_v<T>)
            _data[size - 1].~T();

        --_size;
    }

    EFJ_MACRO const_iterator begin() const { return ((array<T>*)this)->begin(); }
    EFJ_MACRO iterator       begin()       { return iterator(_data); }

    EFJ_MACRO const_iterator end() const { return ((array<T>*)this)->end(); }
    EFJ_MACRO iterator       end()       { return iterator(&_data[_size]); }

    EFJ_MACRO const_iterator cbegin() const { return begin(); }
    EFJ_MACRO const_iterator cend()   const { return end(); }

    // Internal
    EFJ_MACRO void _free_contents();
    EFJ_MACRO void _copy_contents(const efj::array<T>& other, efj::allocator alloc);
};

template <typename T, size_t ExtentV> constexpr EFJ_MACRO size_t
array_size(const T(&)[ExtentV]) { return ExtentV; }

// For annoying member arrays that I can't figure out how to deal with.
#define efj_array_size(arr) (sizeof(arr) / sizeof(arr[0]))

} // namespace efj

