namespace efj
{

struct fd
{
    efj::eventsys_epoll_event event;
};

template <typename T> inline void
fd_add_os_handle(const char* name, int fd, efj::epoll_events events, efj::fd* result, void (T::*callback)(efj::fd_event&))
{
    efj::fd_create(name, fd, events, result);
    efj::fd_monitor(*result, callback);
}

inline int
fd_get_os_handle(efj::fd& fd)
{
    return fd.event.fd;
}

template <typename T> inline void
fd_monitor(efj::fd& fd, efj::epoll_events events, void (T::*callback)(efj::fd_event& e))
{
    efj_assert(fd.event.created && "FD monitored before creation.");
    efj::object* object = efj::this_object();
    efj::thread* thread = object->thread;

    efj::callback cb = efj::_make_object_callback(object, callback);
    efj::_eventsys_monitor_epoll_event(thread, &fd.event, &events, &cb);
}

template <typename T> inline void
fd_monitor(efj::fd& fd, void (T::*callback)(efj::fd_event& e))
{
    efj_assert(fd.event.created && "FD monitored before creation.");
    efj::object* object = efj::this_object();
    efj::thread* thread = object->thread;

    efj::callback cb = efj::_make_object_callback(object, callback);
    efj::_eventsys_monitor_epoll_event(thread, &fd.event, nullptr, &cb);
}

inline bool fd_is_created(efj::fd& fd) { return fd.event.created; }
inline bool fd_is_monitored(efj::fd& fd) { return fd.event.monitored; }

inline ssize_t fd_read(efj::fd& fd, void* buf, umm n) { return ::read(fd.event.fd, buf, n); }
inline ssize_t fd_write(efj::fd& fd, const void* buf, umm n) { return ::write(fd.event.fd, buf, n); }

inline int
fd_close(efj::fd& fd)
{
    efj::thread* thread = efj::this_object()->thread;
    return efj::_eventsys_destroy_epoll_event(thread, &fd.event);
}

inline bool make_nonblocking(efj::fd& fd) { return efj::make_nonblocking(fd.event.fd); }
inline bool make_blocking(efj::fd& fd) { return efj::make_blocking(fd.event.fd); }


} // namespace efj

