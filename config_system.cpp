namespace efj
{

enum
{
    kInotifyEvents = (IN_ALL_EVENTS & ~(IN_OPEN | IN_ACCESS | IN_MODIFY | IN_CLOSE_NOWRITE)),
    //kInotifyEvents = (IN_ALL_EVENTS),
};

static void
_configsys_pre_init(efj::config_system* sys, efj::memory_arena* arena)
{
    sys->arena      = arena;
    sys->fileArena  = efj::allocate_arena("Config", 8_KB);
    sys->configFiles.reset(*arena);
    // :DeferredConfigEvents

    sys->preInitted = true;
}

static void
_configsys_add_config_file(efj::config_system* sys, u32 id, const char* path, efj::config_type type)
{
    //printf("Adding config file %u\n", id);
    efj::config_file* configFile = sys->configFiles.add_default();
    configFile->id      = id;
    configFile->type    = type;
    configFile->path    = path;
    configFile->watchFd = -1;
    configFile->uniqueSubscribedThreads.reset(*sys->arena);
    configFile->uniqueChangeCallbacks.reset(*sys->arena);

    // NOTE: We can't create events here b/c we don't know for sure which thread is going to be
    // the config thread. :DeferredConfigEvents
}

static void
_configsys_init_thread_context(efj::config_system* sys, efj::thread* thread, efj::memory_arena* arena)
{
    efj::configsys_thread_ctx* ctx = thread->configContext;
    ctx->sys    = sys;
    ctx->thread = thread;
    ctx->arena  = arena;

    ctx->uniqueOnInitDoneCallbacks.reset(*arena);
}

static void
_configsys_init_object_context(efj::config_system* sys, efj::object* object)
{
    efj_assert(sys->preInitted);
    object->configContext = efj_push_new(efj::perm_memory(), efj::configsys_object_ctx);
    object->configContext->subscribedFiles.reset(efj::perm_memory());
}

static bool
_configsys_subscribe(efj::config_system* sys, u32 id, efj::callback cb)
{
    efj::object* object = cb.object;
    efj_panic_if(!object);

    for (efj::config_file& file : sys->configFiles) {
        if (file.id == id) {
            if (!object->configContext->subscribedFiles.find(&file)) {
                file.uniqueChangeCallbacks.add(cb);
                object->configContext->subscribedFiles.add(&file);
            }

            return true;
        }
    }

    return false;
}

static bool
_configsys_subscribe(efj::config_system* sys, efj::config_event_type type, efj::callback cb)
{
    efj::object* object = cb.object;
    efj_panic_if(!object);
    efj_panic_if(type == efj::config_event_invalid || type >= efj::config_event_count_);

    if (object->configContext->subscribedEvents[type])
        return false;

    if (type == efj::config_event_init_done)
        object->thread->configContext->uniqueOnInitDoneCallbacks.add(cb);

    object->configContext->subscribedEvents[type] = true;
    return true;
}

static void
_configsys_pre_object_init(efj::config_system* sys, efj::thread* configThread)
{
    sys->configThread = configThread;
}

// All objects have been initted. We need to setup config-file monitoring and, assuming we have config files to
// monitor, send out an initial config event. This runs on the main thread though, so we send an event to the config thread.
static void
_configsys_post_object_init(efj::config_system* sys)
{
    // :DeferredConfigEvents
    efj::eventsys_waitable_desc initialConfigDesc = {};
    initialConfigDesc.udata.as_ptr = sys;
    initialConfigDesc.dispatch     = &_configsys_handle_initial_config_event;
    efj::_eventsys_create_waitable_event(sys->configThread, "Initial Config", &initialConfigDesc, &sys->initialConfigEvent);
    efj::_eventsys_add_waitable_event(sys->configThread, &sys->initialConfigEvent);

    efj::eventsys_timer_desc findLostFilesDesc = {};
    findLostFilesDesc.udata.as_ptr = sys;
    findLostFilesDesc.initial      = 1_ms;
    findLostFilesDesc.interval     = 1000_ms;
    findLostFilesDesc.dispatch     = &_configsys_handle_find_lost_files_event;
    efj::_eventsys_create_timer(sys->configThread, "Find Lost Files", &findLostFilesDesc, &sys->findLostFilesTimer);

    efj::_eventsys_create_fence(&sys->onInitDoneFence);
    efj::_eventsys_create_fence(&sys->validateConfigFence);
    efj::_eventsys_create_breakpoint(&sys->applyConfigBeginBp);
    efj::_eventsys_create_breakpoint(&sys->applyConfigEndBp);

    // :DeferredConfigEvents
    for (efj::config_file& file : sys->configFiles) {
        // NOTE: Originally we were leaving this table unallocated if there weren't any config subscribers to save space. This
        // got really annoying usage-code-wise, so now the table is always allocated and all lists are initialized, even though
        // this wastes some @Space.
        //
        // We want to produce a list of callbacks for a file per thread for convenient access during config events.
        //
        file.uniqueChangeCallbacksByThread.reset(*sys->arena);
        file.uniqueChangeCallbacksByThread.add_space(efj::thread_count());
        for (u32 i = 0; i < efj::thread_count(); i++)
            file.uniqueChangeCallbacksByThread[i].reset(*sys->arena);

        if (!file.uniqueChangeCallbacks.size()) {
            // XXX
            //printf("Not allocating config resources for %s b/c it doesn't have any subscribers\n", file.path);
            continue;
        }

        efj_temp_scope();

        // Make a list of uniquely-subscribed threads other than the config thread.
        //
        // NOTE: While there may be configurable objects on the config thread, the config thread will already
        // be stopped while handling the event, so we only need to keep track of the other threads that we
        // need to synchronize with.
        //
        efj::array<thread*> otherSubcribedThreads{efj::alloc_temp};
        bool threadSeen[efj::thread_count()] = {};

        for (const efj::callback& cb : file.uniqueChangeCallbacks) {
            efj::object* object = cb.object;

            efj::thread* thread = object->thread;
            if (!threadSeen[thread->logicalId]) {

                if (thread != sys->configThread) {
                    otherSubcribedThreads.add(thread);
                    file.otherSubscribedThreadCount++;
                }

                file.uniqueSubscribedThreads.add(thread);
                thread->configContext->hasFileChangeSubscribers = true;
                threadSeen[object->thread->logicalId] = true;
            }

            if (!file.uniqueChangeCallbacksByThread[thread->logicalId].find(cb))
                file.uniqueChangeCallbacksByThread[thread->logicalId].add(cb);
        }
    }

    // Create config events. We don't create events for the config thread. Any events on the config thread
    // are handled inline during dispatch.
    for (efj::thread* thread : efj::threads()) {
        if (thread != sys->configThread) {
            if (thread->configContext->hasFileChangeSubscribers) {
                efj::eventsys_waitable_desc validateConfigDesc = {};
                validateConfigDesc.udata.as_ptr   = thread->configContext;
                validateConfigDesc.dispatch       = &_configsys_handle_validate_config_event;
                validateConfigDesc.completedFence = &sys->validateConfigFence;

                efj::eventsys_waitable_desc applyConfigDesc = {};
                applyConfigDesc.udata.as_ptr = thread->configContext;
                applyConfigDesc.dispatch     = &_configsys_handle_apply_config_event;
                applyConfigDesc.beginBp      = &sys->applyConfigBeginBp;
                applyConfigDesc.endBp        = &sys->applyConfigEndBp;

                efj_assert(thread != sys->configThread);

                // Validate.
                efj::_eventsys_create_waitable_event(thread, "Validate Config", &validateConfigDesc,
                    &thread->configContext->validateConfigEvent);
                efj::_eventsys_add_waitable_event(thread, &thread->configContext->validateConfigEvent);
                // Apply.
                efj::_eventsys_create_waitable_event(thread, "Apply Config", &applyConfigDesc,
                    &thread->configContext->applyConfigEvent);
                efj::_eventsys_add_waitable_event(thread, &thread->configContext->applyConfigEvent);
            }

            if (thread->configContext->uniqueOnInitDoneCallbacks.size()) {
                efj::eventsys_waitable_desc initDoneDesc = {};
                initDoneDesc.udata.as_ptr   = thread->configContext;
                initDoneDesc.completedFence = &sys->onInitDoneFence;
                initDoneDesc.dispatch       = &_configsys_handle_initial_config_done_event;

                efj::_eventsys_create_waitable_event(thread, "Init Done", &initDoneDesc,
                        &thread->configContext->onInitDoneEvent);
                efj::_eventsys_add_waitable_event(thread, &thread->configContext->onInitDoneEvent);
            }
        }
    }

    // Setup the low-level file-changed event using the inotify API.
    if (sys->configFiles.size()) {
        //{ Create the inotify and watch fds from config file paths.
        sys->inotifyFd = ::inotify_init1(IN_CLOEXEC | IN_NONBLOCK);
        efj_assert(sys->inotifyFd != -1);

        for (efj::config_file& file : sys->configFiles) {
            //efj_internal_log(efj::log_level_debug, "Adding watch for %s", file.path);
            file.watchFd = ::inotify_add_watch(sys->inotifyFd, file.path, kInotifyEvents);
            // watchFd will be -1 if the file isn't present.
        }
        //}

        //{ Actual OS config-file-changed event.
        efj::eventsys_epoll_desc inotifyDesc = {};
        inotifyDesc.udata.as_ptr = sys;
        inotifyDesc.dispatch     = &_configsys_handle_config_changed_event;
        inotifyDesc.events       = efj::io_in | efj::io_et;
        efj::_eventsys_create_epoll_event(sys->configThread, "Config File (inotify)", sys->inotifyFd, &inotifyDesc,
            &sys->configChangedEvent);

        efj::_eventsys_add_epoll_event(sys->configThread, &sys->configChangedEvent);
        //}
    }

    // TODO: Validate that all the lists that are supposed to be unique are in fact unique!!
    // Debugging cases where we have duplicate callbacks/threads in these produce bugs that are _very_
    // hard to track down. They might result in a thread being unresponsive, blocked on a breakpoint
    // for an event that was triggered multiple times, for example.
    //
    //_validate_unique_lists(sys);

    efj::_eventsys_trigger(&sys->initialConfigEvent);
}

// TODO: Not too happy about how this works with setting the current allocator to the file arena. It's easy to not have
// the current allocator set right without knowing. It's probably better to make the calls that allocate out of the arena
// explictly take the arena. Also, we could and probably should stream the config files in, since the xml parser allows that.
//
static bool
_load_config_file(efj::config_system* sys, efj::config_file* file)
{
    //efj_internal_log(efj::log_level_info, "loading %s", file->path);
    efj_assert(file->get_type() == efj::config_xml);

    efj_reset(sys->fileArena);
    efj_allocator_scope(sys->fileArena);

    int fd = -1;
    for (int i = 0; i < 20; i++) {
        fd = ::open(file->path, O_RDONLY);
        if (fd != -1) break;
        ::usleep(50000);
    }

    if (fd == -1) {
        efj_internal_log(efj::log_level_crit, "Failed to read config file '%s': file removed?", file->path);
        return false;
    }

    efj::buffer buf = efj::read_file(fd);
    ::close(fd);

    if (!buf.data) {
        efj_internal_log(efj::log_level_crit, "Failed to read config file '%s': %s", file->path, strerror(errno));
        efj_reset(sys->fileArena);
        return false;
    }

    auto* parsed = efj_new(efj::parsed_xml_file);
    *parsed = efj::parse_xml_file(buf);

    if (parsed->error) {
        efj_internal_log(efj::log_level_crit, "Failed to parse config file '%s': %s", file->path, parsed->error.c_str());
        efj_reset(sys->fileArena);
        return false;
    }

    file->xml = &parsed->doc;

    return true;
}

inline efj::config_file*
_find_config_file(efj::config_system* sys, int wd)
{
    for (efj::config_file& file : sys->configFiles) {
        if (file.watchFd == wd)
            return &file;
    }

    return nullptr;
}

// On the config thread. Main, two-phase config event handling where we actually stop all the threads
// and dispatch config events.
static void
_dispatch_config_file_changed_event(efj::config_system* sys, efj::config_file* file)
{
    efj_assert(efj::this_thread() == sys->configThread);

    // If there aren't any subscribers at all, we will crash trying to dispatch b/c resources won't be setup for this file.
    if (file->uniqueChangeCallbacks.size() == 0)
        return;

    if (!efj::_load_config_file(sys, file))
        return;

    __atomic_store_n(&sys->currentConfigFile, file, __ATOMIC_RELAXED);

    // printf("[CONFIG] Config file '%s' modified\n", file->path);

    // We handle validation asynchronously. We only care about syncing up when it's time to apply.

    umm nonConfigThreadSubscribedThreadCount = file->otherSubscribedThreadCount;

    bool rejected = false;

    efj::array<efj::callback>* uniqueCallbacksOnConfigThread =
        &file->uniqueChangeCallbacksByThread[efj::logical_thread_id()];

    //{ Validate on the config thread and all other subscribed threads.
    for (efj::callback& cb : *uniqueCallbacksOnConfigThread) {
        efj::config_event e = {};
        e.type = efj::config_event_validate;
        e.file = file;

        cb.caller(&cb, &e);

        if (e.rejected) {
            efj_internal_log(efj::log_level_warn, "Warning: object '%s' rejected config: %s", cb.object->name,
                    e.rejectReason.c_str());

            rejected = true;
            break;
        }
    }

    if (rejected)
        return;

    __atomic_store_n(&sys->firstRejectedThread, nullptr, __ATOMIC_RELAXED);
    for (efj::thread* thread : file->uniqueSubscribedThreads) {
        if (thread != sys->configThread)
            efj::_eventsys_trigger(&thread->configContext->validateConfigEvent);
    }

    sys->validateConfigFence.wait_for_hits(nonConfigThreadSubscribedThreadCount);
    efj::thread* firstRejectedThread = __atomic_load_n(&sys->firstRejectedThread, __ATOMIC_RELAXED);
    if (firstRejectedThread)
        return;
    //}

    // 1) Tell all the threads stop on a breakpoint
    // 2) Handle the apply event on the config thread. The handling of this event doesn't have to happen in
    // any particular order with respect to the other threads.
    // 3) Let the other threads the apply event and get to a stopping point.

    for (efj::thread* thread : file->uniqueSubscribedThreads) {
        if (thread != sys->configThread) {
            //efj_internal_log(efj::log_level_debug, "triggering apply for %s", thread->name);
            efj::_eventsys_trigger(&thread->configContext->applyConfigEvent);
        }
    }

    //efj_internal_log(efj::log_level_debug, "Waiting for %zu threads to begin", nonConfigThreadSubscribedThreadCount);
    sys->applyConfigBeginBp.wait_for_hits(nonConfigThreadSubscribedThreadCount);
    //efj_internal_log(efj::log_level_debug, "Waiting for %zu threads done", nonConfigThreadSubscribedThreadCount);
    for (efj::callback& cb : *uniqueCallbacksOnConfigThread) {
        efj::config_event e = {};
        e.type = efj::config_event_apply;
        e.file = file;

        cb.caller(&cb, &e);
    }
    //efj_internal_log(efj::log_level_debug, "continuing %d threads", sys->applyConfigBeginBp.waitCount);
    sys->applyConfigBeginBp.continue_all();

    //efj_internal_log(efj::log_level_debug, "Waiting for %zu threads", nonConfigThreadSubscribedThreadCount);
    sys->applyConfigEndBp.wait_for_hits(nonConfigThreadSubscribedThreadCount);
    //efj_internal_log(efj::log_level_debug, "Waiting for %zu threads done", nonConfigThreadSubscribedThreadCount);
    sys->applyConfigEndBp.continue_all();
}

// On the config thread. Triggered by the main thread post-init to send out initial config values.
static void
_configsys_handle_initial_config_event(efj::eventsys_event_data* edata)
{
    efj::config_system* sys = (efj::config_system*)edata->udata.as_ptr;

    bool atLeastOneFileNotFound = false;
    for (efj::config_file& file : sys->configFiles) {
        if (file.watchFd == -1) {
            file.lost = true;
            atLeastOneFileNotFound = true;
            continue;
        }

        // We expect this to hande one full validation + sync + apply + sync for one config file.
        _dispatch_config_file_changed_event(sys, &file);
    }

    // The config thread doesn't have an event created b/c we just handle this event here.
    for (efj::callback& cb : sys->configThread->configContext->uniqueOnInitDoneCallbacks) {
        efj::config_event e = {};
        e.type = efj::config_event_init_done;

        cb.caller(&cb, &e);
    }

    umm initDoneSubscribedThreadCount = 0;

    // NOTE: We don't currently store a list of threads that have init_done-event callbacks, so we linearly
    // search. There shouldn't be enough threads for this to matter, though.
    for (efj::thread* thread : efj::threads()) {
        if (thread == sys->configThread)
            continue;

        if (thread->configContext->uniqueOnInitDoneCallbacks.size()) {
            ++initDoneSubscribedThreadCount;
            efj::_eventsys_trigger(&thread->configContext->onInitDoneEvent);
        }
    }

    sys->onInitDoneFence.wait_for_hits(initDoneSubscribedThreadCount);

    if (atLeastOneFileNotFound) {
        efj::_eventsys_start_timer(&sys->findLostFilesTimer);
    }
}

// On the thread that has init_done subscribers.
static void
_configsys_handle_initial_config_done_event(efj::eventsys_event_data* edata)
{
    efj::configsys_thread_ctx* ctx = (efj::configsys_thread_ctx*)edata->udata.as_ptr;
    efj::config_system* sys = ctx->sys;
    efj_assert(efj::this_thread() != sys->configThread);

    for (efj::callback& cb : ctx->uniqueOnInitDoneCallbacks) {
        efj::config_event e = {};
        e.type = efj::config_event_init_done;

        cb.caller(&cb, &e);
    }
}

// On the thread that has objects subscribed to a config file that changed. This is for asynchronous validation
// of the config file changes.
static void
_configsys_handle_validate_config_event(efj::eventsys_event_data* edata)
{
    efj::configsys_thread_ctx* ctx = (efj::configsys_thread_ctx*)edata->udata.as_ptr;
    efj::config_system* sys = ctx->sys;
    efj_assert(efj::this_thread() != sys->configThread);

    efj::config_file* file = __atomic_load_n(&sys->currentConfigFile, __ATOMIC_RELAXED);

    efj::array<efj::callback>* callbacks = &file->uniqueChangeCallbacksByThread[efj::logical_thread_id()];
    for (efj::callback& cb : *callbacks) {
        efj::config_event e = {};
        e.type = efj::config_event_validate;
        e.file = file;

        cb.caller(&cb, &e);

        if (e.rejected) {
            efj_internal_log(efj::log_level_warn, "Warning: object '%s' rejected config: %s", cb.object->name,
                    e.rejectReason.c_str());

            efj::copy_string(e.rejectReason, ctx->rejectReason);
            efj::thread* expected = nullptr;
            __atomic_compare_exchange_n(&sys->firstRejectedThread, &expected, efj::this_thread(), false,
                __ATOMIC_RELAXED, __ATOMIC_RELAXED);

            ctx->rejectedConfig = true;
            break;
        }
    }
}

// On the thread that has objects subscribed to a config file that changed. This is the event that threads sync up
// up, handle atomically, and continue.
static void
_configsys_handle_apply_config_event(efj::eventsys_event_data* edata)
{
    efj::config_system* sys = ((efj::configsys_thread_ctx*)edata->udata.as_ptr)->sys;
    efj_assert(efj::this_thread() != sys->configThread);

    efj::config_file* file = __atomic_load_n(&sys->currentConfigFile, __ATOMIC_RELAXED);

    efj::array<efj::callback>* callbacks = &file->uniqueChangeCallbacksByThread[efj::logical_thread_id()];
    for (efj::callback& cb : *callbacks) {
        efj::config_event e = {};
        e.type = efj::config_event_apply;
        e.file = file;

        cb.caller(&cb, &e);
    }
}

// On the config thread. Triggered when we get an inotify event from Linux.
static void
_configsys_handle_config_changed_event(efj::eventsys_event_data* edata)
{
    efj_temp_scope();
    efj::config_system* sys = (efj::config_system*)edata->udata.as_ptr;

    constexpr umm kEventBufferSize = 10 * (sizeof(struct inotify_event) + NAME_MAX + 1);

    alignas(struct inotify_event) char buf[kEventBufferSize];

    efj::array<int> modifedWatches(efj::temp_memory());
    efj::array<int> lostWatches(efj::temp_memory());

    u32 waitingForEventsWeJustGenerated = 0;
    for (;;) {
        int nBytes = 0;
        while ((nBytes = ::read(edata->epoll.fd, buf, sizeof(buf))) > 0) {
            for (char* p = buf; p < buf + nBytes; ) {
                auto* event = (struct inotify_event*)p;

#if 0
                printf("[CONFIG] mask = ");
                if (event->mask & IN_ACCESS)        printf("IN_ACCESS ");
                if (event->mask & IN_ATTRIB)        printf("IN_ATTRIB ");
                if (event->mask & IN_CLOSE_NOWRITE) printf("IN_CLOSE_NOWRITE ");
                if (event->mask & IN_CLOSE_WRITE)   printf("IN_CLOSE_WRITE ");
                if (event->mask & IN_CREATE)        printf("IN_CREATE ");
                if (event->mask & IN_DELETE)        printf("IN_DELETE ");
                if (event->mask & IN_DELETE_SELF)   printf("IN_DELETE_SELF ");
                if (event->mask & IN_IGNORED)       printf("IN_IGNORED ");
                if (event->mask & IN_ISDIR)         printf("IN_ISDIR ");
                if (event->mask & IN_MODIFY)        printf("IN_MODIFY ");
                if (event->mask & IN_MOVE_SELF)     printf("IN_MOVE_SELF ");
                if (event->mask & IN_MOVED_FROM)    printf("IN_MOVED_FROM ");
                if (event->mask & IN_MOVED_TO)      printf("IN_MOVED_TO ");
                if (event->mask & IN_OPEN)          printf("IN_OPEN ");
                if (event->mask & IN_Q_OVERFLOW)    printf("IN_Q_OVERFLOW ");
                if (event->mask & IN_UNMOUNT)       printf("IN_UNMOUNT ");
                printf("\n");
#endif
                // NOTE(bmartin): This happens when Vim writes to a file, for example.
                // After this event, this watch is useless, so we need to re-add it at the same path.
                // This also usually corresponds to a modify, so we will assume it is.
                if (event->mask & IN_MOVE || event->mask & IN_MOVE_SELF || event->mask & IN_IGNORED) {
                    efj::config_file* file = efj::_find_config_file(sys, event->wd);
                    if (event->mask & IN_MOVE || event->mask & IN_MOVE_SELF) {
                        efj_assert(file->watchFd != -1);
                        // IMPORTANT: This should generate an IN_IGNORED event since we are explicitly removing the watch fd.
                        // We should get that event after fully handling this one.
                        inotify_rm_watch(edata->epoll.fd, event->wd);
                        // Don't clear out the watch fd here, so we can find the file that was removed during the IN_IGNORE event.
                        file->moved = true;
                        // printf("Moved, moved: %d, removed: %d\n", file->moved, file->removed);
                        ++waitingForEventsWeJustGenerated;
                    }

                    if (event->mask & IN_IGNORED) {
                        if (file) {
                            file->removed = true;
                            // printf("Removed, moved: %d, removed: %d\n", file->moved, file->removed);
                        }

                        lostWatches.add(event->wd);

                        if (waitingForEventsWeJustGenerated)
                            --waitingForEventsWeJustGenerated;
                    }
                }

                if (event->mask & IN_CLOSE_WRITE)
                    modifedWatches.add(event->wd);

                p += sizeof(struct inotify_event) + event->len;
            }
        }

        // TODO(bmartin): Config file monitoring is likely broken if the read() returns 0.
        // Do something better about that?
        efj_panic_if(nBytes == 0);
        efj_panic_if(errno != EAGAIN);

        if (waitingForEventsWeJustGenerated == 0)
            break;
    }

    for (int wd : modifedWatches) {
        efj::config_file* file = _find_config_file(sys, wd);
        efj_panic_if(!file);

        _dispatch_config_file_changed_event(sys, file);
    }

    bool foundAllLostFiles = true;
    // Try to re-add lost files at there current path first.
    for (int wd : lostWatches) {
        efj::config_file* file = _find_config_file(sys, wd);
        efj_panic_if(!file);

        bool readded = false;
        int newWatchFd = ::inotify_add_watch(edata->epoll.fd, file->path, kInotifyEvents);
        if (newWatchFd != -1) {
            file->watchFd = newWatchFd;
            readded = true;
        }

        if (!readded) {
            file->lost = true;
            foundAllLostFiles = false;
        }
    }

    if (!foundAllLostFiles) {
        // printf("Didn't find all the lost files. Starting timer...\n");
        efj::_eventsys_start_timer(&sys->findLostFilesTimer);
    }
}

static void
_configsys_handle_find_lost_files_event(efj::eventsys_event_data* edata)
{
    efj::config_system* sys = (efj::config_system*)edata->udata.as_ptr;

    efj::array<config_file*> foundFiles(efj::temp_memory());

    bool foundAllFiles = true;
    for (efj::config_file& file : sys->configFiles) {
        if (file.lost) {
            int newWatchFd = ::inotify_add_watch(sys->inotifyFd, file.path, kInotifyEvents);
            if (newWatchFd == -1) {
                //printf("watch == -1: %s\n", strerror(errno));
                foundAllFiles = false;
                continue;
            }

            //printf("Found file %s\n", file.path);
            file.watchFd = newWatchFd;
            file.lost = false;
            foundFiles.add(&file);
        }
        else {
            //printf("Not lost...\n");
        }
    }

    if (foundAllFiles) {
        efj::_eventsys_stop_timer(&sys->findLostFilesTimer);
    }

    for (efj::config_file* file : foundFiles) {
        _dispatch_config_file_changed_event(sys, file);
    }
}

} // namespace efj

