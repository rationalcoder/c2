namespace efj
{

/*
template <typename T_, T_ Val_>
struct integral_constant
{
    static constexpr T_ value = Val_;
    constexpr operator T_() { return Val_; }
    constexpr integral_constant() {}
};

using true_type  = integral_constant<bool, true>;
using false_type = integral_constant<bool, false>;

template <bool Cond_, typename T_>
struct enable_if {};

template <typename T_>
struct enable_if<true, T_> { using type = T_; };

template <bool Cond_, typename T1_, typename T2_>
struct conditional { using type = T1_; };

template <typename T1_, typename T2_>
struct conditional<false, T1_, T2_> { using type = T2_; };
*/

template <typename T_> T_&& declval() noexcept;

template <int Size_, typename T_, typename... List_>
struct index_of_impl : std::integral_constant<int, -(Size_+1)> {};

template <int Size_, typename T_, typename NotT_, typename... List_>
struct index_of_impl<Size_, T_, NotT_, List_...> : std::integral_constant<int, 1 + efj::index_of_impl<Size_, T_, List_...>::value> {};

template <int Size_, typename T_, typename... List_>
struct index_of_impl<Size_, T_, T_, List_...> : std::integral_constant<int, 0> {};

template <typename T_, typename... List_>
struct index_of : index_of_impl<sizeof...(List_), T_, List_...> {};

// move and forward. To get them from std, you have to include <utility>, and that thing is huge...

template <typename T_> constexpr typename std::remove_reference<T_>::type&&
move(T_&& t) noexcept { return static_cast<typename std::remove_reference<T_>::type&&>(t); }

template <typename T_> constexpr T_&&
forward(typename std::remove_reference<T_>::type& t) noexcept { return static_cast<T_&&>(t); }

template <typename T_> constexpr T_&&
forward(typename std::remove_reference<T_>::type&& t) noexcept { return static_cast<T_&&>(t); }

template <typename T_> EFJ_FORCE_INLINE void
swap(T_& a, T_& b)
{
    T_ temp = efj::move(a);
    a = efj::move(b);
    b = efj::move(temp);
}

// EFJ specific stuff

template <bool...> struct bool_pack;

template <bool... Bools_>
using all = std::is_same<bool_pack<Bools_..., true>, bool_pack<true, Bools_...>>;

// Used to delay the evaluation of static_asserts until instantiation time.
template <typename...>
struct type_dependent_false : std::false_type {};

#define EFJ_DEFINE_HAS_MEMBER(traitName, memberName)\
template <typename T_, typename = void>\
struct traitName : std::false_type {};\
\
template <typename T_>\
struct traitName<T_, decltype(efj::declval<T_>().memberName, void())> : std::true_type {}

#define EFJ_DEFINE_HAS_MEMBER_FUNC(traitName, memberName, ...)\
template <typename T_, typename = void>\
struct traitName : std::false_type {};\
\
template <typename T_>\
struct traitName<T_, decltype(efj::declval<T_>().memberName(__VA_ARGS__), void())> : std::true_type {}

EFJ_DEFINE_HAS_MEMBER(is_basic_object, efj_object);
EFJ_DEFINE_HAS_MEMBER(is_stateful,     efj_stateful_object);
EFJ_DEFINE_HAS_MEMBER(is_producer,     efj_producer);
EFJ_DEFINE_HAS_MEMBER(is_consumer,     efj_consumer);

template <typename T_>
struct is_object : std::conditional<efj::is_basic_object<T_>::value || efj::is_stateful<T_>::value,
                                    std::true_type,
                                    std::false_type>::type {};

EFJ_DEFINE_HAS_MEMBER_FUNC(has_init, init);
EFJ_DEFINE_HAS_MEMBER_FUNC(has_config, config, efj::declval<const efj::config_file&>(), efj::declval<bool>());
EFJ_DEFINE_HAS_MEMBER_FUNC(has_begin_frame, begin_frame);
EFJ_DEFINE_HAS_MEMBER_FUNC(has_end_frame, end_frame);
EFJ_DEFINE_HAS_MEMBER_FUNC(has_notify_user_event, notify, efj::declval<efj::user_event&>());

struct object;

template <typename T_> inline efj::object*
object_cast(T_* p)
{
    static_assert(efj::is_object<T_>::value, "Not an EFJ_OBJECT");
    efj_assert(&p->efj_object == (void*)p && "efj_object should be the first member in an EFJ_OBJECT");

    return (efj::object*)p;
}

} // namespace efj
