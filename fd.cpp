namespace efj
{

static void
_dispatch_fd_event(efj::eventsys_event_data* edata)
{
    efj::fd_event e;
    e.fd     = (efj::fd*)edata->udata.as_ptr;
    e.events = edata->epoll.events;

    edata->objectCb.caller(&edata->objectCb, &e);
}

extern void
fd_create(const char* name, int fd, efj::epoll_events events, efj::fd* result)
{
    efj_assert(name);
    efj_assert(fd != -1);
    efj::thread* thread = efj::this_object()->thread;

    efj::eventsys_epoll_desc desc = {};
    desc.udata.as_ptr = result;
    desc.dispatch     = &_dispatch_fd_event;
    desc.events       = events;
    efj::_eventsys_create_epoll_event(thread, name, fd, &desc, &result->event);
}

extern bool
make_nonblocking(int fd)
{
    int flags = fcntl(fd, F_GETFL);
    flags |= O_NONBLOCK;
    return fcntl(fd, F_SETFL, flags) != -1;
}

extern bool
make_blocking(int fd)
{
    int flags = fcntl(fd, F_GETFL);
    flags &= ~O_NONBLOCK;
    return fcntl(fd, F_SETFL, flags) != -1;
}

} // end namespace efj
