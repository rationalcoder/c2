namespace efj
{

inline efj::string
string::trim() const
{
    if (!data)
        return efj::string();

    if (!size) {
        char* nul = (char*)efj_push_bytes(efj::temp_memory(), 1);
        *nul = '\0';
        return efj::string::shallow_copy(nul, 0);
    }

    char* left = data;
    while (*left && (*left == ' ' || *left == '\n' || *left == '\t' || *left == '\r'))
        left++;

    char* right = data + size-1;
    while (right >= data && (*right == ' ' || *right == '\n' || *right == '\t' || *right == '\r'))
        right--;

    // Whole string is space.
    if (right < left) {
        char* nul = (char*)efj_push_bytes(efj::temp_memory(), 1);
        *nul = '\0';
        return efj::string::shallow_copy(nul, 0);
    }

    umm   trimmedSize = right - left + 1;
    char* trimmedData = (char*)efj_push_bytes(efj::temp_memory(), trimmedSize + 1);
    memcpy(trimmedData, left, trimmedSize);
    trimmedData[trimmedSize] = '\0';

    return efj::string::shallow_copy(trimmedData, trimmedSize);
}

//{ Conversions

template <typename T_> static inline T_
_to_signed(const efj::string* s, T_ min, T_ max, T_ def, bool* ok)
{
    errno = 0;
    char* firstInvalid;
    auto val = strtoll(s->data, &firstInvalid, 0);

    if (errno != 0 || firstInvalid == s->data || val < min || val > max) {
        if (ok) *ok = false;
        return def;
    }

    if (ok) *ok = true;
    return (T_)val;
}

template <typename T_> static inline T_
_to_unsigned(const efj::string* s, T_ max, T_ def, bool* ok)
{
    errno = 0;
    char* firstInvalid;
    auto val = strtoull(s->data, &firstInvalid, 0);

    if (errno != 0 || firstInvalid == s->data || val > max) {
        if (ok) *ok = false;
        return def;
    }

    if (ok) *ok = true;
    return (T_)val;
}

int string::to_int(int def, bool* ok) const { return efj::_to_signed<int>(this, INT_MIN,   INT_MAX,   def, ok); }
s8  string::to_s8 (s8  def, bool* ok) const { return efj::_to_signed<s8> (this, INT8_MIN,  INT8_MAX,  def, ok); }
s16 string::to_s16(s16 def, bool* ok) const { return efj::_to_signed<s16>(this, INT16_MIN, INT16_MAX, def, ok); }
s32 string::to_s32(s32 def, bool* ok) const { return efj::_to_signed<s32>(this, INT32_MIN, INT32_MAX, def, ok); }
s64 string::to_s64(s64 def, bool* ok) const { return efj::_to_signed<s64>(this, INT64_MIN, INT64_MAX, def, ok); }

unsigned string::to_unsigned(unsigned def, bool* ok) const { return efj::_to_unsigned<unsigned>(this, UINT_MAX,   def, ok); }
u8       string::to_u8      (u8       def, bool* ok) const { return efj::_to_unsigned<u8>      (this, UINT8_MAX,  def, ok); }
u16      string::to_u16     (u16      def, bool* ok) const { return efj::_to_unsigned<u16>     (this, UINT16_MAX, def, ok); }
u32      string::to_u32     (u32      def, bool* ok) const { return efj::_to_unsigned<u32>     (this, UINT32_MAX, def, ok); }
u64      string::to_u64     (u64      def, bool* ok) const { return efj::_to_unsigned<u64>     (this, UINT64_MAX, def, ok); }
umm      string::to_usize   (umm      def, bool* ok) const { return efj::_to_unsigned<umm>     (this, SIZE_MAX,   def, ok); }

extern float
string::to_float(float def, bool* ok) const
{
    errno = 0;
    float val = strtof(data, nullptr);
    if (errno != 0) {
        if (ok) *ok = false;
        return def;
    }

    if (ok) *ok = true;
    return val;
}

extern double
string::to_double(double def, bool* ok) const
{
    errno = 0;
    double val = strtod(data, nullptr);
    if (errno != 0) {
        if (ok) *ok = false;
        return def;
    }

    if (ok) *ok = true;
    return val;
}

extern bool
string::to_bool(bool def, bool* ok) const
{
    if (size >= 4) {
        if (data[0] == 't' && data[1] == 'r' && data[2] == 'u' && data[3] == 'e') {
            if (ok) *ok = true;
            return true;
        }

        // This is fine even if size == 4 b/c of the nul.
        if (data[0] == 'f' && data[1] == 'a' && data[2] == 'l' && data[3] == 's' && data[4] == 'e') {
            if (ok) *ok = true;
            return false;
        }
    }

    if (ok) *ok = false;
    return def;
}

//}

//{ fmt

extern efj::string
fmt(const char* fmt, ...)
{
    va_list va;
    va_start(va, fmt);

    auto result = efj::vfmt(fmt, va);

    va_end(va);
    return result;
}

extern efj::string
vfmt(const char* fmt, va_list va)
{
    efj::string result{efj::uninitialized};
    efj::memory_arena& tempArena = efj::temp_memory();

#if EFJ_USING_ADDRESS_SANITIZER
    {
        constexpr int kMaxMessageSize = 1024;
        char* buf = (char*)efj_push_bytes(efj::temp_memory(), kMaxMessageSize);
        result.data = buf;
        result.size = vsnprintf(buf, kMaxMessageSize-1, fmt, va);
        buf[result.size] = '\0';
        efj_reset_without_counting(tempArena, buf + result.capacity());
        return result;
    }
#endif

    constexpr int kStringChunkSize = STB_SPRINTF_MIN;

    STBSP_SPRINTFCB* sprintfCb = [](char* buf, void* user, int len) -> char* {
        // We may be done printing right at a chunk boundary, in which case we don't need
        // to allocate another chunk, but we have no way of knowing. We shrink the temp
        // arena to fit whatever the string ended up being anyway, so one excess cheap
        // allocation doesn't matter.
        // IMPORTANT(bmartin): This also makes sure that we always have room to add a nul.
        //
        if (len == kStringChunkSize)
            return (char*)efj_push_bytes(*(efj::memory_arena*)user, kStringChunkSize);

        return nullptr;
    };

    char* buf   = (char*)efj_push_bytes(tempArena, kStringChunkSize);
    result.data = buf;
    // Size of efj::string doesn't include the nul, and stb_sprintf returns the size not including nul,
    // since we are responsible for adding that when we use the callback version.
    // :StringHasNul
    result.size = stbsp_vsprintfcb(sprintfCb, &tempArena, buf, fmt, va);
    result.data[result.size] = '\0';

    // Shrink down to fit the allocated string tightly, including the nul.
    // :StringHasNul
    //
    // NOTE: We don't want this reset to update any scope checking for arena debugging, since the only
    // data structures that would be invalidated would be in here between getting the arena mark and
    // resetting it. We just assume that this code is correct and we are only resetting to the end of
    // the string we just pushed onto temp memory. All references to temp memory made before this
    // function was called should still be valid.
    //
    efj_reset_without_counting(tempArena, buf + result.capacity());

    return result;
}

//}

} // end namespace efj

