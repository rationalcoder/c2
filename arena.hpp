namespace efj
{

enum : umm
{
    arena_size_dynamic = ~(umm)0
};

struct memory_arena;
using expand_arena_func = u8* (efj::memory_arena* arena, umm size, umm alignment);

u8* failed_expand_arena(efj::memory_arena* arena, umm size, umm alignment);

// TODO: take optional custom arena expansion functions.

efj::memory_arena allocate_arena(const char* tag, umm size, umm max = efj::arena_size_dynamic,
    efj::allocator_impl* alloc = efj::get_linear_allocator_impl());

void free_arena(efj::memory_arena& arena);

// Internal
u8* expand_arena(efj::memory_arena* arena, umm size, umm alignment);


// TODO: Consider getting rid of fixed arenas. We pretty much always want dynamic arenas in practice to
// handle possible overflow cases. The use case is more of a debug/performance warning if say an arena
// grows past an expected size. In production, even if that happens, we don't want to panic and exit the app.
// Removing the fixed-sized option would remove a significant amount of complexity.
//
enum memory_arena_type
{
    arena_type_invalid,
    arena_type_fixed,
    arena_type_dynamic,
    arena_type_count_,
};

// Circularly linked.
struct memory_arena_chunk
{
    efj::memory_arena_chunk* next;
    efj::memory_arena_chunk* prev;
    u8*                      start;
    u8*                      at;
    u8*                      end; // last + 1. So in a 4KB chunk (0-4095) this is 4096.
};

struct memory_arena
{
    efj::memory_arena_chunk* curChunk  = nullptr;
    efj::memory_arena_chunk* tailChunk = nullptr;

    efj::expand_arena_func*  expand = nullptr;
    void*                    user   = nullptr;
    const char*              tag    = nullptr;

    efj::memory_arena_type   type      = efj::arena_type_invalid;
    umm                      chunkSize = 0;
    umm                      maxSize   = 0;

    efj::allocator_impl*     alloc = nullptr;
    // For debugging. Store any other information for debugging arena allocation here.
    u32                      resetCount = 0;

    // This is convenient for passing to functions that expect an allocator.
    EFJ_MACRO operator efj::allocator()
    {
        efj::allocator result = {};
        result._data.udata = this;
        result._data.gen   = resetCount;
        result._data.func  = alloc->_func;

        return result;
    }
};

template <typename T_> EFJ_MACRO umm
alignment_offset(T_* header, efj::memory_arena& arena)
{
    return (umm)(arena.curChunk->at - (u8*)(header + 1));
}

// This is the total amount of mapped virtual memory, not including chunk struct sizes.
// For fixed-sized arenas, this only includes the committed size and does not include any guard pages.
inline umm
arena_size(efj::memory_arena& arena)
{
    umm size = 0;

    for (efj::memory_arena_chunk* chunk = arena.curChunk; ; chunk = chunk->next) {
        size += chunk->at - chunk->start;

        if (chunk == arena.tailChunk)
            break;
    }

    return size;
}

struct arena_mark
{
    efj::memory_arena_chunk* chunk;
    u8*                      at;
    u32                      resetCount;
};

EFJ_MACRO efj::arena_mark
begin_scope(efj::memory_arena& arena)
{
    efj::arena_mark result = { arena.curChunk, arena.curChunk->at, arena.resetCount };
    ++arena.resetCount;
    return result;
}

EFJ_MACRO void
end_scope(efj::memory_arena& arena, const efj::arena_mark& mark)
{
    arena.curChunk   = mark.chunk;
    mark.chunk->at   = mark.at;
    arena.resetCount = mark.resetCount;
}

EFJ_MACRO void
reset(efj::memory_arena& arena, void* to)
{
    // TODO: Just assert there is only one chunk or maybe that `to` is in the current chunk?
    // We don't want to do anything expensive here, though I doubt looping backwards and
    // doing comparisons would be that bad since `to` will almost always be in the most recent
    // chunk. This function is used for things like limiting how much temp memory is used for
    // each formatted string, so we want it to be fast. If we decide to assert `to` is in the
    // current chunk, we might as well do the loop, since that's the condition.
    efj_assert(arena.type == efj::arena_type_fixed);
    arena.curChunk->at = (u8*)to;
    ++arena.resetCount;
}

// WARNING: This function does _not_ increment the reset count on the arena. It assumes you
// know what you are doing.
EFJ_MACRO void
reset_without_counting(efj::memory_arena& arena, void* to)
{
    efj_assert(arena.type == efj::arena_type_fixed);
    arena.curChunk->at = (u8*)to;
}

// Full reset on the arena. Updates the reset count.
EFJ_MACRO void
reset(efj::memory_arena& arena)
{
    arena.curChunk     = arena.tailChunk->next;
    arena.curChunk->at = arena.curChunk->start;
    ++arena.resetCount;
}

// This is here for code that wants to take advantage of the fact that chunk recycling only needs to
// know the latest chunk still in use. This is useful because keeping track of a single pointer is atomic.
// If this algorithm changes, any code depending on this will generate a compiler error.
//{
EFJ_MACRO efj::memory_arena_chunk*
get_current_chunk(efj::memory_arena& arena)
{
    return arena.curChunk;
}

EFJ_MACRO void
recycle_chunks(efj::memory_arena& arena, efj::memory_arena_chunk* chunkInUse)
{
    arena.tailChunk = chunkInUse->prev;
}
//}

#define efj_push(arena, size, alignment) efj::push(arena, size, alignment)
#define efj_push_bytes(arena, size) efj::push_bytes(arena, size)
#define efj_push_array(arena, n, type) (type*)efj::push(arena, (n)*sizeof(type), alignof(type))
#define efj_push_array_zero(arena, n, type) (type*)efj::push_zero(arena, (n)*sizeof(type), alignof(type))
#define efj_push_array_copy(arena, n, type, data) (type*)efj::push_copy(arena, (n)*sizeof(type), alignof(type), data)
#define efj_push_type(arena, type) ((type*)efj::push(arena, sizeof(type), alignof(type)))
#define efj_push_new(arena, type) efj_placement_new(efj::push(arena, sizeof(type), alignof(type))) type
#define efj_push_copy(arena, type, data) ((type*)efj::push_copy(arena, sizeof(type), alignof(type), data))
#define efj_sub_allocate(arena, size, alignment, tag) efj::sub_allocate(arena, size, alignment, tag)
#define efj_pop_type(arena, type) efj::pop(arena, sizeof(type))
#define efj_reset(...) efj::reset(__VA_ARGS__)
#define efj_reset_without_counting(...) efj::reset_without_counting(__VA_ARGS__)
#define efj_begin_scope(...) efj::begin_scope(__VA_ARGS__)
#define efj_end_scope(...) efj::end_scope(__VA_ARGS__)

inline u8*
push(efj::memory_arena& arena, umm size, umm alignment)
{
    u8* at = efj::align_up(arena.curChunk->at, alignment);
    u8* nextAt = at + size;

    if (nextAt > arena.curChunk->end)
        return arena.expand(&arena, size, alignment);

    arena.curChunk->at = nextAt;
    return at;
}

// Like push(), but without alignment considerations. This avoids a little math.
inline u8*
push_bytes(efj::memory_arena& arena, umm size)
{
    u8* at = arena.curChunk->at;
    u8* nextAt = at + size;

    if (nextAt > arena.curChunk->end)
        return arena.expand(&arena, size, 1);

    arena.curChunk->at = nextAt;
    return at;
}

inline u8*
push_copy(efj::memory_arena& arena, umm size, umm alignment, const void* data)
{
    u8* space = efj::push(arena, size, alignment);
    memcpy(space, data, size);

    return space;
}

inline u8*
push_zero(efj::memory_arena& arena, umm size, umm alignment)
{
    u8* space = efj::push(arena, size, alignment);
    memset(space, 0, size);

    return space;
}

// FIXME: This is old code that we don't currently use, but sub-allocating actually kinda makes sense.
// However, we would want sub-allocated arenas to have a parent arena, and expand by allocating out
// of the parent.
#if 0
inline efj::memory_arena
sub_allocate(efj::memory_arena& arena, umm size, umm alignment, const char* tag,
             efj::expand_arena_func* expand = &efj::failed_expand_arena)
{
    void* start = efj::push(arena, size, alignment);

    efj::memory_arena result = {};
    result.tailChunk        = &result.firstChunk;
    result.firstChunk.next  = nullptr;
    result.firstChunk.start = (u8*)start;
    result.firstChunk.at    = (u8*)start;
    result.firstChunk.end   = (u8*)start + size;
    result.expand           = expand;
    result.user             = nullptr; // TODO: pass this in?
    result.tag              = tag;
    result.type             = efj::arena_type_fixed;
    result.chunkSize        = size;
    result.maxSize          = size;

    return result;
}
#endif

// Utilities

struct memory_arena_scope
{
    efj::memory_arena* arena;
    efj::arena_mark    oldMark;

    memory_arena_scope(efj::memory_arena& arena) : arena(&arena) { oldMark = efj::begin_scope(arena); }
    ~memory_arena_scope() { if (arena) efj_end_scope(*arena, oldMark); }

    memory_arena_scope(efj::memory_arena_scope&& scope)
        : arena(scope.arena), oldMark(scope.oldMark)
    {
        scope.arena = nullptr;
    }
};

inline efj::memory_arena_scope
make_scope(efj::memory_arena& arena) { return efj::memory_arena_scope(arena); }

#define efj_arena_scope_impl2(arena, counter) auto _allocationScope##counter = efj::make_scope(arena)
#define efj_arena_scope_impl1(arena, counter) efj_arena_scope_impl2(arena, counter)
#define efj_arena_scope(arena) efj_arena_scope_impl1(arena, __COUNTER__);

} // namespace efj
