namespace efj
{

struct consumer
{
    efj::object*               object      = nullptr;
    efj::consumer*             next        = nullptr;
    efj::callback              cb          = {};
    const char*                name        = nullptr;

    efj::testsys_consumer_ctx* testContext = nullptr;
};

//{
// NOTE: We only support up to three components right now, because trying to generate the consume
// calls with template metaprogramming with template var args is significantly uglier.
template <typename... ArgsT>
struct consumer_of : consumer
{ static_assert(efj::type_dependent_false<ArgsT...>::value, "not implemented"); };

template <typename Arg1T>
struct consumer_of<Arg1T> : consumer
{
    template <typename ObjectT>
    consumer_of(ObjectT* object, const char* name, void (ObjectT::*callback)(Arg1T&))
    {
        this->object = &object->efj_object;

        cb = efj::_make_custom_callback(&object->efj_object,
            callback, consumer_of::dispatch<ObjectT>);

        name = name;
        efj_slist_push(object->efj_object.consumers, this, next);
    }

    //! We expect this to be called only inside a test and through the test API.
    void test_consume(Arg1T& arg)
    {
        // WARNING: The callback may still be unitialized until after efj::init() time,
        // If someone produced before init(), we just silently drop it. This will also happen
        // if there is no consumer connected though, so we don't want to log.
        if (cb.is_null()) return;

        efj::component_view component =
        {
            efj::component_spec<typename std::remove_cv<Arg1T>::type>::global_info(),
            (void*)&arg,
            //object,
            //nullptr,
            //0,
        };

        // Since we don't expect to have a current object while calling this, we need to set the
        // current object to this one that we are calling into. This work will be duplicated in
        // the callabck dispatcher, but that's fine since this will only happen during testing
        // (though it's not like is expensive to do this twice). We do not, however, duplicate
        // the "touching" of the object done in the dispatcher, since the debug system will not
        // allow that.
        //
        efj::object* oldObject = efj::this_object();
        efj_assert(!oldObject);
        efj::_set_this_object(object EFJ_LOC_ARGS);

        // We don't have a source object when simulating a produce(), in which case we make the decision
        // about queueing based in the calling thread (the main thread at the time of writing) instead of
        // the thread that a source object lives on.
        efj::thread* sourceThread = efj::this_thread();

        if (!efj::_eventsys_should_queue_side_effects(sourceThread, object->thread)) {
            // NOTE: Since we don't have access to the current consumer in the thread-context, we have to
            // call on_consume() both here and during the queued-event dispatch in the pc system.
            efj::_testsys_on_consume(this, &component, 1);
            cb.caller(&cb, &component);
        }
        else {
            efj::_pcsys_queue_consume(sourceThread, this, &component, 1);
        }

        efj::_set_this_object(oldObject EFJ_LOC_ARGS);
    }

    template <typename ObjectT>
    static void dispatch(efj::callback* cb, void* data)
    {
        efj::object* oldObject = efj::this_object();
        //efj::print("old object: %s\n", oldObject ? oldObject->name : "(null)");
        efj::object* o = cb->object;
        efj::_set_this_object(o EFJ_LOC_ARGS);
        efj::_debugsys_object_touched(o EFJ_LOC_ARGS);

        efj_assert(efj::this_object() == cb->object);
        auto* components = (efj::component_view*)data;

        using CB = void (ObjectT::*)(Arg1T&);
        (((ObjectT*)cb->object)->*(*(CB*)&cb->memfn))(*(Arg1T*)components[0].value());

        efj::_set_this_object(oldObject EFJ_LOC_ARGS);
    }
};

struct producer
{
    efj::producer*             next   = nullptr;
    efj::object*               object = nullptr;
    const char*                name   = nullptr;
    efj::array<consumer*>      consumers;

    efj::testsys_producer_ctx* testContext = nullptr;
};

template <typename... ArgsT>
struct producer_of : producer
{
    producer_of(efj::object* object, const char* name)
    {
        efj_slist_push(object->producers, this, next);
        this->object = object;
        this->name   = name;
        // We could just leave this as heap allocated if we want to.
        this->consumers.reset(efj::perm_memory());
    }

    void connect(efj::consumer_of<ArgsT...>& consumer)
    {
        if (!consumers.find(&consumer))
            consumers.add(&consumer);
    }

    //! Produces to connected consumers, either calling the consumer fuctions directly (if possible)
    //! or by sending a message to the consumers' threads. We don't bother doing things like making
    //! copies of components if multiple consumers consume a non-const value or anything like that.
    //! We would rather applications be wrong in ways that are expected by looking at the application code.
    //!
    void produce(ArgsT&... args)
    {
        efj::component_view components[] =
        {
            {
                efj::component_spec<typename std::remove_cv<ArgsT>::type>::global_info(),
                (void*)&args,
                //object,
                //nullptr,
                //0,
            }...
        };

        // There's no if (testing) check here b/c we always want to go through the test system in case
        // the user wants to capture output at runtime.
        //
        // Also, we now call this regardless of whether we have any connected consumers. This is a
        // decision that may be revisited. The reason I made this change was to avoid calling into the
        // test system per consumer, since it's simpler to see one output in files, network captures,
        // the test API, etc. than seeing one per consumer. However, we might end up wanting to do that
        // and just update the test API to allow for more control over what the user wants to see.
        // This also has the added complexity of letting the target object be null.
        //
        efj::consumer* testTarget = consumers.size() ? consumers.at_unsafe(0) : nullptr;
        if (!efj::_testsys_on_produce(this, testTarget, components, efj::array_size(components)))
            return;

        efj::thread* sourceThread = efj::this_thread();
        efj_assert(sourceThread == object->thread);

        for (efj::consumer* c : consumers) {
            efj::callback* cb = &c->cb;
            efj::object* target = cb->object;

            if (!efj::_eventsys_should_queue_side_effects(sourceThread, target->thread))
                cb->caller(cb, components);
            else
                efj::_pcsys_queue_consume(sourceThread, c, components, efj::array_size(components));
        }
    }

    void produce_to(efj::object* target, ArgsT&... args)
    {
        for (efj::consumer* c : consumers) {
            if (c->object == target) {
                efj::callback* cb = &c->cb;

                efj::component_view components[] =
                {
                    {
                        efj::component_spec<typename std::remove_cv<ArgsT>::type>::global_info(),
                        (void*)&args,
                        //object,
                        //nullptr,
                        //0,
                    }...
                };

                if (!efj::_testsys_on_produce(this, c, components, efj::array_size(components)))
                    return;

                efj::thread* sourceThread = efj::this_thread();
                efj_assert(sourceThread == object->thread);

                if (!efj::_eventsys_should_queue_side_effects(sourceThread, target->thread))
                    cb->caller(cb, components);
                else
                    efj::_pcsys_queue_consume(sourceThread, c, components, efj::array_size(components));

                return;
            }
        }

        efj_panic("consumer not found");
    }
};

template <typename... OutputArgsT, typename... InputArgsT> inline void
_connect(efj::producer_of<OutputArgsT...>& p, efj::consumer_of<InputArgsT...>& c)
{
    p.connect(c);
}

// We don't decorate the producer names so we can use efj_produce(o->name, msg) in code that has a pointer to an efj object.
#define efj_connect(o1, outName, o2, inName) efj::_connect(o1.outName, o2.input##inName)

#define efj_produce(name, ...) name.produce(__VA_ARGS__)
#define efj_produce_to(o, name, ...) name.produce_to(o, __VA_ARGS__)

#define EFJ_INPUT(name, ...) efj::consumer_of<__VA_ARGS__> input##name{this, #name, &std::remove_reference<decltype(*this)>::type::consume##name}
#define EFJ_OUTPUT(name, ...) efj::producer_of<__VA_ARGS__> name{&this->efj_object, #name}

} // namespace efj

