struct sockaddr_in;
struct sockaddr_in6;
struct sockaddr_un;

namespace efj
{

enum net_addr_type
{
    net_addr_type_invalid,
    net_addr_type_ipv4,
    net_addr_type_ipv6,
    net_addr_type_unix,
    net_addr_type_count_,
};

using net_addr_ipv4 = ::in_addr;
using net_addr_ipv6 = ::in6_addr;
using net_addr_unix = char[108];

struct net_addr
{
    efj::net_addr_type type;
    union
    {
        efj::net_addr_ipv4 ipv4;
        efj::net_addr_ipv6 ipv6;
        efj::net_addr_unix unix;
        char               data[0]; // not necessary, but convenient.
    };
};

// All return values of this type are in network byte order.
struct net_sock_addr
{
    efj::net_addr address = {};
    u16           family  = 0;
    u16           port    = 0;
};

extern efj::net_addr net_addr_any;
extern efj::net_addr net_addr_broadcast;

efj::net_sock_addr net_make_sock_addr(const char* ip, u16 port);
efj::net_sock_addr net_make_sock_addr(const struct sockaddr_in& addr);
efj::net_sock_addr net_make_sock_addr(const struct sockaddr_un& addr);
efj::net_sock_addr net_make_sock_addr(const struct sockaddr_in6& addr);

// Returns on temp memory.
efj::string net_get_ip(const efj::net_sock_addr& addr);
u16 net_get_port(const efj::net_sock_addr& addr);

} // namespace efj

