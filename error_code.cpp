namespace efj
{

extern const char*
default_error_to_string(int value)
{
    return value == 0 ? "none" : "unknown";
}

} // namespace efj
