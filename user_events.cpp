namespace efj
{

extern void
_subscribe(efj::object* o, u32 id, efj::callback cb, efj::sub_type type, const void** cached, const efj::component_info* info)
{
    if (!efj::_initting_objects()) {
        efj_panic("You can't subscribe to user events outside of init() in an object");
        return;
    }

    efj::user_event_system* sys = efj::app()._userEventSystem;
    efj::_uesys_subscribe(sys, o, id, cb, type, cached, info);
}

extern void
register_user_event(u32 id)
{
    efj::user_event_system* sys = efj::app()._userEventSystem;
    efj::_uesys_register_event(sys, id);
}

extern void
register_user_events(u32 n)
{
    efj::user_event_system* sys = efj::app()._userEventSystem;

    for (u32 i = 0; i < n; i++)
        efj::_uesys_register_event(sys, i);
}

extern void
trigger_from(efj::object* object, u32 id, const efj::component_view& e)
{
    if (efj::_initting_objects()) {
        efj_panic("You can't trigger user events during init() b/c other subscribers may not be registered yet");
        return;
    }

    if (efj::testing() && !efj::_testsys_on_trigger_event(object, id, e))
        return;

    efj::user_event_system* sys = efj::app()._userEventSystem;
    efj::_uesys_trigger_event(sys, object, id, e);
}

extern void
send_event_from(efj::object* source, efj::object* target, u32 id, const efj::component_view& e)
{
    if (efj::_initting_objects()) {
        efj_panic("You can't send user events during init() b/c other subscribers may not be registered yet");
        return;
    }

    if (efj::testing() && !efj::_testsys_on_send_event(source, target, id, e))
        return;

    efj::user_event_system* sys = efj::app()._userEventSystem;
    efj::_uesys_send_event(sys, source, target, id, e);
}

} // namespace efj

