namespace efj
{

static void
_dispatch_udp_event(efj::eventsys_event_data* edata)
{
    efj::udp_socket_event e;
    e.sock = (efj::udp_socket*)edata->udata.as_ptr;

    if (edata->epoll.events & efj::io_in) {
        e.type = efj::udp_socket_event_read;
        edata->objectCb.caller(&edata->objectCb, &e);
    }
    else if (edata->epoll.events & efj::io_err) {
        e.type = efj::udp_socket_event_error;
        edata->objectCb.caller(&edata->objectCb, &e);
    }
    else {
        efj_internal_log(efj::log_level_warn, "not dispatching udp event (flags: %d)", edata->epoll.events);
    }
}

extern bool
udp_create_socket(const char* name, efj::udp_socket* result)
{
    efj::thread* thread = efj::this_object()->thread;

    int fd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (fd == -1) return false;

    efj::eventsys_epoll_desc desc = {};
    desc.udata.as_ptr = result;
    desc.dispatch     = &_dispatch_udp_event;
    efj::_eventsys_create_epoll_event(thread, name, fd, &desc, &result->event);

    if (!efj::make_nonblocking(fd)) {
        efj_not_implemented();
        return false;
    }

    return true;
}

extern void
udp_destroy_socket(efj::udp_socket& sock)
{
    efj::_eventsys_destroy_epoll_event(sock.event.owningThread, &sock.event);
}

extern bool
udp_bind(efj::udp_socket& sock, const char* ip, u16 port)
{
    struct sockaddr_in address = {};
    address.sin_family      = AF_INET;
    address.sin_port        = htons(port);
    address.sin_addr.s_addr = inet_addr(ip);
    //address.sin_addr.s_addr = INADDR_ANY;

    return ::bind(sock.event.fd, (struct sockaddr*)&address, sizeof(address)) == 0;
}

// Linux's default qdisc (pfifo_fast) uses the DSCP/TOS bits in the IPv4 packet header to determine the priority of each packet,
// so it is likely setting the DSCP bits and priority is enough to ensure Linux is performing priority queueing.
// We could also set CAP_NET_ADMIN capabilities, but this would require the executable to have root privileges.
// @NeedsTesting to verify that priority queueing is being performed.
extern bool
udp_set_high_priority(efj::udp_socket& sock)
{
    efj_panic_if(!sock.event.created || sock.event.destroyed);
    
    bool result = true;

    int so = 0x07; // Interactive/Highest Priority -> packets go in fifo0 queue and DSCP bits set in IPv4 packet headers
    int sopResult = setsockopt(sock.event.fd, SOL_SOCKET, SO_PRIORITY, (void*)(&so), sizeof(so));
    if (sopResult != 0) {
        efj_internal_log(efj::log_level_warn, "Setting SO_PRIORITY on socket %s failed with Error %s", sock.event.name, strerror(errno));
        result = false;
    }
    
    int ef = 0xB8; // Expedited Fowarding -> DSCP bits set in IPv4 packet headers
    int tosResult = setsockopt(sock.event.fd, IPPROTO_IP, IP_TOS, (void*)(&ef), sizeof(ef));
    if (tosResult != 0) {
         efj_internal_log(efj::log_level_warn, "Setting DSCP on socket %s failed with Error %s", sock.event.name, strerror(errno));
         result = false;
    }
    return result;
}

extern ssize_t
udp_read(efj::udp_socket& sock, void* buf, umm size, int flags)
{
    return ::recv(sock.event.fd, buf, size, flags);
}

extern ssize_t
udp_recv_from(efj::udp_socket& sock, void* buf, umm size, efj::net_sock_addr* addr, int flags)
{
    struct sockaddr_in address = {};
    address.sin_family = addr->family;
    address.sin_port   = addr->port;
    address.sin_addr   = addr->address.ipv4;

    socklen_t sockLen = sizeof(address);

    ssize_t result = ::recvfrom(sock.event.fd, buf, size, flags, (struct sockaddr*)&address, &sockLen);

    *addr = efj::net_make_sock_addr(address);
    return result;
}

extern ssize_t
udp_send_to(efj::udp_socket& sock, const void* buf, umm size,
    const efj::net_sock_addr& addr, int flags)
{
    efj_assert(addr.family == AF_INET);

    struct sockaddr_in address = {};
    address.sin_family = addr.family;
    address.sin_port   = addr.port;
    address.sin_addr   = addr.address.ipv4;

    return ::sendto(sock.event.fd, buf, size, flags,
                    (struct sockaddr*)&address, sizeof(struct sockaddr_in));
}

extern ssize_t
udp_send_to(efj::udp_socket& sock, const void* buf, umm size,
    const char* ip, u16 port, int flags)
{
    efj::net_sock_addr addr = efj::net_make_sock_addr(ip, port);
    return efj::udp_send_to(sock, buf, size, addr, flags);
}

extern ssize_t
udp_send_to(efj::udp_socket& sock, const void* buf, umm size,
            u32 ipv4, u16 port, int flags)
{
    // Don't call make_sock_addr b/c we expect the ip/port to be in network byte order already.
    struct sockaddr_in address = {};
    address.sin_family      = AF_INET;
    address.sin_port        = port;
    address.sin_addr.s_addr = ipv4;

    return ::sendto(sock.event.fd, buf, size, flags,
                    (struct sockaddr*)&address, sizeof(struct sockaddr_in));
}

extern ssize_t
udp_send_to(int fd, const void* buf, umm size,
    const efj::net_sock_addr& addr, int flags)
{
    efj_assert(addr.family == AF_INET);

    struct sockaddr_in address = {};
    address.sin_family = addr.family;
    address.sin_port   = addr.port;
    address.sin_addr   = addr.address.ipv4;

    return ::sendto(fd, buf, size, flags,
                    (struct sockaddr*)&address, sizeof(struct sockaddr_in));
}


} // namespace efj
