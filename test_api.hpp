namespace efj
{

//! This is a global mostly for performance reasons. We want to abstract out the notion of time for
//! testing, and we want the default time functions to use abstract time during testing and be as fast
//! as possible during normal operation. This is set before threads are spawned and never changed after that.
//! Since this value is read-only after startup and we expect the appropriate memory barriers to be issued by
//! the event system/OS when starting up other threads, we leave this as non-atomic and don't use volatile access
//! to it, allowing the compiler to optimize mulitple checks of it away. When it can't be optimized, it's a completely
//! predictable branch.
struct alignas(EFJ_CACHELINE_SIZE) testing_global
{
    bool value;
    char pad[EFJ_CACHELINE_SIZE-sizeof(value)];
};

struct test_note;
// We need this visible for the test macros, but this is opaque.
using test_func = void();
struct test_case
{
    efj::memory_arena*      arena               = nullptr;
    const char*             name                = nullptr;
    efj::test_func*         run                 = nullptr;
    umm                     testCount           = 0;
    umm                     passCount           = 0;
    umm                     failCount           = 0;
    bool                    testFailedSoExiting = false;

    efj::array<efj::string>     failureMessages;
    efj::array<efj::string>     logMessages;
    efj::array<efj::test_note*> notes;
};

extern efj::test_case*     globalCurrentTest;
extern efj::testing_global globalTesting;
EFJ_MACRO bool testing() { return globalTesting.value; }

//{ User defined
using test_dev_init_func = void();

//! Sets a function to be called during efj::init() to do arbitrary things with the
//! test API before any threads are spawned and any code in objects is called.
void test_use_dev_init(efj::test_dev_init_func* func);

//! Mostly for internal use.
void test_call_dev_init();
//}

void test_disable_object_logging();

template <typename ObjectT> inline void
test_enable_log_level(efj::log_level level);

template <typename ObjectT> inline void
test_enable_log_level(ObjectT* o, efj::log_level level);

template <typename ObjectT> inline void
test_disable_all_but();

template <typename ObjectT> inline void
test_disable_all_but(ObjectT* o);

//! Disables a particular instance of an object with the given type.
template <typename ObjectT> inline void
test_disable(ObjectT* o);

//! Disables the first instance of an object with the given type.
template <typename ObjectT> inline void
test_disable();

//! Gets a pointer to the first instance of an object.
template <typename ObjectT> inline ObjectT*
test_get_object();

//! Writes the serialized output to a file in hex, one line per component.
void test_write_to_hex(efj::producer& p, const efj::string& path);

//! Writes the serialized output to host/port over udp, for use with Wireshark.
void test_write_to_network(efj::producer& p, const efj::string& host, u16 port);


//{ Test-specific stuff.

// Unit tests are test that can be run sequentially.
// Integration tests are tests that run macro-level tests, mocking out IO objects.

#define EFJ_TEST(group, name)                              \
void test_func_##group##_##name();                         \
struct test_struct_##group##_##name                        \
{                                                          \
    test_struct_##group##_##name()                         \
    {                                                      \
        efj::test_register_test(#group "." #name,          \
            test_func_##group##_##name);                   \
    }                                                      \
};                                                         \
test_struct_##group##_##name test_global_##group##_##name; \
void test_func_##group##_##name()

#define EFJ_TEST_INIT()                           \
void test_init();                                 \
struct test_struct_test_init                      \
{                                                 \
    test_struct_test_init()                       \
    {                                             \
        efj::test_register_test_init("test_init", \
            test_init);                           \
    }                                             \
};                                                \
test_struct_test_init test_global_test_init;      \
void test_init()

#define TEST_EXPR_STRING(expr) #expr
#define TEST_BEXPR(expr, lhs, rhs, op)                                       \
do {                                                                         \
    if (!efj::globalCurrentTest) {                                           \
        efj::print_error("No EFJ_TEST currently running. Did you use"        \
        " the wrong test macro?\n");                                         \
        efj_panic(!"no test running");                                       \
    }                                                                        \
    /* Avoid evaluating the arguments if we are supposed to fail manually */ \
    /* because the expression may involve null pointers */                   \
    if (efj::globalCurrentTest->testFailedSoExiting) {                       \
        return;                                                              \
    }                                                                        \
                                                                             \
    ++efj::globalCurrentTest->testCount;                                     \
    const auto& lhsValue = lhs;                                              \
    const auto& rhsValue = rhs;                                              \
    if (!(lhsValue op rhsValue)) {                                           \
        efj_temp_scope();                                                    \
        efj::string lhsText = efj::to_string(lhsValue);                      \
        efj::string rhsText = efj::to_string(rhsValue);                      \
        efj::string msg = efj::fmt(TEST_EXPR_STRING(expr)                    \
            "\n  lhs: %s\n  rhs: %s", lhsText.c_str(),                       \
            rhsText.c_str());                                                \
        efj_test_failure(msg);                                               \
        return;                                                              \
    }                                                                        \
    else {                                                                   \
        ++efj::globalCurrentTest->passCount;                                 \
    }                                                                        \
} while (false)

#define TEST_EQ(lhs, rhs) TEST_BEXPR(lhs == rhs, lhs, rhs, ==)
#define TEST_NEQ(lhs, rhs) TEST_BEXPR(lhs != rhs, lhs, rhs, !=)
#define TEST_LT(lhs, rhs) TEST_BEXPR(lhs < rhs, lhs, rhs, <)
#define TEST_LEQ(lhs, rhs) TEST_BEXPR(lhs <= rhs, lhs, rhs, <=)
#define TEST_GT(lhs, rhs) TEST_BEXPR(lhs > rhs, lhs, rhs, >)
#define TEST_GEQ(lhs, rhs) TEST_BEXPR(lhs >= rhs, lhs, rhs, >=)

#define TEST_TRUE(cond)                                                \
do {                                                                   \
    /* Avoid evaluating the arguments if we are supposed*/             \
    /* to fail manually because the expression may involve */          \
    /* null pointers. Also we want a different failure */              \
    /* message if we are failing manually */                           \
    if (efj::globalCurrentTest->testFailedSoExiting) {                 \
        return;                                                        \
    }                                                                  \
                                                                       \
    ++efj::globalCurrentTest->testCount;                               \
    if (!(cond)) {                                                     \
        efj_test_failure("  expr: " TEST_EXPR_STRING(cond) "");        \
        return;                                                        \
    }                                                                  \
    else {                                                             \
        ++efj::globalCurrentTest->passCount;                           \
    }                                                                  \
} while (false)

struct test_note
{
    efj::string     msg;
    efj::arena_mark mark;

    test_note(const char* fmt, ...);
    ~test_note();
};

#define TEST_NOTE(fmt, ...) efj::test_note EFJ_UNIQUE_NAME(note)("%s::%i: " fmt "\n", __FILE__, __LINE__, ##__VA_ARGS__)

using test_func = void();
void test_register_test(const char* name, efj::test_func* run);
void test_register_test_init(const char* name, efj::test_func* run);

#define efj_test_failure(msg) efj::test_failure(msg, EFJ_LOCATION)
void test_failure(const efj::string& msg, const efj::source_location& loc);

//! Advance testing-time.
void test_advance(efj::nsec amount);
//! Query monotonic testing-time (the time used for timers).
efj::timestamp test_now();
//! Query ptime testing-time;
efj::posix_time test_now_ptime();

enum test_output_type
{
    test_output_pc,
    test_output_ue,
    test_output_count_,
};

struct test_output
{
    efj::test_output_type     type     = efj::test_output_count_;
    efj::object*              source   = nullptr;
    efj::object*              target   = nullptr;
    efj::producer*            producer = nullptr;
    u32                       eventId  = 0; // Only valid for user events.
    efj::view<efj::component> values   = {};
};

template <typename T>
struct test_output_iterator
{
    efj::arena_queue_entry** _cur = nullptr;

    test_output_iterator(efj::arena_queue_entry* const* node)
        : _cur(const_cast<efj::arena_queue_entry**>(node)) {}

    EFJ_MACRO bool operator == (const test_output_iterator& rhs) const { return _cur == rhs._cur; }
    EFJ_MACRO bool operator != (const test_output_iterator& rhs) const { return _cur != rhs._cur; }

    EFJ_MACRO T& operator *  () const { return *(*_cur)->as<T>(); }
    EFJ_MACRO T* operator -> () const { return (*_cur)->as<T>(); }

    EFJ_MACRO test_output_iterator operator ++ (int) const
    {
        return { &(*_cur)->next };
    }

    EFJ_MACRO test_output_iterator& operator ++ ()
    {
        _cur = &(*_cur)->next;
        return *this;
    }
};

struct test_output_queue
{
    using iterator       = efj::test_output_iterator<test_output>;
    using const_iterator = efj::test_output_iterator<const test_output>;

    efj::arena_queue _queue;

    umm size() const { return _queue.size(); }

    // We push values as components instead of component_views just so people don't have to worry
    // about an extra concept during testing. This costs the space of an efj::allocator per value.
    //
    // FIXME: We support multiple consumers now, but these events only have one target object to set.
    // For now, we just use the first consumer.
    //
    void push_pc(efj::producer* p, efj::consumer* target, const efj::component_view* components, umm count)
    {
        push(efj::test_output_pc, p->object, target->object, p, 0, components, count);
    }

    EFJ_MACRO void push_ue(efj::object* source, efj::object* target, u32 id,
        const efj::component_view& components)
    {
        push(efj::test_output_ue, source, target, nullptr, id, &components, 1);
    }

    void push(efj::test_output_type type, efj::object* source, efj::object* target,
        efj::producer* p, u32 id, const efj::component_view* components, umm count);

    void pop() { _queue.pop(); }

    const efj::test_output& front() const
    {
        return *_queue.front().as<efj::test_output>();
    }

    efj::test_output& front()
    {
        return *_queue.front().as<efj::test_output>();
    }

    template <typename T>
    const T* front_as(umm index = 0) const
    {
        if (!size())
            return nullptr;

        const efj::test_output* output = _queue.front().as<efj::test_output>();
        return output->values[index].as<T>();
    }

    template <typename T>
    T* front_as(umm index = 0)
    {
        if (!size())
            return nullptr;

        efj::test_output* output = _queue.front().as<efj::test_output>();
        return output->values[index].as<T>();
    }

    // NOTE: I've omitted const overloads b/c it's not clear we need them in practice.

    template <typename T>
    T* find_event(u32 id, u32 index, iterator* it = nullptr)
    {
        // Start with the iterator if we have one. Otherwise, start with the first node.
        efj::arena_queue_entry* node = it ? *it->_cur : _queue.front_node();
        for ( ; node; node = node->next) {
            efj::test_output* output = node->as<test_output>();
            if (index < output->values.size) {
                *it = { &node->next };
                return output->values.data[index].as<T>();
            }
        }

        return nullptr;
    }

    template <typename T>
    T* find_event_or_fail(u32 id, u32 index, iterator* it = nullptr)
    {
        T* result = find_event<T>(id, index, it);

        ++globalCurrentTest->testCount;
        if (!result) {
            globalCurrentTest->testFailedSoExiting = true;
            efj_test_failure(efj::fmt("failed to find %s", efj::info_of<T>()->name));
        }

        return result;
    }

    template <typename T>
    T* find_event(u32 index, iterator* it = nullptr)
    {
        // Start with the iterator if we have one. Otherwise, start with the first node.
        efj::arena_queue_entry* node = it ? *it->_cur : _queue.front_node();
        for ( ; node; node = node->next) {
            efj::test_output* output = node->as<test_output>();
            if (output->type = efj::test_output_ue && index < output->values.size) {
                *it = { &node->next };
                return output->values.data[index].as<T>();
            }
        }

        return nullptr;
    }

    template <typename T>
    T* find_event_or_fail(u32 index, iterator* it = nullptr)
    {
        T* result = find_event<T>(index, it);

        ++globalCurrentTest->testCount;
        if (!result) {
            efj_test_failure(efj::fmt("failed to find %s", efj::info_of<T>()->name));
            globalCurrentTest->testFailedSoExiting = true;
        }

        return result;
    }

    template <typename T>
    T* find_pc(efj::producer& p, u32 index, iterator* it = nullptr)
    {
        // Start with the iterator if we have one. Otherwise, start with the first node.
        efj::arena_queue_entry* node = it ? *it->_cur : _queue.front_node();
        for ( ; node; node = node->next) {
            efj::test_output* output = node->as<test_output>();
            if (output->type == efj::test_output_pc && index < output->values.size
                && output->producer == &p) {
                *it = { &node->next };
                return output->values.data[index].as<T>();
            }
        }

        return nullptr;
    }

    template <typename T>
    T* find_pc_or_fail(efj::producer& p, u32 index, iterator* it = nullptr)
    {
        T* result = find_pc<T>(p, index, it);

        ++globalCurrentTest->testCount;
        if (!result) {
            efj_test_failure(efj::fmt("failed to find %s", efj::info_of<T>()->name));
            globalCurrentTest->testFailedSoExiting = true;
        }

        return result;
    }

    const_iterator begin()  const { return { &_queue._front }; };
    iterator       begin()        { return { &_queue._front }; };

    const_iterator end()  const { return { _queue._backNext }; };
    iterator       end()        { return { _queue._backNext }; };

    void clear() { _queue.clear(); }
};

template <typename ObjectT> inline void
test_disconnect_output(ObjectT* o);

template <typename ObjectT> inline void
test_capture(ObjectT* o, efj::test_output_queue* queue);

template <typename T> inline void
test_capture(efj::producer_of<T>& p, efj::test_output_queue* queue);

// This is an annoying technique to prevent "inconsistent parameter pack deduction" errors.
// We want to be able to call test_produce(o->InboundMessage, InboundMessage{}), since that's
// what you would be able to do with a normal efj_produce(), but writing
// test_produce(consumer_of<ArgsT...>&, ArgsT...) when o->InboundMessage takes a const InboundMessage,
// the call will be instantiated like this: test_produce(consumer_of<const InboundMessage>&, InboundMessage&),
// which C++ doesn't allow to work. We would like to be able to specify which parameter to use for deduction,
// but we can't currently do that with C++.
//
template <typename T> struct type_forward { using type = T; };

template <typename... ArgsT> inline void
test_produce(efj::consumer_of<ArgsT...>&, typename efj::type_forward<ArgsT>::type&...);

template <typename EventT> inline void
test_trigger(u32 id, const EventT& e);

template <typename EventT, typename ObjectT> inline void
test_send_event(ObjectT* o, u32 id, const EventT&);

//}

} // namespace efj
