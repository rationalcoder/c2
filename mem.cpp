namespace efj
{

//{ heap_allocator

// NOTE: We can't easily support custom alignments when using the the heap, since realloc() doesn't
// have an aligned version like aligned_alloc. We could implement it ourselves, but this only matters
// for things like SSE and we aren't likely to use any of that. If we do, we can solve the problem in
// context, and do what we need to get 16-byte alignment.
void* heap_allocator::allocate(efj::allocator_data* data, efj::allocator_op* op)
{
    //printf("[%u] Allocating %p, %d, %p, %zu\n", efj::logical_thread_id(), impl, op, p, size);

    switch (op->type) {
    case efj::allocator_op_alloc:
        return ::malloc(((efj::allocate_op*)op)->size);
    case efj::allocator_op_free:
        ::free(((efj::free_op*)op)->p);
        return nullptr;
    case efj::allocator_op_get_pool:
        // malloc() does it's own pooling, so this is like an identity function.
        ((efj::get_pool_op*)op)->pool->_data = *data;
        return nullptr;
    case efj::allocator_op_get_flags:
    {
        ((efj::get_flags_op*)op)->flags = efj::allocator_has_free;
        return nullptr;
    }
    case efj::allocator_op_alloc_array:
        return ::malloc(((efj::allocate_array_op*)op)->size);
    case efj::allocator_op_resize_array: {
        auto* resizeOp = (efj::resize_array_op*)op;
        return ::realloc(resizeOp->p, resizeOp->newSize);
    }
    case efj::allocator_op_resize_in_place:
        return nullptr;
    case efj::allocator_op_free_array:
        ::free(((efj::free_op*)op)->p);
        return nullptr;
    default:
        efj_invalid_code_path();
        return nullptr;
    }
}
//}

struct arena_array_header
{
    umm size;
    u8 data[];
};

//! Allocates space for an array and an internal header to keep track of it.
//! The size and alignemt here refer to the tracked array, not the entire resulting allocation.
EFJ_MACRO efj::arena_array_header*
_arena_allocate_array(efj::memory_arena* arena, umm size, umm alignment)
{
    umm maxAlignment = efj::max(alignof(efj::arena_array_header), alignment);
    // Pass null pointers through.
    static_assert(sizeof(efj::arena_array_header) == sizeof(umm));
    auto* header = (efj::arena_array_header*)efj_push(*arena, sizeof(efj::arena_array_header) + size, maxAlignment);
    header->size = size;
    return header;
}

EFJ_MACRO efj::arena_array_header*
_arena_get_array_header(efj::memory_arena* arena, void* p)
{
    return (efj::arena_array_header*)((u8*)p - sizeof(efj::arena_array_header));
}

void* linear_allocator::allocate(efj::allocator_data* data, efj::allocator_op* op)
{
    auto* arena = (efj::memory_arena*)data->udata;
    if (arena->resetCount != data->gen) {
        efj::print("arena: %s, reset count: %u, gen: %u\n", arena->tag, arena->resetCount, data->gen);
        efj_log_crit("arena: %s, reset count: %u, gen: %u", arena->tag, arena->resetCount, data->gen);
        efj_panic("arena->resetCount != data->gen (the arena was reset while a data structure was still referencing it)");
    }

    //printf("Linear allocate: impl: %p, arena: %s, op: %d, p: %p, size: %zu, align: %zu\n", impl, self->_arena->tag, op, p, size, align);

    switch (op->type) {
    case efj::allocator_op_alloc: {
        auto* allocateOp = (efj::allocate_op*)op;
        return efj_push(*arena, allocateOp->size, allocateOp->alignment);
    }
    case efj::allocator_op_free:
        return nullptr;
    case efj::allocator_op_get_pool: {
        // This is also an identity function because there's no free().
        auto* getPoolOp = (efj::get_pool_op*)op;
        getPoolOp->pool->_data = *data;
        return nullptr;
    }
    case efj::allocator_op_get_flags:
        ((efj::get_flags_op*)op)->flags = 0;
        return nullptr;
    case efj::allocator_op_alloc_array: {
        auto* allocateOp = (efj::allocate_array_op*)op;
        auto* header = _arena_allocate_array(arena, allocateOp->size, allocateOp->alignment);
        // Let null pointers pass through.
        if (!header) return nullptr;
        return header->data;
    }
    case efj::allocator_op_resize_array: {
        // @NeedsTesting
        auto* resizeOp = (efj::resize_array_op*)op;

        // Since this is currently modeled after realloc(), we have to handle resizing a null array.
        if (!resizeOp->p) {
            auto* newHeader = _arena_allocate_array(arena, resizeOp->newSize, resizeOp->alignment);
            if (!newHeader)
                return nullptr;

            newHeader->size = resizeOp->newSize;
            return newHeader->data;
        }

        auto* header = _arena_get_array_header(arena, resizeOp->p);

        // If this array is the last allocation in the arena, we can resize in place if there is room for it.
        auto* curChunk = arena->curChunk;
        if (curChunk->at == header->data + header->size && header->data + resizeOp->newSize <= curChunk->end) {
            curChunk->at = header->data + resizeOp->newSize;
            header->size = resizeOp->newSize;
            return header->data;
        }

        auto* newHeader = _arena_allocate_array(arena, resizeOp->newSize, resizeOp->alignment);
        // Let null pointers pass through.
        if (!newHeader)
            return nullptr;

        ::memcpy(newHeader->data, resizeOp->p, header->size);
        newHeader->size = resizeOp->newSize;
        return newHeader->data;
    }
    case efj::allocator_op_resize_in_place: {
        auto* resizeOp = (efj::resize_in_place_op*)op;

        // We could just return null here, since callers would notice the failure and allocate a new array.
        // We might as well support this though, since we can do it correctly.
        if (!resizeOp->p) {
            auto* newHeader = _arena_allocate_array(arena, resizeOp->newSize, alignof(arena_array_header));
            // Let null pointers pass through.
            if (!newHeader)
                return nullptr;

            newHeader->size = resizeOp->newSize;
            return newHeader->data;
        }

        auto* header = _arena_get_array_header(arena, resizeOp->p);

        // If this array is the last allocation in the arena, we can resize in place if there is room for it.
        auto* curChunk = arena->curChunk;
        if (curChunk->at == header->data + header->size && header->data + resizeOp->newSize <= curChunk->end) {
            //efj::print("Resizing in place: %zu -> %zu\n", header->size, resizeOp->newSize);
            curChunk->at = header->data + resizeOp->newSize;
            header->size = resizeOp->newSize;
            return header->data;
        }
        //efj::print("Not resizing in place: %zu -> %zu: %d, %d\n", header->size, resizeOp->newSize,
        //    curChunk->at == header->data + header->size, header->data + resizeOp->newSize <= curChunk->end);

        return nullptr;
    }
    case efj::allocator_op_free_array:
        return nullptr;
    default:
        efj_invalid_code_path();
        return nullptr;
    }
}

} // namespace efj

