#!/bin/bash

if [[ -z "$1" ]]; then
    echo "error: missing executable name"
    exit
fi

# Make the "read" command read full lines instead of words.
IFS="
"

SymbolRegex="$1.*\[(.*)\]"
AddressRegex="^0x[a-f0-9]*$"

while read -r line; do
    if [[ $line =~ $SymbolRegex ]]; then
        Symbol="${BASH_REMATCH[1]}"
        addr2line -ie "$1" "$Symbol"
    else
        if [[ $line =~ $AddressRegex ]]; then
            addr2line -ie "$1" "$line"
        else
            echo $line | sed 's/$ *//g'
        fi
    fi
done
