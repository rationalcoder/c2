namespace efj
{

static void
_uesys_pre_init(efj::user_event_system* sys, efj::memory_arena* arena)
{
    sys->arena = arena;
}

static void
_uesys_init_thread_context(efj::user_event_system* sys, efj::thread* thread, efj::memory_arena* arena)
{
    efj::uesys_thread_ctx* ctx = thread->ueContext;

    ctx->sys   = sys;
    ctx->arena = arena;
}

static void
_uesys_init_object_context(efj::user_event_system* sys, efj::object* object)
{
    // FIXME: This could happen before the system or any thread contexts are intialized at all,
    // if the EFJ_OBJECT is a global. This should be handled in efj::object().
    object->ueContext = efj_push_new(efj::perm_memory(), efj::uesys_object_ctx);
}

static void
_uesys_register_event(efj::user_event_system* sys, u32 id)
{
    sys->registeredEventCount = id + 1;
}

static void
_uesys_handle_event_registrations(efj::user_event_system* sys)
{
    u32 nRegisteredEvents = sys->registeredEventCount;
    if (nRegisteredEvents == 0)
        return;

    sys->subInfoByEvent = efj_push_array(*sys->arena, nRegisteredEvents, efj::uesys_sub_info);
    for (u32 i = 0; i < nRegisteredEvents; i++) {
        sys->subInfoByEvent[i].asyncThreads.reset(*sys->arena);
        sys->subInfoByEvent[i].syncThreads.reset(*sys->arena);
    }

    for (efj::thread* thread : efj::threads()) {
        efj::uesys_thread_ctx* ctx = thread->ueContext;
        ctx->subsByEvent = efj_push_array(*ctx->arena, nRegisteredEvents, efj::uesys_thread_sub_info);
        ctx->subbedObjectsByEvent = efj_push_array(*ctx->arena, nRegisteredEvents, efj::array<efj::object*>);

        for (u32 i = 0; i < nRegisteredEvents; i++) {
            ctx->subsByEvent[i].subs.reset(*ctx->arena);
            ctx->subbedObjectsByEvent[i].reset(*ctx->arena);
        }
    }
}

static inline efj::uesys_sub_info*
_get_sub_info(efj::user_event_system* sys, u32 id)
{
    efj_assert(id < sys->registeredEventCount);
    return &sys->subInfoByEvent[id];
}

static void
_uesys_subscribe(efj::user_event_system* sys, efj::object* object, u32 id, efj::callback cb, efj::sub_type type,
    const void** cached, const efj::component_info* info)
{
    // Right now, we don't support subscribing to user-events outside of object init() time.
    efj_panic_if(!efj::_initting_objects());
    // The scheduling thread is not currently allowed to have user event subscribers. It shouldn't be too hard to
    // implement if we need it, though.
    efj_panic_if(object->thread == efj::app()._eventSchedulingThread);
    // The config thread can't have user event subscribers right now either. Technically it's only a problem if
    // the subscription is synchronous, but whatever. This will be true until the config system synchronizes
    // events using the sync-event API of the shared internal event system.
    efj_panic_if(object->thread == efj::app()._configThread);

    efj::uesys_thread_ctx* threadContext = object->thread->ueContext;
    efj::uesys_object_ctx* objectContext = object->ueContext;

    // If the user wants us to update a pointer to point to a common thread-local copy of this event, set up the state for it.
    // Right, now we preallocate space for every event that is subscribed to in this way, regardless of whether events of
    // this type are triggered at runtime.
    if (cached) {
        if (!info->is_trivial()) {
            efj_panic("components used as user events must be trivial (memcpyable, fixed size)");
            return;
        }

        // We need an object-local table to support caching for targeted events.
        if (!objectContext->cachedEvents)
            objectContext->cachedEvents = efj_push_array_zero(*threadContext->arena, sys->registeredEventCount, efj::uesys_cached_event);

        // We need a thread-local table to support caching for broadcasted events.
        if (!threadContext->cachedEvents)
            threadContext->cachedEvents = efj_push_array_zero(*threadContext->arena, sys->registeredEventCount, efj::uesys_cached_event);

        // We only allocate space for events that we know are cached, and we do this for both the thread-local and object-local tables.
        if (!objectContext->cachedEvents[id].value) {
            objectContext->cachedEvents[id].value = efj_push(*threadContext->arena, info->size, info->alignment);
            objectContext->cachedEvents[id].size  = info->size;
        }

        if (!threadContext->cachedEvents[id].value) {
            threadContext->cachedEvents[id].value = efj_push(*threadContext->arena, info->size, info->alignment);
            threadContext->cachedEvents[id].size  = info->size;
        }
    }

    efj::uesys_sub sub = {};
    sub.o      = object;
    sub.cb     = cb;
    sub.type   = type;
    sub.cached = cached;

    if (!objectContext->subsByEvent)
        objectContext->subsByEvent = efj_push_array_zero(*threadContext->arena, sys->registeredEventCount, efj::uesys_sub);

    if (objectContext->subsByEvent[id].type == efj::sub_invalid)
        threadContext->subbedObjectsByEvent[id].add(object);

    objectContext->subsByEvent[id] = sub;
}

static void
_uesys_post_object_init(efj::user_event_system* sys)
{
    for (efj::thread* thread : efj::threads()) {
        efj::uesys_thread_ctx* threadContext = thread->ueContext;

        for (u32 id = 0; id < sys->registeredEventCount; id++) {
            efj::uesys_thread_sub_info* threadSubInfo = &threadContext->subsByEvent[id];

            bool threadIsSyncSubbed = false;
            for (efj::object* object : threadContext->subbedObjectsByEvent[id]) {
                //printf("Recording object: %s on thread: %s\n", object->name, object->thread->name);
                efj::uesys_sub* sub = &object->ueContext->subsByEvent[id];
                // If any objects are subscribed as sync, the thread as a whole is sync.
                if (sub->type == efj::sub_sync)
                    threadIsSyncSubbed = true;

                threadSubInfo->subs.add(*sub);
            }

            if (threadContext->subbedObjectsByEvent[id].size()) {
                if (threadIsSyncSubbed)
                    sys->subInfoByEvent[id].syncThreads.add(thread);
                else
                    sys->subInfoByEvent[id].asyncThreads.add(thread);
            }
        }
    }
}

// Common broadcast-dispatch implementation. Pulled out to bypass the event-system under various special cases.
static void
_dispatch_broadcasted_event(efj::user_event_system* sys, efj::object* source,
    efj::uesys_thread_ctx* threadContext, u32 id, const efj::component_view& view)
{
    //printf("Dispatching user event %s on thread %s\n", view._info->name, efj::this_thread()->name);

    // Update the thread-local cached value for this event, so we can update object's pointers
    // to point to it when dispatching the event.
    if (threadContext->cachedEvents) {
        efj::uesys_cached_event* cachedEvent = &threadContext->cachedEvents[id];
        if (cachedEvent->value)
            ::memcpy(cachedEvent->value, view.value(), cachedEvent->size);
    }

    //printf("Sub count for %u on thread %s: %zu\n", id, efj::this_thread()->name, threadContext->subsByEvent[id].subs.size());
    for (efj::uesys_sub& sub : threadContext->subsByEvent[id].subs) {
        //printf("Dispatching user event to: %s\n", sub.cb.object->name);
        if (sub.cached)
            *sub.cached = threadContext->cachedEvents[id].value;

        efj::user_event e = {};
        e.sender = source;
        e.type   = id;
        e.value  = &view;

        sub.cb.caller(&sub.cb, &e);
    }
}

static void
_uesys_trigger_event(efj::user_event_system* sys, efj::object* source, u32 id, const efj::component_view& e)
{
    // TODO: Add a clone function or something to components so we can get a full copy of a component including
    // the component struct itself down to one allocation and know we are dealing with a memory arena specifically.
    // Alternatively, we could just memcpy the thing since we only handle trivial components, but the idea is that
    // the component struct could contain debug info, and we want that copied. I guess we just just manually copy all that stuff
    // over too.
    // :FasterComponentCopy

    efj::thread* sourceThread = efj::this_thread();
    efj::uesys_thread_ctx* sourceThreadContext = sourceThread->ueContext;

    const efj::uesys_sub_info* globalSubInfo = _get_sub_info(sys, id);

    for (efj::thread* targetThread : globalSubInfo->asyncThreads) {
        if (!efj::_eventsys_should_queue_side_effects(sourceThread, targetThread)) {
            // We treat async subscribers on this thread the same way we do consumers on this thread.
            // That is, we handle them inline, here. No point in sending an event to ourselves.
            _dispatch_broadcasted_event(sys, source, sourceThreadContext, id, e);
            continue;
        }

        //printf("Dispatching aync to %s\n", targetThread->name);

        efj::eventsys_allocated_event handle =
            efj::_eventsys_allocate_event(sourceThread, targetThread, sizeof(uesys_thread_event));

        auto* threadEvent = (efj::uesys_thread_event*)handle.e;
        threadEvent->dispatch      = _uesys_dispatch_thread_event;
        threadEvent->eventId       = id;
        threadEvent->sourceObject  = source;
        threadEvent->targetObject  = nullptr;
        // :FasterComponentCopy
        threadEvent->componentCopy = efj_push_new(*handle.arena, efj::component)(e, *handle.arena);

        efj::_eventsys_send_event(sourceThread, targetThread, &handle);
    }

    u32 syncThreadCount = globalSubInfo->syncThreads.size();

    // Specify the target threads based on sync subscriptions, and supply the callback that will be executed.
    // on each subscribed thread. The target threads may include this thread, the triggering thread.
    if (syncThreadCount) {
        // If there is only one subscribed sync thread, we can just send an async event directly to that thread instead of
        // going through the event scheduling thread, and if that thread happens to be this thread, we can just dispatch the
        // event here without going through the event system at all.
        if (syncThreadCount == 1) {
            efj::thread* targetThread = globalSubInfo->syncThreads[0];
            //printf("UESYS: Only subbed thread is %s\n", targetThread->name);
            if (!efj::_eventsys_should_queue_side_effects(sourceThread, targetThread)) {
                //printf("UESYS: target thread is current thread!\n");
                _dispatch_broadcasted_event(sys, source, sourceThreadContext, id, e);
            }
            else {
                efj::eventsys_allocated_event handle =
                    efj::_eventsys_allocate_event(sourceThread, targetThread, sizeof(uesys_thread_event));

                auto* threadEvent = (efj::uesys_thread_event*)handle.e;
                threadEvent->dispatch      = _uesys_dispatch_thread_event;
                threadEvent->eventId       = id;
                threadEvent->sourceObject  = source;
                threadEvent->targetObject  = nullptr;
                // :FasterComponentCopy
                threadEvent->componentCopy = efj_push_new(*handle.arena, efj::component)(e, *handle.arena);

                efj::_eventsys_send_event(sourceThread, targetThread, &handle);
            }
        }
        else {
            efj::eventsys_allocated_event handle =
                efj::_eventsys_allocate_sync_event(sourceThread, sizeof(uesys_thread_event),
                    (efj::thread**)globalSubInfo->syncThreads.data(), globalSubInfo->syncThreads.size());

            auto* threadEvent = (efj::uesys_thread_event*)handle.e;
            threadEvent->dispatch      = _uesys_dispatch_thread_event;
            threadEvent->eventId       = id;
            threadEvent->sourceObject  = source;
            threadEvent->targetObject  = nullptr;
            // :FasterComponentCopy
            threadEvent->componentCopy = efj_push_new(*handle.arena, efj::component)(e, *handle.arena);

            efj::_eventsys_send_sync_event(sourceThread, &handle);
        }
    }
}

// Common targeted-dispatch implementation. Pulled out to bypass the event-system under various special cases.
// We don't care about sync vs async here since there is only one object involved.
static void
_dispatch_targeted_event(efj::user_event_system* sys, efj::object* source, efj::object* target,
    u32 id, const efj::component_view& view)
{
    //printf("Dispatching user event %s on thread %s\n", view._info->name, efj::this_thread()->name);

    efj::user_event e = {};
    e.sender = source;
    e.type   = id;
    e.value  = &view;

    // We still need to support the cached-event API, but we can't update the thread-local cached-event, since
    // that would update the value potentially pointed to by multiple other objects on this thread.
    // Instead, we update an object-local cached value, and have the target object's pointer point to that.
    //
    efj::uesys_object_ctx* objectContext = target->ueContext;
    efj::uesys_sub*        sub           = &objectContext->subsByEvent[id];
    if (objectContext->cachedEvents) {
        efj::uesys_cached_event* cachedEvent = &objectContext->cachedEvents[id];
        if (cachedEvent->value) {
            ::memcpy(cachedEvent->value, view.value(), cachedEvent->size);
            *sub->cached = cachedEvent->value;
        }
    }

    sub->cb.caller(&sub->cb, &e);
}

static void
_uesys_send_event(efj::user_event_system* sys, efj::object* source, efj::object* target,
    u32 id, const efj::component_view& e)
{
    efj::thread* sourceThread = efj::this_thread();
    efj::thread* targetThread = target->thread;

    // If the target object lives on this thread, we can dispatch directly.
    // Otherwise, we can just send the event asynchronously. Whether or not an object
    // is sync-subscribed doesn't matter for object-directed events.
    if (!efj::_eventsys_should_queue_side_effects(sourceThread, targetThread)) {
        _dispatch_targeted_event(sys, source, target, id, e);
        return;
    }

    efj::eventsys_allocated_event handle =
        efj::_eventsys_allocate_event(sourceThread, target->thread, sizeof(uesys_thread_event));

    auto* threadEvent = (efj::uesys_thread_event*)handle.e;
    threadEvent->dispatch      = _uesys_dispatch_thread_event;
    threadEvent->eventId       = id;
    threadEvent->sourceObject  = source;
    threadEvent->targetObject  = target;
    // :FasterComponentCopy
    threadEvent->componentCopy = efj_push_new(*handle.arena, efj::component)(e, *handle.arena);

    efj::_eventsys_send_event(sourceThread, targetThread, &handle);
}

// Dispatch the given event to all subs on this thread, unless it's a targeted event,
// in which case we dispatch it to only the target object.
static void
_uesys_dispatch_thread_event(efj::eventsys_thread_event* e)
{
    efj::uesys_thread_ctx* threadContext = efj::this_thread()->ueContext;
    auto* ourEvent = (efj::uesys_thread_event*)e;

    umm eventId = ourEvent->eventId;
    efj_panic_if(eventId >= threadContext->sys->registeredEventCount);
    //printf("Source object: %s\n", ourEvent->sourceObject->name);

    if (ourEvent->targetObject) {
        _dispatch_targeted_event(threadContext->sys, ourEvent->sourceObject,
            ourEvent->targetObject, eventId, *ourEvent->componentCopy);
    }
    else {
        _dispatch_broadcasted_event(threadContext->sys, ourEvent->sourceObject,
            threadContext, eventId, *ourEvent->componentCopy);
    }
}

static void
_uesys_handle_deferred(efj::thread* thread)
{
}

//{ [TESTING]
static void
_uesys_test_trigger_event(efj::user_event_system* sys, u32 id, const efj::component_view& e)
{
    _uesys_trigger_event(sys, nullptr, id, e);
}

static void
_uesys_test_send_event(efj::user_event_system* sys, efj::object* to, u32 id, const efj::component_view& e)
{
    _uesys_send_event(sys, nullptr, to, id, e);
}
//} [TESTING]

} // namespace efj
