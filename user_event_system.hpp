namespace efj
{

struct uesys_thread_event : efj::eventsys_thread_event
{
    efj::component* componentCopy = nullptr;
    efj::object*    sourceObject  = nullptr;
    efj::object*    targetObject  = nullptr;
    u32             eventId       = 0;
};

struct uesys_thread_ctx;
struct user_event_system;

// Per user event
struct uesys_sub_info
{
    efj::array<efj::thread*> asyncThreads;
    efj::array<efj::thread*> syncThreads;
};

struct uesys_sub
{
    efj::object*  o      = nullptr;
    efj::callback cb     = {};
    efj::sub_type type   = sub_invalid;
    // A pointer to a pointer in an object that is updated by the cached-event API.
    // For example `ConfigEvent* _config = nullptr;`. "cached" would be set to `&_config`.
    const void**  cached = nullptr;
};

// Per user event, per thread.
// thread[threadId].subInfo[eventId]
struct uesys_thread_sub_info
{
    efj::array<efj::uesys_sub> subs;
};

struct uesys_cached_event
{
    void* value = nullptr;
    umm   size  = 0;
};

struct uesys_object_ctx
{
    efj::uesys_sub*          subsByEvent  = nullptr;
    efj::uesys_cached_event* cachedEvents = nullptr;
};

struct uesys_thread_ctx
{
    efj::user_event_system*     sys   = nullptr;
    efj::memory_arena*          arena = nullptr;

    efj::array<efj::object*>*   subbedObjectsByEvent = nullptr;
    efj::uesys_thread_sub_info* subsByEvent = nullptr;
    efj::uesys_cached_event*    cachedEvents = nullptr;
};

struct user_event_system
{
    efj::memory_arena*   arena                = nullptr;

    efj::uesys_sub_info* subInfoByEvent       = nullptr;
    u32                  registeredEventCount = 0;
};

static void _uesys_pre_init(efj::user_event_system* sys, efj::memory_arena* arena);
static void _uesys_init_thread_context(efj::user_event_system* sys, efj::thread* thread, efj::memory_arena* arena);
static void _uesys_init_object_context(efj::user_event_system* sys, efj::object* object);

static void _uesys_register_event(efj::user_event_system* sys);
//! Called after all events have been registered, which happens before efj::init().
static void _uesys_handle_event_registrations(efj::user_event_system* sys);
//! Pass in cached value and the corresponding component info to have the system
static void _uesys_subscribe(efj::user_event_system* sys, efj::object* object, u32 id, efj::callback cb,
    efj::sub_type type, const void** cached = nullptr, const efj::component_info* info = nullptr);

static void _uesys_post_object_init(efj::user_event_system*);

static void _uesys_trigger_event(efj::user_event_system* sys, efj::object* source, u32 id, const efj::component_view& e);
static void _uesys_send_event(efj::user_event_system* sys, efj::object* source, efj::object* target, u32 id, const efj::component_view& e);

static void _uesys_handle_deferred(efj::thread* thread);
static void _uesys_dispatch_thread_event(efj::eventsys_thread_event* e);

//{ [TESTING]
static void _uesys_test_trigger_event(efj::user_event_system* sys, u32 id, const efj::component_view& e);
static void _uesys_test_send_event(efj::user_event_system* sys, efj::object* to, u32 id, const efj::component_view& e);
//} [TESTING]

} // namespace efj
