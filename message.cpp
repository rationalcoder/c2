namespace efj
{

static char gUpperHexTable[] = {
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
    'A', 'B', 'C', 'D', 'E', 'F',
};


extern efj::string
to_hex(const void* data, umm size, bool addNewline)
{
    // We need to multiply by 2 for hex and add 1 for a nul, so the input size has to
    // be less than the max int value / 2.
    if (size >= ((~(umm)0)>>1) || size == 0)
        return "";

    umm hexBufSize = size << 1;
    umm hexBufSizeIncludingTrailing = hexBufSize + addNewline + 1;
    char* hexBuf = (char*)efj_push_bytes(efj::temp_memory(), hexBufSizeIncludingTrailing);

    umm hexOffset = 0;
    for (umm i = 0; i < size; i++, hexOffset += 2) {
        u8 byte = ((u8*)data)[i];
        hexBuf[hexOffset]     = gUpperHexTable[byte >> 4];
        hexBuf[hexOffset + 1] = gUpperHexTable[byte & 0xF];
    }

    if (addNewline) {
        hexBuf[hexBufSize]     = '\n';
        hexBuf[hexBufSize + 1] = '\0';
        return efj::string::shallow_copy(hexBuf, hexBufSize + 1);
    }

    hexBuf[hexBufSize] = '\0';
    return efj::string::shallow_copy(hexBuf, hexBufSize);
}

} // namespace efj
