namespace efj
{

template <typename T>
struct list_iter
{
    mutable T* _cur;

    explicit list_iter(T* cur) : _cur(cur) {}

    bool operator == (const list_iter& rhs) const { return _cur == rhs._cur; }
    bool operator != (const list_iter& rhs) const { return _cur != rhs._cur; }

    const T& operator  * () const { return *_cur; }
    T&       operator  * ()       { return *_cur; }

    const T* operator -> () const { return _cur; }
    T*       operator -> ()       { return _cur; }

    const list_iter& operator ++ ()    const { _cur = _cur->next; return *this;  }
    const list_iter  operator ++ (int) const { return list_iter(_cur->next); }

    list_iter& operator ++ ()    { return (list_iter&)++(*(const list_iter*)this); }
    list_iter  operator ++ (int) { return (list_iter)(*(const list_iter*)this)++; }
};

template <typename T>
struct clist_iter // c => circular
{
    mutable T* _cur;
    mutable u32 _size;

    explicit clist_iter(T* cur, u32 size) : _cur(cur), _size(size) {}

    bool operator == (const clist_iter& rhs) const { return _cur == rhs._cur && _size == rhs._size; }
    bool operator != (const clist_iter& rhs) const { return _cur != rhs._cur || _size != rhs._size; }

    const T& operator  * () const { return *_cur; }
    T&       operator  * ()       { return *_cur; }

    const T* operator -> () const { return _cur; }
    T*       operator -> ()       { return _cur; }

    const clist_iter& operator ++ ()    const { _cur = _cur->next; _size--; return *this;  }
    const clist_iter  operator ++ (int) const { return clist_iter(_cur->next, _size-1); }

    clist_iter& operator ++ ()    { return (clist_iter&)++(*(const clist_iter*)this); }
    clist_iter  operator ++ (int) { return (clist_iter)(*(const clist_iter*)this)++; }
};


#define efj_slist_push(list, node, nextField) \
    do {                                      \
        node->nextField = list;               \
        list = node;                          \
    } while (false)

#define efj_slist_remove(prev, node, nextField) \
    do {                                        \
        if (*prev) (*prev) = node->nextField;   \
        node->nextField = nullptr;              \
    } while (false)


struct list_head
{
    list_head* next;
    list_head* prev;
};

#define EFJ_LIST_HEAD_INITIALIZER(node) { &node, &node }


EFJ_FORCE_INLINE void
list_init(efj::list_head* list)
{
    list->next = list;
    list->prev = list;
}

EFJ_FORCE_INLINE bool
list_empty(efj::list_head* list)
{
    return list->next == list;
}

EFJ_FORCE_INLINE void
list_push_front(efj::list_head* list, efj::list_head* node)
{
    efj::list_head* next = list->next;
    next->prev = node;
    node->next = next;
    node->prev = list;
    list->next = node;
}

EFJ_FORCE_INLINE void
list_push_back(efj::list_head* list, efj::list_head* node)
{
    efj::list_head* prev = list->prev;
    list->prev = node;
    node->next = list;
    node->prev = prev;
    prev->next = node;
}

EFJ_FORCE_INLINE void
list_remove(efj::list_head* node)
{
    efj::list_head* next = node->next;
    efj::list_head* prev = node->prev;

    prev->next = next;
    next->prev = prev;

    node->next = nullptr;
    node->prev = nullptr;
}

EFJ_FORCE_INLINE void
list_push_back_list(efj::list_head* list1, efj::list_head* list2)
{
    if (efj::list_empty(list2))
        return;

    efj::list_head* last = list1->prev;

    efj::list_head* first2 = list2->next;
    efj::list_head* last2  = list2->prev;

    last->next   = first2;
    first2->prev = last;

    last2->next = list1;
    list1->prev = last2;
}

#define efj_list_entry(node, type, member) efj_container_of(node, type, member)
// We don't really need *_entry like the linux kernel, since if we are operating on erased node
// types, we can call inline functions. If you are using the macros, getting the containing type is implied.
#define efj_list_front(head, type, member) efj_list_entry((head)->next, type, member)
#define efj_list_back(head, type, member) efj_list_entry((head)->prev, type, member)
#define efj_list_next(var, member) efj_list_entry((var)->member.next, efj_typeof(*(var)), member)
#define efj_list_prev(var, member) efj_list_entry((var)->member.prev, efj_typeof(*(var)), member)
#define efj_list_is_head(var, head, member) (&(var)->member == (head))

#define efj_list_for_each_entry(var, head, member)         \
for (var = efj_list_front(head, efj_typeof(*var), member); \
     !efj_list_is_head(var, head, member);                 \
     var = efj_list_next(var, member))


} // namespace efj
