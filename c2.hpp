#ifndef EFJ_C2_HPP
#define EFJ_C2_HPP

//{ System and standard C library dependencies for headers and .inl files.
//
// TODO: We can probably get rid of a few of these, but whatever.
#include <unistd.h>
#include <pthread.h>
#include <sys/epoll.h>
#include <semaphore.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <fcntl.h>

#include <stdlib.h> // malloc()/free()

// These standard headers come out to be ~8K loc. Which, all things considered, is pretty okay.
#include <assert.h>
#include <stddef.h> // size_t
#include <stdint.h>
#include <string.h> // memcpy
#include <stdarg.h>
#include <type_traits>
//}

// The dependencies between these should be resolved by this ordering
// in order for amalgamation into an STB-style (single file) library to work (easily).

#include "c2_default_config.hpp"
#include "types.hpp"

#include "shared.hpp"
#include "forward.hpp"
#include "exit_code.hpp"
#include "error_code.hpp"
#include "traits.hpp"
#include "defer.hpp"
#include "mem.hpp"
#include "optional.hpp"
#include "list.hpp"
#include "arena.hpp"
#include "free_list.hpp"
#include "user_defined.hpp"
#include "time.hpp"
#include "callback.hpp"
#include "ds.hpp"
#include "array.hpp"
#include "string.hpp"
#include "btree.hpp"
#include "timer_queue.hpp"

// It was annoying to not have access to event structs in the header files, so I put this here.
// Now, however, this is required since the event system exposes certain inline functions used
// internally for performance.
#include "event_system.hpp"

#include "user_events.hpp"
#include "debug.hpp"
#include "fd.hpp"
#include "timer.hpp"
#include "net.hpp"
#include "udp.hpp"
#include "tcp.hpp"
#include "logging.hpp"
#include "object.hpp"
#include "component.hpp"
#include "pc.hpp"
#include "config.hpp"
#include "app.hpp"
#include "arena_queue.hpp"
#include "message.hpp"
#include "message_queue.hpp"
#include "ts.hpp"
#include "file.hpp"
#include "xml.hpp"
#include "test_api.hpp"

#include "event_system.inl"
#include "user_defined.inl"
#include "callback.inl"
#include "string.inl"
#include "logging.inl"
#include "app.inl"
#include "fd.inl"
#include "timer_queue.inl"
#include "timer.inl"
#include "udp.inl"
#include "tcp.inl"
#include "config.inl"
#include "component.inl"
#include "user_events.inl"
#include "time.inl"
#include "ds.inl"
#include "array.inl"
#include "xml.inl"
#include "test_api.inl"

#endif // EFJ_C2_HPP

#ifdef EFJ_C2_IMPLEMENTATION
#include "c2.cpp"
#endif

