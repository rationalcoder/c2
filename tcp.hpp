namespace efj
{

//{ TCP Clients @Incomplete
struct tcp_client;

struct tcp_client_event
{
};

template <typename T_> inline void
tcp_create_client(const char* name, efj::tcp_client& result,
                  void (T_::*callback)(efj::tcp_client& client, flag32 events));

efj::tcp_client _tcp_create_client(const char* name, efj::callback cb);

bool tcp_connect(efj::tcp_client& c, const char* ip, u16 port);
//inline bool connection_handled(const efj::tcp_client& c) { return c.handlingConnect; }
//inline bool connection_succeeded(const efj::tcp_client& c, flag32 events);

inline ssize_t tcp_write(efj::tcp_client& client, const void* buf, umm size);
inline ssize_t tcp_read(efj::tcp_client& client, void* buf, umm size);
inline ssize_t tcp_send(efj::tcp_client& client, const void* buf, umm size, int flags);
inline ssize_t tcp_recv(efj::tcp_client& client, void* buf, umm size, int flags);
void tcp_close(efj::tcp_client&);

inline ssize_t tcp_send_all(efj::tcp_client& client, const void* buf, umm size, int flags);
//}

//{ TCP Servers
struct tcp_server;

struct tcp_server_info
{
};

// TODO: Rename to tcp_remote_client. The name is too long. @Refactor.
struct tcp_connected_client;
struct tcp_connected_client_event;

inline ssize_t write(efj::tcp_connected_client& client, const void* buf, umm size);
inline ssize_t read(efj::tcp_connected_client& client, void* buf, umm size);
inline ssize_t send(efj::tcp_connected_client& client, const void* buf, umm size, int flags);
inline ssize_t recv(efj::tcp_connected_client& client, void* buf, umm size, int flags);
void close(efj::tcp_connected_client& client);

struct tcp_connected_client_list
{
    // TODO: pretty sure just making the iterator const doesn't work for a
    // const_iterator, see message_queue stuff for what I think is good enough
    // as of 9/23/21
    using iterator = efj::list_iter<efj::tcp_connected_client>;
    using const_iterator = const iterator;

    efj::tcp_connected_client* _head     = nullptr;
    efj::memory_arena*         _arena    = nullptr;
    efj::free_list             _freeList = {};
    u32                        _size     = 0;

    const_iterator begin() const { return iterator(_head); }
    iterator       begin()       { return iterator(_head); }
    const_iterator end() const { return iterator(nullptr); }
    iterator       end()       { return iterator(nullptr); }
    const_iterator cbegin() const { return begin(); }
    const_iterator cend()   const { return end();   }

    u32 size() const { return _size; }

    void _init(efj::memory_arena* arena);
    efj::tcp_connected_client* _add_default();
    void _remove(efj::tcp_connected_client* c);
};

struct tcp_server;

struct tcp_server_event
{
};

bool tcp_create_server(const char* name,
                       const efj::tcp_server_info& info,
                       efj::tcp_server& result);

bool tcp_bind(efj::tcp_server& server, u16 port);

template <typename T_> inline bool
tcp_listen(efj::tcp_server& server, int backlog,
           void (T_::*callback)(efj::tcp_server& server, flag32 events));

inline bool connection_ready(const efj::tcp_server& server, flag32 events);

template <typename T_> inline bool
tcp_accept(efj::tcp_server& server, struct sockaddr_in* clientAddress,
           void (T_::*callback)(efj::tcp_connected_client& client,
                                flag32 events, void* udata),
           void* udata = NULL);

void close(efj::tcp_server& server);

}
