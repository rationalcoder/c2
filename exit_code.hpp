namespace efj
{

enum exit_code
{
    exit_normal,
    exit_crash,
    // A "bad crash" is a crash in a signal handler, in the watchdog thread, or generally in a place where
    // we shouldn't crash and can't handle it very well.
    exit_bad_crash,
    exit_thread_unresponsive,
    exit_test_pass,
    exit_test_fail,
    exit_count_,
};

// The template definition of c_str() may not be available, but we can just provide
// an overload instead.
inline const char*
c_str(efj::exit_code code)
{
    switch (code) {
    case efj::exit_normal:              return "Normal";
    case efj::exit_crash:               return "Crash";
    case efj::exit_bad_crash:           return "Bad Crash";
    case efj::exit_thread_unresponsive: return "Thread Unresponsive";
    case efj::exit_test_pass:           return "Test Passed";
    case efj::exit_test_fail:           return "Test Failed";
    default:                            return "Unknown";
    }
}

} // namespace efj
