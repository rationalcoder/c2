namespace efj
{

template <typename T, typename KeyT, typename CompareT> inline T*
binary_search(T* base, umm n, const KeyT& key, CompareT cmp);

template <typename T> inline void
reverse_list(T** head);

} // namespace efj


