
namespace efj
{

template <typename T>
array<T>::array(const array<T>& other) noexcept
{
    _copy_contents(other, other._alloc);
}

template <typename T>
array<T>::array(array<T>&& other) noexcept
{
    // We don't have to check allocators here because we don't have valid one. That
    // means a move should always be valid.
    _alloc = other._alloc;
    _data  = other._data;
    _size  = other._size;
    _cap   = other._cap;

    // No point in clearing the other array's allocator.
    other._data = nullptr;
    other._size = 0;
    other._cap  = 0;
}

template <typename T>
array<T>::~array()
{
    _free_contents();

    // Clear out the allocator to help catch use-after-free.
    _alloc = {};
    _size = 0;
    _cap  = 0;
}

template <typename T>
array<T>& array<T>::operator = (const array<T>& other) noexcept
{
    // Copy with our existing allocator instead of the other's.
    _free_contents();
    _copy_contents(other, _alloc);

    return *this;
}

template <typename T>
array<T>& array<T>::operator = (array<T>&& other) noexcept
{
    _free_contents();

    if (_alloc == other._alloc) {
        _data = other._data;
        _size = other._size;
        _cap = other._cap;

        // No point in clearing the other array's allocator.
        other._data = nullptr;
        other._size = 0;
        other._cap  = 0;
    }
    else {
        // Copy with our existing allocator instead of the other's.
        // This is intended to crash/use a null allocator if someone uses delay_init and forgets
        // to call reset or construct the array before assigning to it.
        _copy_contents(other, _alloc);
    }

    return *this;
}

template <typename T> EFJ_MACRO void
array<T>::reset(efj::allocator alloc)
{
    _free_contents();

    // This one should always be equivalent to a constructor call.
    efj_placement_new(this) array<T>(alloc);
}

template <typename T> EFJ_MACRO void
array<T>::_free_contents()
{
    // IMPORTANT: We want to do this check regardless of whether free_array()
    // accepts null pointers b/c we could have a null allocator. We assume that if
    // our data pointer is non-null that our allocator is valid as well.
    if (_data) {
        if constexpr (!std::is_trivially_destructible_v<T>) {
            for (size_t i = 0; i < _size; i++)
                _data[i].~T();
        }

        efj_allocator_free_array(_alloc, _data);
    }
}

// I pulled this out because sometimes we want to allocate from our existing allocator,
// and sometimes we want to allocate out of another array's allocator.
//
template <typename T> EFJ_MACRO void
array<T>::_copy_contents(const efj::array<T>& other, efj::allocator alloc)
{
    _alloc = alloc;

    if (!other._data) {
        _data = nullptr;
        _size = 0;
        _cap = 0;
        return;
    }

    _data = (T*)efj_allocator_alloc_array(alloc, other._size * sizeof(T), alignof(T));
    _size = other._size;
    // We only allocate/copy _size elements, which means that _cap can become a
    // non-power-of-2 here. Not that that should matter.
    _cap  = other._size;

    if constexpr (std::is_trivially_copyable_v<T>) {
        memcpy(_data, other._data, other._size * sizeof(T));
    }
    else {
        for (size_t i = 0; i < other._size; i++)
            efj_placement_new(&_data[i]) T(other._data[i]);
    }
}

// TODO: If we have a array of POD types, we can make use of alloc.resize_array() (realloc),
// especially if we are adding space instead of just reserving, which is why I originally had
// a separate call for add_space_expand(). Right now, I just want less code to test.

//! \pre capacity < newCapacity
inline void*
array_expand(void* begin, size_t elemSize, size_t size, size_t* capacity, size_t newCapacity,
    efj::allocator* alloc, bool memcopyable)
{
    size_t newCapacityElems = efj::max((size_t)efj::array_min_capacity, efj::bit_ceil(newCapacity));
    size_t newCapacityBytes = newCapacityElems * elemSize;

    // IMPORTANT: We always set the new capacity regardless of allocation success, since we are the only one
    // who knows what the new capacity should be, and we consider allocation failure a panic. If we fail to
    // resize in place, we expect the caller to manually allocate a new array with the returned capacity.
    *capacity = newCapacityElems;
    void* result = efj_allocator_resize_in_place(*alloc, begin, newCapacityBytes);
    if (result)
        return result;

    // Allocate an array and memcpy the old values over if we can. Otherwise, we let the caller do the copying.
    result = efj_allocator_alloc_array(*alloc, newCapacityBytes, efj::array_max_alignment);
    if (!memcopyable)
        return result;

    return memcpy(result, begin, size * elemSize);
}

} // namespace efj

