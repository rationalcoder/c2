
// TODO: Simplify allocators. I was experimenting with stuff, and I haven't cleaned it up based
// on what we are likely to use in practice. We probably aren't likely to use the concept of pools, etc.
// @Cleanup

// Placement new b/c <new> is bloated and can complain about exceptions being turned off.
#define efj_placement_new(p) new(efj_new_dummy(), p)

struct efj_new_dummy {};
inline void* operator new(size_t, efj_new_dummy, void* p) { return p; }
inline void  operator delete(void*, efj_new_dummy, void* p) {}

namespace efj
{

enum
{
    // Max alignment supported by efj::allocator/malloc() in practice.
    mem_max_alignment = sizeof(void*) * 2,
};

namespace mem_literals
{
constexpr umm operator ""_B  (unsigned long long value) { return value; }
constexpr umm operator ""_KB (unsigned long long value) { return value * (1 << 10); }
constexpr umm operator ""_MB (unsigned long long value) { return value * (1 << 20); }
constexpr umm operator ""_GB (unsigned long long value) { return value * (1 << 30); }
}

using namespace mem_literals;

// WARNING: alignment == 0 is invalid and will cause disasterous results.
EFJ_MACRO constexpr bool is_aligned(const void* address, u32 alignPow2) { return ((uptr)address & (alignPow2-1u)) == 0; }

// These return u8* instead of void* because that's a common use case that is compatible with void*. We would want this to
// be void* in C for the implicit casting, but in C++ we have to cast anyway, so this seems like the right thing to do.
EFJ_MACRO constexpr u8* align_up  (const void* address, u32 alignPow2) { return (u8*)(((uptr)address + (alignPow2-1u)) & -(s32)alignPow2); }
EFJ_MACRO constexpr u8* align_down(const void* address, u32 alignPow2) { return (u8*)((uptr)address & ~(alignPow2-1u)); }

EFJ_MACRO constexpr bool is_aligned(uptr address, u32 alignPow2) { return efj::is_aligned((void*)address, alignPow2); }
EFJ_MACRO constexpr uptr align_up  (uptr address, u32 alignPow2) { return (uptr)efj::align_up  ((void*)address, alignPow2); }
EFJ_MACRO constexpr uptr align_down(uptr address, u32 alignPow2) { return (uptr)efj::align_down((void*)address, alignPow2); }

EFJ_MACRO constexpr u8*
aligned_offset(void* address, umm offset, u32 alignPow2)
{
    return efj::align_up((u8*)address + offset, alignPow2);
}

//! Rounds to next power of 2 >= val.
//! Should be equivalent to C++ 20's bit_ceil(), for our purposes at least.
EFJ_MACRO constexpr size_t
bit_ceil(size_t val)
{
    // TODO: If we switch to C++20+, replace this with std::bit_ceil()?
    static_assert(sizeof(size_t) == 4 || sizeof(size_t) == 8);

    if constexpr (sizeof(size_t) == 4)
        return 1 << (32 - __builtin_clz (val - 1));
    else
        return 1 << (64 - __builtin_clzll (val - 1));
}

enum allocator_op_type
{
    allocator_op_alloc,
    allocator_op_free,
    allocator_op_get_flags,
    allocator_op_get_pool,
    allocator_op_alloc_array,
    allocator_op_resize_array,
    allocator_op_resize_in_place,
    allocator_op_free_array,
    allocator_op_count_
};

enum allocator_flags_
{
    // If the allocator doesn't support/need free, free() is a no-op, and code like destructors
    // can skip freeing memory. This is especially useful for node-based data structures that would otherwise
    // need to traverse the entire data structure to free memory.
    allocator_has_free = 0x1,
};
using allocator_flags = int;

struct allocator_op
{
    efj::allocator_op_type type;
};

struct allocate_op : efj::allocator_op
{
    umm size;
    umm alignment;
};

struct free_op : efj::allocator_op
{
    void* p;
};

struct get_flags_op : efj::allocator_op
{
    efj::allocator_flags flags;
};

struct get_pool_op : efj::allocator_op
{
    efj::allocator* pool;
    umm             size;
    umm             alignment;
};

struct allocate_array_op : efj::allocator_op
{
    umm size;
    umm alignment;
};

struct resize_array_op : efj::allocator_op
{
    void* p;
    umm   newSize;
    umm   alignment;
};

struct resize_in_place_op : efj::allocator_op
{
    void* p;
    umm   newSize;
};

struct free_array_op : efj::allocator_op
{
    void* p;
};

// Having one general-purpose function is ugly and a little error-prone, but it offers extensibility and is cheap
// to copy around everywhere. We would otherwise need to pass around quite a few function pointers or have a
// C++-style vtable. With this, the vtable pointer is replaced with a pointer to the function we care about. The
// switch on the op in the implementation should be optimizable.
//
// We could also just do our own vtable implementation, and store the pointer to that in our allocator struct.
// The only real reason we don't make the allocator type have virtual functions is that it's more like a handle.
// We would get the vtable in the right place in memory, but allocator types inheriting from it would pull in fields
// that they don't need (not that that's a huge deal or anything). Also, efj::allocator would become a non-POD type
// unnecessarily.
//
struct allocator_data;
using allocate_func = void* (efj::allocator_data* data, efj::allocator_op* op);

#define efj_allocator_new(allocator, type) efj_placement_new((allocator).allocate(sizeof(type), alignof(type))) type
#define efj_allocator_alloc_type(allocator, type) (type*)(allocator).allocate(sizeof(type), alignof(type))
#define efj_allocator_alloc(allocator, size, align) (allocator).allocate(size, align)
#define efj_allocator_free(allocator, p) (allocator).free(p)
#define efj_allocator_alloc_array(allocator, size, align) (allocator).allocate_array(size, align)
#define efj_allocator_resize_array(allocator, p, newSize, align) (allocator).resize_array(p, newSize, align)
#define efj_allocator_resize_in_place(allocator, p, newSize) (allocator).resize_in_place(p, newSize)
#define efj_allocator_free_array(allocator, p) (allocator).free_array(p)

// Selectors used for conversion constructors in the user-facing allocator type. We just overload on these and
// select the appropriate allocator implementation.
enum allocator_category_heap  { alloc_heap };
enum allocator_category_temp  { alloc_temp };
enum allocator_category_event { alloc_event };

struct allocator_data
{
    efj::allocate_func* func;

    // An arena or whatever. We expect any other information necessary to be stored in whatever this points to.
    void* udata;

    // For storing the reset count of something like a memory arena, for debugging and validation.
    // For example, if a data structure holds onto an allocator that was for an arena when it was
    // on reset count (gen) 1, and that arena has since been reset, any further operations with
    // the allocator are invalid. We then expect reset() operations on the allocator to update
    // the gen count. This is like a cheap use-after-free checker for a common case.
    //
    // Example 1: Invalid usage.
    //
    // allocator alloc1 = someArena; // assume gen 0
    // void* space = alloc.allocate(size, align);
    // allocator newAlloc = alloc.reset(); // gen 1
    //
    // allocator alloc2 = someArena; // gen 1
    // // alloc1.free(space); // error. alloc1 has gen 0; arena has gen 1
    // newAlloc.free(space) // unchecked, but invalid double-free condition.
    //
    // Example 2: Expected usage.
    //
    // allocator alloc1 = someArena; // assume gen 0
    // void* space = alloc.allocate(size, align);
    // alloc.free(space);
    // alloc1 = alloc.reset(); // replace allocator with a new gen 1 version.
    // // Forget about `space` because we know we reset the allocator.
    //
    // This is primarily for the case of a data structure with a well-defined lifetime:
    // {
    //     array<int> ints(temp_memory());
    //     reset(temp_memory());
    //     // Use of array is now invalid. We can't check normal usage. However, we _can_  check certain things.
    //     ints.add(0); // error if this causes an allocation.
    //     // destructor => error if it calls free().
    // }
    //
    u32 gen;

    EFJ_MACRO bool operator == (const efj::allocator_data& other) const
    {
        return func == other.func && udata == other.udata && gen == other.gen;
    }

    EFJ_MACRO bool operator != (const efj::allocator_data& other) const
    {
        return !(*this == other);
    }
};

// The user-facing struct to be passed by value to data structures. The idea here is not be coupled to the fact that the current
// implementation is just a function pointer and to be able to use conversion constructors that can select the appropriate allocator
// implementation to save some typing. This way, we can extend this later if there are more allocator categories, if the underlying types
// for the allocators used for different allocators changes, etc.
//
struct allocator
{
    efj::allocator_data _data = {};

    allocator() = default;
    EFJ_MACRO void init(const efj::allocator_data& data) { _data = data; }

    EFJ_MACRO allocator(const efj::allocator& other) { *this = other; }
    EFJ_MACRO allocator(efj::allocator_category_heap) { *this = efj::get_heap_allocator(); }
    EFJ_MACRO allocator(efj::allocator_category_event) { *this = efj::get_event_allocator(); };
    EFJ_MACRO allocator(efj::allocator_category_temp) { *this = efj::get_temp_allocator(); }

    EFJ_MACRO bool is_null() { return !_data.func; }

    EFJ_MACRO bool has_free() const
    {
        efj::get_flags_op op;
        op.type = efj::allocator_op_get_flags;

        _data.func(const_cast<efj::allocator_data*>(&_data), &op);
        return op.flags & efj::allocator_has_free;
    }

    EFJ_MACRO void* allocate(umm size, umm alignment)
    {
        efj::allocate_op op;
        op.type      = efj::allocator_op_alloc;
        op.size      = size;
        op.alignment = alignment;

        return _data.func(&_data, &op);
    }

    EFJ_MACRO void free(void* p)
    {
        efj::free_op op;
        op.type = allocator_op_free;
        op.p    = p;

        _data.func(&_data, &op);
    }

    EFJ_MACRO efj::allocator get_pool(umm size, umm alignment)
    {
        efj::allocator pool;

        efj::get_pool_op op;
        op.type      = efj::allocator_op_get_pool;
        op.pool      = &pool;
        op.size      = size;
        op.alignment = alignment;

        _data.func(&_data, &op);
        return pool;
    }

    EFJ_MACRO void* allocate_array(umm size, umm alignment)
    {
        efj::allocate_array_op op;
        op.type      = efj::allocator_op_alloc_array;
        op.size      = size;
        op.alignment = alignment;

        return _data.func(&_data, &op);
    }

    //! Equivalent to realloc.
    EFJ_MACRO void* resize_array(void* p, umm newSize, umm alignment)
    {
        efj::resize_array_op op;
        op.type      = efj::allocator_op_resize_in_place;
        op.p         = p;
        op.newSize   = newSize;
        op.alignment = alignment;

        return _data.func(&_data, &op);
    }

    //! Only returns non-null if the array could be resized in place.
    EFJ_MACRO void* resize_in_place(void* p, umm newSize)
    {
        efj::resize_in_place_op op;
        op.type      = efj::allocator_op_resize_in_place;
        op.p         = p;
        op.newSize   = newSize;

        return _data.func(&_data, &op);
    }

    EFJ_MACRO void free_array(void* p)
    {
        efj::free_array_op op;
        op.type      = efj::allocator_op_free_array;
        op.p         = p;

        _data.func(&_data, &op);
    }

    EFJ_MACRO bool operator == (const efj::allocator& other) const
    { return _data == other._data; }

    EFJ_MACRO bool operator != (const efj::allocator& other) const
    { return _data != other._data; }
};

struct allocator_impl
{
    efj::allocate_func* _func;
};

// IMPORTANT: This heap allocator is _not_ thread-safe, since it exposes non thread-safe APIs in
// addition to normal allocation. You will want these to be stored per thread.
struct heap_allocator : efj::allocator_impl
{
    void** _curAllocations     = nullptr; // This is non-null if we are currently tracking allocations.
    umm    _curAllocationCount = 0;
    umm    _curAllocationMax   = 0;

    heap_allocator() { _func = heap_allocator::allocate; }

    // NOTE: We can't easily support custom alignments when using the the heap, since realloc() doesn't
    // have an aligned version like aligned_alloc. We could implement it ourselves, but this only matters
    // for things like SSE and we aren't likely to use any of that. If we do, we can solve the problem in
    // context, and do what we need to get 16-byte alignment.
    static void* allocate(efj::allocator_data* data, efj::allocator_op* op);

    EFJ_MACRO operator efj::allocator()
    {
        efj::allocator result = {};
        result._data.func = &heap_allocator::allocate;

        return result;
    }
};

// Allocator mainly for things like temp memory, where we are using an arena. There is no free() or pools or
// arrays or anything. Allocations just keep pushing data into the arena. At the end of some explicit or implicit
// scope, we expect the underlying arena to be reset().
//
// IMPORTANT: Not thread-safe. Store per thread.
//
struct linear_allocator : efj::allocator_impl
{
    linear_allocator() { _func = &linear_allocator::allocate; }
    static void* allocate(efj::allocator_data* data, efj::allocator_op* op);
};

// More general arena allocator that is a bit of a hybrid, intended to be used for more long lived arenas. This supports
// allocating and freeing from/to pools and alllocating dynamic arrays.
//
// IMPORTANT: Not thread-safe. Store these per thread.
//
using arena_allocator = efj::linear_allocator;
// TODO
//struct arena_allocator : efj::allocator_impl
//{
//    arena_allocator() { _alloc = &arena_allocator::allocate; }
//    static void* allocate(efj::allocator_data* data, efj::allocator_op* op);
//};

struct allocator_scope
{
    efj::allocator old;
    efj::allocator cur;

    allocator_scope(const efj::allocator& alloc)
        : cur(alloc)
    {
        old = efj::set_allocator(alloc);
    }

    ~allocator_scope() { efj::set_allocator(old); }

    allocator_scope(allocator_scope&& scope)
        : old(scope.old), cur(scope.cur)
    { scope.old = {}; }
};

// Overload this function if you want more conversions.
EFJ_MACRO efj::allocator allocator_from(efj::allocator alloc) { return alloc; }

#if 1
#define efj_allocator_scope_impl2(counter, ...)\
_Pragma("GCC diagnostic push")\
_Pragma("GCC diagnostic ignored \"-Wuninitialized\"")\
auto _allocatorScope##counter = efj::allocator_scope(efj::allocator_from(__VA_ARGS__));\
_Pragma("GCC diagnostic pop")
#define efj_allocator_scope_impl1(counter, ...) efj_allocator_scope_impl2(counter, __VA_ARGS__)
#define efj_allocator_scope(...) efj_allocator_scope_impl1(__COUNTER__, __VA_ARGS__)
#endif

#if 0
_Pragma("GCC diagnostic push")\
_Pragma("GCC diagnostic ignored \"-Wuninitialized\"")\
auto _allocatorScopeData##counter = efj::allocator_data_from(__VA_ARGS__);\
auto _allocatorScope##counter = efj::allocator_scope(efj::allocator_from(_allocatorScopeData##counter));\
_Pragma("GCC diagnostic pop")
#define efj_allocator_scope_impl1(counter, ...) efj_allocator_scope_impl2(counter, __VA_ARGS__)
#define efj_allocator_scope(...) efj_allocator_scope_impl1(__COUNTER__, __VA_ARGS__)
#endif

} // namespace efj

#if EFJ_USE_MEM_LITERALS
using namespace efj::mem_literals;
#endif
