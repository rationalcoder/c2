// [SYSTEM_LIFETIME]: efj::application internal-system interface.
// [INTERNAL_API]: Internal API usable by other internal code.
// [TESTING]: Test API support.

namespace efj
{

enum epoll_events_
{
    io_et      = EPOLLET,
    io_in      = EPOLLIN,
    io_out     = EPOLLOUT,
    io_hup     = EPOLLHUP,
    io_rdhup   = EPOLLRDHUP,
    io_oneshot = EPOLLONESHOT,

    // These don't need to be passed. They are reported automatically.
    io_pri     = EPOLLPRI, // urgent (OOB) data received.
    io_err     = EPOLLERR,

    io_in_out  = io_in | io_out,
};

using epoll_events = flag32;

efj::string string_from_fd_events(flag32 events);


// All event types are thread-local.
enum eventsys_event_type
{
    eventsys_event_invalid,
    eventsys_event_timer,
    eventsys_event_waitable, // Custom event with an optional breakpoint/fence to synchronize with.
    eventsys_event_epoll,    // File descriptor event.
    eventsys_event_count_,
};

struct eventsys_fence
{
    sem_t hitSem;

    void init();
    void hit();
    u32 wait_for_hits(u32 count);
    // Interval is the minimum wait time and retry interval for failures that
    // are due to timing edge cases. This function will still return early
    // if an unknown error happens in a syscall. It has to be <= 1000.
    u32 wait_for_hits(u32 count, int timeoutMs, int intervalMs = 10);
};

struct eventsys_breakpoint
{
    sem_t hitSem;
    sem_t waitSem;
    int   waitCount;

    void init();
    void hit_and_wait();
    void wait_for_hits(int count);
    void continue_all();
};

struct eventsys_epoll_data
{
    efj::epoll_events events;
    int               fd;
};

// NOTE: We currently expect to access the epoll data for every event, regardless of
// the event type, so the inheritance here is important to make sure the epoll data isn't
// overlapped by other data when the event type is not an epoll event.

struct eventsys_waitable_data : efj::eventsys_epoll_data
{
    efj::eventsys_breakpoint* beginBp;
    efj::eventsys_breakpoint* endBp;
    efj::eventsys_fence*      completedFence;
};

struct eventsys_timer_data
{
    u64 expiredTime;
    u32 expiredCount;
};

struct eventsys_udata
{
    union
    {
        void* as_ptr;
        umm   as_umm;
    };
};

struct eventsys_event_data;
using eventsys_dispatch_fn = void(efj::eventsys_event_data* data);

struct eventsys_event
{
    // The thread this event was created on. This is important to keep track of b/c the stale event
    // logic only makes sense for events that were created on the current thread.
    efj::thread*                  owningThread = nullptr;
    efj::object*                  owningObject = nullptr; // Source object. May be NULL for thread-directed events.
    efj::eventsys_event_data*     edata        = nullptr;
    const char*                   name         = nullptr;
    bool                          created      = false;
    bool                          destroyed    = false;
    bool                          monitored    = false; // Has this event been added to any threads?
};

struct eventsys_timer_event : efj::eventsys_event
{
    efj::timer_handle handle   = {};
    // We mirror the Linux API, here. If an absolute timeout is specified, `initial` becomes
    // an absolute time instead of a duration like 1_s.
    efj::nsec         initial  = {};
    efj::nsec         interval = {};
    efj::timer_type   type     = efj::timer_invalid_;
    bool              absolute = false;
    bool              running  = false;
};

//{ Intended to be stored either on their own in internal systems or embedded in user-facing event structs.
struct eventsys_epoll_event : efj::eventsys_event
{
    struct epoll_event epollEvent = {};
    int                fd         = -1;
};

struct eventsys_waitable_event : efj::eventsys_epoll_event
{
    efj::eventsys_breakpoint* beginBp        = nullptr;
    efj::eventsys_breakpoint* endBp          = nullptr;
    efj::eventsys_fence*      completedFence = nullptr;
};

//}

enum eventsys_deferred_
{
    eventsys_defer_pc          = 1 << 0,
    eventsys_defer_user_events = 1 << 1,
    eventsys_defer_logging     = 1 << 2,
    eventsys_defer_events      = 1 << 3,
};
using eventsys_deferred = int;

struct eventsys_thread_outbox;

// All other event types are thread-local. That is, they live on the thread that they are created on.
// "Thread events" are events that have data associated with them that need to be sent other threads
// and put in the same queue. For example, if you want to make sure a state transition, user event,
// and consume event happen in a particular order, you would send those as this type of event using
// _eventsys_send_event().
//
// WARNING: It's common to just have system-specific event types derive from this one. However, we
// assume the derived event type has an alignment <= the alignment of this struct. :ThreadEventAlignment
// As long as your struct doesn't have anything in it with an alignment larger than a pointer, you're fine.
//
struct eventsys_thread_event
{
    //{ [internal]
    efj::eventsys_thread_event*     next;
    efj::eventsys_fence*            completedFence;
    efj::thread**                   targetThreads; // only used for sync events. Allocated alongside the event data.
    umm                             targetThreadCount; // only used for sync events.
    efj::eventsys_thread_outbox*    srcOutbox;
    efj::memory_arena_chunk*        endChunk; // end of all allocations for this event.
    //}

    void                            (*dispatch)(efj::eventsys_thread_event* e); // [in]
};

// Stored per thread, one per other thread that the owning thread communicates with.
struct eventsys_thread_outbox
{
    efj::eventsys_thread_event*  events          = nullptr;
    efj::eventsys_thread_event** tail            = &events;
    efj::memory_arena            arena           = {};
    // @TS Atomically set by consuming threads to mark how much data has been consumed. Producing threads use this to
    // recycle the arena chunks up to that point.
    efj::memory_arena_chunk*     consumeProgress = nullptr;

    void init() { efj_placement_new(this) eventsys_thread_outbox; }

    EFJ_MACRO void maybe_allocate()
    {
        //printf("Maybe allocate\n");
        if (!arena.curChunk) {
            arena = efj::allocate_arena("Outbox", 4_KB);
            consumeProgress = arena.curChunk;
            //printf("Allocating outbox: %p\n", consumeProgress);
        }
    }

    EFJ_MACRO void push(efj::eventsys_thread_event* e)
    {
        efj_assert(!e->next);
        *tail = e;
        tail = &e->next;
    }

    EFJ_MACRO efj::eventsys_thread_event* consume_nodes()
    {
        efj::eventsys_thread_event* result = events;
        events    = nullptr;
        tail      = &events;
        return result;
    }

    EFJ_MACRO void recycle_memory()
    {
        efj::memory_arena_chunk* chunk = __atomic_load_n(&consumeProgress, __ATOMIC_RELAXED);
        //printf("Recycle to %p\n", chunk);
        efj::recycle_chunks(arena, chunk);
    }

    EFJ_MACRO void mark_handled(efj::eventsys_thread_event* e)
    {
        //printf("marking handled: %p\n", e->endChunk);
        __atomic_store_n(&consumeProgress, e->endChunk, __ATOMIC_RELAXED);
    }
};

struct eventsys_thread_inbox
{
    efj::eventsys_thread_event*  events = nullptr;
    efj::eventsys_thread_event** tail   = &events;

    pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
    void push(efj::eventsys_thread_event* events);
    efj::eventsys_thread_event* pop_all();
};

struct event_system;
struct eventsys_thread_ctx
{
    efj::event_system*           sys = nullptr;
    efj::memory_arena*           arena = nullptr;
    int                          epollFd = -1;

    efj::timer_queue             timerQueue;
    efj::eventsys_epoll_event    timerQueueEvent = {};
    int                          timerQueueFd = -1;

    // :EventFlushing
    efj::eventsys_waitable_event killEvent = {};
    u32                          eventsToFlushBeforeKill = 0; // @TS atomic
    bool                         flushingEvents = false;

    // Running event data id. Zero is an invalid ID.
    u32                          lastEventId = 0;
    // Pool of free event data structs.
    efj::eventsys_event_data*    eventDataFreeList = nullptr;
    // List of stale event data structs that need to be handled after we finish processing a batch of events.
    efj::eventsys_event_data*    staleEventDataList = nullptr;

    // Event-local state.
    bool*                        threadTouchedThisEvent = nullptr; // [thread count]
    efj::thread**                touchedThreads         = nullptr; // [thread count]
    umm                          touchedThreadCount     = 0;

    efj::eventsys_thread_outbox* outboxes = nullptr; // [thread count]
    efj::eventsys_thread_inbox   inbox;
    efj::eventsys_waitable_event threadEventsAvailable = {};

    efj::eventsys_waitable_event syncEventAvailable = {};

    efj::eventsys_deferred       deferred = 0;
    bool                         initted = false;

    // Queue object ouput even if they live on the same thread.
    bool                         forceQueueSideEffects = false;
};

struct event_system
{
    efj::memory_arena*           arena;

    efj::posix_time              testPosixTime = {};
    efj::timestamp               testTimerTime = {};
    pthread_rwlock_t             testTimeMutex = PTHREAD_RWLOCK_INITIALIZER;

    // Sync event state. Set by the scheduling thread and accessed by all target threads
    // when woken up to handle the sync event.
    efj::eventsys_thread_event*  syncEvent = nullptr;
    efj::eventsys_breakpoint     syncBpBefore;
    efj::eventsys_breakpoint     syncBpAfter;
    efj::eventsys_fence*         syncCompletedFence;

    // Shared fence for thread-kill events. Used to wait for all threads to handle the kill event before exiting.
    efj::eventsys_fence          killEventFence;
    bool                         initted;
};

//{ [SYSTEM_LIFETIME]
static void _eventsys_pre_init(efj::event_system* sys, efj::memory_arena* arena);
static void _eventsys_init_thread_context(efj::event_system* sys, efj::thread* thread, efj::memory_arena* arena);
//! Must be called before any systems can use timers.
//! TODO: We could also disallow the use of the entire API until after this function is called for uniformity, but
//! it's common to create events in thread-context initialization, so I'm deferring this. Also, this is only a problem
//! during startup, so we don't want to add a bunch of checks for no reason. If someone uses timers
//! before this function is called, we expect to crash with a null timer queue event.
static void _eventsys_pre_object_init(efj::event_system* sys);
//! Called after all systems are completely initialized and have had post_init() called, including this one.
static void _eventsys_start(efj::event_system* sys);
//} [SYSTEM_LIFETIME]

//{ [INTERNAL_API]
enum eventsys_update_result
{
    eventsys_update_invalid,
    eventsys_update_continue,
    eventsys_update_killed,
    eventsys_update_count_,
};
// Called to process events on the current thread, for some amount of time. -1 timeout => infinite.
static efj::eventsys_update_result
_eventsys_update_thread(efj::thread* thread, int timeoutMs, efj::exit_code* code);

//! Mark and event to be handled before the stopping a thread's event loop on the kill event.
//! This is used for events like log messages being available so the logging thread will wait
//! for all messages to be written out before exiting.
//!
//! \note This flag is reset after the event is handled.
//!
static void _eventsys_handle_before_kill(efj::eventsys_event* e);
static void _eventsys_kill_all_threads(efj::event_system* sys);

static bool _eventsys_create_fence(efj::eventsys_fence* fence);
static bool _eventsys_create_breakpoint(efj::eventsys_breakpoint* bp);

struct eventsys_timer_desc
{
    efj::eventsys_udata        udata;
    efj::eventsys_dispatch_fn* dispatch;
    efj::callback              objectCallback;
    efj::nsec                  initial;
    efj::nsec                  interval;
    efj::timer_type            type;
    bool                       absolute;
};

struct eventsys_waitable_desc
{
    efj::eventsys_udata        udata;
    efj::eventsys_dispatch_fn* dispatch;
    efj::callback              objectCallback;
    efj::eventsys_breakpoint*  beginBp;
    efj::eventsys_breakpoint*  endBp;
    efj::eventsys_fence*       completedFence;
};

struct eventsys_epoll_desc
{
    efj::eventsys_udata        udata;
    efj::eventsys_dispatch_fn* dispatch;
    efj::callback              objectCallback;
    efj::epoll_events          events;
};

// Timers are always thread-local, so the thread you create them on is the only thread they can be added to.
static bool _eventsys_create_timer(efj::thread* thread, const char* name,
    const efj::eventsys_timer_desc* desc, efj::eventsys_timer_event* e);
//! Changes the callback, *without* marking any pending timer event as stale.
static void _eventsys_monitor_timer(efj::eventsys_timer_event* e, const efj::callback& cb);
static void _eventsys_start_timer(efj::eventsys_timer_event* e);
static void _eventsys_start_timer_absolute(efj::eventsys_timer_event* e, efj::nsec expireTime);
static void _eventsys_start_timer_absolute(efj::eventsys_timer_event* e, efj::nsec expireTime, efj::nsec interval);
static void _eventsys_set_timeout(efj::eventsys_timer_event* e, efj::nsec initial);
static void _eventsys_set_timeout(efj::eventsys_timer_event* e, efj::nsec initial, efj::nsec interval);
//! Sets a timeout relative to a known timestamp returned by efj::now().
//! This is useful to avoid having oneshot timers and newly-started periodic timers drift due to processing delay.
//! For example, if a timer event is handled X microseconds off of its theoretical expire time,
//! starting it for 1 second will cause it to expire 1 second + X microseconds in the future.
//! Periodic timers don't have this problem after the initial expiration.
static void _eventsys_set_absolute_timeout(efj::eventsys_timer_event* e, efj::nsec expireTime);
static void _eventsys_set_absolute_timeout(efj::eventsys_timer_event* e, efj::nsec expireTime, efj::nsec interval);
static void _eventsys_stop_timer(efj::eventsys_timer_event* e);
static void _eventsys_destroy_timer(efj::eventsys_timer_event* e);

static bool _eventsys_create_waitable_event(efj::thread* thread, const char* name,
    const efj::eventsys_waitable_desc* desc, efj::eventsys_waitable_event* e);
static void _eventsys_add_waitable_event(efj::thread* thread, efj::eventsys_waitable_event* e);
static void _eventsys_trigger(efj::eventsys_waitable_event* e);

//{ Very Linux-specific event API for when coupling to an epoll implementation is fine, e.g. for sockets.
// Check udp/fd API implementation for usage code.
static bool _eventsys_create_epoll_event(efj::thread* thread, const char* name, int fd,
    const efj::eventsys_epoll_desc* desc, efj::eventsys_epoll_event* e);
static int  _eventsys_destroy_epoll_event(efj::thread* thread, efj::eventsys_epoll_event* e);
static void _eventsys_add_epoll_event(efj::thread* thread, efj::eventsys_epoll_event* e);
// NULL events => keep existing events.
// NULL cb => keep existing cb.
static void _eventsys_mod_epoll_event(efj::thread* thread, efj::eventsys_epoll_event* e,
    efj::epoll_events* events, efj::callback* objectCb);
static void _eventsys_del_epoll_event(efj::thread* thread, efj::eventsys_epoll_event* e);
static void _eventsys_monitor_epoll_event(efj::thread* thread, efj::eventsys_epoll_event* e,
    efj::epoll_events* events, efj::callback* objectCb);
//}

//! Decides whether communication between two threads should be queued or otherwise
//! be done directly with a function call. For example, we may need to queue side-effects
//! if objects live on different threads or if we are handling a sync-event.
//!
//! NOTE: I'm not sure if this will stay implemented like this, so I have inlined it here.
//! The event system is considered an implementation detail from an application's perspective,
//! but they end up including this header indirectly anyway. We could also force internal code
//! to go through extern APIs like the producer/consumer one which then call this internally,
//! but that seemed to have performance implications.
//!
//! Check the producer/consumer implementation for usage code.
//!
inline bool _eventsys_should_queue_side_effects(efj::thread* from, efj::thread* to);
//! Enables/disables forcing the queueing of object communication.
//! \returns the previous value.
static bool _eventsys_force_queue_side_effects(efj::thread* thread, bool value);

//! Here to add some type safety to the thread-event allocation and sending API since
//! Events and event memory have to be allocated out of a specific arena.
struct eventsys_allocated_event
{
    efj::eventsys_thread_event* e;
    efj::memory_arena*          arena;
};

//! Allocates some event struct that inherits from efj::eventsys_thread_event.
//! `eventSize` is the size of the (potentially) custom event struct, which is at least `sizeof(efj::eventsys_thread_event)`.
//!
//! \usage
//!
//! struct mysys_event : efj::eventsys_thread_event { int data; void* p; };
//! efj::eventsys_allocated_event threadEvent =
//!     efj::_eventsys_allocate_thread_event(thisThread, targetThread, sizeof(msys_event));
//! threadEvent->e->data = 42;
//! threadEvent->e->p    = push_whatever(e->arena);
//!
//! efj::eventsys_send_event(threadThread, targetThread, e);
//!
static efj::eventsys_allocated_event
_eventsys_allocate_event(efj::thread* sourceThread, efj::thread* targetThread, umm eventSize);

static void _eventsys_send_event(efj::thread* sourceThread, efj::thread* targetThread,
    const efj::eventsys_allocated_event* e);

//! Same as the non-sync version, except this function knows you are communicating with the scheduling thread.
static efj::eventsys_allocated_event
_eventsys_allocate_sync_event(efj::thread* sourceThread, umm eventSize,
    efj::thread** targetThreads, umm targetThreadCount);

//! Sends an event to be handled atomically. That is, all threads will stop what they are doing, handle the event, get to another stopping
//! point, and resume handling events only after all threads have fully handled the event.
//!
//! IMPORTANT: All sync events are synchronous with respect to each other. No two sync events can be processed concurrently.
//! This is accomplished by copying the event over to the sync-event scheduling thread to be dispatched.
//!
static void _eventsys_send_sync_event(efj::thread* sourceThread, const efj::eventsys_allocated_event* e);


//! Adds to the set of things that need to be handled at the end of the current event.
static void _eventsys_add_deferred_action(efj::thread* thisThread, efj::eventsys_deferred deferred);
//! Handle async events that have been sent to this thread.
static void _eventsys_handle_async_events(efj::eventsys_event_data* edata);

static void _eventsys_dispatch_sync_event(efj::eventsys_thread_event* e);
// Triggered by the scheduling thread. Handled on threads that are targeted by a sync event.
static void _eventsys_handle_sync_event(efj::eventsys_event_data* edata);


// We generally defer inter-thread communication to the end of an event to be
// sure that any side effects of an event (like writing to a socket) are completed
// before letting other threads run. There are some cases where this is suboptimal,
// but it generally gives us the opportunity to minimize context switches, and any
// performance impact can be worked around by adjusting the threading situation.
//
// This is internal to the event system and shouldn't be called by anyone else.
//
static void _eventsys_handle_deferred_actions(efj::thread* thread);

// Called from the context of a signal handler on the thread that crashed.
static void _eventsys_handle_thread_crashed(efj::thread* thread);

template <> const char* c_str<>(efj::eventsys_event_type type);
//} [INTERNAL_API]

//{ [TESTING]
static efj::posix_time _eventys_get_ptime(efj::event_system* sys);
static efj::timestamp _eventys_get_now(efj::event_system* sys);
static void _eventsys_test_advance(efj::event_system* sys, efj::nsec amount);
//} [TESTING]

} // namespace efj

