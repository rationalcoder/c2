namespace efj
{

extern efj::buffer
read_file(int fd, bool nulTerminate)
{
    efj::buffer result = {};

    off_t size = ::lseek(fd, 0, SEEK_END);
    if (size < 0) return result;
    ::lseek(fd, 0, SEEK_SET);

    auto paddedSize = size + nulTerminate;
    u8* space = (u8*)efj_allocate(paddedSize, 16);

    ssize_t bytesWritten = 0;
    while (bytesWritten < size) {
        ssize_t nRead = ::read(fd, space + bytesWritten, (umm)size - bytesWritten);
        if (nRead < 0) {
            if (nRead == -1 && errno == EINTR) {
                continue;
            }
            else {
                ::close(fd);
                return result;
            }
        }

        bytesWritten += nRead;
    }

    result.data = space;
    result.size = (umm)paddedSize;

    if (nulTerminate)
        space[size] = '\0';

    return result;
}

extern efj::buffer
read_file(const efj::string& path, bool nulTerminate)
{
    efj::buffer result = {};

    int fd = ::open(path.c_str(), O_RDONLY | O_CLOEXEC);
    if (fd == -1)
        return result;

    result = efj::read_file(fd, nulTerminate);
    ::close(fd);

    return result;
}

} // namespace efj
