
#ifdef EFJ_CONFIG_FILE
#include EFJ_CONFIG_FILE
#endif

#ifndef EFJ_USE_SHORT_PRIMITIVES
#define EFJ_USE_SHORT_PRIMITIVES 1
#endif

#ifndef EFJ_USE_TIME_LITERALS
#define EFJ_USE_TIME_LITERALS 1
#endif

#ifndef EFJ_USE_MEM_LITERALS
#define EFJ_USE_MEM_LITERALS 1
#endif

// Pass source code information to functions that can use them for better debugging.
#ifndef EFJ_DEBUG_LOCATIONS
#define EFJ_DEBUG_LOCATIONS 0
#endif

// Debug "touched object" reporting to the debug system. Useful to sanity check
// debug system API calls regardless of whether a debugger is connected.
#ifndef EFJ_DEBUG_OBJECT_TOUCHING
#define EFJ_DEBUG_OBJECT_TOUCHING 0
#endif
