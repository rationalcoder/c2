namespace efj
{

EFJ_MACRO efj::timestamp
now_real()
{
    struct timespec now;
    ::clock_gettime(CLOCK_MONOTONIC, &now);

    return { (u32)now.tv_sec, (u32)now.tv_nsec };
}

EFJ_MACRO efj::posix_time
now_ptime_real()
{
    struct timespec now;
    ::clock_gettime(CLOCK_REALTIME, &now);

    return { (u32)now.tv_sec, (u32)now.tv_nsec };
}


EFJ_MACRO efj::posix_time
now_ptime()
{
    return efj::testing() ? efj::test_now_ptime() : efj::now_ptime_real();
}

EFJ_MACRO efj::posix_time
to_ptime(const efj::ntp_time& time)
{
    // 1970 - 1900 in seconds. No special time conversions are necessary.
    u32 sec = time.sec - 2208988800;

    // 1/2^32 fracs to nanoseconds (1/1,000,000,000)
    // x/1000000000 = fracs/2^32
    // x = fracs * 1,000,000,000 / 2^32
    // x = (fracs * 1,000,000,000) >> 32
    u32 nsec = (u32)(((u64)time.frac * 1000000000u) >> 32);

    return { sec, nsec };
}

EFJ_MACRO efj::sec to_sec(const efj::posix_time& time)
{ return efj::sec{time.sec}; }

EFJ_MACRO efj::msec to_msec(const efj::posix_time& time)
{ return efj::to_msec(efj::timestamp{time.sec, time.nsec}); }

EFJ_MACRO efj::usec to_usec(const efj::posix_time& time)
{ return efj::to_usec(efj::timestamp{time.sec, time.nsec}); }

EFJ_MACRO efj::nsec to_nsec(const efj::posix_time& time)
{ return efj::to_nsec(efj::timestamp{time.sec, time.nsec}); }

EFJ_MACRO efj::sec now_sec() { return efj::to_sec(efj::now()); }
EFJ_MACRO efj::msec now_msec() { return efj::to_msec(efj::now()); }
EFJ_MACRO efj::usec now_usec() { return efj::to_usec(efj::now()); }
EFJ_MACRO efj::nsec now_nsec() { return efj::to_nsec(efj::now()); }

EFJ_MACRO efj::sec  now_real_sec() { return efj::to_sec(efj::now_real()); }
EFJ_MACRO efj::msec now_real_msec() { return efj::to_msec(efj::now_real()); }
EFJ_MACRO efj::usec now_real_usec() { return efj::to_usec(efj::now_real()); }
EFJ_MACRO efj::nsec now_real_nsec() { return efj::to_nsec(efj::now_real()); }


EFJ_MACRO efj::ntp_time
now_ntp()
{
    return efj::to_ntp(efj::now_ptime());
}

EFJ_MACRO efj::ntp_time
to_ntp(const efj::posix_time& time)
{
    // 1970 - 1900 in seconds. No special time conversions are necessary.
    u32 seconds = time.sec + 2208988800;

    // nanoseconds (1/1,000,000,000) to 1/2^32
    // nano/1000000000 = x/2^32
    // x = (nano * 2^32) / 1,000,000,000
    u32 fraction = (u32)(((u64)time.nsec << 32) / 1000000000u);

    return { seconds, fraction };
}

EFJ_MACRO efj::timestamp
now()
{
    return efj::testing() ? efj::test_now() : efj::now_real();
}

EFJ_MACRO efj::sec
to_sec(const efj::timestamp& ts)
{
    return efj::sec{(u64)ts.sec};
}

EFJ_MACRO efj::msec
to_msec(const efj::timestamp& ts)
{
    return efj::msec{(u64)ts.sec * 1000u + (u64)ts.nsec / 1000000u};
}

EFJ_MACRO efj::usec
to_usec(const efj::timestamp& ts)
{
    return efj::usec{(u64)ts.sec * 1000000u + (u64)ts.nsec / 1000u};
}

EFJ_MACRO efj::nsec
to_nsec(const efj::timestamp& ts)
{
    return efj::nsec{(u64)ts.sec * 1000000000u + (u64)ts.nsec};
}

EFJ_MACRO u32
now_8khz()
{
    efj::posix_time ts = efj::now_ptime();
    u32 value = ts.sec * 8000 + ts.nsec / 125000;
    return value;
}

EFJ_MACRO efj::date_utc
now_utc()
{
    efj::posix_time ts = efj::now_ptime();
    return efj::to_utc({ts.sec, ts.nsec});
}

template <> inline efj::string
to_string<>(const efj::date_utc& utc)
{
    return efj::fmt("%d-%02d-%02d %02d:%02d:%02d.%06u",
        utc.year, utc.month, utc.day, utc.hour,
        utc.minute, utc.second, utc.micro);
}

template <> inline efj::string
to_string<>(const efj::timestamp& ts)
{
    return efj::fmt("%u.%u", ts.sec, ts.nsec);
}

template <> inline efj::string
to_string<>(const efj::posix_time& ts)
{
    return efj::fmt("%u.%u", ts.sec, ts.nsec);
}

} // namespace efj
