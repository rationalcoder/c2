namespace efj
{

template <typename To_, typename From_> inline const To_*
content_cast(const From_&);

// TODO: to_network ?? Probably not.

template <typename T>
struct alias_of : T { using T::T; };

#define EFJ_DEFINE_ALIAS(_of, _alias_type) struct _alias_type : efj::alias_of<_of> { using efj::alias_of<_of>::alias_of; }
#define EFJ_DEFINE_ALIAS_NS(_of, _ns, _alias_type) struct _alias_type : efj::alias_of<_ns::_of> { using efj::alias_of<_ns::_of>::alias_of; }
// TODO: alias_cast

template <typename T> inline const char*
c_str(T)
{
    static_assert(efj::type_dependent_false<T>::value, "No efj::c_str defined for your type");
    return nullptr;
}

template <typename T> inline efj::string
to_string(const T&);

//{ Convenience macros

#define EFJ_DEFINE_UNION_CONTENT_CAST(fromType, idField, id, toField)\
template <> inline auto \
content_cast(const fromType& from) -> const decltype(((fromType*)0)->toField)*\
{\
    if (from.idField != id)\
        return nullptr;\
\
    return &from.toField;\
}

//}

} // namespace efj

