namespace efj
{

struct free_list_node
{
    efj::free_list_node* next;
};

struct free_list
{
    efj::free_list_node* head = nullptr;

    void add(void* data);
    void* get();

	template <typename T> EFJ_FORCE_INLINE T*
    get_or_allocate(efj::memory_arena& arena);
    EFJ_FORCE_INLINE void*
	get_or_allocate(umm size, efj::memory_arena& arena);
	template <typename T> EFJ_FORCE_INLINE T*
    get_or_allocate(efj::allocator& alloc);
    EFJ_FORCE_INLINE void*
	get_or_allocate(umm size, efj::allocator& alloc);

    // Since the free list uses overlapping memory, any data added to the free list
    // _must_ be allocated from it to ensure there is an appropriate size and alignment.
    // If you add memory allocated elsewhere, you're on your own.
	template <typename T> EFJ_FORCE_INLINE T*
    allocate(efj::memory_arena& arena);
    EFJ_FORCE_INLINE void*
	allocate(umm size, efj::memory_arena& arena);
	template <typename T> EFJ_FORCE_INLINE T*
    allocate(efj::allocator& alloc);
    EFJ_FORCE_INLINE void*
	allocate(umm size, efj::allocator& alloc);
};

inline void
free_list::add(void* data)
{
    efj_rare_assert(efj::is_aligned(data, alignof(efj::free_list_node)));

    auto* node = (efj::free_list_node*)data;
    node->next = head;
    head       = node;
}

inline void*
free_list::get()
{
    efj::free_list_node* result = head;
    if (result)
        head = head->next;

    return result;
}

template <typename T> EFJ_FORCE_INLINE T*
free_list::allocate(efj::memory_arena& arena)
{
	constexpr umm kSize = sizeof(T) > sizeof(efj::free_list_node)
                        ? sizeof(T) : sizeof(efj::free_list_node);
	constexpr umm kAlign = alignof(T) > alignof(efj::free_list_node)
                         ? alignof(T) : alignof(efj::free_list_node);

    return (T*)efj_push(arena, kSize, kAlign);
}

EFJ_FORCE_INLINE void*
free_list::allocate(umm size, efj::memory_arena& arena)
{
	efj_rare_assert(size >= sizeof(efj::free_list_node));
    return efj_push(arena, size, alignof(efj::free_list_node));
}

template <typename T> EFJ_FORCE_INLINE T*
free_list::allocate(efj::allocator& alloc)
{
	constexpr umm kSize = sizeof(T) > sizeof(efj::free_list_node)
                        ? sizeof(T) : sizeof(efj::free_list_node);
	constexpr umm kAlign = alignof(T) > alignof(efj::free_list_node)
                         ? alignof(T) : alignof(efj::free_list_node);

    return (T*)efj_allocator_alloc(alloc, kSize, kAlign);
}

EFJ_FORCE_INLINE void*
free_list::allocate(umm size, efj::allocator& alloc)
{
	efj_rare_assert(size >= sizeof(efj::free_list_node));
    return efj_allocator_alloc(alloc, size, alignof(efj::free_list_node));
}

template <typename T> EFJ_FORCE_INLINE T*
free_list::get_or_allocate(efj::memory_arena& arena)
{
    void* result = get();
    return result ? (T*)result : allocate<T>(arena);
}

EFJ_FORCE_INLINE void*
free_list::get_or_allocate(umm size, efj::memory_arena& arena)
{
	void* result = get();
	return result ? result : allocate(size, arena);
}

template <typename T> EFJ_FORCE_INLINE T*
free_list::get_or_allocate(efj::allocator& alloc)
{
    void* result = get();
    return result ? (T*)result : allocate<T>(alloc);
}

EFJ_FORCE_INLINE void*
free_list::get_or_allocate(umm size, efj::allocator& alloc)
{
	void* result = get();
	return result ? result : allocate(size, alloc);
}

//}

//{ Atomic free list
// MPSC free list where one thread gets/allocates nodes from the free list and
// multiple threads consume the data and free the data back to the list when done.
//
// You would want something like this when one thread produces a datagram that
// needs to be handled by multiple other threads before being freed back to the
// producing thread. The effect is the consumers of the datagram are the multiple
// producers producing nodes back into this free list, and the producer thread is
// the single consumer pulling nodes off to reuse.
//

struct mpsc_free_list_node
{
    efj::mpsc_free_list_node* next;
};

struct mpsc_free_list
{
    efj::mpsc_free_list_node* head = nullptr;

    void add(void* data);
    void* get();

	template <typename T> EFJ_FORCE_INLINE T*
    get_or_allocate(efj::memory_arena& arena);
    EFJ_FORCE_INLINE void*
	get_or_allocate(umm size, efj::memory_arena& arena);
	template <typename T> EFJ_FORCE_INLINE T*
    get_or_allocate(efj::allocator& alloc);
    EFJ_FORCE_INLINE void*
	get_or_allocate(umm size, efj::allocator& alloc);

    // Since the free list uses overlapping memory, any data added to the free list
    // _must_ be allocated from it to ensure there is an appropriate size and alignment.
    // If you add memory allocated elsewhere, you're on your own.
	template <typename T> EFJ_FORCE_INLINE T*
    allocate(efj::memory_arena& arena);
    EFJ_FORCE_INLINE void*
	allocate(umm size, efj::memory_arena& arena);
	template <typename T> EFJ_FORCE_INLINE T*
    allocate(efj::allocator& alloc);
    EFJ_FORCE_INLINE void*
	allocate(umm size, efj::allocator& alloc);
};

inline void
mpsc_free_list::add(void* data)
{
    efj_rare_assert(efj::is_aligned(data, alignof(efj::mpsc_free_list_node)));

    auto* node = (efj::mpsc_free_list_node*)data;

    for (;;) {
        efj::mpsc_free_list_node* localHead = __atomic_load_n(&this->head, __ATOMIC_ACQUIRE);
        node->next = localHead;

        if (__atomic_compare_exchange_n(&this->head, &localHead, node, true,
                    __ATOMIC_RELEASE, __ATOMIC_RELAXED)) {
            break;
        }
    }
}

inline void*
mpsc_free_list::get()
{
    for (;;) {
        efj::mpsc_free_list_node* localHead = __atomic_load_n(&this->head, __ATOMIC_ACQUIRE);
        if (!localHead) return nullptr;

        efj::mpsc_free_list_node* localNext = __atomic_load_n(&localHead->next, __ATOMIC_RELAXED);

        if (!__atomic_compare_exchange_n(&this->head, &localHead, localNext, true,
                                         __ATOMIC_RELEASE, __ATOMIC_RELAXED)) {
            continue;
        }

        return localHead;
    }
}

template <typename T> EFJ_FORCE_INLINE T*
mpsc_free_list::allocate(efj::memory_arena& arena)
{
	constexpr umm kSize = sizeof(T) > sizeof(efj::mpsc_free_list_node)
                        ? sizeof(T) : sizeof(efj::mpsc_free_list_node);
	constexpr umm kAlign = alignof(T) > alignof(efj::mpsc_free_list_node)
                         ? alignof(T) : alignof(efj::mpsc_free_list_node);

    return (T*)efj_push(arena, kSize, kAlign);
}

EFJ_FORCE_INLINE void*
mpsc_free_list::allocate(umm size, efj::memory_arena& arena)
{
	efj_rare_assert(size >= sizeof(efj::mpsc_free_list_node));
    return efj_push(arena, size, alignof(efj::mpsc_free_list_node));
}

template <typename T> EFJ_FORCE_INLINE T*
mpsc_free_list::allocate(efj::allocator& alloc)
{
	constexpr umm kSize = sizeof(T) > sizeof(efj::mpsc_free_list_node)
                        ? sizeof(T) : sizeof(efj::mpsc_free_list_node);
	constexpr umm kAlign = alignof(T) > alignof(efj::mpsc_free_list_node)
                         ? alignof(T) : alignof(efj::mpsc_free_list_node);

    return (T*)efj_allocator_alloc(alloc, kSize, kAlign);
}

EFJ_FORCE_INLINE void*
mpsc_free_list::allocate(umm size, efj::allocator& alloc)
{
	efj_rare_assert(size >= sizeof(efj::mpsc_free_list_node));
    return efj_allocator_alloc(alloc, size, alignof(efj::mpsc_free_list_node));
}

template <typename T> EFJ_FORCE_INLINE T*
mpsc_free_list::get_or_allocate(efj::memory_arena& arena)
{
    void* result = get();
    return result ? (T*)result : allocate<T>(arena);
}

EFJ_FORCE_INLINE void*
mpsc_free_list::get_or_allocate(umm size, efj::memory_arena& arena)
{
	void* result = get();
	return result ? result : allocate(size, arena);
}

template <typename T> EFJ_FORCE_INLINE T*
mpsc_free_list::get_or_allocate(efj::allocator& alloc)
{
    void* result = get();
    return result ? (T*)result : allocate<T>(alloc);
}

EFJ_FORCE_INLINE void*
mpsc_free_list::get_or_allocate(umm size, efj::allocator& alloc)
{
	void* result = get();
	return result ? result : allocate(size, alloc);
}

} // namespace efj
