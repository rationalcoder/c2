
namespace efj
{

// This is here b/c C++ did the C++ thing with its optional header/type. It includes a bunch
// of unnecessary crap and stores its value as a pointer.
// All I've wanted to use this for is for things that aren't pointers, like a simple u32 for which
// all values are valid.
//
// We attempt have a similar interface to the standard one so it's less surprising.
// That said, we don't really care about completeness. we can add what's missing over time.
//
// WARNING: This is not a replacement for std::optional!
// Since we don't care about being pedantically correct, we are intentionally _not_
// being pedantically correct. For example, we assume the types we are dealing with are POD.
//
// So primitives and structs with initializers are fine. Don't have an
// efj::optional<std::vector<int>>, that wouldn't work.
//
// @Incomplete
//

// TODO: Avoid assigning value from non-existent optional if the sizeof of the value
// is past a threshold. We would do that with a constexpr if. In current compilers
// it would just be a normal if that would get optimized out. :SizeThreshold
//
enum
{
    optional_small_value_size = 16,
};

template <typename T>
struct optional
{
    T    _value;
    bool _exists;

    // Leaves value uninitialized on purpose.
    optional()
        : _exists(false)
    {}

    optional(const T& val)
        : _value(val), _exists(true)
    {}

    optional(const optional<T>& other)
    {
        // :SizeThreshold
        _exists = other._exists;
        _value = other._value;
    }

    optional(optional<T>&& other)
    {
        _exists = other._exists;
        if (other._exists) {
            _value = efj::move(other._value);
            other._exists = false;
        }
    }

    optional& operator = (const optional<T>& other)
    {
        // :SizeThreshold
        _exists = other._exists;
        _value = other._value;
        return *this;
    }

    optional& operator = (optional<T>&& other)
    {
        _exists = other._exists;
        if (other._exists) {
            _value = efj::move(other._value);
            other._exists = false;
        }

        return *this;
    }

    optional& operator = (const T& val)
    {
        _value  = val;
        _exists = true;
        return *this;
    }

    optional& operator = (const T&& val)
    {
        _value  = efj::move(val);
        _exists = true;
        return *this;
    }

    EFJ_MACRO bool exists() const
    {
        return _exists;
    }

    EFJ_MACRO const T* value_or_null() const
    {
        if (!_exists)
            return nullptr;

        return &_value;
    }

    EFJ_MACRO T* value_or_null()
    {
        if (!_exists)
            return nullptr;

        return &_value;
    }

    EFJ_MACRO const T& value() const
    {
        if (!_exists)
            efj_panic("Tried to access a value that doesn't exist");

        return _value;
    }

    EFJ_MACRO T& value()
    {
        if (!_exists)
            efj_panic("Tried to access a value that doesn't exist");

        return _value;
    }

    EFJ_MACRO const T& value_or(const T& value) const
    {
        return _exists ? _value : value;
    }

    // Useful for passing to a function expecting a T* that will assign to the value.
    EFJ_MACRO T& value_or_emplace(const T& value)
    {
        if (_exists)
            return _value;

        efj_placement_new(&_value) T(value);
        _exists = true;

        return _value;
    }

    // Useful for passing to a function expecting a T* that will assign to the value.
    EFJ_MACRO T& value_or_emplace()
    {
        if (_exists)
            return _value;

        efj_placement_new(&_value) T();
        _exists = true;

        return _value;
    }

    EFJ_MACRO void clear()
    {
        _exists = false;
    }
};

} // namespace efj
