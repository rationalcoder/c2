namespace efj
{

extern bool
object::init()
{
    if (!enabled)
        return true;

    efj::debug_add_watch("Log debug", &logContext->logDebug, &object::_log_level_edited);
    efj::debug_add_watch("Log info",  &logContext->logInfo,  &object::_log_level_edited);
    efj::debug_add_watch("Log warn",  &logContext->logWarn,  &object::_log_level_edited);
    efj::debug_add_watch("Log crit",  &logContext->logCrit,  &object::_log_level_edited);

    efj::_debugsys_object_touched(this EFJ_LOC_ARGS);
    return initFunc(this);
}

// :ReplaceWithCallbackMechanism
void
object::_log_level_edited(efj::object* o, bool* value, bool newValue)
{
    *value = newValue;

    efj::logsys_object_ctx* ctx = o->logContext;

    ctx->logged &= ~efj::log_level_all_standard; // only clear out the standard log levels.
    if (ctx->logDebug) ctx->logged |= efj::log_level_debug;
    if (ctx->logInfo)  ctx->logged |= efj::log_level_info;
    if (ctx->logWarn)  ctx->logged |= efj::log_level_warn;
    if (ctx->logCrit)  ctx->logged |= efj::log_level_crit;
}

} // namespace efj
