
// The test system is responsible for both develoment work APIs (like capturing producer/consumer with Wireshark)
// and unit/integration testing. The test system is always on, though there may not be any tests compiled in.

namespace efj
{

struct test_captured_output
{
    efj::string        path;
    int                fd = -1;

    efj::net_sock_addr addr   = {};
    int                socket = -1;

    bool               captured = false;
};

struct testsys_producer_ctx
{
    efj::test_output_queue*   outputQueue = nullptr;
    efj::test_captured_output output;
};

struct testsys_consumer_ctx
{
    efj::test_captured_output output;
};

struct testsys_object_ctx
{
    efj::test_output_queue* outputQueue  = nullptr;
    bool                    disconnected = false;
};

struct testsys_thread_ctx
{
};

struct test_system
{
    efj::memory_arena* arena = nullptr;
    efj::memory_arena  curTestArena = {};
    efj::memory_arena  testNotesArena = {};
    // Delay initialization until we have a valid allocator, which we may not have if the application global
    // ever has this system by value instead of by pointer, due to how global initialization works.
    efj::array<efj::test_captured_output*> capturedOutputs { efj::delay_init };

    efj::array<efj::test_case> tests { efj::delay_init };
    efj::test_case             testInit;

    const char*     executablePath    = nullptr;
    // This testing flag is whether we have been told to execute tests, which is not exactly the same
    // as the testing global. The global is whether we are current executing a unit/integration test.
    bool            testing           = false;
    bool            runSingleTestCase = false;
    efj::test_case* singleTestCase    = nullptr;

    efj::array<efj::test_case*> failedTests { efj::delay_init };
};

extern efj::test_case* globalCurrentTest;

static void _testsys_pre_init(efj::test_system* sys, efj::memory_arena* arena);
// NOTE: This also inits any sub contexts for objects like the producer/consumer ones.
static void _testsys_init_object_context(efj::test_system* sys, efj::object* object);
static void _testsys_init_thread_context(efj::test_system* sys, efj::thread* thread, efj::memory_arena* arena);
static void _testsys_pre_object_init(efj::test_system* sys, int arc, const char** argv);
static void _testsys_post_object_init(efj::test_system* sys);
static void _testsys_start(efj::test_system* sys, bool* shouldExit, int* exitCode);

static void _testsys_capture_to_hex(efj::test_system* sys, efj::producer* p, const efj::string& path, int fd);
static void _testsys_capture_to_hex(efj::test_system* sys, efj::consumer* c, const efj::string& path, int fd);
static void _testsys_capture_to_network(efj::test_system* sys, efj::producer* p, const efj::string& path, u16 port);
static void _testsys_capture_to_network(efj::test_system* sys, efj::consumer* c, const efj::string& path, u16 port);

// IMPORTANT: These are called before any initialization besides
// 1. Initialization of the main thread / the main thread's context.
// 2. The construction of the test system object.
static void _testsys_register_test(efj::test_system* sys, const char* name, efj::test_func* func);
static void _testsys_report_failure(efj::test_system* sys, const efj::string& msg, const efj::source_location& loc);

//{
//! These are actually extern, since the producer/consumer stuff calls it, and that's in included in user code.
extern bool _testsys_on_produce(efj::producer* p, efj::consumer* target, const efj::component_view* components, umm count);
extern void _testsys_on_consume(efj::consumer* c, const efj::component_view* components, umm count);
//}

static bool _testsys_on_log(const efj::log_message_ctx* ctx, const efj::string& msg);
static bool _testsys_on_trigger_event(efj::object* source, u32 id, const efj::component_view& e);
static bool _testsys_on_send_event(efj::object* source, efj::object* target, u32 id, const efj::component_view& e);

static void _testsys_write_log_messages(efj::test_case* test);

static void _testsys_push_note(const efj::string& msg, efj::test_note* note);
static void _testsys_pop_note(efj::test_note* note);

} // namespace efj

