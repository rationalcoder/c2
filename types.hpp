//: Extended primitives

// NOTE: the use of char here is mostly a joke; apparently, the int8 types
// don't have to be chars, making certain casting business technically UB...

namespace efj
{

// This is for practically casting u64 values for use with %llu in printf.
// It's intentionally not exposed as a short primitive (it's always efj::llu).
using llu = long long unsigned;
using lld = long long;

using u8  = unsigned char;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;
using umm = size_t;

using uptr = uintptr_t;

using s8  = signed char;
using s16 = int16_t;
using s32 = int32_t;
using s64 = int64_t;

using b32 = s32;

using flag8  = u8;
using flag16 = u16;
using flag32 = u32;
using flag64 = u64;


// TODO: fast ints, least ints, etc.

using f32 = float;
using f64 = double;

}

// NOTE: Repeated out here instead of a namespace to improve error messages.
#if EFJ_USE_SHORT_PRIMITIVES
using u8  = unsigned char;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;
using umm = size_t;

using uptr = uintptr_t;

using s8  = signed char;
using s16 = int16_t;
using s32 = int32_t;
using s64 = int64_t;

using b32 = s32;

using flag8  = u8;
using flag16 = u16;
using flag32 = u32;
using flag64 = u64;


// TODO: fast ints, least ints, etc.

using f32 = float;
using f64 = double;
#endif


// NOTE: This is mostly an internal thing, to avoid depending on the standard definitions
// from c99 being present, which can require __STDC_CONSTANT_MACROS to be defined.
#define EFJ_UINT8_MAX ((efj::u8)~(efj::u8)0)
#define EFJ_UINT16_MAX ((efj::u16)~(efj::u16)0)
#define EFJ_UINT32_MAX ((efj::u32)~(efj::u32)0)
#define EFJ_UINT64_MAX ((efj::u64)~(efj::u64)0)

