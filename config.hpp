namespace efj
{

enum config_type
{
    config_xml,
    config_count_,
};

struct config_value_list
{
    efj::string* values;
    umm          count;
};

enum config_event_type
{
    config_event_invalid,
    // Validate config. Call reject* if config is bad. Always monitored.
    config_event_validate,
    // All objects accepted. Actually apply the config. Always monitored.
    config_event_apply,
    // The first config events during init have been handled. If multiple config files have
    // been registered, all files that were available at init time have been processed.
    // Optional.
    config_event_init_done,
    config_event_count_
};

struct config_event
{
    efj::config_event_type   type;
    struct efj::config_file* file;
    efj::string              rejectReason; // local to the config event.
    bool                     rejected;
};

// Before efj::init().
bool config_add_file(u32 id, const char* path, efj::config_type type);

// During efj::init().
template <typename T> inline bool
config_subscribe(u32 id, void (T::*callback)(efj::config_event&));

//! Subscribes to optional config events.
//! \returns true if `type` is an optional config event and the subscription was successful.
template <typename T> inline bool
config_subscribe(efj::config_event_type type, void (T::*callback)(efj::config_event&));

// During config_event.
inline bool config_accepted(efj::config_event& e);
void config_reject(efj::config_event& e, const efj::string& reason);

// Returns on temp memory.
inline efj::string config_get_text(efj::config_event& e, const efj::string& path, bool* rejected = nullptr);
inline bool config_get_bool(efj::config_event& e, const efj::string& path, bool* rejected = nullptr);
inline void config_get_value(efj::config_event& e, const efj::string& path, bool* value, bool* rejected = nullptr);
inline void config_get_value(efj::config_event& e, const efj::string& path, u8* value, bool* rejected = nullptr);
inline void config_get_value(efj::config_event& e, const efj::string& path, u16* value, bool* rejected = nullptr);
inline void config_get_value(efj::config_event& e, const efj::string& path, u32* value, bool* rejected = nullptr);

// These are the same as the non-optional overloads except they don't reject the config if the value isn't present.
inline void config_get_value(efj::config_event& e, const efj::string& path, efj::optional<bool>* value, bool* rejected = nullptr);
inline void config_get_value(efj::config_event& e, const efj::string& path, efj::optional<u8>* value, bool* rejected = nullptr);
inline void config_get_value(efj::config_event& e, const efj::string& path, efj::optional<u16>* value, bool* rejected = nullptr);
inline void config_get_value(efj::config_event& e, const efj::string& path, efj::optional<u32>* value, bool* rejected = nullptr);

} // namespace efj

