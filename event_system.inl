namespace efj
{

inline bool
_eventsys_should_queue_side_effects(efj::thread* from, efj::thread* to)
{
    return from != to || from->eventContext->forceQueueSideEffects;
}

}// namespace efj
