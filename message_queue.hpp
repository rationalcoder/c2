namespace efj
{

// NOTE: A lot of generic queueing stuff is ultimately the same arena allocation logic,
// assuming we had parent arena support implemented. Once something like that is implemented,
// we should probably just use arenas for everything. That said, there is the issue of
// passing allocators to things, not always just arenas. Maybe we just have the same
// logic twice, one for arenas and one for allocators?
//

// @incomplete
// We want to make the message_queue types as convienient and full featured as possible since
// message queueing is one the main things our applications need to do. That being said, we are
// growing this organically for now. For now, I'm just doing the fixed size version.

template <typename BlockT>
struct block_queue_iterator
{
    BlockT* _cur;
    BlockT* _arrayFirst;
    BlockT* _arrayLast;

    block_queue_iterator(BlockT* cur, BlockT* arrayFirst, BlockT* arrayLast)
        : _cur(cur), _arrayFirst(arrayFirst), _arrayLast(arrayLast)
    {}

    EFJ_MACRO bool operator == (const block_queue_iterator& rhs) const { return _cur == rhs._cur; }
    EFJ_MACRO bool operator != (const block_queue_iterator& rhs) const { return _cur != rhs._cur; }

    EFJ_MACRO BlockT& operator *  () const { return *_cur; }
    EFJ_MACRO BlockT* operator -> () const { return _cur; }

    EFJ_MACRO block_queue_iterator operator ++ (int) const
    {
        auto* next = _cur == _arrayLast ? _arrayFirst : _cur + 1;
        return block_queue_iterator{next, _arrayFirst, _arrayLast};
    }

    EFJ_MACRO block_queue_iterator operator -- (int) const
    {
        auto* next = _cur == _arrayFirst ? _arrayLast : _cur - 1;
        return block_queue_iterator{next, _arrayFirst, _arrayLast};
    }

    EFJ_MACRO block_queue_iterator& operator ++ ()
    {
        _cur = _cur == _arrayLast ? _arrayFirst : _cur + 1;
        return *this;
    }

    EFJ_MACRO block_queue_iterator& operator -- ()
    {
        _cur = _cur == _arrayFirst ? _arrayLast : _cur - 1;
        return *this;
    }
};

enum block_queue_error
{
    block_queue_error_none,
    block_queue_error_full,
    block_queue_error_size,
    block_queue_error_count_,
};

template <typename BlockT, umm BlockCountV>
struct block_queue
{
    using iterator       = efj::block_queue_iterator<BlockT>;
    using const_iterator = efj::block_queue_iterator<const BlockT>;

    BlockT _blocks[BlockCountV];
    umm    _writeHead = 0;
    umm    _readHead  = 0;
    umm    _size      = 0;

    // TODO: iteration.

    EFJ_MACRO const_iterator cbegin() const { return const_iterator{&_blocks[_readHead], _blocks, &_blocks[BlockCountV-1] }; }
    EFJ_MACRO const_iterator begin()  const { return const_iterator{&_blocks[_readHead], _blocks, &_blocks[BlockCountV-1] }; }
    EFJ_MACRO iterator       begin()        { return iterator{&_blocks[_readHead], _blocks, &_blocks[BlockCountV-1] }; }

    EFJ_MACRO const_iterator cend() const { return const_iterator{&_blocks[_writeHead], _blocks, &_blocks[BlockCountV-1] }; }
    EFJ_MACRO const_iterator end()  const { return const_iterator{&_blocks[_writeHead], _blocks, &_blocks[BlockCountV-1] }; }
    EFJ_MACRO iterator       end()        { return iterator{&_blocks[_writeHead], _blocks, &_blocks[BlockCountV-1] }; }

    EFJ_MACRO umm size() const { return _size; }
    EFJ_MACRO void clear() { _readHead = _writeHead = _size = 0; }

    BlockT* push_space(efj::error_code* ec)
    {
        if (_size == BlockCountV) {
            ec->set(efj::block_queue_error_full, &efj::c_str<efj::block_queue_error>);
            return nullptr;
        }

        auto* nextOpen = &_blocks[_writeHead];

        ++_writeHead;
        if (_writeHead == BlockCountV)
            _writeHead = 0;

        ++_size;
        return nextOpen;
    }

    void push(const BlockT& block, efj::error_code* ec)
    {
        if (_size == BlockCountV) {
            ec->set(efj::block_queue_error_full, &efj::c_str<efj::block_queue_error>);
            return;
        }

        auto* nextOpen = &_blocks[_writeHead];
        *nextOpen = block;

        ++_writeHead;
        if (_writeHead == BlockCountV)
            _writeHead = 0;

        ++_size;
    }

    void push(const efj::message& msg, efj::error_code* ec)
    {
        if (_size == BlockCountV) {
            ec->set(efj::block_queue_error_full, &efj::c_str<efj::block_queue_error>);
            return;
        }

        if (msg.size > sizeof(BlockT)) {
            ec->set(efj::block_queue_error_size, &efj::c_str<efj::block_queue_error>);
            return;
        }

        auto* nextOpen = &_blocks[_writeHead];
        memcpy(nextOpen, msg.data, msg.size);

        ++_writeHead;
        if (_writeHead == BlockCountV)
            _writeHead = 0;

        ++_size;
    }

    EFJ_MACRO const BlockT& front() const
    {
        efj_panic_if(_size == 0);
        return _blocks[_readHead];
    }

    EFJ_MACRO BlockT& front()
    {
        efj_panic_if(_size == 0);
        return _blocks[_readHead];
    }

    EFJ_MACRO void pop()
    {
        efj_assert(_size);

        ++_readHead;
        if (_readHead == BlockCountV)
            _readHead = 0;

        --_size;
    }
};

template <> inline const char*
c_str<>(efj::block_queue_error ec)
{
    switch (ec)
    {
    case block_queue_error_none: return "none";
    case block_queue_error_size: return "size";
    case block_queue_error_full: return "full";
    default: return "invalid";
    }
}

// NOTE: We could be more efficient here with a u16 size and a data[] field, relying on
// the message data being stored right after this stuct in memory but it increases complexity
// and makes accessor semantics weird, since we often want to return a message&, and we would
// end up returning message structs by value in that case.
//
template <typename T>
struct message_queue_node
{
    efj::message_queue_node<T>* next;     // Next chunk in the queue, one per message.
    efj::memory_arena_chunk*    endChunk; // The chunk the message allocation ended on.
    T                           msg;
};

template <typename T>
struct message_queue
{
    static_assert(std::is_base_of<efj::message, T>::value,
        "Expected a type that inherits from efj::message");

    using node_type = message_queue_node<T>;

    efj::memory_arena _arena    = efj::allocate_arena("message_queue buffer", 4_KB);
    node_type*        _front    = nullptr;
    node_type**       _backNext = &_front;
    umm               _size     = 0;

    message_queue()
    {}

    message_queue(umm chunkSize)
        : _arena(efj::allocate_arena("message_queue buffer", chunkSize))
    {}

    ~message_queue()
    {
        efj::free_arena(_arena);
    }

    umm size() const { return _size; }

    void push(const T& msg)
    {
        // NOTE: If we change the node struct to be more space efficient we would need to use the
        // unpadded struct size here instead of just a sizeof.
        auto* node = (node_type*)efj_push(_arena, sizeof(node_type) + msg.size,
            alignof(node_type));

        u8* data = (u8*)(node + 1);

        node->next     = nullptr;
        node->endChunk = efj::get_current_chunk(_arena);
        node->msg      = msg;
        node->msg.data = data;

        memcpy(data, msg.data, msg.size);

        *_backNext = node;
        _backNext  = &node->next;
        ++_size;
    }

    EFJ_MACRO const T& front() const { return _front->msg; }
    EFJ_MACRO T& front() { return _front->msg; }

    void pop()
    {
        auto* next = _front->next;
        efj::recycle_chunks(_arena, _front->endChunk);
        _front = next;
        --_size;

        if (!next)
            _backNext = &_front;
    }
};

} // namespace efj
