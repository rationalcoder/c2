namespace efj
{

// IMPORTANT: This is allocated out of the main thread's perm_memory. If you add to any data structures in here
// at runtime, you will be racing with the main thread! @TS.
struct configsys_object_ctx
{
    efj::array<config_file*> subscribedFiles;
    bool                     subscribedEvents[efj::config_event_count_] = {};
};

struct config_system;
struct configsys_thread_ctx
{
    efj::config_system*          sys    = nullptr;
    efj::thread*                 thread = nullptr;
    efj::memory_arena*           arena  = nullptr;

    // Events are only created on threads that subscribe to at least 1 file.
    efj::array<callback>         uniqueOnInitDoneCallbacks;
    efj::eventsys_waitable_event onInitDoneEvent;
    efj::eventsys_waitable_event validateConfigEvent;
    efj::eventsys_waitable_event applyConfigEvent;
    bool                         hasFileChangeSubscribers = false;
    bool                         rejectedConfig = false;
    char                         rejectReason[128];
};

struct config_system
{
    efj::memory_arena*           arena = nullptr;
    efj::memory_arena            fileArena;

    efj::thread*                 configThread = nullptr;

    efj::array<config_file>      configFiles;
    efj::eventsys_epoll_event    configChangedEvent;
    efj::eventsys_waitable_event initialConfigEvent;

    efj::eventsys_timer_event    findLostFilesTimer;

    //{ @TS
    efj::thread*                 firstRejectedThread = nullptr;
    efj::config_file*            currentConfigFile = nullptr;
    efj::eventsys_fence          onInitDoneFence;
    efj::eventsys_fence          validateConfigFence;
    efj::eventsys_breakpoint     applyConfigBeginBp;
    efj::eventsys_breakpoint     applyConfigEndBp;
    //}

    int                          inotifyFd = -1;
    int                          otherConfigurableThreadCount = 0;

    bool                         preInitted = false;
};

static void _configsys_pre_init(efj::config_system* sys, efj::memory_arena* arena);
static void _configsys_init_thread_context(efj::config_system* sys, efj::thread* thread, efj::memory_arena* arena);
static void _configsys_init_object_context(efj::config_system* sys, efj::object* object);

static void _configsys_pre_object_init(efj::config_system* sys, efj::thread* configThread);
static void _configsys_post_object_init(efj::config_system* sys);

// Before efj::init().
static void _configsys_add_config_file(u32 id, const char* path, efj::config_type type);
// During efj::init().
static bool _configsys_subscribe(efj::config_system* sys, u32 id, efj::callback cb);
static bool _configsys_subscribe(efj::config_system* sys, efj::config_event_type type, efj::callback cb);

// Internal events.
static void _configsys_handle_initial_config_event(efj::eventsys_event_data* edata);
static void _configsys_handle_initial_config_done_event(efj::eventsys_event_data* edata);
static void _configsys_handle_validate_config_event(efj::eventsys_event_data* edata);
static void _configsys_handle_apply_config_event(efj::eventsys_event_data* edata);

// During an event.
static void _configsys_handle_config_changed_event(efj::eventsys_event_data* edata);
// During an event.
static void _configsys_handle_find_lost_files_event(efj::eventsys_event_data* edata);

} // namespace efj

