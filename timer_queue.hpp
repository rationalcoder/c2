
// NOTE: The units of time aren't specified in most places since the code doesn't care
// what we use, only that it fits in a u64. In practice, we expect nanoseconds relative
// to some origin time, and default time parameters will use CLOCK_MONOTONIC time in nanoseconds.

namespace efj
{

enum
{
    // Set to true to print inputs to the timer queue that can be grep'd to help
    // reproduce bugs found during testing. Any bugs found and fixed should be accompanied
    // by a test case in the timer queue unit test.
    TIMER_QUEUE_LOG_INPUTS = false,
};

struct timer_handle;

// u8 because this is stored as-is next to bools in many structs.
enum timer_type : u8
{
    timer_invalid_,
    timer_oneshot,
    timer_periodic,
    timer_count_,
};

struct timer_entry
{
    efj::timer_handle* handle;
    u64                duration;
    u64                expireTime;
};

// Designed such that zero initialization is fine.
struct timer_handle
{
    void*             timer = nullptr;
    efj::timer_entry* entry = nullptr;
};

struct timer_queue_event
{
    efj::timer_handle* handle;
    u64                expiredTime;
    u32                expiredCount;
};

enum : u64
{
    timer_queue_infinite = UINT64_MAX,
    // @Temporary
    timer_queue_max_timers = 1024,
};

struct timer_queue
{
    // TODO: Right now, we just have a fixed number of timers. We can change this to an array
    // and keep track of an allocator, later.
    efj::timer_entry _entries[timer_queue_max_timers];
    umm              _size = 0;
    u64              _origin = 0;
    u64              _currentTime = 0;

    void init(u64 origin = efj::now_nsec().value);
    umm size() const { return _size; }

    EFJ_MACRO void start(efj::timer_handle* handle, void* timer, u64 initial, u64 duration, bool absolute = false);
    EFJ_MACRO void stop(efj::timer_handle* handle);

    //! Relative periodic and oneshot timers.
    EFJ_MACRO void insert_relative(void* timer, u64 initial, u64 duration, efj::timer_handle* out);
    EFJ_MACRO void insert_absolute(void* timer, u64 expireTime, u64 duration, efj::timer_handle* out);
    void remove(efj::timer_handle* handle);

    //! Convenience wrapper for duration == 0.
    EFJ_MACRO void insert_absolute_oneshot(void* timer, u64 expireTime, efj::timer_handle* handle)
    { insert_absolute(timer, expireTime, 0, handle); }

    void set_time(u64 time = efj::now_nsec().value);
    u64 wait_time() const;
    EFJ_MACRO u64 wait_time(u64 time) { set_time(time); return wait_time(); }
    //! Get the next absolute expire time. Assumes size > 0.
    u64 next_expire_time() const;

    EFJ_MACRO umm poll(efj::view<efj::timer_queue_event> events) { return poll(events.data, events.size); }
    umm poll(efj::timer_queue_event* events, umm n);

    void _insert(const efj::timer_entry& entry);
    void _pop();
};

} // namespace efj

